module.exports = {
  setupFilesAfterEnv: ['<rootDir>setup-tests.js'],
  setupFiles: ['./node_modules/react-native-gesture-handler/jestSetup.js'],
  preset: 'react-native',
  moduleNameMapper: {
    //'@canastarosa': '<rootDir>/__mocks__/fileMock.js',
    '\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|less)$': '<rootDir>/__mocks__/fileMock.js',
  },
  transformIgnorePatterns: [
    'node_modules/(?!(react-native|react-native-vector-icons|react-native-swiper|react-native-gesture-handler|react-native-zoom-image|react-native-calendars|react-native-picker-select|@react-native-community|react-navigation|@canastarosa)/)',
  ],
};
