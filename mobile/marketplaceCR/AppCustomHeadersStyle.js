import { StyleSheet, Platform, Dimensions } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

const alto = Dimensions.get('window').height;

export default StyleSheet.create({
  backStyles: {
    width: '100%',
    display: 'flex',
    marginLeft: 15,
    textAlign: 'center',
  },
  backStylesText: {
    textAlign: 'center',
    color: colors.colorMain300,
    fontSize: 16
  },
  imageBack:{
    width: 35,
    height: 35,
    resizeMode: 'contain',
    marginBottom: Platform.OS === 'android' ? -1 : alto < 737 ? 1 : 10,
  }
});
