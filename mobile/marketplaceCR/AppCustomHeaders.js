import React, { Component } from 'react';
import { colors } from '@canastarosa/ds-theme/colors';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  View,
  Button,
  Alert,
  Text,
  Platform,
  TouchableOpacity,
  StatusBar,
  Image,
} from 'react-native';
import AddressesWrapper from './src/routes/TopMenu/AddressesWrapper';
import CRSearchInput from './src/components/CRSearchInput/CRSearchInput';
import SearchIcon from './SearchIcon';
import IconWithBadge from './src/routes/Badge/IconWithBadge';
import styles from './AppCustomHeadersStyle';

import back from './src/assets/icons/back.png';

declare var globalThis: any;
const rootScreens = [
  '_HomeScreen',
  '_CategoriesList',
  '_ShoppingCart',
  '_Orders',
  '_Profile',
];

export function LogoTitle(props) {
  const { navigation, addressList = [] } = props;
  return (
    <View>
      <AddressesWrapper
        addresses={addressList}
        style={layout.addressWrapper}
        route={props.route}
        navigation={props.navigation}
      ></AddressesWrapper>
    </View>
  );
}

const checkSection = (screen) => {
  try {
    if (Boolean(screen)) {
      if (screen.includes('___')) {
        var realScreen = screen.split('___');
        if (
          realScreen[0] === 'ProductDetail' ||
          realScreen[0] === 'CatalogScreen' ||
          realScreen[0] === 'SearchResult'
        ) {
          return realScreen[0];
        }
      }
    }
    return screen;
  } catch (e) {
    console.log('error', e);
  }
};

function CustomBackButton(props) {
  const { navigation, route } = props;
  const globalBackScreen = globalThis.ReactApplication.BackScreen;
  // console.log("------------->", globalBackScreen.allsections.length)
  // console.log("-------------*", globalBackScreen.allsections)
  if (globalBackScreen.allsections.length === 1 || globalBackScreen.disable) {
    return null;
  } else {
    return (
      <TouchableOpacity
        style={styles.backStyles}
        onPress={() => {
          let goTo = [false, {}];
          try {
            //Remove current element
            globalBackScreen.allsections.pop();
            //Name screen ej: OnBoardingScreen, ProductDetail___cuadrosucu80098089
            let getLast =
              globalBackScreen.allsections[
                globalBackScreen.allsections.length - 1
              ];
            //Check especial sections (subcategories, products)
            let getTransform = checkSection(getLast);
            // console.log('getTransform', getTransform);

            //Set action navigation
            goTo = [getTransform, globalBackScreen.allsectionsprops[getLast]];

            //Check if root screens
            if (rootScreens.includes(getTransform)) {
              globalBackScreen.allsections = [goTo[0]];
              globalBackScreen.allsectionsprops = [];
            }

            if (!Boolean(goTo[0])) {
              console.log('Reset execute ');
              globalBackScreen.allsectionsprops = [];
              globalBackScreen.allsections = ['HomeScreen'];
              goTo[0] = 'HomeScreen';
            }

            console.log('Back to: ', goTo[0]);
            navigation.navigate(goTo[0], goTo[1]);
          } catch (e) {
            globalBackScreen.allsectionsprops = [];
            globalBackScreen.allsections = ['HomeScreen'];
            goTo[0] = 'HomeScreen';
            navigation.navigate(goTo[0]);
            console.log('No hay niveles.');
          }
        }}
      >
        <Image source={back} style={styles.imageBack} />
      </TouchableOpacity>
    );
  }
}

function CustomCRSearchInput(props) {
  const { navigation, route } = props;
  const globalBackScreen = globalThis.ReactApplication.BackScreen;

  if (
    globalThis.ReactApplication.BackScreen.disable
  ) {
    // props.navigation.setParams({
    //   headerStyle: {
    //     backgroundColor: colors.colorWhite,
    //     shadowColor: 'transparent',
    //     borderBottomWidth: 0,
    //   },
    // });
    return null;
  } else {
    return <CRSearchInput {...props} />;
  }
}

function Cart(props) {
  const { navigation, color } = props;

  const globalBackScreen = globalThis.ReactApplication.BackScreen;


  if (
    globalThis.ReactApplication.BackScreen.disable 
  ) {
    return <StatusBar hidden={true} />;
  } else {
    return (
      <TouchableOpacity
        onPress={() => {
          globalThis.ReactApplication.ActiveScreen(
            'ShoppingCart',
            {},
            props.navigation,
          );
        }}
      >
        <IconWithBadge name="cart" color={colors.colorWhite} />

        <StatusBar hidden={true} />
      </TouchableOpacity>
    );
  }
}

export function CustomHeader(props, headerShown = true) {
  const { navigation, route } = props;

  return {
    headerLeft: () => <CustomBackButton {...props} />,
    headerTitle: () => <CustomCRSearchInput {...props} />,
    headerRight: () => <Cart {...props} />,
    //headerShown: headerShown,
    //headerBackTitle: 'prueba',

    gesturesEnabled: false,
    swipeEnabled: false,
    animationEnabled: false,
    headerStyle: {
      backgroundColor: colors.colorDeep300,
      shadowColor: 'transparent',
      borderBottomWidth: 0,
    },
    //headerTintColor: colors.colorMain300,
  };
}

export function CustomHeaderDisabledBack(props, headerShown = true) {
  const { navigation, route } = props;
  return {
    //headerTitle: () => <LogoTitle {...props} />,
    //headerRight: () => <SearchIcon {...props} />,
    headerShown: headerShown,
    headerLeft: null,
    gesturesEnabled: false,
    swipeEnabled: false,
    animationEnabled: false,
    headerStyle: {
      backgroundColor: colors.colorDeep300,
      shadowColor: 'transparent',
      borderBottomWidth: 0,
    },
  };
}

export function CustomSplash() {
  return {
    headerTitle: null,
    headerRight: null,
    headerShown: null,
    headerLeft: null,
    gesturesEnabled: false,
    swipeEnabled: false,
    animationEnabled: false,
    headerStyle: {
      backgroundColor: colors.colorDeep300,
      shadowColor: 'transparent',
      borderBottomWidth: 0,
    },
  };
}
