// module.exports = {
//   printWidth: 100,
//   bracketSpacing: true,
//   jsxSingleQuote: false,
//   jsxBracketSameLine: true,
//   singleQuote: true,
//   tabWidth: 2,
//   semi: true,
//   trailingComma: 'all',
//   trailingComma: 'es5',

//   // printWidth: 100,
//   // trailingComma: 'all',
//   // tabWidth: 2,
//   // semi: true,
//   // bracketSpacing: false,
//   // jsxBracketSameLine: true,
//   // singleQuote: true,
// };

module.exports = {
  singleQuote: true,
  semi: true,
  bracketSpacing: true,
  trailingComma: 'all',
  arrowParens: 'always',
  tabWidth: 2,
  overrides: [
    {
      files: '*.ts',
      options: {
        trailingComma: 'all',
      },
    },
  ],
};
