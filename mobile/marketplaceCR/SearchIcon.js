import { colors } from '@canastarosa/ds-theme/colors';
import { layout } from './src/routes/TopMenu/TopMenuStyles';

import { CRIcon } from './src/components/CRIcon';

import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TouchableOpacity } from 'react-native';

import { appActions } from './src/redux/actions';

class SearchIcon extends Component {
  openSearch = async () => {
    // if (
    //   this.props.navigation.state.key === 'Cart' &&
    //   this.props.cart.length === 0
    // ) {
    //   return false;
    // }
    // this.setState({ isOpen: true });
    //await this.setState({ isOpen: !this.state.isOpen });
    this.props.changeSearch(!this.props.openSearch);
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          return this.openSearch();
        }}
        style={layout.searchWrapper}
      >
        <CRIcon
          name="search"
          size={20}
          color={
            !this.props.openSearch ? colors.colorDark100 : colors.colorMain300
          }
        />
      </TouchableOpacity>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    openSearch: state.app.openSearch,
  };
}
function mapDispatchToProps(dispatch) {
  const { changeSearch } = appActions;
  return bindActionCreators({ changeSearch }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchIcon);
