import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Button, StyleSheet, Text, TouchableOpacity, Platform, Dimensions } from 'react-native';
import { useRoute, NavigationContainer } from '@react-navigation/native';
import AddressesWrapper from './src/routes/TopMenu/AddressesWrapper';
import { CRIcon } from './src/components/CRIcon';
import IconWithPhoto from './src/routes/Photo/IconWithPhoto';

import CRSearchInput from './src/components/CRSearchInput/CRSearchInput';
import { colors } from '@canastarosa/ds-theme/colors';

let sleepTab = colors.colorGray300;
let activeTab = colors.colorMain300;
const alto = Dimensions.get('window').height;

declare var globalThis: any;

class WrapperTopSearch extends Component {
  openSearch = async () => {
    // if (
    //   this.props.navigation.state.key === 'Cart' &&
    //   this.props.cart.length === 0
    // ) {
    //   return false;
    // }
    // this.setState({ isOpen: true });
    await this.setState({ ...this.state, isOpen: !this.state.isOpen });
  };

  constructor(props) {
    super(props);

    console.log('global.ReactApplication', global.ReactApplication);

    this.state = {
      isOpen: false,
      tab1: global.ReactApplication.ButtonMenu.tab1 ? activeTab : sleepTab,
      tab2: global.ReactApplication.ButtonMenu.tab2 ? activeTab : sleepTab,
      tab3: global.ReactApplication.ButtonMenu.tab3 ? activeTab : sleepTab,
    };
  }

  changeTab = (tab) => {
    global.ReactApplication.ButtonMenu.tab1 = false;
    global.ReactApplication.ButtonMenu.tab2 = false;
    global.ReactApplication.ButtonMenu.tab3 = false;

    global.ReactApplication.ButtonMenu[tab] = true;

    this.setState({
      ...this.state,
      tab1: sleepTab,
      tab2: sleepTab,
      tab3: sleepTab,
    });
    this.setState({
      ...this.state,
      [tab]: activeTab,
    });
  };

  render() {
    const { navigation, addressList } = this.props;
    const isAndroid: Boolean = Platform.OS === 'android';
    return (
      <React.Fragment>
        <View style={styles.searchWrapper}>
          <AddressesWrapper
            addresses={addressList}
            //style={layout.addressWrapper}
            route={this.props.route}
            navigation={navigation}
          />
        </View>

        {this.props.children}
        <View style={styles.bottomMenuWrapper}>
            <View>
              <TouchableOpacity
                onPress={() => {
                  this.changeTab('tab1');
                  globalThis.ReactApplication.BackScreen.disable = false;
                  globalThis.ReactApplication.ActiveScreen(
                    'HomeScreen',
                    {},
                    this.props.navigation,
                  );
                }}
                style={styles.bottomMenuIcon}
              >
                <React.Fragment>
                  <CRIcon name="home" size={isAndroid ? 18 : alto < 737 ? 20 : 23} color={this.state.tab1} />
                 <Text
                    style={[
                      styles.bottomMenuIconText,
                      { color: this.state.tab1 },
                    ]}
                  >
                    Inicio
                  </Text>
                </React.Fragment>
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity
                onPress={() => {
                  globalThis.ReactApplication.BackScreen.disable = false;
                  this.changeTab('tab2');
                  globalThis.ReactApplication.ActiveScreen(
                    'CategoriesList',
                    {},
                    this.props.navigation,
                  );
                }}
                style={styles.bottomMenuIcon}
              >
                <React.Fragment>
                  <CRIcon name="categories" size={isAndroid ? 18 : alto < 737 ? 20 : 23} color={this.state.tab2} />

                  <Text
                    style={[
                      styles.bottomMenuIconText,
                      { color: this.state.tab2 },
                    ]}
                  >
                    Categorías
                  </Text>
                </React.Fragment>
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity
                onPress={() => {
                  globalThis.ReactApplication.BackScreen.disable = false;
                  this.changeTab('tab3');
                  globalThis.ReactApplication.ActiveScreen(
                    'Profile',
                    {},
                    this.props.navigation,
                  );
                }}
                style={styles.bottomMenuIcon}
              >
                <IconWithPhoto name="profile" color={this.state.tab3} />
                <Text
                  style={[
                    styles.bottomMenuIconText,
                    { color: this.state.tab3 },
                  ]}
                >
                  Perfil
                </Text>
              </TouchableOpacity>
            </View>
          </View>
      </React.Fragment>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    openSearch: state.app.openSearch,
    zipCode: state.app.zipCode,
    addressList: globalThis.ShoppingCartManager.getAddresses(),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(WrapperTopSearch);

const styles = StyleSheet.create({
  searchWrapper: {
    zIndex: 1,
    height: 35,
    backgroundColor: '#1A1A25',
  },
  bottomMenuWrapper: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    height: Platform.OS === 'android' ? 60 : alto < 737 ? 55 : 70,
    zIndex: 200,
    elevation: 200,
    backgroundColor: colors.colorWhite,
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'space-around',
    //
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,

    elevation: 13,
  },
  bottomMenuIcon: {
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  bottomMenuIconText: {
    marginTop: 4,
    fontSize: 14,
    color: colors.colorMain300,
  },
});