import React, { Component } from 'react';
import { colors } from '@canastarosa/ds-theme/colors';
import { layout } from './src/routes/TopMenu/TopMenuStyles';
import AddressesWrapper from './src/routes/TopMenu/AddressesWrapper';
import { CRIcon } from './src/components/CRIcon';
import SearchIcon from './SearchIcon';
import CRSearchInput from './src/components/CRSearchInput/CRSearchInput'
import WrapperTopSearch from './WrapperTopSearch';

import {
  useIsFocused,
  useRoute,
  NavigationContainer,
} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View, Button, Alert, Text, Platform } from 'react-native';

import IconWithBadge from './src/routes/Badge/IconWithBadge';
import IconWithPhoto from './src/routes/Photo/IconWithPhoto';

import OnBoardingScreen from './src/pages/Onboarding/OnboardingScreen';
import LoginScreen from './src/pages/Login/LoginScreen';
import MessageMailScreen from './src/pages/MessageMail/MessageMailScreen';

import SignUpScreen from './src/pages/SignUp/SignUpScreen';
import ForgetPassScreen from './src/pages/ForgetPass/ForgetPassScreen';
import SignUpFormValidate from './src/pages/SignUp/SignUpFormValidate';
import SplashScreen from './src/pages/Splash/SplashScreen';

import CatalogScreen from './src/pages/Catalog/CatalogScreen';
import CategoriesList from './src/pages/CategoriesList/CategoriesList';
import SearchResult from './src/pages/SearchResult/SearchResult';

// SHOPPING CART
import ShoppingCart from './src/pages/ShoppingCart/ShoppingCart';

// HOME, STORE AND PRODUCT
import HomeScreen from './src/pages/Home/HomeScreen.js';
import ProductDetail from './src/pages/ProductDetail/ProductDetail';

import StoreScreen from './src/pages/StoreScreen/StoreScreen';
import NewStores from './src/pages/NewStores/NewStores';
import NewStoresNLJAL from './src/pages/NewStores/NewStoresNLJAL';
import ChangeZipCode from './src/pages/ChangeZipCode/ChangeZipCode';
import Collection from './src/pages/Collection/Collection';
import HotSale from './src/pages/Hotsale/Hotsale';

// ORDERS
import Orders from './src/pages/Profile/Orders/Orders';
import OrderDetail from './src/pages/Profile/Orders/OrderDetail';

// PROFILE
import Profile from './src/pages/Profile/ProfileScreen';
import Addresses from './src/pages/Addresses/AddressesScreen';
import PaymentMethods from './src/components/Profile/PaymentMethods/PaymentMethods';
import Reviews from './src/components/Profile/Reviews/Reviews';

// CHECKOUT
import Checkout from './src/pages/Checkout/Checkout';
import ProductNotAvailable from './src/pages/Checkout/ProductNotAvailable';
import SelectPayment from './src/pages/Checkout/SelectPayment';

//PAYMENT STATUS
import PaymentSuccess from './src/pages/PaymentResult/PaymentSuccess';

import { _getData } from './src/utils/asyncStorage';


import {
  CustomSplash,
  CustomHeaderDisabledBack,
  CustomHeader,
  LogoTitle,
} from './AppCustomHeaders';

import * as Sentry from '@sentry/react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
Sentry.init({
  dsn:
    'https://5eed5463dda54d15928dfe16324ee2c8@o144533.ingest.sentry.io/5227710',
});

declare var globalThis: any;

const StackController = createStackNavigator();
const Tabs = createBottomTabNavigator();

function RootScreens() {
  return (
    <StackController.Navigator initialRouteName="SplashScreen">
      {/*********** PROFILE ************/}

      <StackController.Screen
        name="Profile"
        // options={(props) => CustomHeaderDisabledBack(props, true)}
        options={CustomHeader}
        component={(props) => (
          <WrapperTopSearch {...props}>
            <Profile {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="Reviews"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <Reviews {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="MessageMailScreen"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <MessageMailScreen {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        // options={(props) => CustomHeaderDisabledBack(props, true)}
        options={CustomHeader}
        name="Orders"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <Orders {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="OrderDetail"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <OrderDetail {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="Addresses"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <Addresses {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="PaymentMethods"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <PaymentMethods {...props} />
          </WrapperTopSearch>
        )}
      />
      {/*********** PROFILE ************/}
      {/*********** HOME ************/}

      <StackController.Screen
        name="OnBoardingScreen"
        options={CustomSplash}
        //options={CustomHeader}
        component={OnBoardingScreen}
      />

      <StackController.Screen
        options={CustomHeader}
        name="LoginScreen"
        component={LoginScreen}
      />
      <StackController.Screen
        options={CustomHeader}
        name="SignUpScreen"
        component={SignUpScreen}
      />
      <StackController.Screen
        options={CustomHeader}
        name="ForgetPassScreen"
        component={ForgetPassScreen}
      />
      <StackController.Screen
        options={CustomHeader}
        name="SignUpFormValidate"
        component={SignUpFormValidate}
      />
      <StackController.Screen
        options={CustomSplash}
        name="SplashScreen"
        component={SplashScreen}
      />
      <StackController.Screen
        // options={(props) => CustomHeader(props, false)}
        options={CustomHeader}
        name="SplashScreenReload"
        component={SplashScreen}
      />
      <StackController.Screen
        options={CustomHeader}
        name="ChangeZipCode"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <ChangeZipCode {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        // options={(props) => CustomHeaderDisabledBack(props, true)}
        options={CustomHeader}
        name="HomeScreen"
        // component={HomeScreen}
        component={(props) => (
          <WrapperTopSearch {...props}>
            <HomeScreen {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="Collection"
        // component={Collection}
        component={(props) => (
          <WrapperTopSearch {...props}>
            <Collection {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="Hotsale"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <HotSale {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="SearchResult"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <SearchResult {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="NewStores"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <NewStores {...props} />
          </WrapperTopSearch>
        )}
      />

      <StackController.Screen
        options={CustomHeader}
        name="NewStoresNLJAL"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <NewStoresNLJAL {...props} />
          </WrapperTopSearch>
        )}
      />

      <StackController.Screen
        options={CustomHeader}
        name="StoreScreen"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <StoreScreen {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        // options={(props) => CustomHeaderDisabledBack(props, true)}
        options={CustomHeader}
        name="ShoppingCart"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <ShoppingCart {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="ProductDetail"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <ProductDetail {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        // options={(props) => CustomHeaderDisabledBack(props, true)}
        options={CustomHeader}
        name="CategoriesList"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <CategoriesList {...props} />
          </WrapperTopSearch>
        )}
      />

      <StackController.Screen
        options={CustomHeader}
        name="CatalogScreen"
        component={(props) => (
          <WrapperTopSearch {...props}>
            <CatalogScreen {...props} />
          </WrapperTopSearch>
        )}
      />
      <StackController.Screen
        options={CustomHeader}
        name="ProductNotAvailable"
        component={ProductNotAvailable}
      />
      <StackController.Screen
        // options={{
        //   headerTitle: '',
        //   headerBackTitle: 'atrás',
        //   headerTintColor: colors.colorMain300,
        // }}
        options={CustomHeader}
        name="Checkout"
        component={Checkout}
      />
      <StackController.Screen
        // options={{
        //   headerTitle: '',
        //   headerBackTitle: 'atrás',
        //   headerTintColor: colors.colorMain300,
        // }}
         options={CustomHeader}
        name="SelectPayment"
        component={SelectPayment}
      />
      <StackController.Screen
        options={CustomHeader}
        name="PaymentSuccess"
        component={PaymentSuccess}
      />
    </StackController.Navigator>
  );
}

const isAndroid = Platform.OS === 'android';

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={HomeScreen} />
    </Tab.Navigator>
  );
}

export class App extends Component {
  constructor(props) {
    super(props);
    
    
  }
  render() {
    return (
      <NavigationContainer>
        <StackController.Navigator
          screenOptions={{
            unmountInactiveRoutes: true,
          }}
        >
          <StackController.Screen
            name="RootScreensController"
            component={RootScreens}
            options={{ title: 'Root', headerShown: false }}
          />

          <StackController.Screen
            name="MyTabs"
            component={MyTabs}
            options={{ title: 'Root', headerShown: false }}
          />
        </StackController.Navigator>
      </NavigationContainer>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { app } = state;
  return {
    zipCode: app.zipCode,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
