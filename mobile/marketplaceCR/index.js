import 'react-native-gesture-handler';

import { decode, encode } from 'base-64';
import React, { Component } from 'react';
import dayjs from 'dayjs';
import 'dayjs/locale/es';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import * as Sentry from '@sentry/react-native';

import { name as appName } from './app.json';
import store from './src/redux/store';
import App from './App';
import {
  _clearData,
  _clearVersion,
  _getData,
  _customSetData,
} from './src/utils/asyncStorage';

import { GlobalReactApplication } from './src/utils/GlobalReactApplication/GlobalReactApplication';

const currentAsyncVersion = '2.0.1';

// define global 'btoa' and 'atob' pollyfills
// These are used to use Axios 'auth' feature.
// (Ex. used in paypal payments)
if (!global.btoa) {
  global.btoa = encode;
}

if (!global.atob) {
  global.atob = decode;
}

// Set dates locale to Spanish.
dayjs.locale('es');

// Sentry setup
Sentry.init({
  dsn:
    'https://5eed5463dda54d15928dfe16324ee2c8@o144533.ingest.sentry.io/5227710',
});

class Main extends Component<{}> {
  async componentDidMount() {
    const asyncversion = (await _getData()).asyncversion;
    if (asyncversion) {
      if (currentAsyncVersion === asyncversion) {
      } else {
        await _clearData();
        _customSetData('asyncversion', currentAsyncVersion);
      }
    } else {
      await _clearData();
      _customSetData('asyncversion', currentAsyncVersion);
    }
    GlobalReactApplication();
  }
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

// Register component
AppRegistry.registerComponent(appName, () => Main);
