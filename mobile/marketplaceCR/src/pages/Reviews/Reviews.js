import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { View, Text } from 'react-native';
import { appActions } from '../../redux/actions';

class Reviews extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <React.Fragment>
        <View>
          <Text>Espera un momento...</Text>
        </View>
      </React.Fragment>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  const { getMarketCategories, getCategories } = appActions;

  return bindActionCreators(
    {
      getMarketCategories,
      getCategories,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Reviews);
