import React, { Component } from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Sentry from '@sentry/react-native';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';
// import OTPInput from 'react-native-otp';

import { colors, typography, buttons } from '../../assets';
import iconMail from '../../images/icon/iconPadlock.png';
import HeaderWelcome from '../../components/Welcome/HeaderWelcome';
import { userActions } from '../../redux/actions';

// const code = value =>
//   value && value.length === 4 ? 'Máximo 4 dígitos' : undefined;

const renderField = ({
  keyboardType,
  meta: { touched, error, warning },
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
}) => {
  return (
    <View>
      <TextInput
        style={[styles.inputForm, { color: colors.white }]}
        placeholder={placeholder}
        placeholderTextColor={colors.black300}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        {...restInput}
        secureTextEntry={secureTextEntry}
        maxLength={4}
      />
      {touched &&
        ((error && <Text style={styles.formErrors}>{error}</Text>) ||
          (warning && <Text style={styles.formErrors}>{warning}</Text>))}
    </View>
  );
};

export class VerifyAccountForm extends Component {
  constructor() {
    super();
    this.state = {
      otp: '',
      success: false,
    };
    this.formSubmit = this.formSubmit.bind(this);
  }

  handleOTPChange = (otp) => {
    this.setState({ otp });
  };

  formSubmit(formData) {
    const code = formData.code;
    const query = `validates?email=${this.props.user.email}&code=${code}`;

    // Register user
    return this.props
      .validateCode(query)
      .then((response) => {
        this.setState({
          success: true,
        });
      })
      .catch((error) => {
        const uiMessage = 'Código inválido';
        Sentry.captureException(error);
        throw new SubmissionError({
          _error: uiMessage,
        });
      });
  }

  render() {
    const { handleSubmit, error } = this.props;
    const { success } = this.state;
    return (
      <View style={styles.background}>
        <HeaderWelcome />
        <View style={styles.messageContainer}>
          <Image source={iconMail} style={styles.icon} />
          <Text style={styles.messageTitle}>VERIFICACIÓN</Text>
          <Text style={styles.message}>
            Por favor ingresa los 4 dígitos del código de verificación que
            enviamos a tu correo.
          </Text>
          <Field
            id="code"
            name="code"
            fieldName="code"
            keyboardType="number-pad"
            placeholder="Ingresar código"
            component={renderField}
          />
          {/* <OTPInput
            value={this.state.otp}
            onChange={this.handleOTPChange}
            style={styles.optInput}
            tintColor={colors.corePink}
            color={colors.white}
            offTintColor="#BBBCBE"
            otpLength={4}
          /> */}
          <TouchableOpacity
            onPress={() => {
              //this.props.navigation.navigate('VerifyAccountScreen');

              globalThis.ReactApplication.ActiveScreen(
                'VerifyAccountScreen',
                {},
                this.props.navigation,
              );
            }}
            activeOpacity={0.5}
          >
            <Text style={styles.txtBlack}>Reenviar código</Text>
          </TouchableOpacity>
        </View>
        {success &&
          globalThis.ReactApplication.ActiveScreen(
            'HomeScreen',
            {},
            this.props.navigation,
          )}
        {error && <Text style={styles.submitErrors}>{error}</Text>}
        <View style={styles.inputContainer}>
          <TouchableOpacity
            style={styles.btnLogin}
            activeOpacity={0.5}
            onPress={handleSubmit(this.formSubmit)}
          >
            <Text style={styles.textBtnLogin}>Verificar</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              // this.props.navigation.navigate('HomeScreen');
              globalThis.ReactApplication.ActiveScreen(
                'HomeScreen',
                {},
                this.props.navigation,
              );
            }}
            activeOpacity={0.5}
          >
            <Text style={styles.txtBlack}>Continuar sin verificar</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

VerifyAccountForm = reduxForm({
  form: 'verifyAccount',
})(VerifyAccountForm);

// Redux Map Functions
function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  const { validateCode } = userActions;
  return bindActionCreators(
    {
      validateCode,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(VerifyAccountForm);

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: colors.colorMain400,
  },
  messageContainer: {
    flex: 6,
    alignItems: 'center',
    justifyContent: 'flex-start',
    textAlign: 'center',
    width: 320,
  },
  icon: {
    marginVertical: 20,
    width: 60,
    height: 80,
  },
  messageTitle: {
    ...typography.txtExtraBold,
    fontSize: 26,
    color: colors.corePink,
    marginVertical: 20,
  },
  message: {
    ...typography.txtLight,
    fontSize: 18,
    color: colors.white,
    textAlign: 'center',
  },
  btnLogin: {
    ...buttons.square,
  },
  textBtnLogin: {
    textAlign: 'center',
    ...typography.txtRegular,
    fontSize: 20,
    color: colors.white,
  },

  inputContainer: {
    flex: 3,
    justifyContent: 'flex-start',
  },
  txtWhiteBold: {
    ...typography.txtLight,
    fontSize: 16,
    color: colors.white,
    textAlign: 'center',
  },
  borderStyleBase: {
    width: 30,
    height: 45,
  },
  inputForm: {
    marginVertical: 30,
    fontSize: 26,
  },
  txtBlack: {
    ...typography.txtLight,
    fontSize: 16,
    color: colors.black300,
    textAlign: 'center',
    marginTop: 15,
  },
});
