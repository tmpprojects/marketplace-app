import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  ScrollView,
  Image,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';

import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography } from '../../assets';
import { CheckBox } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import backArrow from '../../images/icon/BackArrow.png';

export default class Address extends Component {
  PropTypes = {
    route: PropTypes.shape({
      params: PropTypes.shape({
        addresses: PropTypes.array.required,
        selectedShippingAddress: PropTypes.oneOfType([
          PropTypes.string.isRequired,
          PropTypes.oneOf([null]).isRequired,
        ]),
      }).isRequired,
    }).isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      checked: false,
    };
  }

  /**
   * onChangeAddress()
   * This method executes when users select one address from the list.
   * This executes parent callback if it´s present.
   * @param <object> address | Address Object.
   */
  onChangeAddress = (address: object) => {
    const { navigation, route } = this.props;

    const { onChangeAddress } = route.params;

    // const onChangeAddress = getOnChangeAddress

    onChangeAddress(address);
  };

  /**
   * render()
   */
  render() {
    const { navigation, route } = this.props;
    const { addresses, selectedShippingAddress } = route.params;

    // const addresses = getAddresses;
    // const selectedShippingAddress = getSelectedShippingAddress;
    // Return JSX
    return (
      <View>
        <React.Fragment>
          <TouchableOpacity
            style={styles.arrowBtn}
            onPress={() => {
              //navigation.navigate('Checkout');

              globalThis.ReactApplication.ActiveScreen(
                'Checkout',
                {},
                navigation,
              );
            }}
          >
            <Image source={backArrow} style={styles.iconArrow} />
          </TouchableOpacity>
          <Text style={styles.title}>Seleccione una Dirección</Text>

          {/* ADDRESSES LIST */}
          <ScrollView>
            {addresses.map((address) => (
              <View key={address.uuid} style={styles.addressesList}>
                <View style={styles.addressCard}>
                  <View style={styles.headerCard}>
                    <View style={styles.checkboxContainer}>
                      <CheckBox
                        checkedIcon={
                          <Icon
                            name={'md-checkbox'}
                            style={styles.checkedIcon}
                          />
                        }
                        uncheckedIcon={
                          <Icon
                            name={'md-square-outline'}
                            style={styles.uncheckedIcon}
                          />
                        }
                        checked={address.uuid === selectedShippingAddress}
                        onPress={() => this.onChangeAddress(address)}
                      />
                      <Text
                        style={[
                          styles.default,
                          this.state.checked
                            ? {
                                color: colors.colorMain300,
                              }
                            : {
                                color: colors.colorDark400,
                              },
                        ]}
                      >
                        Enviar a esta dirección
                      </Text>
                    </View>
                  </View>

                  {/* ADDRESS DETAILS */}
                  <View style={styles.addressContainer}>
                    <Text style={styles.addressName}>
                      {address.address_name}
                    </Text>
                    <Text style={styles.owner}>{address.client_name}</Text>
                    <Text style={styles.address}>
                      {`${address.street_address} ${address.num_ext}, ${
                        address.num_int || ''
                      }`}
                    </Text>
                    <Text style={styles.address}>
                      {`${address.neighborhood}, C.P. ${address.zip_code}`}
                    </Text>
                    <Text style={styles.address}>
                      {`${address.city}, C.P. ${address.state}`}
                    </Text>
                  </View>
                  {/* END: ADDRESS DETAILS */}
                </View>
              </View>
            ))}
          </ScrollView>
          {/* END: ADDRESSES LIST */}
        </React.Fragment>
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  arrowBtn: {
    marginTop: 20,
    marginHorizontal: 15,
  },
  title: {
    ...typography.txtExtraBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  addressesList: {
    padding: 15,
    marginVertical: 30,
  },
  //CARD ADDRESS
  addressCard: {
    backgroundColor: colors.colorWhite,
    shadowColor: colors.colorGray400,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    padding: 15,
  },
  headerCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    marginLeft: 24,
  },
  addressName: {
    ...typography.txtSemiBold,
    marginBottom: 10,
  },
  addressContainer: {
    marginVertical: 15,
    maxWidth: '85%',
  },
  owner: {
    ...typography.txtSemiBold,
    fontSize: 16,
  },
  address: {
    ...typography.txtLight,
    marginTop: 8,
  },
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: -20,
  },
  checkedIcon: {
    color: colors.colorMain300,
    fontSize: 25,
  },
  uncheckedIcon: {
    color: colors.colorGray400,
    fontSize: 25,
  },
  default: {
    ...typography.txtSemiBold,
    paddingBottom: 2,
  },
  btnAdd: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
});
