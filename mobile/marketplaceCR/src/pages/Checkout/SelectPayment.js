//import config from 'react-native-config';
import { config } from '../../../env';
import React, { Component } from 'react';
import stripe from 'tipsi-stripe';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { userActions } from '../../redux/actions';
import { StyleSheet, Image, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import * as Sentry from '@sentry/react-native';
import {
  PAYMENT_METHOD_BANK_TRANSFER,
  PAYMENT_METHOD_STRIPE,
  PAYMENT_METHOD_PAYPAL,
  PAYMENT_METHOD_OXXO,
  PAYMENT_METHOD_SAVED_CARD,
} from '../../utils/shoppingCartUtils/helpers';
import cardMC from '../../images/icon/icon_mastercard.png';
import cardV from '../../images/icon/visaCard.png';
import paypal from '../../images/icon/payPal.png';
import oxxo from '@canastarosa/ds-theme/assets/images/cards/oxxo.png';

//DS
import { colors } from '@canastarosa/ds-theme/colors';
import { typography } from '@canastarosa/ds-theme/typography';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';

//Will be added to the ds
import { CRIcon } from '../../components/CRIcon';
import { CRRowBoth, CRRowFull } from '../../components/Layout';
import { CRCard } from '../../components/CRCard';
import { CRTitle } from '../../components/CRTitle';

stripe.setOptions({
  publishableKey: config.STRIPE_PK,
});
declare var globalThis: any;
export class SelectPayment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      checkedCardMain: false,
    };
  }

  /*   handleCardPayPress = async () => {
    try {
      this.setState({ loading: true, token: null });

      return token.tokenId;
    } catch (error) {
      //this.setState({ loading: false });
    }
  }; */

  openCardForm = () => {
    return stripe.paymentRequestWithCardForm();
  };

  /**
   * onChangePaymentMethod()
   * This method executes when users select a payment method from the list.
   * This executes parent callback if it´s present.
   * @param <object> paymentMethod | Payment Method Object.
   */
  onChangePaymentMethod = (paymentMethod: object, item) => {
    // Get onChangePaymentMethod callback from navigation props.
    const { navigation, route } = this.props;
    const { onChangePaymentMethod } = route.params;

    // const onChangePaymentMethod = getOnChangePaymentMethod;
    // Ask user to fill card form (Stripe) before submitting payment method.
    if (paymentMethod.slug === PAYMENT_METHOD_STRIPE.slug) {
      this.openCardForm()
        .then((response) => {
          onChangePaymentMethod({
            ...paymentMethod,
            valid: true,
            data: response,
            payment_card_token: response.tokenId,
            payment_method_id: response.card.brand,
          });
        })
        .catch((e) => {
          Sentry.captureException(e);
        });
      return;
    }

    if (paymentMethod.slug === PAYMENT_METHOD_SAVED_CARD.slug) {
      onChangePaymentMethod({
        ...paymentMethod,
        valid: true,
        data: item,
        payment_card_token: item.id,
        payment_method_id: item.card.brand,
        paySavedCard: true,
      });
      return;
    }
    // Ask user to fill card form (Stripe) before submitting payment method.
    // if (paymentMethod.slug === PAYMENT_METHOD_PAYPAL.slug) {
    //   this.openCardForm()
    //     .then((response) => {
    //       onChangePaymentMethod({
    //         ...paymentMethod,
    //         valid: true,
    //         data: response,
    //       });
    //     })
    //     .catch((err) => {});
    //   return;
    // }

    //
    onChangePaymentMethod(paymentMethod);
  };

  /**
   * render()
   */
  render() {
    const { navigation, route, cards } = this.props;

    const { paymentMethods } = route.params;

    // const paymentMethods = getPaymentMethods;
    /*const selectedPaymentMethod = navigation.getParam(
      'selectedPaymentMethod',
      null,
    );*/

    // Return JSX
    return (
      <ScrollView style={styles.paymentMethodWrapper}>
        <React.Fragment>
          <CRTitle title="Métodos de pago" />
          {/* CREDIT CARD */}
          <CRRowFull
            addedStyle={[
              cards.lenght > 0 ? styles.sectionSeparator : styles.sectionMargin,
            ]}
          >
            <TouchableOpacity
              onPress={() =>
                this.onChangePaymentMethod(
                  paymentMethods[PAYMENT_METHOD_STRIPE.slug],
                )
              }
            >
              <CRRowBoth>
                <CRCard size="full" attitude="disabled">
                  <View style={styles.headerCard}>
                    <CRIcon name="card" size={18} color={colors.colorDark100} />
                    <CRRowBoth>
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark300}
                      >
                        Añadir Tarjeta de Crédito / Débito
                      </CRText>
                    </CRRowBoth>
                  </View>
                </CRCard>
              </CRRowBoth>
            </TouchableOpacity>
          </CRRowFull>
          {/* SAVED CARDS */}
          <CRRowFull
            addedStyle={[
              cards.lenght > 0 ? styles.sectionSeparator : styles.sectionMargin,
            ]}
          >
            {cards.cards.map((item, i) => (
              <TouchableOpacity
                key={i}
                onPress={() =>
                  this.onChangePaymentMethod(
                    paymentMethods[PAYMENT_METHOD_SAVED_CARD.slug],
                    item,
                  )
                }
              >
                <CRRowBoth>
                  <CRCard size="full" attitude="disabled">
                    <View style={styles.headerCard}>
                      {item.card.brand === 'visa' ? (
                        <Image source={cardV} style={styles.cardIMG} />
                      ) : (
                        <Image source={cardMC} style={styles.cardIMG} />
                      )}
                      <CRRowBoth>
                        <CRText
                          variant={typography.paragraph}
                          color={colors.colorDark300}
                        >
                          •••• •••• •••• {item.card.last4}
                        </CRText>
                      </CRRowBoth>
                      <CRRowBoth>
                        <CRText
                          variant={typography.caption}
                          color={colors.colorDark100}
                        >
                          {item.card.exp_month}/{item.card.exp_year}
                        </CRText>
                      </CRRowBoth>
                    </View>
                  </CRCard>
                </CRRowBoth>
              </TouchableOpacity>
            ))}
          </CRRowFull>

          <TouchableOpacity
            onPress={() =>
              this.onChangePaymentMethod(
                paymentMethods[PAYMENT_METHOD_PAYPAL.slug],
              )
            }
          >
            <CRRowBoth>
              <CRCard size="full" attitude="disabled">
                <View style={styles.headerCard}>
                  <Image source={paypal} style={styles.cardPayPal} />
                  <CRRowBoth>
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorDark300}
                    >
                      PayPal
                    </CRText>
                  </CRRowBoth>
                </View>
              </CRCard>
            </CRRowBoth>
          </TouchableOpacity>

          {/* OXXO */}
          <TouchableOpacity
            onPress={() =>
              this.onChangePaymentMethod(
                paymentMethods[PAYMENT_METHOD_OXXO.slug],
              )
            }
          >
            <CRRowBoth>
              <CRCard size="full" attitude="disabled">
                <View style={styles.headerCard}>
                  <Image source={oxxo} style={styles.cardOXXO} />
                  <CRRowBoth>
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorDark300}
                    >
                      OXXO
                    </CRText>
                  </CRRowBoth>
                </View>
              </CRCard>
            </CRRowBoth>
          </TouchableOpacity>
          {/* END: OXXO */}

          {/* BANK TRANSFER */}
          <TouchableOpacity
            onPress={() =>
              this.onChangePaymentMethod(
                paymentMethods[PAYMENT_METHOD_BANK_TRANSFER.slug],
              )
            }
          >
            <CRRowBoth>
              <CRCard size="full" attitude="disabled">
                <View style={styles.headerCard}>
                  <CRIcon
                    name="transfer"
                    size={18}
                    color={colors.colorDark100}
                  />
                  <CRRowBoth>
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorDark300}
                    >
                      Transferencia bancaria
                    </CRText>
                  </CRRowBoth>
                </View>
              </CRCard>
            </CRRowBoth>
          </TouchableOpacity>
          {/* END: BANK TRANSFER */}
        </React.Fragment>
      </ScrollView>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    cards: state.user.cards,
  };
}

function mapDispatchToProps(dispatch) {
  const { getUserCards } = userActions;

  return bindActionCreators(
    {
      getUserCards,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(SelectPayment);

const styles = StyleSheet.create({
  paymentMethodWrapper: {
    backgroundColor: colors.colorWhite,
    flex: 1,
  },
  arrowBtn: {
    marginTop: 20,
    marginHorizontal: 15,
  },
  title: {
    ...typography.txtExtraBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  sectionSeparator: {
    paddingBottom: 15,
    marginBottom: 10,
    borderBottomColor: colors.colorGray100,
    borderBottomWidth: 1,
  },
  sectionMargin: {
    marginVertical: 0,
  },
  cardsList: {},
  //CARD
  card: {
    backgroundColor: colors.colorWhite,
    shadowColor: colors.colorGray400,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    padding: 15,
  },
  headerCard: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  cardContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    marginLeft: 24,
  },
  cardNumber: {
    ...typography.txtRegular,
    marginLeft: 15,
  },
  validation: {
    ...typography.txtLight,
  },
  owner: {
    ...typography.txtSemiRegular,
    marginTop: 15,
    marginLeft: 32,
    fontSize: 16,
  },
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: -20,
  },
  checkedIcon: {
    color: colors.colorMain300,
    fontSize: 25,
  },
  uncheckedIcon: {
    color: colors.colorGray400,
    fontSize: 25,
  },
  default: {
    ...typography.txtLight,
    paddingBottom: 2,
  },

  titleSelectPayment: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
    fontSize: 20,
    textAlign: 'left',
    paddingLeft: 10,
  },
  cardIMG: {
    height: 27,
    width: 50,
    resizeMode: 'contain',
  },
  cardOXXO: {
    height: 20,
    width: 50,
    resizeMode: 'contain',
  },
  cardPayPal: {
    height: 25,
    width: 50,
    resizeMode: 'contain',
  },
});
