// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  FieldArray,
  Field,
  getFormSyncErrors,
  formValueSelector,
} from 'redux-form';
import {
  Alert,
  View,
  Image,
  Platform,
  TextInput,
  SafeAreaView,
} from 'react-native';
import * as Sentry from '@sentry/react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { SHOPPING_CART_FORM_CONFIG } from '../../utils/shoppingCartUtils/shoppingCartFormConfig';
import {
  PAYMENT_METHOD_STRIPE,
  PAYMENT_METHOD_OXXO,
  PAYMENT_METHOD_BANK_TRANSFER,
  PAYMENT_METHOD_PAYPAL,
  PAYMENT_METHOD_SAVED_CARD,
} from '../../utils/shoppingCartUtils/helpers';
import { formatNumberToPrice } from '../../utils/normalizePrice';
import {
  COUPON_MODEL,
  couponErrorTypes,
} from '../../utils/shoppingCartUtils/helpers';
import {
  COUPON_TYPES,
  PAYMENT_METHODS,
} from '../../redux/constants/app.constants';
import withShoppingCartUI from '../../utils/hocs/withShoppingCartUI';

import CartOrder from '../../components/ShoppingCart/CartOrder';
import { CheckBox } from 'react-native-elements';
import { getAddress } from '../../redux/reducers/user.reducer';
import { shoppingCartActions } from '../../redux/actions';
import CartSummary from '../../components/ShoppingCart/CartSummary';

import cardMC from '../../images/icon/icon_mastercard.png';
import cardV from '../../images/icon/visaCard.png';
import visaIMG from '../../images/assets/VISALogo--blue.png';
import payPalp from '../../images/icon/payPal.png';
import oxxo from '@canastarosa/ds-theme/assets/images/cards/oxxo.png';
import { field } from '../../components/FieldsForm/FieldsFormStyles';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  required,
  email as emailValidator,
  phoneNumberValid,
  number,
} from '../../utils/formValidators';
import { layout } from './layout';
//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
//will be added to the ds
import { CRIcon } from '../../components/CRIcon';
import { CRRowBoth, CRRowLeft, CRRowFull } from '../../components/Layout';
import { CRCard } from '../../components/CRCard';
import { CRBottomButton } from '../../components/CRBottomButton';
import { CRButton } from '../../components/CRButton';
import { CRLoaderScreen } from '../../components/CRAnim/CRLoaderScreen';
import { CRTitle } from '../../components/CRTitle';

import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { styles } from '../../components/Profile/Address/AddressModal/AddressModalStyles';
import AsyncStorage from '@react-native-community/async-storage';
import { _getPaymentMethodSaved } from './../../utils/asyncStorage';

//Google Analytics
import analytics from '@react-native-firebase/analytics';
declare var globalThis: any;

/**--------------------------------------
 * Validation helper functions
 --------------------------------------*/
const phoneMinLength = (min) => (value) =>
  value && value.length < min
    ? 'El número debe contener al menos 8 digitos.'
    : undefined;
const phoneNumber = phoneMinLength(8);

const CouponField = (props) => {
  const { input, meta, ...inputProps } = props;
  return (
    <TextInput
      {...inputProps}
      placeholderTextColor={colors.colorGray300}
      onChangeText={input.onChange}
      onBlur={input.onBlur}
      onFocus={input.onFocus}
      value={input.value.toUpperCase()}
    />
  );
};

const renderField = ({
  keyboardType,
  meta: { touched, error },
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
}) => {
  return (
    <View style={layout.fieldWrapper}>
      <TextInput
        style={layout.field}
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray300}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        {...restInput}
        secureTextEntry={secureTextEntry}
        autoCapitalize={'none'}
      />
      {touched && error && (
        <CRText
          variant={typography.caption}
          color={colors.colorRed300}
          style={layout.errorsWrapper}
        >
          {error}
        </CRText>
      )}
    </View>
  );
};

type CheckoutProps = {
  sendPayPalOrder: Function,
  sendOrder: Function,
  updateCartProduct: Function,
  removeFromCart: Function,
  validateCoupon: Function,
  fetchShoppingCart: Function,
  renderPayPal: Function,
  dispatch: Function,
  change: Function,
  renderAddressList: Function,
  orderHasShippingErrors: ?{},
  orderHasPaymentErrors: ?{},
  formErrors: Object,
  navigation: Object,

  user: Object,
  coupon: Object,
  orders: Array<Object>,
  paymentMethod: Object,
  shippingAddress: Object,
  requiresInvoice: boolean,
  clientDataEmail: String,
  cartPrice: number,
  cartPriceWithDiscount: number,
  shippingPrice: number,
  totalPrice: number,
  totalPriceWithDiscount: number,
  addressList: Array<Object>,
  paymentMethodsList: Object,
  unavailableOrders: Array<Object>,
};
type CheckoutState = {
  checked: boolean,
  modalView: Object,
  modalVisible: boolean,
  showPayPalModal: boolean,
  loadingPayment: boolean,
  checkedSave: boolean,
  couponStatus: String,
};
class Checkout extends Component<CheckoutProps, CheckoutState> {
  _paypal_payment_details: Object | null = null;
  state = {
    checked: false,
    modalView: null,
    modalVisible: false,
    showPayPalModal: false,
    loadingPayment: false,
    checkedSave: false,
    checkedAddresses: false,
    couponStatus: '',
    paymentMethodSaved: '',
    last4: '',
    itemBrand: [],
    itemId: [],
    itemName: [],
    itemVariant: [],
  };

  PropTypes = {
    route: PropTypes.shape({
      params: PropTypes.shape({
        viewTrigger: 'props',
        addresses: PropTypes.array.required,
        selectedShippingAddress: PropTypes.oneOfType([
          PropTypes.string.isRequired,
          PropTypes.oneOf([null]).isRequired,
        ]),
      }).isRequired,
    }).isRequired,
  };

  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    await AsyncStorage.removeItem('last4');
    await analytics().setCurrentScreen('CheckoutScreen');
    await analytics().logEvent('CheckoutScreen');
    await this.analyticslogBeginCheckout();
    //await AsyncStorage.removeItem('pmCard');
    this.setState({
      paymentMethodSaved: (await _getPaymentMethodSaved()).paymentMethodSaved,
    });
    // Update ShoppingCartManager
    try {
      globalThis.ShoppingCartManager.setPaymentMethod(
        this.state.paymentMethodSaved,
      );
    } catch (e) {
      const errorPayment = {
        email: this.props.user.profile.email,
        screen: 'Checkout',
        action: 'Try payment',
        func: 'componentDidMount()',
        error: e,
        dataGlobalThis: globalThis,
      };
      Sentry.captureException(errorPayment);
      globalThis.ReactApplication.SentryCatch(
        'Checkout',
        'componentDidMount()',
        'Error in componentDidMount',
        this.props.user.profile.email,
        errorPayment,
      );
    }
  }

  //Analytics
  async analyticslogBeginCheckout() {
    await this.props.orders.map((product, i) =>
      product.products.map(
        (productDetail, i) => (
          this.state.itemBrand.push(productDetail.product.store.name),
          this.state.itemId.push(productDetail.id),
          this.state.itemName.push(productDetail.product.name),
          this.state.itemVariant.push(
            productDetail.attribute === null
              ? ''
              : productDetail.attribute.tree_string,
          )
        ),
      ),
    );
    await analytics().logBeginCheckout({
      value: this.props.totalPrice,
      currency: 'MXN',
      //coupon: '',
      items: [
        {
          item_brand: this.state.itemBrand.join(', ').toString(),
          item_id: this.state.itemId.join(', ').toString(),
          item_name: this.state.itemName.join(', ').toString(),
          item_variant: this.state.itemVariant.join(', ').toString(),
          //item_category: 'round necked t-shirts',
        },
      ],
    });
  }

  completeOrder = () => {
    /*
     * On Complete Order
     * Sends payment to payment providers
     */
    globalThis.ReactApplication.BackScreen.disable = true;
    console.log('disable');

    this.setState({ loadingPayment: true });
    try {
      // Process order
      return (
        globalThis.ShoppingCartManager.completeOrder()
          .then((response) => {
            return this.makePayment(response)
              .then((orderResponse) => {
                globalThis.ReactApplication.BackScreen.reset();

                setTimeout(() => {
                  this.setState({ loadingPayment: false });
                }, 1000);

                // Errores al procesar la compra
              })
              .catch((e) => {
                globalThis.ReactApplication.BackScreen.reset();
                Alert.alert('Se encontró un error al procesar tu compra.');
                const errorPayment = {
                  email: this.props.user.profile.email,
                  screen: 'Checkout',
                  action: 'Try payment',
                  func: 'completeOrder()',
                  dataGlobalThis: globalThis,
                  error: e,
                };
                Sentry.captureException(errorPayment);
                globalThis.ReactApplication.SentryCatch(
                  'Checkout',
                  'completeOrder()',
                  'Error in completeOrder',
                  this.props.user.profile.email,
                  errorPayment,
                );
                throw e;
              });
          })

          // Errores al validar el formulario de compra.
          .catch((e) => {
            const errorPayment = {
              email: this.props.user.profile.email,
              screen: 'Checkout',
              action: 'Try payment',
              func: 'completeOrder()',
              dataGlobalThis: globalThis,
              error: e,
            };

            Sentry.captureException(errorPayment);
            globalThis.ReactApplication.SentryCatch(
              'Checkout',
              'completeOrder()',
              'Error in completeOrder',
              this.props.user.profile.email,
              errorPayment,
            );

            this.setState({ loadingPayment: false });

            // TODO
            // SHOW / ALERT users with error description

            //
            return Promise.reject(e);
          })
      );

      // Error processing the order.
    } catch (e) {
      this.setState({ loadingPayment: false });

      const errorPayment = {
        email: this.props.user.profile.email,
        screen: 'Checkout',
        action: 'Try payment',
        func: 'completeOrder()',
        dataGlobalThis: globalThis,
        error: e,
      };

      Sentry.captureException(errorPayment);
      globalThis.ReactApplication.SentryCatch(
        'Checkout',
        'completeOrder()',
        'Error in completeOrder',
        this.props.user.profile.email,
        errorPayment,
      );

      return Promise.reject(e);
    }
  };

  testCheck() {
    if (this.state.checkedSave === true) {
      AsyncStorage.setItem(
        'last4',
        JSON.stringify(this.props.paymentMethod.data.card.last4),
      );
    } else {
      return;
      //AsyncStorage.removeItem('last4');
    }
  }

  makePayment = (order: Object) => {
    /**
     * makePayment()
     * Select the corresponding payment method and proccess transaction.
     * @params order: <object> | An order group object.
     * @returns Promise | Indicates success or failure of transaction.
     */

    // We must 'switch' logic behaviour according to user preferences.
    // Some payment methods have different implementation 'flows'.
    // Each case depends on API implementations, technical specs, UX flows, etc...

    switch (order.payment_provider) {
      // CREDIT/DEBIT CARDS
      // Currently managed by MERCADO PAGO.
      // This provider could change in the future.
      case PAYMENT_METHODS.STRIPE.slug:
        return this.props
          .sendOrder({
            ...order,
            payment_card_token: this.props.paymentMethod.data.tokenId,
            payment_method_id: this.props.paymentMethod.payment_method_id,
            is_saving_card: this.state.checkedSave,
            was_paid_with_saved_card: false,
            origin_frontend: 'mobile',
            //propiedad de select
          })
          .then((response) => {
            // Verify Payment Status.
            // (Payment Status: 3 - Paid, 2 - Refused, 1 - Awaiting)
            if (response.status === 201) {
              switch (response.data.payment_status.value) {
                // Approved
                case 'approved':
                  // this.props.navigation.navigate('PaymentSuccess', {
                  //   orderID: response.data.uid,
                  //   nameClient: this.props.user.profile.first_name,
                  //   emailClient: this.props.user.profile.email,
                  //   orderDetail: response.data,
                  //   paymentProvider: response.data.payment_provider_static_name,
                  // });

                  globalThis.ReactApplication.ActiveScreen(
                    'PaymentSuccess',
                    {
                      orderID: response.data.uid,
                      nameClient: this.props.user.profile.first_name,
                      emailClient: this.props.user.profile.email,
                      orderDetail: response.data,
                      paymentProvider:
                        response.data.payment_provider_static_name,
                    },
                    this.props.navigation,
                  );

                  break;
                case 'in_process':
                  // If the user used a new credit/debit card and
                  // choosed to store it on the database via the UI
                  // send card details to the backend.
                  //if (this.props.paymentMethod.saveCreditCard) {
                  //this.props.addCreditCard({
                  //token: order.payment_card_token,
                  //});
                  //}
                  //
                  break;

                // The order registry was created on the database,
                // but for some reason, the order payment has been rejected.
                case 'rejected':
                  const errorPayment = {
                    email: this.props.user.profile.email,
                    screen: 'Checkout',
                    action: 'Try payment',
                    func: 'makePayment()',
                    dataGlobalThis: globalThis,
                    error: e,
                    response: response.data,
                  };
                  Sentry.captureException(errorPayment);
                  globalThis.ReactApplication.SentryCatch(
                    'Checkout',
                    'makePayment()',
                    'Error in makePayment',
                    this.props.user.profile.email,
                    errorPayment,
                  );

                  Alert.alert(
                    'Tu compra no pudo completarse,',
                    'Lo sentimos, pero por un problema ajeno a nosotros, no pudimos procesar tu compra.',
                    [
                      {
                        text: 'Aceptar',
                        onPress: () => {},
                      },
                    ],
                    { cancelable: false },
                  );
                  break;
                default:
                  break;
              }
            }
          })
          .catch((e) => {
            const errorPayment = {
              email: this.props.user.profile.email,
              screen: 'Checkout',
              action: 'Try payment',
              func: 'makePayment()',
              dataGlobalThis: globalThis,
              error: e,
              response: response.data,
            };
            Sentry.captureException(errorPayment);
            globalThis.ReactApplication.SentryCatch(
              'Checkout',
              'makePayment()',
              'Error in makePayment',
              this.props.user.profile.email,
              errorPayment,
            );
            throw e;
          });

      //SAVED CARD
      case PAYMENT_METHODS.SAVED_CARD.slug:
        return (
          this.props
            .sendOrder({
              ...order,
              payment_provider: 'stripe',
              payment_card_token: this.props.paymentMethod.data.id,
              payment_method_id: this.props.paymentMethod.payment_method_id,
              is_paying_with_saved_card: this.props.paymentMethod.paySavedCard,
              was_paid_with_saved_card: true,
              origin_frontend: 'mobile',
              //propiedad de select
            })
            .then((response) => {
              // Verify Payment Status.
              // (Payment Status: 3 - Paid, 2 - Refused, 1 - Awaiting)
              if (response.status === 201) {
                switch (response.data.payment_status.value) {
                  // Approved
                  case 'approved':
                    // this.props.navigation.navigate('PaymentSuccess', {
                    //   orderID: response.data.uid,
                    //   nameClient: this.props.user.profile.first_name,
                    //   emailClient: this.props.user.profile.email,
                    //   orderDetail: response.data,
                    //   paymentProvider:
                    //     response.data.payment_provider_static_name,
                    // });

                    globalThis.ReactApplication.ActiveScreen(
                      'PaymentSuccess',
                      {
                        orderID: response.data.uid,
                        nameClient: this.props.user.profile.first_name,
                        emailClient: this.props.user.profile.email,
                        orderDetail: response.data,
                        paymentProvider:
                          response.data.payment_provider_static_name,
                      },
                      this.props.navigation,
                    );

                    break;
                  case 'in_process':
                    // If the user used a new credit/debit card and
                    // choosed to store it on the database via the UI
                    // send card details to the backend.
                    //if (this.props.paymentMethod.saveCreditCard) {
                    //this.props.addCreditCard({
                    //token: order.payment_card_token,
                    //});
                    //}
                    //
                    break;

                  // The order registry was created on the database,
                  // but for some reason, the order payment has been rejected.
                  case 'rejected':
                    const errorPayment = {
                      email: this.props.user.profile.email,
                      screen: 'Checkout',
                      action: 'Try payment',
                      func: 'makePayment()',
                      dataGlobalThis: globalThis,
                      response: response.data,
                    };

                    Sentry.captureException(errorPayment);
                    globalThis.ReactApplication.SentryCatch(
                      'Checkout',
                      'makePayment()',
                      'Error in makePayment SAVED_CARD',
                      this.props.user.profile.email,
                      errorPayment,
                    );
                    Alert.alert(
                      'Tu compra no pudo completarse,',
                      'Lo sentimos, pero por un problema ajeno a nosotros, no pudimos procesar tu compra.',
                      [
                        {
                          text: 'Aceptar',
                          onPress: () => {},
                        },
                      ],
                      { cancelable: false },
                    );
                    break;
                  default:
                    break;
                }
              }
            })

            // There was a problem with the order request.
            // Backend has rejected the POST request or is return some kind of error
            // Ej. 400 (bad requests), 500 (server errors)
            .catch((e) => {
              const errorPayment = {
                email: this.props.user.profile.email,
                screen: 'Checkout',
                action: 'Try payment',
                func: 'makePayment()',
                dataGlobalThis: globalThis,
                error: e,
              };

              Sentry.captureException(errorPayment);
              globalThis.ReactApplication.SentryCatch(
                'Checkout',
                'makePayment()',
                'Error in makePayment SAVED_CARD',
                this.props.user.profile.email,
                errorPayment,
              );
              throw e;
            })
        );

      // PAYPAL
      case PAYMENT_METHODS.PAYPAL.slug: {
        // order.payment_method_id = 'paypal';
        // Send Order to backend API to request a payment.
        // Getting a valid payment request from paypal let the users continue
        // with the PayPal payment flow (popup window)
        return (
          this.props
            .sendPayPalOrder({
              ...order,
              origin_frontend: 'mobile',
            })
            .then((response) => {
              this._paypal_payment_details = {
                ...response.data.payment_response,
              };
              return (
                this.props
                  .renderPayPal(this._paypal_payment_details)
                  .then((paypalToken) => {
                    // this.props.navigation.navigate('PaymentSuccess', {
                    //   orderID: response.data.uid,
                    //   nameClient: this.props.user.profile.first_name,
                    //   emailClient: this.props.user.profile.email,
                    //   orderDetail: response.data,
                    //   paymentProvider:
                    //     response.data.payment_provider_static_name,
                    // });

                    globalThis.ReactApplication.ActiveScreen(
                      'PaymentSuccess',
                      {
                        orderID: response.data.uid,
                        nameClient: this.props.user.profile.first_name,
                        emailClient: this.props.user.profile.email,
                        orderDetail: response.data,
                        paymentProvider:
                          response.data.payment_provider_static_name,
                      },
                      this.props.navigation,
                    );
                  })

                  // An error ocurred with the PayPal payment flow.
                  // Maybe the user cancelled or did not authorize the transaction.
                  // Other kind of trouble might be security issues or problems with paypal webview.
                  .catch((e) => {
                    const errorPayment = {
                      email: this.props.user.profile.email,
                      screen: 'Checkout',
                      action: 'Try payment',
                      func: 'makePayment()',
                      dataGlobalThis: globalThis,
                      error: e,
                    };
                    Sentry.captureException(errorPayment);
                    globalThis.ReactApplication.SentryCatch(
                      'Checkout',
                      'makePayment()',
                      'Error in makePayment in PAYPAL',
                      this.props.user.profile.email,
                      globalThis,
                    );
                  })
              );
            })

            // There was a problem with the order request.
            // Backend has rejected the POST request or is return some kind of error
            // Ej. 400 (bad requests), 500 (server errors)
            .catch((e) => {
              const errorPayment = {
                email: this.props.user.profile.email,
                screen: 'Checkout',
                action: 'Try payment',
                func: 'makePayment()',
                dataGlobalThis: globalThis,
                error: e,
              };
              Sentry.captureException(errorPayment);
              globalThis.ReactApplication.SentryCatch(
                'Checkout',
                'makePayment()',
                'Error in makePayment in PAYPAL',
                this.props.user.profile.email,
                errorPayment,
              );
              throw e;
            })
        );
      }

      // BANK TRANSFER & OXXO
      // This payment methods are UX-seamless for the user.
      // The frontend just sends the order creation request and
      // each case is handled by the backend logic.
      case PAYMENT_METHODS.BANK_TRANSFER.slug:
      case PAYMENT_METHODS.OXXO.slug:
        return (
          this.props
            .sendOrder({
              ...order,
              origin_frontend: 'mobile',
            })
            .then((response) => {
              // Verify Payment Status.
              // (Payment Status: 3 - Paid, 2 - Refused, 1 - Awaiting)
              if (response.status === 201) {
                switch (response.data.payment_provider_static_name) {
                  // Approved
                  case 'oxxo':
                    // this.props.navigation.navigate('PaymentSuccess', {
                    //   orderID: response.data.uid,
                    //   nameClient: this.props.user.profile.first_name,
                    //   emailClient: this.props.user.profile.email,
                    //   orderDetail: response.data,
                    //   paymentProvider:
                    //     response.data.payment_provider_static_name,
                    // });
                    globalThis.ReactApplication.ActiveScreen(
                      'PaymentSuccess',
                      {
                        orderID: response.data.uid,
                        nameClient: this.props.user.profile.first_name,
                        emailClient: this.props.user.profile.email,
                        orderDetail: response.data,
                        paymentProvider:
                          response.data.payment_provider_static_name,
                      },
                      this.props.navigation,
                    );
                    break;
                  case 'bank-transfer':
                    // this.props.navigation.navigate('PaymentSuccess', {
                    //   orderID: response.data.uid,
                    //   nameClient: this.props.user.profile.first_name,
                    //   emailClient: this.props.user.profile.email,
                    //   orderDetail: response.data,
                    //   paymentProvider:
                    //     response.data.payment_provider_static_name,
                    //   //barcode: response.data.payment_response.response.barcode.content
                    // });

                    globalThis.ReactApplication.ActiveScreen(
                      'PaymentSuccess',
                      {
                        orderID: response.data.uid,
                        nameClient: this.props.user.profile.first_name,
                        emailClient: this.props.user.profile.email,
                        orderDetail: response.data,
                        paymentProvider:
                          response.data.payment_provider_static_name,
                        //barcode: response.data.payment_response.response.barcode.content
                      },
                      this.props.navigation,
                    );

                    break;

                  // The order registry was created on the database,
                  // but for some reason, the order payment has been rejected.
                  case 'rejected':
                    const errorPayment = {
                      email: this.props.user.profile.email,
                      screen: 'Checkout',
                      action: 'Try payment',
                      func: 'makePayment()',
                      msg: 'Error al procesar compra OXXO/Transfer',
                      dataGlobalThis: globalThis,
                    };

                    Sentry.captureException(errorPayment);
                    globalThis.ReactApplication.SentryCatch(
                      'Checkout',
                      'makePayment()',
                      'Error in makePayment in PAYPAL',
                      this.props.user.profile.email,
                      errorPayment,
                    );

                    Alert.alert(
                      'Tu compra no pudo completarse,',
                      'Lo sentimos, pero por un problema ajeno a nosotros, no pudimos procesar tu compra.',
                      [
                        {
                          text: 'Aceptar',
                          onPress: () => {},
                        },
                      ],
                      { cancelable: false },
                    );
                    break;
                  default:
                    break;
                }
              }
            })

            // There was a problem with the order request.
            // Backend has rejected the POST request or is return some kind of error
            // Ej. 400 (bad requests), 500 (server errors)
            .catch((e) => {
              const errorPayment = {
                email: this.props.user.profile.email,
                screen: 'Checkout',
                action: 'Try payment',
                func: 'makePayment()',
                msg: 'Error al procesar compra OXXO/Transfer',
                dataGlobalThis: globalThis,
                error: e,
              };

              Sentry.captureException(errorPayment);
              globalThis.ReactApplication.SentryCatch(
                'Checkout',
                'makePayment()',
                'Error in makePayment in PAYPAL',
                this.props.user.profile.email,
                errorPayment,
              );
              throw e;
            })
        );

      // Payment Method not selected.
      default:
    }

    // No Payment Method selected
    return (
      //new PaymentError('No haz elegido ningún método de pago válido.', 'none'),
      Promise.reject()
    );
  };

  validateCoupon = (couponCode: string) => {
    /**
     * validateCoupon()
     * Calls and endpoint to verify if promo code is valid.
     */
    //debugger;

    if (!couponCode || couponCode === '') {
      return;
    }

    // Format coupon
    let formattedCoupon = couponCode.toUpperCase().trim();

    //Request validation of promo coupon
    return this.props
      .validateCoupon(formattedCoupon)
      .then((response) => {
        this.setState({ couponStatus: '' });
      })
      .catch((error) => {
        const errorResponse = error.response.data.code;
        let uiMessage = '';
        if (errorResponse == 'invalid; Código Invalido') {
          uiMessage = 'Lo sentimos, no es válido el cupón en tu carrito.';
        } else if (errorResponse == 'unknown; Código desconocido') {
          uiMessage = 'Introduce un código de descuento inválido';
        } else {
          uiMessage = 'Verifica el número de caracteres.';
        }
        Alert.alert(
          'Cupón Inválido',
          uiMessage,
          [
            {
              text: 'Aceptar',
              onPress: () => {},
            },
          ],
          { cancelable: false },
        );
        this.setState({ couponStatus: 'rejected' });

        const errorPayment = {
          email: this.props.user.profile.email,
          screen: 'Checkout',
          action: 'Try payment',
          func: 'validateCoupon()',
          error: error,
          dataGlobalThis: globalThis,
        };
        Sentry.captureException(errorPayment);
        globalThis.ReactApplication.SentryCatch(
          'Checkout',
          'validateCoupon()',
          'Error in validateCoupon',
          this.props.user.profile.email,
          errorPayment,
        );
      });
  };

  onCouponChange = (event: object, code: string) => {
    /**
     * onCouponChange()
     * Handler for the promoCode input
     * @param {string} code | Coupon Code
     */

    // If user has typed something.,
    // enable the UI validation button.
    if (code && code !== '') {
      // Reset coupon details and update coupon code only
      globalThis.ShoppingCartManager.setCoupon({
        ...COUPON_MODEL,
        code: code,
      });
    }
  };

  changePaymentMethod = async (paymentMethod: Object) => {
    /**
     * changePaymentMethod()
     * @param {object} paymentMethod | Payment Method object.
     */

    // Update ShoppingCartManager
    try {
      globalThis.ShoppingCartManager.setPaymentMethod(paymentMethod);
    } catch (e) {
      const errorPayment = {
        email: this.props.user.profile.email,
        screen: 'Checkout',
        action: 'Try payment',
        func: 'changePaymentMethod()',
        error: e,
        dataGlobalThis: globalThis,
      };
      Sentry.captureException(errorPayment);
      globalThis.ReactApplication.SentryCatch(
        'Checkout',
        'changePaymentMethod()',
        'Error in changePaymentMethod',
        this.props.user.profile.email,
        errorPayment,
      );
    }

    this.props.orders.map((product, i) =>
      product.products.map(
        (productDetail, i) => (
          this.state.itemBrand.push(productDetail.product.store.name),
          this.state.itemId.push(productDetail.id),
          this.state.itemName.push(productDetail.product.name),
          this.state.itemVariant.push(
            productDetail.attribute === null
              ? ''
              : productDetail.attribute.tree_string,
          )
        ),
      ),
    );

    analytics().logAddPaymentInfo({
      value: this.props.totalPrice,
      currency: 'MXN',
      payment_type: paymentMethod.slug,
      items: [
        {
          item_brand: this.state.itemBrand.join(', ').toString(),
          item_id: this.state.itemId.join(', ').toString(),
          item_name: this.state.itemName.join(', ').toString(),
          item_variant: this.state.itemVariant.join(', ').toString(),
          //item_category: 'round necked t-shirts',
        },
      ],
    });

    // Update redux form values
    this.props.change('paymentMethod', paymentMethod);
    if (paymentMethod.slug === 'stripe') {
      //console.log('Nada');
    } else {
      AsyncStorage.setItem('paymentMethodSaved', JSON.stringify(paymentMethod));
    }
    //this.props.fetchShoppingCart();
    // Return to Checkout view
    //this.props.navigation.navigate('Checkout');

    globalThis.ReactApplication.ActiveScreen(
      'Checkout',
      {},
      this.props.navigation,
    );
  };

  renderPaymentMethod = (paymentMethod: Object = null) => {
    /**
     * renderPaymentMethod()
     * Chooses which payment method details should be rendered,
     * based on payment method.
     * @param <object> paymentMethod | Payment Method object.
     * @returns <jsx>
     */

    /*     let billCheckBox = (
      <View style={layout.checkboxContainer}>
        <Field
          name="requiresInvoice"
          component={({ input, meta, ...inputProps }) => (
            <CheckBox
              {...inputProps}
              onPress={() => input.onChange(!input.value)}
              checked={input.value}
            />
          )}
          onChange={this.changeInvoice}
          checkedIcon={<Icon name={'md-checkbox'} style={layout.checkedIcon} />}
          uncheckedIcon={
            <Icon name={'md-square-outline'} style={layout.uncheckedIcon} />
          }
          style={{
            height: 25,
            width: 250,
            borderColor: 'gray',
            borderWidth: 1,
            marginLeft: 30,
          }}
        />
        <Text
          style={[
            layout.default,
            this.state.checked
              ? { color: colors.colorMain300 }
              : { color: colors.colorGray400 },
          ]}
        >
          Requiero Factura
        </Text>
      </View>
    ); */
    //add_payment_info
    //*Se ejecuta cuando se actualiza un método de pago
    //*Agregar parámetro payment_type, con el nombre del método de pago

    if (!paymentMethod) {
      return null;
    }
    let paymentMethodComponent;

    // Select corresponding view.
    switch (paymentMethod.slug) {
      default:
        paymentMethodComponent = null;
        break;

      // OXXO
      case PAYMENT_METHOD_OXXO.slug:
        paymentMethodComponent = (
          <View style={layout.cardDescription}>
            <CRRowLeft>
              <Image source={oxxo} style={layout.imgOXXO} />
            </CRRowLeft>
            <CRRowBoth addedStyle={layout.cardContainer}>
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark100}
              >
                OXXO
              </CRText>
            </CRRowBoth>
          </View>
        );
        break;

      // PAYPAL
      case PAYMENT_METHOD_PAYPAL.slug:
        paymentMethodComponent = (
          <View style={layout.cardDescription}>
            <CRRowLeft>
              <Image source={payPalp} style={layout.imgPayPal} />
            </CRRowLeft>
            <CRRowBoth addedStyle={layout.cardContainer}>
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark100}
              >
                PayPal
              </CRText>
            </CRRowBoth>
          </View>
        );
        break;

      // STRIPE
      case PAYMENT_METHOD_STRIPE.slug:
        paymentMethodComponent = (
          <View>
            <View style={layout.cardDescription}>
              <CRRowLeft>
                {paymentMethod.data.card.brand === 'Visa' ? (
                  <Image source={cardV} style={{ marginLeft: 10 }} />
                ) : (
                  <Image source={cardMC} style={{ marginLeft: 10 }} />
                )}
              </CRRowLeft>
              <CRRowBoth addedStyle={layout.cardContainer}>
                <CRText
                  variant={typography.paragraph}
                  color={colors.colorDark100}
                >
                  •••• •••• •••• {paymentMethod.data.card.last4}
                </CRText>
              </CRRowBoth>
              <CRRowLeft>
                <CRText
                  variant={typography.caption}
                  color={colors.colorDark100}
                >{`${paymentMethod.data.card.expMonth}/${paymentMethod.data.card.expYear}`}</CRText>
              </CRRowLeft>
              {/* {billCheckBox} */}
            </View>
            <View style={layout.checkboxContainer}>
              <CheckBox
                checkedIcon={
                  <View style={layout.selectedCheck}>
                    <CRIcon
                      name={'check'}
                      size={12}
                      color={colors.colorWhite}
                    />
                  </View>
                }
                uncheckedIcon={<View style={layout.unselectedCheck}></View>}
                checked={this.state.checkedSave}
                onPress={() =>
                  this.setState({
                    checkedSave: !this.state.checkedSave,
                  })
                }
              />
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark100}
              >
                Guardar Tarjeta
              </CRText>
            </View>
          </View>
        );
        break;

      // SAVED CARD
      case PAYMENT_METHOD_SAVED_CARD.slug:
        paymentMethodComponent = (
          <View style={layout.cardDescription}>
            {paymentMethod.data.card.brand === 'visa' ? (
              <Image source={cardV} style={{ marginLeft: 10 }} />
            ) : (
              <Image source={cardMC} style={{ marginLeft: 10 }} />
            )}
            <CRRowLeft>
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark100}
              >
                •••• •••• •••• {paymentMethod.data.card.last4}
              </CRText>
            </CRRowLeft>
            <CRRowBoth addedStyle={layout.cardContainer}>
              <CRText
                variant={typography.caption}
                color={colors.colorDark100}
              >{`${paymentMethod.data.card.exp_month}/${paymentMethod.data.card.exp_year}`}</CRText>
            </CRRowBoth>
          </View>
        );
        break;

      // BANK TRANSFER
      case PAYMENT_METHOD_BANK_TRANSFER.slug:
        paymentMethodComponent = (
          <View style={layout.cardDescription}>
            <CRRowLeft>
              <CRIcon name="transfer" size={18} color={colors.colorDark200} />
            </CRRowLeft>
            <CRRowBoth addedStyle={layout.cardContainer}>
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark100}
              >
                Transferencia bancaria
              </CRText>
            </CRRowBoth>
          </View>
        );
        break;
    }

    // Return JSX
    return paymentMethodComponent;
  };

  changeShippingAddress = (address: Object) => {
    /**
     * changeShippingAddress()
     * @param {object} address || Address object.
     */
    new Promise((resolve, reject): Object => {
      const formattedAddress: Object = getAddress(address);

      // Validate Address
      //if (!ShoppingCartManager.validateAddress(address)) {
      // Ask user to review her address details.
      // this.verifySippingAddress(formattedAddress)
      //   .then((response) => resolve(formattedAddress))
      //   .catch((error) => reject(error));
      //}
      resolve(formattedAddress);
    })
      .then((response) => {
        try {
          globalThis.ShoppingCartManager.setShippingAddress(response);

          // Update redux form values
          this.props.change('shippingAddress', response);
          this.props.change('selectedShippingAddress', response.uuid);

          // Sync shopping cart
          this.props.fetchShoppingCart();

          // Return to Checkout view
          // this.props.navigation.navigate('Checkout');

          globalThis.ReactApplication.ActiveScreen(
            'Checkout',
            {},
            this.props.navigation,
          );
        } catch (e) {
          const errorPayment = {
            email: this.props.user.profile.email,
            screen: 'Checkout',
            action: 'Try payment',
            func: 'changeShippingAddress()',
            error: e,
            dataGlobalThis: globalThis,
          };
          Sentry.captureException(errorPayment);
          globalThis.ReactApplication.SentryCatch(
            'Checkout',
            'changeShippingAddress()',
            'Error in changeShippingAddress',
            this.props.user.profile.email,
            errorPayment,
          );
        }
      })
      .catch((e) => {
        const errorPayment = {
          email: this.props.user.profile.email,
          screen: 'Checkout',
          action: 'Try payment',
          func: 'changeShippingAddress()',
          error: e,
          dataGlobalThis: globalThis,
        };
        Sentry.captureException(errorPayment);
        globalThis.ReactApplication.SentryCatch(
          'Checkout',
          'changeShippingAddress()',
          'Error in changeShippingAddress',
          this.props.user.profile.email,
          errorPayment,
        );
      });
  };

  renderPriceDetails = (): Object => {
    const {
      coupon,
      cartPrice,
      shippingPrice,
      totalPrice,
      totalPriceWithDiscount,
      cartPriceWithDiscount,
    } = this.props;
    let couponRow = null;
    let shippingRow = (
      <View style={layout.subtotalOrder}>
        <CRText variant={typography.subtitle3} color={colors.colorDark100}>
          Envío
        </CRText>
        {shippingPrice === 0 ? (
          <CRText variant={typography.subtitle3} color={colors.colorDark300}>
            0.00 MXN
          </CRText>
        ) : (
          <CRText variant={typography.subtitle3} color={colors.colorDark300}>
            {formatNumberToPrice(shippingPrice)} MXN
          </CRText>
        )}
      </View>
    );

    if (coupon.valid) {
      switch (coupon.type) {
        case COUPON_TYPES.FREE_SHIPPING:
        case COUPON_TYPES.FREE_SHIPPING_AND_PERCENTUAL:
          couponRow = (
            <View style={layout.subtotalOrder}>
              <CRText
                variant={typography.subtitle3}
                color={colors.colorDark100}
              >
                Envío gratis
              </CRText>
              <CRText
                variant={typography.subtitle3}
                color={colors.colorDark300}
              >
                0.00 MXN
              </CRText>
            </View>
          );
          break;
        case COUPON_TYPES.QUANTITY:
          couponRow = (
            <React.Fragment>
              {shippingRow}
              <View style={layout.subtotalOrder}>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorTurquiose300}
                >
                  Cupón
                </CRText>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorTurquiose300}
                >
                  -{coupon.discount}.00 MXN
                </CRText>
              </View>
            </React.Fragment>
          );
          break;
        case COUPON_TYPES.PERCENTUAL:
          couponRow = (
            <React.Fragment>
              {shippingRow}
              <View style={layout.subtotalOrder}>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorTurquiose300}
                >
                  Cupón
                </CRText>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorTurquiose300}
                >
                  -{coupon.discount * 100}%
                </CRText>
              </View>
            </React.Fragment>
          );
          break;
        default:
          couponRow = shippingRow;
      }
    }

    return (
      <CRRowBoth addeStyle={{ marginTop: 20 }}>
        <View style={layout.subtotalOrder}>
          <CRText variant={typography.subtitle3} color={colors.colorDark100}>
            Subtotal
          </CRText>
          <CRText variant={typography.subtitle3} color={colors.colorDark300}>
            {formatNumberToPrice(cartPrice)}MXN
          </CRText>
        </View>

        {shippingRow}
        {couponRow}

        <View style={layout.subtotalOrder}>
          <CRText variant={typography.subtitle3} color={colors.colorDark100}>
            Total
            <CRText variant={typography.caption} color={colors.colorDark100}>
              {' '}
              (IVA Incluido)
            </CRText>
          </CRText>
          <CRText variant={typography.subtitle3} color={colors.colorMain300}>
            {formatNumberToPrice(totalPrice)} MXN
          </CRText>
        </View>
      </CRRowBoth>
    );
  };

  renderPriceDetailsWithDiscount = (): Object => {
    const {
      coupon,
      cartPrice,
      shippingPrice,
      totalPrice,
      totalPriceWithDiscount,
      cartPriceWithDiscount,
    } = this.props;
    let couponRow = null;
    let shippingRow = (
      <View style={layout.subtotalOrder}>
        <CRText variant={typography.subtitle3} color={colors.colorDark100}>
          Envío
        </CRText>
        {shippingPrice === 0 ? (
          <CRText variant={typography.subtitle3} color={colors.colorDark300}>
            0.00 MXN
          </CRText>
        ) : (
          <CRText variant={typography.subtitle3} color={colors.colorDark300}>
            {formatNumberToPrice(shippingPrice)} MXN
          </CRText>
        )}
      </View>
    );

    if (coupon.valid) {
      switch (coupon.type) {
        case COUPON_TYPES.FREE_SHIPPING:
        case COUPON_TYPES.FREE_SHIPPING_AND_PERCENTUAL:
          couponRow = (
            <React.Fragment>
              <View style={layout.subtotalOrder}>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorDark100}
                >
                  Envío gratis
                </CRText>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorDark300}
                >
                  0.00MXN
                </CRText>
              </View>
            </React.Fragment>
          );
          break;
        case COUPON_TYPES.QUANTITY:
          couponRow = (
            <React.Fragment>
              {shippingRow}
              <View style={layout.subtotalOrder}>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorTurquiose300}
                >
                  Cupón
                </CRText>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorTurquiose300}
                >
                  -{coupon.discount}.00 MXN
                </CRText>
              </View>
            </React.Fragment>
          );
          break;
        case COUPON_TYPES.PERCENTUAL:
          couponRow = (
            <React.Fragment>
              {shippingRow}

              <View style={layout.subtotalOrder}>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorTurquiose300}
                >
                  Cupón
                </CRText>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorTurquiose300}
                >
                  -{coupon.discount * 100}%
                </CRText>
              </View>
            </React.Fragment>
          );
          break;
        default:
          couponRow = shippingRow;
      }
    }

    return (
      <CRRowBoth addeStyle={{ marginTop: 20 }}>
        <View style={layout.subtotalOrder}>
          <CRText variant={typography.subtitle3} color={colors.colorDark100}>
            Subtotal
            {/*             <CRText variant={typography.caption} color={'#1A1F71'}>
              {' '}
              (Sólo con <Image source={visaIMG} style={layout.visa} />)
            </CRText> */}
          </CRText>
          <CRText variant={typography.subtitle3} color={colors.colorDark300}>
            {formatNumberToPrice(cartPriceWithDiscount)} MXN
          </CRText>
        </View>
        {shippingRow}
        {couponRow}
        <View style={layout.subtotalOrder}>
          <CRText variant={typography.subtitle3} color={colors.colorDark100}>
            Total
            <CRText variant={typography.caption} color={colors.colorDark100}>
              {' '}
              (IVA Incluido)
            </CRText>
          </CRText>
          <CRText variant={typography.subtitle3} color={colors.colorMain300}>
            {formatNumberToPrice(totalPriceWithDiscount)} MXN
          </CRText>
        </View>
      </CRRowBoth>
    );
  };

  updateEmail = (event: Object, email: string) => {
    globalThis.ShoppingCartManager.setClientData({
      ...globalThis.ShoppingCartManager.setClientData(),
      email,
    });
  };

  updateGiftReceiverName = (event: Object, name: string) => {
    this.updateGiftReceiverData({ name });
  };
  updateGiftReceiverPhone = (event: Object, phone: string) => {
    this.updateGiftReceiverData({ phone });
  };
  updateGiftReceiverNote = (event: Object, note: string) => {
    this.updateGiftReceiverData({ note });
  };
  updateGiftReceiverData = (data: Object) => {
    globalThis.ShoppingCartManager.setGiftReceiverData({
      ...(globalThis.ShoppingCartManager.getGiftReceiverData()
        ? globalThis.ShoppingCartManager.getGiftReceiverData()
        : {}),
      ...data,
    });
  };
  toggleOrderAsGift = (event: Object) => {
    const { checked } = this.state;
    const { giftReceiverValues } = this.props;
    const nextState = !checked;

    //
    if (!nextState) {
      globalThis.ShoppingCartManager.setGiftReceiverData(null);
    } else {
      globalThis.ShoppingCartManager.setGiftReceiverData({
        ...(giftReceiverValues ? giftReceiverValues : {}),
      });
    }

    // Update UI state
    this.setState({
      checked: nextState,
    });
  };

  /**
   * renderGiftForm()
   * Renders a form with fields for gift-receiver details.
   * @returns {jsx}
   */
  renderGiftForm = () => (
    <React.Fragment>
      <CRText variant={typography.bold} color={colors.colorDark300}>
        ¿Para quién es el regalo?
      </CRText>
      <View style={layout.formWrapper}>
        <Field
          component={renderField}
          maxLength={150}
          id="giftReceiverName"
          name="giftReceiver.name"
          className="giftReceiverName"
          placeholder="Nombre completo"
          onChange={this.updateGiftReceiverName}
          validate={[required]}
        />
        <Field
          component={renderField}
          validate={[required, phoneNumber, number]}
          //keyboardType="number-pad"
          id="giftReceiverPhone"
          name="giftReceiver.phone"
          className="giftReceiverPhone"
          placeholder="Teléfono*"
          onChange={this.updateGiftReceiverPhone}
        />
        <Field
          maxLength={200}
          name="giftReceiver.note"
          className="giftReceiverNote"
          onChange={this.updateGiftReceiverNote}
          component={renderField}
          placeholder="Mensaje o dedicatoria"
        />
      </View>
    </React.Fragment>
  );

  render() {
    this.testCheck();
    /* console.log("paymentMethodSaved: ", this.state.paymentMethodSaved)
    console.log("??? ", this.state.paymentMethodSaved.payment_method_id);

    if(this.state?.paymentMethodSaved?.payment_method_id==='Visa'){
      console.log("***********************ES VISA");
    } */

    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 100;
    const SPACER_SIZE_ANDROID = 100;
    const {
      coupon,
      totalPrice,
      totalPriceWithDiscount,
      cartPriceWithDiscount,
      shippingAddress,
      paymentMethod,
      paymentMethodsList,
      unavailableOrders,
      formErrors,
      submitFailed,
      change,
      fetchShoppingCart,
      handleSubmit,
      renderShippingMethods,
      renderShippingDate,
      changeShippingDate,
      changeShippingMethod,
      changeShippingSchedule,
      orderHasShippingErrors,
      orderHasPaymentErrors,
      orders,
      gift,
      user,
    } = this.props;
    // console.log('paymentMethod: ', paymentMethod);
    const formErrorsList: Array<Object> = Object.keys(formErrors).map(
      (key) => ({
        name: key,
        message: formErrors[key],
      }),
    );

    // Payment processing.
    const { loadingPayment } = this.state;
    if (loadingPayment) {
      return <CRLoaderScreen loaderMessage="Estamos procesando tu pago" />;
    }

    // Return JSX
    return (
      <View style={layout.checkoutWrapper}>
        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled
        >
          <View style={layout.checkoutWrapperScrollView}>
            <CRTitle title="Checkout" />
            <View style={layout.menuProfile}>
              {/* UNAVAILABLE ORDERS */}
              {unavailableOrders.length > 0 && (
                <CartSummary
                  onPress={() => {
                    //this.props.navigation.navigate('ProductNotAvailable');
                    globalThis.ReactApplication.ActiveScreen(
                      'ProductNotAvailable',
                      {},
                      this.props.navigation,
                    );
                  }}
                />
              )}
              {/* END: UNAVAILABLE ORDERS */}

              {/* SHIPPING DETAILS */}
              <CRRowBoth>
                <CRCard
                  size="full"
                  onPress={() => {
                    this.props
                      .renderAddressList([])
                      .then((e) => {
                        this.changeShippingAddress(e);
                      })
                      .catch((e) => {
                        const errorPayment = {
                          email: this.props.user.profile.email,
                          screen: 'Checkout',
                          action: 'Try payment',
                          func: 'render()',
                          error: e,
                          dataGlobalThis: globalThis,
                        };
                        Sentry.captureException(errorPayment);
                        globalThis.ReactApplication.SentryCatch(
                          'Checkout',
                          'render()',
                          'Error in render',
                          this.props.user.profile.email,
                          errorPayment,
                        );
                      });
                  }}
                >
                  <View style={layout.headerCardWrapper}>
                    <CRRowBoth addedStyle={layout.headerCard}>
                      <View style={layout.headerCardText}>
                        {!orderHasShippingErrors ? (
                          <View style={layout.checkWrapperActive}>
                            <CRIcon
                              name="check"
                              size={10}
                              color={colors.colorWhite}
                            />
                          </View>
                        ) : (
                          <View style={layout.checkWrapper}>
                            <CRIcon
                              name="warning"
                              size={25}
                              color={colors.colorYellow400}
                            />
                          </View>
                        )}
                        <CRText variant={typography.subtitle3}>
                          Datos de envío
                        </CRText>
                      </View>
                      <View style={layout.iconItemArrow}>
                        <CRIcon
                          name={'angle--right'}
                          size={16}
                          color={colors.colorDark100}
                        />
                      </View>
                    </CRRowBoth>
                  </View>

                  {!orderHasShippingErrors && (
                    <CRRowBoth addedStyle={layout.cardContainer}>
                      {shippingAddress.address_name ? (
                        <CRText
                          variant={typography.bold}
                          color={colors.colorDark300}
                        >
                          {shippingAddress.address_name}
                        </CRText>
                      ) : null}

                      {shippingAddress.client_name ? (
                        <CRText
                          variant={typography.bold}
                          color={colors.colorDark300}
                        >
                          Recibe: {shippingAddress.client_name}
                        </CRText>
                      ) : null}

                      {shippingAddress.phone ? (
                        <CRText
                          variant={typography.paragraph}
                          color={colors.colorDark300}
                        >
                          Telefono: {shippingAddress.phone}
                        </CRText>
                      ) : null}

                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark100}
                      >
                        {`${shippingAddress.street_address} ${shippingAddress.num_ext}`}
                        {`, ${
                          shippingAddress.num_int
                            ? `${shippingAddress.num_int}. `
                            : ''
                        }CP. ${shippingAddress.zip_code}.`}
                      </CRText>
                    </CRRowBoth>
                  )}

                  {submitFailed && orderHasShippingErrors && (
                    <CRRowFull addedStyle={layout.cardContainerWarning}>
                      {Object.keys(orderHasShippingErrors).map((key) => (
                        <CRText
                          variant={typography.paragraph}
                          color={colors.colorYellow400}
                        >
                          {JSON.stringify(orderHasShippingErrors[key])}
                        </CRText>
                      ))}
                    </CRRowFull>
                  )}
                </CRCard>
              </CRRowBoth>
              {/* END: SHIPPING DETAILS */}

              {/* GUEST EMAIL */}
              {user.isGuest === true && (
                <CRRowBoth addedStyle={layout.couponWrapper}>
                  <CRCard size="full">
                    <View style={layout.headerCardWrapper}>
                      <CRRowBoth addedStyle={layout.headerCard}>
                        <View style={layout.headerCardText}>
                          <CRText variant={typography.subtitle3}>
                            Ingresa tu email
                          </CRText>
                        </View>
                      </CRRowBoth>
                    </View>
                    <View style={layout.formWrapper}>
                      <Field
                        type="text"
                        name="clientData.email"
                        component={renderField}
                        keyboardType="default"
                        validate={[required, emailValidator]}
                        showErrorsOnTouch={true}
                        placeholder="Correo electrónico"
                        onChange={this.updateEmail}
                      />
                    </View>
                  </CRCard>
                </CRRowBoth>
              )}
              {/* END: GUEST EMAIL */}

              {/* GIFT INFO */}
              <CRRowBoth>
                <CRCard size="full">
                  <View style={layout.headerCardWrapper}>
                    <View style={layout.containerCheckbox}>
                      <CheckBox
                        checkedIcon={
                          <Icon
                            name={'md-checkbox'}
                            style={layout.checkedIcon}
                          />
                        }
                        uncheckedIcon={
                          <Icon
                            name={'md-square-outline'}
                            style={layout.uncheckedIcon}
                          />
                        }
                        checked={this.state.checked}
                        onPress={this.toggleOrderAsGift}
                      />
                      <View style={layout.checkTitle}>
                        <CRText
                          variant={typography.paragraph}
                          color={colors.colorDark200}
                        >
                          Mi compra es un regalo
                        </CRText>
                      </View>
                    </View>
                  </View>
                  <View style={layout.message}>
                    {!this.state.checked ? (
                      <CRText
                        variant={typography.caption}
                        color={colors.colorGray300}
                      >
                        ¿Quieres sorprender a alguien? {'\n'}
                        Deja un mensaje personalizado y los datos de quien lo
                        recibirá.
                      </CRText>
                    ) : (
                      this.renderGiftForm()
                      //console.log("aaaa")
                    )}
                  </View>
                </CRCard>
              </CRRowBoth>
              {/* END: GIFT INFO */}

              {/* PAYMENT DETAILS */}
              <CRRowBoth>
                <CRCard
                  size="full"
                  onPress={() => {
                    // this.props.navigation.navigate('SelectPayment', {
                    //   paymentMethods: paymentMethodsList,
                    //   selectedPaymentMethod: paymentMethod,
                    //   onChangePaymentMethod: this.changePaymentMethod,
                    // });

                    globalThis.ReactApplication.ActiveScreen(
                      'SelectPayment',
                      {
                        paymentMethods: paymentMethodsList,
                        selectedPaymentMethod: paymentMethod,
                        onChangePaymentMethod: this.changePaymentMethod,
                      },
                      this.props.navigation,
                    );
                  }}
                >
                  <View style={layout.headerCardWrapper}>
                    <CRRowBoth addedStyle={layout.headerCard}>
                      <View style={layout.headerCardText}>
                        {!orderHasPaymentErrors ? (
                          <View style={layout.checkWrapperActive}>
                            <CRIcon
                              name="check"
                              size={10}
                              color={colors.colorWhite}
                            />
                          </View>
                        ) : (
                          <View style={layout.checkWrapper}>
                            <CRIcon
                              name="warning"
                              size={25}
                              color={colors.colorYellow400}
                            />
                          </View>
                        )}
                        <CRText variant={typography.subtitle3}>
                          Método de pago
                        </CRText>
                      </View>
                      <View style={layout.iconItemArrow}>
                        <CRIcon
                          name={'angle--right'}
                          size={16}
                          color={colors.colorDark100}
                        />
                      </View>
                    </CRRowBoth>
                  </View>
                  {paymentMethod ? (
                    this.renderPaymentMethod(paymentMethod)
                  ) : (
                    <CRRowBoth addedStyle={layout.cardContainer}>
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark100}
                      >
                        Elige un método de pago
                      </CRText>
                    </CRRowBoth>
                  )}

                  {submitFailed && orderHasPaymentErrors && (
                    <CRRowFull addedStyle={layout.cardContainerWarning}>
                      {Object.keys(orderHasPaymentErrors).map((key) => (
                        <CRText
                          variant={typography.paragraph}
                          color={colors.colorYellow400}
                        >
                          {JSON.stringify(orderHasPaymentErrors[key].name)}
                        </CRText>
                      ))}
                    </CRRowFull>
                  )}
                </CRCard>
              </CRRowBoth>
              {/* END: PAYMENT DETAILS */}
              {/* COUPON DETAILS */}
              <CRRowBoth>
                <CRCard size="full">
                  <View style={layout.headerCardWrapper}>
                    <CRRowBoth addedStyle={layout.headerCard}>
                      <View style={layout.headerCardText}>
                        <CRText variant={typography.subtitle3}>
                          ¿Tienes un cupón?
                        </CRText>
                      </View>
                    </CRRowBoth>
                  </View>
                  <CRRowBoth addedStyle={layout.couponWrapper}>
                    <View style={layout.couponInputWrapper}>
                      <Field
                        type="text"
                        name="coupon.code"
                        component={CouponField}
                        keyboardType="default"
                        onChange={this.onCouponChange}
                        placeholder="Ingresa tu cupón"
                        style={layout.couponInput}
                      />
                      {this.state.couponStatus === 'rejected' && (
                        <View style={layout.couponBadge}>
                          <CRIcon
                            name="warning"
                            size={25}
                            color={colors.colorRed300}
                          />
                        </View>
                      )}
                      {coupon.valid && (
                        <View style={layout.checkWrapperActive}>
                          <CRIcon
                            name="check"
                            size={10}
                            color={colors.colorWhite}
                          />
                        </View>
                      )}
                    </View>
                    <View style={layout.couponBtn}>
                      <TouchableOpacity
                        onPress={(e) => this.validateCoupon(coupon.code)}
                      >
                        <CRText
                          variant={typography.subtitle3}
                          color={colors.colorDark300}
                          align="center"
                        >
                          Validar
                        </CRText>
                      </TouchableOpacity>
                    </View>
                  </CRRowBoth>
                </CRCard>
              </CRRowBoth>
              {/* END: OUPON DETAILS */}

              {/* ORDERS */}
              {orders.length > 0 && (
                <CRRowBoth addedStyle={layout.productList}>
                  <FieldArray
                    name="orders"
                    component={CartOrder}
                    change={change}
                    fetchShoppingCart={fetchShoppingCart}
                    onRenderShippingDate={renderShippingDate}
                    onRenderShippingMethods={renderShippingMethods}
                    onChangeShippingDate={changeShippingDate}
                    onChangeShippingMethod={changeShippingMethod}
                    onChangeShippingSchedule={changeShippingSchedule}
                  />
                </CRRowBoth>
              )}

              {/* END: ORDERS */}
            </View>

            {/* SUBTOTAL */}
            {paymentMethod === null
              ? this.renderPriceDetails()
              : paymentMethod.payment_method_id === 'Visa'
              ? this.renderPriceDetailsWithDiscount()
              : paymentMethod.payment_method_id === 'visa'
              ? this.renderPriceDetailsWithDiscount()
              : this.renderPriceDetails()}
            {/*this.renderPriceDetails()}
          {this.renderPriceDetailsWithDiscount()*/}
            {/* END: SUBTOTAL */}

            {/* PAY BUTTON */}
          </View>
        </KeyboardAwareScrollView>
        <View style={layout.viewBottomCheckout}>
          <CRBottomButton
            onPress={handleSubmit(this.completeOrder)}
            attitude={
              !this.state.checked
                ? ''
                : formErrors && !Object.keys(formErrors).length
                ? ''
                : 'disabled'
              //formErrors && !Object.keys(formErrors).length ? '' : 'disabled'
            }
            customContainerBtn={true}
          >
            {paymentMethod === null ? (
              <CRText
                variant={typography.subtitle3}
                color={colors.colorWhite}
                align="center"
              >
                Pagar | {formatNumberToPrice(totalPrice)} MXN
              </CRText>
            ) : paymentMethod.payment_method_id === 'Visa' ? (
              <CRText
                variant={typography.subtitle3}
                color={colors.colorWhite}
                align="center"
              >
                Pagar | {formatNumberToPrice(totalPriceWithDiscount)} MXN
              </CRText>
            ) : paymentMethod.payment_method_id === 'visa' ? (
              <CRText
                variant={typography.subtitle3}
                color={colors.colorWhite}
                align="center"
              >
                Pagar | {formatNumberToPrice(totalPriceWithDiscount)} MXN
              </CRText>
            ) : (
              <CRText
                variant={typography.subtitle3}
                color={colors.colorWhite}
                align="center"
              >
                Pagar | {formatNumberToPrice(totalPrice)} MXN
              </CRText>
            )}
          </CRBottomButton>
        </View>
        {/* END: PAY BUTTON */}
      </View>
    );
  }
}

// Wrap component within reduxForm
Checkout = withShoppingCartUI(Checkout, {});

const selector = formValueSelector('shoppingOrder_form');

// Pass Redux state and actions to component
function mapStateToProps(state: Object, props: Object): Object {
  // Define or retrieve default form values
  const orders: Array<Object> = globalThis.ShoppingCartManager.getOrders();
  const shippingAddress: Object = globalThis.ShoppingCartManager.getShippingAddress();
  const paymentMethod: Object = globalThis.ShoppingCartManager.getPaymentMethod();
  const requiresInvoice: boolean = globalThis.ShoppingCartManager.getRequiresInvoice();
  const clientDataEmail: String = globalThis.ShoppingCartManager.getClientData();
  const giftReceiverValues = selector(state, 'giftReceiver');

  return {
    orders,
    paymentMethod,
    shippingAddress,
    requiresInvoice,
    clientDataEmail,
    giftReceiverValues,
    user: state.user,
    addressList: globalThis.ShoppingCartManager.getAddresses(),
    paymentMethodsList: globalThis.ShoppingCartManager.getPaymentMethods(),
    totalPrice: globalThis.ShoppingCartManager.getCartGrandTotal(),
    totalPriceWithDiscount: globalThis.ShoppingCartManager.getCartGrandTotalWithDiscount(),
    cartPrice: globalThis.ShoppingCartManager.getCartProductsTotal(),
    cartPriceWithDiscount: globalThis.ShoppingCartManager.getCartProductsTotalWithDiscount(),
    productsCount: globalThis.ShoppingCartManager.getCartProductsCount(),
    unavailableOrders: globalThis.ShoppingCartManager.getUnavailableOrders(),
    formErrors: getFormSyncErrors(SHOPPING_CART_FORM_CONFIG.formName)(state),
  };
}

function mapDispatchToProps(dispatch: Function): Object {
  return {
    dispatch,
    ...bindActionCreators(
      {
        sendOrder: shoppingCartActions.sendOrder,
        validateCoupon: shoppingCartActions.validateCoupon,
        removeFromCart: shoppingCartActions.removeFromCart,
        updateCartProduct: shoppingCartActions.updateCartProduct,
        fetchShoppingCart: shoppingCartActions.fetchShoppingCart,
        sendPayPalOrder: shoppingCartActions.sendPayPalOrder,
      },
      dispatch,
    ),
  };
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
