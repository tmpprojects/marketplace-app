import { StyleSheet, Platform, Dimensions } from 'react-native';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { buttons } from '../../assets';
const { height: HEIGHT } = Dimensions.get('window');

const isAndroid = Platform.OS === 'android';
const alto = Dimensions.get('window').height;

export const layout = StyleSheet.create({
  //Titulo y flecha de regreso
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: 20,
  },
  backArrowBtn: {
    marginHorizontal: 15,
    flex: 2,
  },
  title: {
    flex: 9,
  },
  categoriesList: {
    marginHorizontal: 8,
  },
  arrowBtn: {
    top: 20,
    left: 15,
  },
  iconArrow: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    transform: [{ rotate: '180deg' }],
  },

  //Gift
  unselectedCheck: {
    width: 20,
    height: 20,
    borderWidth: 2,
    borderColor: colors.colorGray400,
    borderRadius: 2,
  },
  selectedCheck: {
    width: 20,
    height: 20,
    borderWidth: 2,
    borderColor: colors.colorMain300,
    backgroundColor: colors.colorMain300,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },

  //Productos no Disponibles
  containerItemMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 20,
    padding: 20,
    width: '90%',
    height: 65,
    backgroundColor: '#FAF7F5',
    shadowColor: colors.colorGray400,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  //Elementos Cards
  iconItem: {
    marginRight: 22,
  },
  itemMenu: {
    ...typography.txtRegular,
    color: colors.colorDark400,
    flex: 1,
    alignSelf: 'flex-start',
  },
  iconItemArrow: {
    marginRight: 20,
  },

  //Checkbox
  containerCheckbox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkedIcon: {
    color: colors.colorMain300,
    fontSize: 25,
  },
  uncheckedIcon: {
    color: colors.colorGray400,
    fontSize: 25,
  },
  message: {
    flex: 1,
    padding: 12,
  },
  checkGift: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 8,
  },

  //Cards
  cardList: {
    marginTop: 20,
  },
  bodyCard: {
    backgroundColor: colors.colorWhite,
    shadowColor: colors.colorGray400,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  btnsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardContainer: {
    marginVertical: 20,
  },
  cardContainerWarning: {
    backgroundColor: colors.colorYellow200,
    paddingHorizontal: 10,
    marginVertical: 0,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: colors.colorYellow300,
  },
  cardDescription: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  cardPersonName: {
    ...typography.txtSemiBold,
    fontSize: 16,
  },
  cardPersonAddress: {
    ...typography.txtLight,
    marginTop: 8,
  },
  creditCardText: {
    ...typography.txtRegular,
    marginLeft: 15,
  },

  //Warning
  warning: {
    backgroundColor: colors.colorYellow200,
    padding: 10,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginVertical: 0,
  },
  warningText: {
    flex: 5,
  },
  warningItem: {
    flex: 1,
    alignItems: 'center',
  },

  //Card Productos

  productList: {
    marginTop: 20,
  },
  containerStoreCard: {
    backgroundColor: colors.colorWhite,
    padding: 15,
    marginTop: 50,
  },
  store: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  storeName: {
    ...typography.txtSemiBold,
    marginLeft: 20,
  },
  thumbnail: {
    width: 30,
    height: 30,
    borderWidth: 1,
    borderColor: colors.colorGray200,
  },
  countProductContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    flex: 3,
  },
  countProduct: {
    ...typography.txtLight,
    color: colors.colorGray400,
  },
  productCartContainer: {
    borderTopColor: colors.colorGray200,
    borderTopWidth: 1,
    flexDirection: 'row',
    paddingVertical: 20,
  },
  detail: {
    flex: 2,
    marginLeft: 20,
  },
  productName: {
    ...typography.txtSemiBold,
    fontSize: 16,
  },
  attribute: {
    ...typography.txtLight,
    color: colors.colorGray400,
    fontSize: 14,
  },
  subtotalProduct: {
    ...typography.txtLight,
    fontSize: 14,
  },
  subtotalOrder: {
    marginTop: -10,
    paddingVertical: 10,
    paddingHorizontal: 30,
    backgroundColor: '#FAF7F5',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  subtotal: {
    ...typography.txtSemiBold,
    color: colors.colorGray400,
  },
  subtotalTitle: {
    ...typography.txtSemiBold,
    color: colors.colorGray400,
  },

  //Btn PAgar
  btnAddToCart: {
    ...buttons.squareCart,
    marginBottom: 0,
    width: '100%',
    height: 80,
  },
  txtAddToCart: {
    textAlign: 'center',
    ...typography.txtRegular,
    color: colors.colorWhite,
    fontSize: 20,
  },

  //Modales
  modalView: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    height: '100%',
  },
  modalViewTO: {
    height: '100%',
  },
  containerModal: {
    backgroundColor: '#FAF7F5',
    width: '100%',
    height: '60%',
    position: 'absolute',
    bottom: 0,
    padding: 20,
  },
  containerModalFull: {
    backgroundColor: '#FAF7F5',
    width: '100%',
    height: '100%',
    position: 'absolute',
    bottom: 0,
    padding: 20,
    paddingTop: isAndroid ? 5 : 20,
  },
  titleModal: {
    marginBottom: 20,
  },
  itemsModal: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnAceptar: {
    ...typography.txtRegular,
    fontSize: 18,
    color: colors.colorMain300,
    marginTop: 30,
  },

  //Cards Modal Envio
  addressesList: {
    padding: 15,
    marginVertical: 0,
  },
  addressCard: {
    backgroundColor: colors.colorWhite,
    shadowColor: colors.colorGray400,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    padding: 15,
  },
  checkoutWrapper: {
    //flex: 1,
    backgroundColor: '#FAF7F5',

    height: HEIGHT - 90,
  },
  checkoutWrapperScrollView: {
    paddingBottom: 150,
  },
  //CARD
  card: {
    backgroundColor: colors.colorWhite,
    shadowColor: colors.colorGray400,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    padding: 15,
  },
  formWrapper: {
    marginTop: 10,
  },
  checkTitle: {
    marginLeft: -10,
  },
  headerCardWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: colors.colorGray200,
  },
  headerCard: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 20,
  },
  headerCardText: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  checkWrapper: {
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 8,
    height: 25,
    width: 25,
    paddingTop: 1,
    backgroundColor: colors.colorYellow200,
  },
  checkWrapperActive: {
    backgroundColor: colors.colorTurquiose300,
    borderColor: colors.colorTurquiose300,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    marginRight: 8,
    height: 25,
    width: 25,
  },
  cardContainerModal: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  cardNumber: {
    ...typography.txtRegular,
    marginLeft: 15,
  },

  owner: {
    ...typography.txtSemiRegular,
    marginTop: 15,
    marginLeft: 32,
    fontSize: 16,
  },
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 0,
  },
  default: {
    ...typography.txtLight,
    paddingBottom: 2,
  },
  addressSelected: {
    ...typography.txtLight,
    color: colors.colorMain300,
    paddingBottom: 2,
  },

  //Coupons
  couponWrapper: {
    marginTop: 8,
    marginHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  fieldWrapper: {
    flex: 8,
    marginVertical: 5,
  },
  field: {
    borderColor: colors.colorGray300,
    borderWidth: 1,
    borderRadius: 3,
    paddingHorizontal: 10,
    color: colors.colorDark300,
    height: 40,
    marginBottom: 5,
  },
  couponInputWrapper: {
    borderColor: colors.colorGray300,
    borderWidth: 1,
    borderRadius: 3,
    flex: 8,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
  },
  couponInput: {
    paddingHorizontal: 10,
    borderRadius: 3,
    color: colors.colorDark300,
    // paddingVertical: 15,
    height: 40,
    flex: 8,
  },

  couponBadge: {
    borderColor: colors.colorRed300,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    height: 25,
    width: 25,
    marginHorizontal: 10,
  },

  couponBtn: {
    flex: 3,
    alignItems: 'center',
    paddingVertical: 15,
    marginBottom: 10,
  },

  imgOXXO: {
    width: 50,
    resizeMode: 'contain',
  },
  imgPayPal: {
    height: 25,
    resizeMode: 'contain',
  },
  marginTitle: {
    marginBottom: 10,
  },
  btnCloseModal: {
    alignItems: 'flex-end',
  },
  iconStyleModal: {
    color: '#898989',
    fontSize: 20,
    marginTop: isAndroid ? 0 : 30,
  },
  visa: {
    height: 10,
    width: 29,
    resizeMode: 'contain',
  },
  //
  viewBottomCheckout: {
    bottom: isAndroid ? -50 : alto < 737 ? '-10%' : 0,
  },
});
