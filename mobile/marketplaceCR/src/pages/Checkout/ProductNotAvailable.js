import React from 'react';
import { View, ScrollView, Platform } from 'react-native';
import { FieldArray } from 'redux-form';

import CartOrder from '../../components/ShoppingCart/CartOrder';
import withShoppingCartUI from '../../utils/hocs/withShoppingCartUI';

//Will be added to ds
import { CRTitle } from '../../components/CRTitle';
import { colors } from '@canastarosa/ds-theme/colors';

/**--------------------------------------------
ProductNotAvailableType Class
----------------------------------------------*/
type ProductNotAvailableType = {};
let ProductNotAvailable = (props: ProductNotAvailableType) => {
  const isIos: boolean = Platform.OS === 'ios';
  const isAndroid: boolean = Platform.OS === 'android';
  const SPACER_SIZE_IOS: number = 50;
  const SPACER_SIZE_ANDROID: number = 50;

  return (
    <View style={{ backgroundColor: colors.colorWhite, flex: 1 }}>
      <ScrollView>
        <CRTitle
          title="No disponibles"
          subtitle="Productos no disponibles a la dirección seleccionada"
        />

        <FieldArray
          name="unavailableOrders"
          component={CartOrder}
          showCalendar
          showShippingMethods={false}
        />

        {/*  */}
        {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
        {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
      </ScrollView>
    </View>
  );
};

// Wrap component within reduxForm
ProductNotAvailable = withShoppingCartUI(ProductNotAvailable, {});
export default ProductNotAvailable;
