import { StyleSheet, Dimensions } from 'react-native';
import { colors, typography } from '../../assets';
const { height: HEIGHT, width: WIDTH } = Dimensions.get('window');
export const styles = StyleSheet.create({
  searchDetail: {
    backgroundColor: '#FAF7F5',
  },
  containerStore: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  filterContainer: {
    position: 'absolute',
    top: 20,
    right: 10,
  },
  containerProduct: {
    // marginTop: 25,
    // justifyContent: 'space-between',
    // alignItems: 'center',
    width: '100%',
  },
  space: { height: 20, width: 20 },
  arrowBtn: {
    marginTop: 20,
    left: 15,
  },
  iconArrow: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    transform: [{ rotate: '180deg' }],
  },
  txtResults: {
    ...typography.txtExtraBold,
    color: colors.colorMain300,
    marginTop: 10,
    maxWidth: 300,
    alignSelf: 'center',
    textAlign: 'center',
    marginBottom: 20,
  },
  menu: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  activeDivider: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: -2,
    borderBottomColor: colors.colorTurquiose300,
    borderBottomWidth: 3,
    height: '50%',
    width: '90%',
  },
  name: {
    ...typography.txtLight,
    fontSize: 16,
  },
  nameActive: {
    ...typography.txtSemiBold,
    fontSize: 16,
    color: colors.colorTurquiose300,
    textAlign: 'center',
    borderBottomWidth: 2,
    borderBottomColor: colors.colorTurquiose300,
  },
  titleResults: {
    ...typography.txtLight,
    textAlign: 'center',
    color: colors.colorGray400,
    marginTop: 30,
    marginBottom: 15,
    fontSize: 16,
  },
  containerNoView: {
    height: 20,
    width: 20,
  },
  //images result covid
  containerImage: {
    height: HEIGHT - 250,
    marginBottom: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleImage: {
    marginTop: 16,
  },
  messageImage: {
    marginTop: 30,
  },
  imageSearch: {
    width: 320,
    height: 190,
    resizeMode: 'cover',
  },
});
