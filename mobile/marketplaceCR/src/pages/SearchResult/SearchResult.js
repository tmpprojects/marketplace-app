import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Text,
  View,
  Platform,
  Animated,
  TouchableOpacity,
  Image,
} from 'react-native';

import { PRODUCT_TYPES } from '../../redux/constants/app.constants';
import { appActions } from '../../redux/actions';

import { Filters } from '../../components/Filters/Filters';
import { styles } from './SearchResultStyle';

import { CRRowFull, CRRowLeft } from '../../components/Layout';
import CRTabWrapper from '../../components/CRTabs/CRTabWrapper/CRTabWrapper';
import CRTab from '../../components/CRTabs/CRTab/CRTab';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { CRTitle } from '../../components/CRTitle';
import VirtualListContainer from '../../components/VirtualList/VirtualListContainer/VirtualListContainer';
import ZipCodeObserver from '../../components/ZipCodeObserver/ZipCodeObserver';
//Google Analytics
import analytics from '@react-native-firebase/analytics';
import { CRDinamicMenu } from '../../components/CRDinamicMenu/CRDinamicMenu';
import search from '../../images/illustration/doll_search.png';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

/********* Virtualized list ************/
let maxItemsPerPage = 24;
let maxItemsInfiniteLoading = 24;
let infiniteItems = [];
let infiniteStoreItems = [];
//let activeRange = 0;
let itemSearchHistory = '';
let filterHistory = '';
let currentPage = 1;
let currentStorePage = 1;
let dynamicLoading = false;
//Dinamic menu constants
let menuHeight = 75;
let scrollY = new Animated.Value(0);
let diffClamp = Animated.diffClamp(scrollY, 0, menuHeight);
let translateY = diffClamp.interpolate({
  inputRange: [0, menuHeight],
  outputRange: [0, menuHeight * -1],
});

/********* Virtualized list ************/

declare var globalThis: any;
export class SearchResult extends Component {
  constructor(props) {
    super(props);
    const { navigation, route } = this.props;

    let { itemSearch } = route.params;
    if (!itemSearch) {
      itemSearch = '';
    }
    this.itemSearch = itemSearch;

    this.initValues = {
      block: false,
      shouldRenderProduct: true,
      shouldRenderStore: false,
      shouldRenderInspire: false,
      /********* Virtualized list ************/
      isModalVisible: false,
      showItems: [],
      showStoreItems: [],
      activeSlug: this.itemSearch,
      productsCount: 1,
      //disabledInfinite: false,
      //disabledStoreInfinite: false,
      request: false,
      config: {
        disabled: false,
      },
      storesConfig: {
        disabled: false,
      },
    };
    this.state = this.initValues;
  }
  getProductsCount(value) {
    if (value === undefined) {
      return '...';
    }
    return value;
  }

  async componentDidMount() {
    try {
      const check = await globalThis.ReactApplication.utils.checkFilter(
        this.itemSearch,
      );

      if (check) {
        this.setState({
          ...this.state,
          block: true,
        });
        console.log('this.state', this.state);
      }
    } catch (e) {
      console.log(e);
    }

    await analytics().logViewSearchResults({
      search_term: this.itemSearch,
    });
    await analytics().setCurrentScreen('SearchResultScreen');
    await analytics().logEvent('SearchResultScreen');
    this.initContent();
  }

  async initContent() {
    infiniteItems.length = 0;
    infiniteStoreItems.length = 0;
    //currentPage = 1;
    this.props.products.results = null;

    await this.onFilterSubmit(this.itemSearch);
  }

  componentWillUnmount() {
    //this.setState(this.initValues)
    itemSearchHistory = '';
    filterHistory = '';
    currentPage = 1;
    currentStorePage = 1;
    infiniteItems = [];
    infiniteStoreItems = [];
    this.props.stores.count = undefined;
    this.props.products.count = undefined;
    this.props.stores.results = undefined;
    this.props.products.results = undefined;
    //this.initContent();
  }

  componentWillReceiveProps(nextProps) {
    // const { navigation } = nextProps;
    const { products, stores } = nextProps;

    if (products.results !== this.props.products.results) {
      if (products.results) {
        products.results.map((element) => {
          if (!infiniteItems.includes(element)) {
            infiniteItems.push(element);
          }
        });

        this.setState({
          ...this.state,
          // disabledInfinite: products.results.length < 4 ? true : false,
          showItems: infiniteItems,
        });
      }
    }

    /////////////////////

    if (stores.results !== this.props.stores.results) {
      if (stores.results) {
        stores.results.map((element) => {
          if (!infiniteStoreItems.includes(element)) {
            infiniteStoreItems.push(element);
          }
        });

        this.setState({
          ...this.state,
          disabledStoreInfinite: stores.results.length < 4 ? true : false,
          showStoreItems: infiniteStoreItems,
        });
      }
    }
  }

  /**
   * fetchContent()
   * Retrieves results from the API. Call redux actions.
   * @param {string} query | Query string to send to API
   */
  fetchContent = async (query, type) => {
    this.setState({
      ...this.state,
      request: true,
    });
    dynamicLoading = true;
    if (type === 'products' || type === null) {
      await Promise.all([this.props.getSearchProducts(query.itemSearch)]);
    }
    if (type === 'stores' || type === null) {
      await Promise.all([this.props.getSearchStores(query.storesSearch)]);
    }
    dynamicLoading = false;
    this.setState({
      ...this.state,
      request: false,
    });
  };

  /**
   * onFilterSubmit()
   * @param {string} query | Filters querystring.
   */
  onFilterSubmit = (
    itemSearch = this.itemSearch,
    filters = filterHistory,
    page = currentPage,
    itemsPerPage = maxItemsPerPage,
    type = null,
  ) => {
    if (filters) {
      filterHistory = filters;
    }

    if (itemSearch !== itemSearchHistory) {
      infiniteItems.length = 0;
      infiniteStoreItems.length = 0;
      currentPage = 1;
      currentStorePage = 1;
      //filterHistory = '';

      this.setState({
        ...this.state,
      });
      if (itemSearch) {
        itemSearchHistory = itemSearch;
      }
    }

    const searchQueries = buildQuery({
      itemSearch,
      filters,
      page,
      pageSize: itemsPerPage,
    });
    this.fetchContent(searchQueries, type);
  };
  showMore = (type) => {
    if (type === 'products') {
      if (this.props.products.npages > 1) {
        if (!dynamicLoading) {
          //dynamicLoading = true;
          //activeRange = calc;

          currentPage++;
          this.onFilterSubmit(
            this.state.activeSlug,
            filterHistory,
            currentPage,
            maxItemsInfiniteLoading,
            type,
          );
        }
      } else {
        this.setState({
          ...this.state,
          config: {
            disabled: true,
          },
        });
      }
    } else {
      if (this.props.stores.npages > 1) {
        if (!dynamicLoading && !this.state.disabledStoreInfinite) {
          currentStorePage++;
          this.onFilterSubmit(
            this.state.activeSlug,
            filterHistory,
            currentStorePage,
            maxItemsInfiniteLoading,
            type,
          );
        }
      } else {
        this.setState({
          ...this.state,
          storesConfig: {
            disabled: true,
          },
        });
      }
    }
  };

  showResultStore = () => {
    this.setState({
      shouldRenderStore: true,
      shouldRenderProduct: false,
      shouldRenderInspire: false,
    });
    scrollY.setValue(0);
    analytics().setCurrentScreen('SearchStoreResultScreen');
    analytics().logEvent('SearchStoreResultScreen');
  };

  showResultProduct = () => {
    this.setState({
      shouldRenderProduct: true,
      shouldRenderStore: false,
      shouldRenderInspire: false,
    });
    scrollY.setValue(0);
    analytics().setCurrentScreen('SearchProductResultScreen');
    analytics().logEvent('SearchProductResultScreen');
  };

  showResultInspire = () => {
    this.setState({
      shouldRenderInspire: true,
      shouldRenderStore: false,
      shouldRenderProduct: false,
    });
  };

  resetResults() {
    currentPage = 1;
    currentStorePage = 1;
    infiniteItems = [];
    infiniteStoreItems = [];
    this.setState({
      ...this.state,
      showItems: [],
      showStoreItems: [],
    });
  }

  render() {
    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 100;
    const SPACER_SIZE_ANDROID = 50;
    const { products, stores, navigation } = this.props;
    const {
      shouldRenderStore,
      shouldRenderProduct,
      showStoreItems,
      showItems,
      request,
      // disabledStoreInfinite,
      // disabledInfinite,
    } = this.state;

    const config = {
      products: this.state.showItems,
      screen: this.state.sendScreen,
      navigation: this.props.navigation,
      type: 'products',
      showMore: (type) => this.showMore(type),
      request: this.props.products,
    };

    const storesConfig = {
      stores: this.state.showStoreItems,
      navigation: this.props.navigation,
      type: 'stores',
      request: this.props.stores,
      showMore: (type) => this.showMore(type),
    };

    const blockConfig = {
      stores: [],
      navigation: this.props.navigation,
      type: 'products',
      request: this.props.stores,
      showMore: (type) => false,
    };
    //Resets menu scrolling on new search
    scrollY.setValue(0);
    // this.setState({
    //   ...this.state,
    //   config: {
    //     disabled: false,
    //   },
    //   storesConfig: {
    //     disabled: false,
    //   },
    // });

    return (
      <View style={styles.searchDetail}>
        <ZipCodeObserver
          observe="SearchResult"
          isFocused={() => {
            return this.props.navigation.isFocused();
          }}
          search={this.state.activeSlug}
          navigation={this.props.navigation}
        />
        <CRDinamicMenu position={translateY}>
          <TouchableOpacity
            onPress={() => {
              this.props.changeSearch(true);
            }}
          >
            {this.state.block && (
              <React.Fragment>
                <CRTitle title={`"${this.itemSearch}"`} />
                <CRRowLeft>
                  <View style={styles.containerImage}>
                    <Image source={search} style={styles.imageSearch} />
                    <View style={styles.messageImage}>
                      <CRText
                        variant={typography.subtitle3}
                        color={colors.colorDark300}
                        align="center"
                      >
                        Tu búsqueda no obtuvo resultados.
                      </CRText>
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark300}
                        align="center"
                      >
                        Algunos productos sólo están disponibles en{'\n'}nuestro
                        sitio web.
                      </CRText>
                    </View>
                  </View>
                </CRRowLeft>
              </React.Fragment>
            )}
            {!this.state.block && <CRTitle title={`"${this.itemSearch}"`} />}
          </TouchableOpacity>
          <View style={styles.filterContainer}>
            {shouldRenderProduct ? (
              <Filters
                categoryResults={products.results}
                categoryName={'categoryName'}
                categorySlug={this.state.activeSlug}
                isenabled={shouldRenderProduct}
                onFilterSubmit={async (e, filters) => {
                  await this.resetResults();
                  this.onFilterSubmit(e, filters);
                }}
              />
            ) : (
              <View style={styles.space} />
            )}
          </View>
          {!this.state.block && (
            <React.Fragment>
              <CRTabWrapper>
                <CRTab
                  title={
                    'Tiendas (' + this.getProductsCount(stores.count) + ')'
                  }
                  isActive={shouldRenderStore}
                  onPress={() => this.showResultStore()}
                />
                <CRTab
                  title={
                    'Productos (' + this.getProductsCount(products.count) + ')'
                  }
                  isActive={shouldRenderProduct}
                  onPress={() => this.showResultProduct()}
                />
              </CRTabWrapper>
            </React.Fragment>
          )}
        </CRDinamicMenu>

        {shouldRenderStore && !this.state.block && (
          <View style={styles.containerStore}>
            <VirtualListContainer
              config={storesConfig}
              onScrollAction={(e) => {
                scrollY.setValue(e.nativeEvent.contentOffset.y);
              }}
              paddingTop={menuHeight + 80}
              disabled={this.state.storesConfig.disabled}
            />
            {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
            {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
          </View>
        )}
        {shouldRenderProduct && !this.state.block && (
          <View style={styles.containerProduct}>
            <CRRowLeft>
              <VirtualListContainer
                config={config}
                onScrollAction={(e) => {
                  scrollY.setValue(e.nativeEvent.contentOffset.y);
                }}
                paddingTop={menuHeight + 80}
                disabled={this.state.config.disabled}
              />
            </CRRowLeft>
            {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
            {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
          </View>
        )}
      </View>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { store, app } = state;
  return {
    product: state.store.productDetail,
    stores: app.searchResults.stores,
    products: app.searchResults.products,
    store: state.store.data,
    productsList:
      store.productsList.results && store.productsList.results.length
        ? store.productsList.results.filter(
            (p) => p.product_type.value === PRODUCT_TYPES.PHYSICAL,
          )
        : [],
    delivery: app.pickupSchedules,
    reviews: state.store.reviews.store,
    pendingReviews: state.store.pending_reviews.store,
  };
}

const buildQuery = (itemSearch) => {
  return {
    itemSearch: `?sort=${itemSearch.filters}&q=${itemSearch.itemSearch}&page=${itemSearch.page}&page_size=${itemSearch.pageSize}&type=products`,
    storesSearch: `?sort=${itemSearch.filters}&q=${itemSearch.itemSearch}&page=${itemSearch.page}&page_size=${itemSearch.pageSize}&type=stores`,
  };
};

function mapDispatchToProps(dispatch) {
  const { getSearchStores, getSearchProducts, changeSearch } = appActions;
  return bindActionCreators(
    { getSearchStores, getSearchProducts, changeSearch },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(SearchResult);
