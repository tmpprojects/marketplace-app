import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Platform,
  Dimensions,
  Button,
} from 'react-native';
import onboarding from '../../images/illustration/onboarding.png';

import { styles } from './OnboardingScreenStyles';
import { field } from '../../components/FieldsForm/FieldsFormStyles';
import logo from '../../images/logo/vertical-core.png';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

//Google Analytics
import analytics from '@react-native-firebase/analytics';

const { height: HEIGHT } = Dimensions.get('window');

declare var globalThis: any;

export default class OnboardingScreen extends Component {
  async componentDidMount() {
    await analytics().setCurrentScreen('OnboardingScreen');
    await analytics().logEvent('OnboardingScreen');
  }

  componentWillUnmount() {}

  render() {
    let isIos = Platform.OS === 'ios';
    let isAndroid = Platform.OS === 'android';
    //isIos = false;
    return (
      <View style={styles.backgroundContainer}>
        <TouchableOpacity
          style={styles.guest}
          onPress={() => {
            //this.props.navigation.navigate('TabsScreenController');
            //this.props.navigation.navigate('LoginScreen');
            globalThis.ReactApplication.ActiveScreen(
              'HomeScreen',
              {},
              this.props.navigation,
              true,
            );
          }}
          activeOpacity={0.5}
        >
          <CRText
            variant={typography.paragraph}
            color={colors.colorGray400}
            align="right"
          >
            Entrar como invitado
          </CRText>
        </TouchableOpacity>
        <View style={styles.aboutWrapper}>
          <Image style={styles.logo} source={logo} />
          <View style={styles.textWrapper}>
            <CRText
              variant={typography.paragraph}
              color={colors.colorGray400}
              align="center"
            >
              Descubre creaciones y productos únicos, elaborados por
              emprendedores locales.
            </CRText>
          </View>

          <TouchableOpacity
            style={field.buttonSquare}
            onPress={() => {
              //this.props.navigation.navigate('SignUpScreen');

              globalThis.ReactApplication.ActiveScreen(
                'SignUpScreen',
                {},
                this.props.navigation,
              );
            }}
            activeOpacity={0.5}
          >
            <CRText
              variant={typography.button}
              color={colors.colorMain300}
              align="center"
            >
              Crea una cuenta
            </CRText>
          </TouchableOpacity>
          <View style={styles.linkContainer}>
            <View style={styles.text}>
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark200}
                align="center"
              >
                ¿Tienes una cuenta?
              </CRText>
            </View>
            <TouchableOpacity
              style={styles.textButton}
              onPress={() => {
                //this.props.navigation.navigate('LoginScreen');
                globalThis.ReactApplication.ActiveScreen(
                  'LoginScreen',
                  {},
                  this.props.navigation,
                );
              }}
              activeOpacity={0.5}
            >
              <CRText
                variant={typography.bold}
                color={colors.colorDark200}
                align="center"
              >
                Iniciar sesión
              </CRText>
            </TouchableOpacity>
          </View>
        </View>

        {HEIGHT >= 700 && <Image source={onboarding} style={styles.image} />}
      </View>
    );
  }
}
