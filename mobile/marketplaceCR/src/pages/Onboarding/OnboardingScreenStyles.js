import { StyleSheet, Dimensions } from 'react-native';
import { buttons } from '../../assets';
import { colors } from '@canastarosa/ds-theme/colors';

const { height: HEIGHT, width: WIDTH } = Dimensions.get('window');
export const styles = StyleSheet.create({
  backgroundContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },
  logo: {
    width: 280,
    height: 100,
    resizeMode: 'contain',
    // transform: [{ scaleX: 0.12 }, { scaleY: 0.12 }],
  },
  guest: {
    flex: 1,
    justifyContent: 'flex-end',
    marginRight: 20,
  },
  textWrapper: {
    maxWidth: 300,
    marginTop: 30,
  },
  aboutWrapper: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  linkContainer: {
    marginTop: 30,
  },
  textButton: {
    height: 35,
  },

  image: {
    // position: 'absolute',
    // right: 0,
    flex: 2,
    width: HEIGHT >= 800 ? WIDTH : WIDTH / 1.1,
    bottom: -18,
    resizeMode: 'contain',
  },
  imageXS: {
    // position: 'absolute',
    // bottom: 0,
    // right: 0,
    flex: 1,
    width: WIDTH,
  },
});
