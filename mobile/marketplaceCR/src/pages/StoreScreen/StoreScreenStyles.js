import { StyleSheet, Platform } from 'react-native';
import { typography, buttons } from '../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const styles = StyleSheet.create({
  storeScreen: {
    backgroundColor: '#FAF7F5',
    flexGrow: 1,
    marginBottom: Platform.OS === 'android' ? 100 : 100,
  },
  subtitle: {
    ...typography.txtRegular,
  },

  //MENU SECTIONS STORE
  menuContainer: {
    paddingHorizontal: 15,
    marginVertical: 20,
  },
  divider: {
    borderBottomColor: colors.colorGray300,
    borderBottomWidth: 1,
    zIndex: -1,
  },
  activeDivider: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: -2,
    borderBottomColor: colors.colorTurquiose300,
    borderBottomWidth: 3,
    height: '50%',
    width: '90%',
  },
  menu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemMenu: {
    flex: 3,
    padding: 20,
  },
  name: {
    ...typography.txtLight,
    textAlign: 'center',
  },
  nameActive: {
    ...typography.txtSemiBold,
    fontSize: 16,
    color: colors.colorTurquiose300,
    textAlign: 'center',
    borderBottomWidth: 2,
    borderBottomColor: colors.colorTurquiose300,
  },

  //CATEGORIES
  categoriesContainer: {
    marginTop: 15,
  },
  category: {
    margin: 10,
    ...buttons.btnInterests,
    borderColor: colors.colorGray300,
  },
  categoryActive: {
    margin: 10,
    ...buttons.btnInterests,
    borderColor: colors.colorMain300,
    backgroundColor: colors.colorMain300,
  },
  categoryName: {
    ...typography.txtLight,
    padding: 8,
    textAlign: 'center',
  },
  iconSearch: {
    width: 20,
    height: 20,
    marginRight: 8,
  },
  inputForm: {
    // borderWidth: 1,
    // borderColor: colors.colorGray300,
    borderRadius: 20,
    backgroundColor: colors.colorGray100,
    padding: 10,
    marginBottom: 10,
    flexDirection: 'row',
  },

  textInput: { color: colors.colorDark300, ...typography.txtLight },

  //ANDROID
  inputFormAnd: {
    marginTop: 10,
    marginVertical: 10,
    borderRadius: 15,
    backgroundColor: colors.colorGray100,
    padding: 0,
    flexDirection: 'row',
  },
  iconSearchAnd: {
    width: 20,
    height: 20,
    marginRight: 8,
    marginTop: 14,
    marginLeft: 10,
  },
});
