import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';

import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
  Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { PRODUCT_TYPES } from '../../redux/constants/app.constants';
import { storeActions } from '../../redux/actions';
import AboutStore from '../../components/StoreScreen/AboutStore/AboutStore';
import StoreProfile from '../../components/StoreScreen/StoreProfile/StoreProfile';
import ProductsList from '../../components/ProductsList';
import ReviewsList from '../../components/StoreScreen/Reviews/ReviewsList';
import Loader from '../../components/Loader';
import search from '../../images/icon/search_gray.png';
import { styles } from './StoreScreenStyles';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { CRRowFull, CRRowLeft } from '../../components/Layout';
import CRTabWrapper from '../../components/CRTabs/CRTabWrapper/CRTabWrapper';
import CRTab from '../../components/CRTabs/CRTab/CRTab';

//Google Analytics
import analytics from '@react-native-firebase/analytics';

const MAX_ITEMS_PER_PAGE = 5;
const isAndroid: Boolean = Platform.OS === 'android';
declare var globalThis: any;

const renderField = ({
  keyboardType,
  input: { onChange },
  placeholder,
  secureTextEntry,
}) => {
  return (
    <View style={isAndroid ? styles.inputFormAnd : styles.inputForm}>
      <Image
        source={search}
        style={isAndroid ? styles.iconSearchAnd : styles.iconSearch}
      />
      <TextInput
        style={styles.textInput}
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray400}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
      />
    </View>
  );
};

export class StoreScreen extends Component {
  constructor(props) {
    super(props);
    const { navigation, route } = this.props;
    const { itemStore } = route.params;
    this.currentPage = 1;
    this.storeSlug = itemStore;
    this.state = {
      shouldRenderProducts: true,
      shouldRenderReviews: false,
      shouldRenderAbout: false,
      results: [],
      checkedPrice: false,
      checkedName: false,
      checked: false,
      selected: false,
    };
  }
  componentDidMount() {
    const { navigation, route } = this.props;
    const { itemStore } = route.params;

    this.props.fetchStore(this.storeSlug);
    this.props.fetchProducts(this.storeSlug);
    this.props.fetchStoreSections(this.storeSlug);
    this.getReviews(this.storeSlug, this.currentPage);
    this.props.getPendingStoreReviews(this.storeSlug);
    analytics().setCurrentScreen('StoresScreen', this.storeSlug);
    analytics().logEvent('StoresScreen', {
      item: this.storeSlug,
    });
  }
  componentWillUnmount() {
    this.setState({ results: [] });
  }
  searchProducts(filter) {
    // Fetch filtered products
    this.props.fetchProducts(this.storeSlug, filter);
  }
  getReviews(store, page) {
    this.props.getStoreReviews(
      store,
      `?page=${page}&page_size=${MAX_ITEMS_PER_PAGE}`,
    );
    this.currentPage = page;
  }

  showProductsList = () => {
    this.setState({
      shouldRenderProducts: true,
      shouldRenderReviews: false,
      shouldRenderAbout: false,
    });
  };
  showReviews = () => {
    this.setState({
      shouldRenderReviews: true,
      shouldRenderProducts: false,
      shouldRenderAbout: false,
    });
  };
  showAbout = () => {
    this.setState({
      shouldRenderAbout: true,
      shouldRenderProducts: false,
      shouldRenderReviews: false,
    });
  };

  sortPrice = () => {
    const newResults = this.props.productsList.sort((a, b) => {
      return a.price - b.price;
    });
    this.setState({
      results: newResults,
      checkedPrice: true,
      checkedName: false,
      checked: false,
    });
  };
  sortName = () => {
    const newResults = this.props.productsList.sort((a, b) =>
      a.name !== b.name ? (a.name < b.name ? -1 : 1) : 0,
    );
    this.setState({
      results: newResults,
      checkedName: true,
      checkedPrice: false,
      checked: false,
    });
  };
  sortCreated = () => {
    const newResults = this.props.productsList.sort((a, b) => {
      var c = new Date(a.created);
      var d = new Date(b.created);
      return c - d;
    });
    this.setState({
      results: newResults,
      checked: true,
      checkedName: false,
      checkedPrice: false,
    });
  };

  render() {
    const {
      store,
      reviews,
      pendingReviews,
      sections,
      productsList,
    } = this.props;
    const {
      shouldRenderAbout,
      shouldRenderProducts,
      shouldRenderReviews,
      checkedPrice,
      checkedName,
      checked,
      selected,
    } = this.state;

    const formSubmit = (e) => {
      const term = e;
      this.searchProducts(`&search=${term}`);
    };
    const { navigation } = this.props;
    return (
      <View style={styles.storeScreen}>
        {!store.loaded ? (
          <Loader />
        ) : (
          <KeyboardAwareScrollView
            resetScrollToCoords={{ x: 0, y: 0 }}
            scrollEnabled
          >
            <StoreProfile store={store} />
            <CRRowFull>
              <CRTabWrapper>
                <CRTab
                  title="Productos"
                  isActive={shouldRenderProducts}
                  onPress={() => this.showProductsList()}
                />
                <CRTab
                  title="Reseñas"
                  isActive={shouldRenderReviews}
                  onPress={() => this.showReviews()}
                />
                <CRTab
                  title="Acerca de"
                  isActive={shouldRenderAbout}
                  onPress={() => this.showAbout()}
                />
              </CRTabWrapper>
            </CRRowFull>
            {shouldRenderProducts && (
              <React.Fragment>
                <View style={styles.categoriesContainer}>
                  <Field
                    name="search"
                    fieldName="search"
                    keyboardType="default"
                    placeholder="Buscar"
                    onChange={formSubmit}
                    component={renderField}
                  />
                  {/* <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{
                      alignItems: 'center',
                    }}
                  >
                    <Text style={styles.subtitle}>Ordenar por:</Text>
                    <TouchableOpacity
                      style={checked ? styles.categoryActive : styles.category}
                      onPress={() => {
                        this.sortCreated();
                      }}
                    >
                      <Text
                        style={[
                          styles.categoryName,
                          {
                            color: checked
                              ? colors.colorWhite
                              : colors.colorDark300,
                          },
                        ]}
                      >
                        Creación
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={
                        checkedPrice ? styles.categoryActive : styles.category
                      }
                      onPress={() => {
                        this.sortPrice();
                      }}
                    >
                      <Text
                        style={[
                          styles.categoryName,
                          {
                            color: checkedPrice
                              ? colors.colorWhite
                              : colors.colorDark300,
                          },
                        ]}
                      >
                        Precio
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={
                        checkedName ? styles.categoryActive : styles.category
                      }
                      onPress={() => {
                        this.sortName();
                      }}
                    >
                      <Text
                        style={[
                          styles.categoryName,
                          {
                            color: checkedName
                              ? colors.colorWhite
                              : colors.colorDark300,
                          },
                        ]}
                      >
                        Nombre
                      </Text>
                    </TouchableOpacity>
                  </ScrollView> */}
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    <TouchableOpacity
                      style={styles.category}
                      onPress={() => {
                        this.searchProducts('');
                      }}
                    >
                      <Text style={styles.categoryName}>Todos</Text>
                    </TouchableOpacity>
                    {sections.map((item, i) => (
                      <TouchableOpacity
                        key={i}
                        id={item.slug}
                        style={
                          selected ? styles.categoryActive : styles.category
                        }
                        onPress={() => {
                          this.searchProducts(`&section__slug=${item.slug}`);
                        }}
                      >
                        <Text style={styles.categoryName}>{item.name}</Text>
                      </TouchableOpacity>
                    ))}
                  </ScrollView>
                </View>
                <CRRowLeft>
                  <ProductsList
                    products={productsList}
                    navigation={navigation}
                    {...this.props}
                  />
                </CRRowLeft>
              </React.Fragment>
            )}

            {shouldRenderReviews && (
              <ReviewsList
                reviews={reviews.results}
                pendingReviews={pendingReviews.results}
                reviewsCount={reviews.count}
                rating={reviews.rating}
                purchase={{}}
              />
            )}

            {shouldRenderAbout && <AboutStore store={store} />}
          </KeyboardAwareScrollView>
        )}
      </View>
    );
  }
}

StoreScreen = reduxForm({
  form: 'searchStoreScreen',
})(StoreScreen);

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { store } = state;

  return {
    store: store.data,
    productsList:
      store.productsList.results && store.productsList.results.length
        ? store.productsList.results.filter(
            (p) => p.product_type.value === PRODUCT_TYPES.PHYSICAL,
          )
        : [],
    sections: store.sections.sections,
    reviews: state.store.reviews.store,
    pendingReviews: state.store.pending_reviews.store,
  };
}
function mapDispatchToProps(dispatch) {
  const {
    fetchStore,
    fetchProducts,
    fetchStoreSections,
    getStoreReviews,
    getPendingStoreReviews,
  } = storeActions;
  return bindActionCreators(
    {
      fetchStore,
      fetchProducts,
      fetchStoreSections,
      getStoreReviews,
      getPendingStoreReviews,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(StoreScreen);
