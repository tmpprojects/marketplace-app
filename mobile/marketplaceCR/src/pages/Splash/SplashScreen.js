import React, { Fragment, PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Animated, Text, Platform, StyleSheet } from 'react-native';
import { styles } from './SplashStyle';
import logoSplash from '../../images/logo/horizontal.png';
import { Spinner } from '../../utils/animations/Spinner';
import * as Sentry from '@sentry/react-native';
// import Video from 'react-native-video';
import {
  getBuildNumber,
  getBundleId,
  getVersion,
} from 'react-native-device-info';
// import LoadInitDataApp from './../../utils/LoadInitDataApp';
// import testVideo from './../../assets/video/test.mp4';
import {
  appActions,
  userActions,
  shoppingCartActions,
  storeActions,
} from '../../redux/actions';
import { stylesLoadInit } from './../../utils/LoadInitDataAppStyle';

import { _clearData, _setData, _getData } from './../../utils/asyncStorage';
import { ShoppingCartManager } from './../../utils/shoppingCartUtils/helpers';

declare var globalThis: any;
globalThis.ShoppingCartManager = new ShoppingCartManager([], [], []);

console.log(`Showing the build version ${getVersion()}`);

class SplashScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      logoOpacity: new Animated.Value(0),
      loginUser: false,
    };
  }

  changeText(text = 'Cargando...') {
    this.setState({
      ...this.state,
      textLoading: text,
    });
  }

  async loadPrivateData(getSession) {
    try {
      await this.props.setCookiesUser(getSession);
      await this.props.autoLogin();
      await this.props.verifyAutoLogin();
      if (!this.props.verifyStatusAutoLogin) {
        await this.props.logout();
      } else {
        await this.props.getCurrentUser();
        this.changeText('Cargando tiendas increíbles...');
        await this.props.listAddresses();
        this.changeText('Cargando productos únicos...');
        await this.props.getUserCards();
        await this.changeText('Cargando nuevas tendencias...');
      }
    } catch (e) {
      Sentry.captureException(e);
    }
    return true;
    //await this.props.getNewsStores();
  }
  async loadPublicData() {
    try {
      ///Load public data:
      await this.changeText('Cargando productos sorprendentes...');
      // Get Market Categories

      await this.props.getCategories();
      // // Get Pickup Schedules

      await this.props.getPickupSchedules();
      // // Get shipping methods

      await this.props.getShippingMethods();
      this.changeText('Espera...');
      // // Get payment methods

      await this.props.getPaymentMethods();
      // Get Market Categories

      await this.props.getMarketCategories();
      // Get New Stores
      //this.props.getNewsStores();
      this.setState({ loading: false });

      return true;
    } catch (e) {
      //console.log(e);
      Sentry.captureException(e);
    }
  }

  async getAsyncStorage() {
    //if (this.props.currentUser && this.props.currentUser.isLogged) {
    try {
      const asyncversion = (await _getData()).asyncversion;
      const email = (await _getData()).email;
      const pwd = (await _getData()).password;

      if (asyncversion && email && pwd) {
        this.changeText('Iniciando sesión...');
        this.props
          .loginUser(email, pwd)
          .then(async (response) => {
            await this.loadPublicData();
            ///////////////////////
            await _setData(response.data);
            await this.loadPrivateData(response.data);

            //await this.loadPrivateData(this.props.cookies);

            //this.props.navigation.navigate('TabsScreenController');
            globalThis.ReactApplication.ActiveScreen(
              'HomeScreen',
              {},
              this.props.navigation,
            );

            // this.props.navigation.navigate('HomeScreen');
          })
          .catch(async (e) => {
            // this.props.navigation.navigate('OnBoardingScreen');

            globalThis.ReactApplication.ActiveScreen(
              'HomeScreen',
              {},
              this.props.navigation,
            );

            Sentry.captureException(e);
          });
      } else {
        await this.loadPublicData();
        await this.props.listAddresses();
        // this.props.navigation.navigate('OnBoardingScreen');
        globalThis.ReactApplication.ActiveScreen(
          'HomeScreen',
          {},
          this.props.navigation,
        );
      }
    } catch (e) {
      Sentry.captureException(e);
    }
  }

  loopAnimation() {
    let returnAnimation = [];
    let show = 1;
    let hide = 0;
    let current = show;

    for (let x = 0; x < 1; x++) {
      returnAnimation.push(
        Animated.timing(this.state.logoOpacity, {
          toValue: current,
          duration: 1200,
          useNativeDriver: true,
        }),
      );
      current = current === show ? (current = hide) : (current = show);
    }
    return returnAnimation;
  }
  componentDidMount() {
    Animated.sequence(this.loopAnimation()).start(() => {
      //this.props.navigation.navigate("LoginScreen")
    });
    this.getAsyncStorage();
  }

  render() {
    return (
      <Fragment>
        <View style={styles.container}>
          <Animated.Image
            source={logoSplash}
            style={{
              ...styles.logoSplash,
              opacity: this.state.logoOpacity,
            }}
            repeat={false}
          />
        </View>
        <View style={styles.containerLoading}>
          <View style={styles.loader}>
            <Spinner />
          </View>
          <Fragment>
            {/* <LoadInitDataApp {...this.props} /> */}
            {/* <Text style={stylesLoadInit.version}>v{getVersion()}</Text> */}
            <Text style={stylesLoadInit.loadingText}>
              {this.state.textLoading}
            </Text>
          </Fragment>
        </View>
      </Fragment>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { app, user, cart } = state;
  return {
    cart,
    categories: app.categories.categories,
    paymentMethods: app.paymentMethods,
    currentUser: user,
    cookies: state.user.cookies,
    verifyStatusAutoLogin: state.user.verifyAutoLogin,
  };
}
function mapDispatchToProps(dispatch) {
  const {
    getShippingMethods,
    getPaymentMethods,
    getPickupSchedules,
    getMarketCategories,
    getCategories,
  } = appActions;
  const {
    getCurrentUser,
    listAddresses,
    //getUserReviews,
    setCookiesUser,
    getUserCards,
    loginUser,
    autoLogin,
    verifyAutoLogin,
    logout,
  } = userActions;
  const { fetchShoppingCart } = shoppingCartActions;
  const { getNewsStores } = storeActions;

  return bindActionCreators(
    {
      getShippingMethods,
      getPaymentMethods,
      getPickupSchedules,
      getMarketCategories,
      getCategories,
      getCurrentUser,
      listAddresses,
      //getUserReviews,
      setCookiesUser,
      fetchShoppingCart,
      getUserCards,
      loginUser,
      getNewsStores,
      autoLogin,
      verifyAutoLogin,
      logout,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
