import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export const styles = StyleSheet.create({
  loadingText: {
    textAlign: 'center',
    color: colors.colorGray400,
    fontSize: 16,
    marginTop: -120,
  },
  container: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: colors.colorDark400,
  },
  containerLoading: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: colors.colorDark400,
  },
  logoSplash: {
    transform: [{ scaleX: 0.12 }, { scaleY: 0.12 }],
  },
  loader: {
    marginTop: 0,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 150,
  },
  iconLoader: {
    width: 110,
    height: 110,
    transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }],
  },
});
