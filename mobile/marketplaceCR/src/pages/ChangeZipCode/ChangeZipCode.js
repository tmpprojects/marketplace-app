import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { View, Text } from 'react-native';
import { appActions } from '../../redux/actions';

import { _clearData, _setData, _getData } from './../../utils/asyncStorage';

class ChangeZipCode extends Component {
  constructor(props) {
    super(props);
  }
  async componentDidMount() {
    const { reloadTo } = this.props.route.params;

    /*** Only HomeScreen ***/
    if (reloadTo === 'HomeScreen') {
      await this.loadPublicData();
    }
    /*** Only HomeScreen ***/
    // this.props.navigation.navigate(reloadTo);

    globalThis.ReactApplication.ActiveScreen(
      'reloadTo',
      {},
      this.props.navigation,
    );
  }

  async loadPublicData() {
    try {
      await this.props.getCategories();
      await this.props.getMarketCategories();
    } catch (e) {}
  }

  render() {
    return (
      <React.Fragment>
        <View>
          <Text>Espera un momento...</Text>
        </View>
      </React.Fragment>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  const { getMarketCategories, getCategories } = appActions;

  return bindActionCreators(
    {
      getMarketCategories,
      getCategories,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeZipCode);
