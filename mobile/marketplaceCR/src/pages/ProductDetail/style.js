import { StyleSheet } from 'react-native';

import { buttons } from '../../assets';
import { Platform } from 'react-native';
const isAndroid = Platform.OS === 'android';

// DS
import { typography } from '@canastarosa/ds-theme/typography';
import { colors, rgba } from '@canastarosa/ds-theme/colors';

export const style = StyleSheet.create({
  thumbnails: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    width: '100%',
  },
  touchThumb: {
    width: 100,
    height: 100,
    marginBottom: 6,
    marginLeft: 3,
  },
  thumb: {
    width: '100%',
    height: 100,
    // marginBottom: 6,
    // marginLeft: '2.5%',
  },
  'thumb:last-child': {
    marginLeft: 0,
  },
  productDetailWrapper: {
    flex: 1,
    //backgroundColor: colors.colorWhite,
    backgroundColor: '#FAF7F5',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FAF7F5',

    //backgroundColor: colors.colorWhite,
  },
  storeHeader: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  storePhoto: {
    width: 20,
    height: 20,
    marginRight: 10,
    borderWidth: 1,
    borderColor: colors.colorGray200,
    borderRadius: 2,
  },
  bannerContainer: {
    height: 250,
    justifyContent: 'center',
  },
  wrapper: {},
  bullet: {
    width: 10,
    height: 10,
    backgroundColor: colors.colorGray300,
    borderRadius: 50,
    margin: 5,
  },
  bulletActive: {
    width: 10,
    height: 10,
    backgroundColor: colors.colorMain300,
    borderRadius: 50,
    margin: 5,
  },
  imageStyle: {
    width: '100%',
    height: 180,
    resizeMode: 'cover',
    position: 'relative',
    flex: 2.5,
  },
  titleStore: {
    ...typography.txtSemiBold,
    textAlign: 'center',
    color: colors.titleColor,
    marginTop: -10,
    marginBottom: 10,
    fontSize: 18,
  },
  titleProduct: {
    ...typography.txtExtraBold,
    textAlign: 'center',
    color: colors.corePink,
    marginBottom: 10,
    fontSize: 24,
  },
  containerPrice: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titlePrice: {
    ...typography.txtExtraBold,
    textAlign: 'center',
    color: colors.black,
    fontSize: 26,
  },
  counter: {
    marginTop: 20,
    marginBottom: 30,
  },
  pickerWrapper: {
    marginVertical: 20,
    width: '100%',
    backgroundColor: rgba(colors.colorGray100, 0.5),
  },

  shippingContainer: {
    marginTop: 10,
    paddingVertical: 40,
    borderColor: colors.colorGray100,
    borderTopWidth: 1,
    paddingBottom: 50,
  },
  menu: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderBottomColor: colors.colorGray100,
    borderBottomWidth: 1,
    marginBottom: 30,
    backgroundColor: colors.colorWhite,
  },
  menuItem: {
    flex: 1,
    borderBottomColor: colors.colorWhite,
    borderBottomWidth: 4,
    paddingVertical: 15,
    paddingBottom: 18,
  },
  menuItemActive: {
    flex: 1,
    borderBottomColor: colors.colorTurquiose300,
    borderBottomWidth: 3,
    paddingVertical: 15,
    paddingBottom: 18,
  },
  name: {
    color: colors.colorDark100,
    textAlign: 'center',
  },
  nameActive: {
    color: colors.colorTurquiose300,
    textAlign: 'center',
  },
  description: {
    marginHorizontal: 15,
  },
  descContainer: {
    marginVertical: 30,
    marginBottom: 110,
  },

  productDimensions: {
    marginTop: 20,
    marginHorizontal: 15,
  },
  txtDetail: {
    flex: 1,
    width: '100%',
  },
  titleProductDetail: {
    ...typography.txtSemiBold,
    color: colors.coreDarkGray,
    fontSize: 16,
    marginLeft: 15,
    marginTop: 20,
    marginBottom: 20,
  },
  txtTime: {
    backgroundColor: '#fff',
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtProductDetail: {
    ...typography.txtLight,
    textAlign: 'center',
    color: colors.black,
    fontSize: 18,
    paddingTop: 20,
    paddingBottom: 20,
  },
  containerInfoDeliveryDate: {
    marginTop: 15,
    marginBottom: 15,
    backgroundColor: '#FFD739',
    height: 60,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleInfoDeliveryDate: {
    ...typography.txtSemiBold,
    textAlign: 'center',
    color: colors.coreDarkGray,
    fontSize: 16,
  },
  logoContainerShiping: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  btnShipping: {
    ...buttons.squareShippedDate,
    borderColor: colors.colorDark100,
    marginBottom: 20,
    marginRight: 10,
  },
  btnShippingActive: {
    ...buttons.squareShippedDate,
    marginBottom: 20,
    borderColor: colors.colorMain300,
    backgroundColor: colors.colorMain300,
    marginRight: 10,
  },
  btnZone: {
    ...buttons.squareZone,
    marginBottom: 20,
  },
  textBtnShipping: {
    textAlign: 'center',
    ...typography.txtRegular,
    color: colors.black300,
    fontSize: 20,
  },
  textBtnShippingSelected: {
    textAlign: 'center',
    ...typography.txtRegular,
    color: colors.backgroundColor,
    fontSize: 20,
  },
  productDescription: {
    ...typography.txtLight,
    textAlign: 'center',
    color: colors.titleColor,
    fontSize: 16,
    marginTop: 10,
    marginBottom: 10,
  },
  containerSchedules: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    width: '100%',
    backgroundColor: '#fff',
  },
  itemSchedules: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 250,
  },
  schedulesName: {
    ...typography.txtLight,
    marginBottom: 8,
  },
  schedules: {
    ...typography.txtLight,
  },
  calendarContainer: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  calendar: {
    marginVertical: 20,
  },
  containerBtnAdd: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    borderColor: colors.colorGray200,
    borderTopWidth: 1,
    paddingVertical: 15,
    backgroundColor: colors.colorWhite,
  },
  btnAddToCart: {
    flex: 1,
    paddingVertical: 5,
    backgroundColor: colors.colorMain300,
    height: 80,
    borderRadius: 3,
  },
  txtAddToCart: {
    textAlign: 'center',
    ...typography.txtRegular,
    color: colors.white,
    fontSize: 20,
  },
  titleMoreProduct: {
    ...typography.txtSemiBold,
    textAlign: 'center',
    color: colors.corePink,
    marginTop: 100,
    marginBottom: 15,
    fontSize: 22,
  },

  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  badgeStyle: {
    zIndex: 1,
    marginTop: isAndroid ? -70 : -60,
    marginLeft: isAndroid ? 300 : 330,
  },
  bestSellerStyle: {
    zIndex: 1,
    marginTop: -50,
  },
  containerNoStock: {
    marginBottom: 40,
  },
  containerStock: {
    marginTop: 20,
    marginBottom: 40,
  },
  visa: {
    height: 17,
    width: 47,
    resizeMode: 'contain',
  },
});

//
export const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    width: '100%',
    ...typography.paragraph,
    paddingVertical: 15,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: colors.colorGray100,
    color: colors.colorDark300,
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    width: '100%',
    ...typography.paragraph,
    paddingHorizontal: 10,
    paddingVertical: 11,
    borderWidth: 0.5,
    borderColor: colors.colorGray100,
    color: colors.colorDark300,
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
