import DayJs from 'dayjs';
import type {
  AttributeType,
  AttributeOptionType,
  ShippingScheduleType,
  ProductDetailType,
  ProductProperties,
  ShippingDateType,
  SelectedProductType,
} from '../../redux/types/ProductTypes';
import { isValidShippingScheduleTime } from '../../utils/date';
import * as Sentry from '@sentry/react-native';
// Default props and constants
export const SELECTED_PRODUCT_MODEL: SelectedProductType = {
  uuid: null,
  slug: '',
  price: 0,
  price_without_discount: 0,
  discount: 0,
  stock: 1,
  quantity: 1,
  attributeSelectionMap: [],
  attributesList: [],
  availableShippingSchedules: [],
  shipping_date: '',
  shipping_schedule: null,
};
const DEFAULT_ATTRIBUTE_INDEX = 0;
const DEFAULT_QUANTITY = 1;

/**
 * ProductDetailManager | Class
 */
export default class ProductDetailManager {
  _product: ProductDetailType;
  _selectedProduct: SelectedProductType;
  constructor(product: ProductDetailType) {
    this._product = product;
    this._selectedProduct = this._setupProduct(this._product);
  }

  /**
   * _setupProduct()
   * Transform the product object received from the backend
   * into a 'product page frontend-friendly' format.
   * @param {object} product | Product object.
   * @returns {obj} An object based on SELECTED_PRODUCT_MODEL scheme.
   */
  _setupProduct = (product: ProductDetailType): SelectedProductType => {
    try {
      const DEFAULT_SHIPPING_DATE: string =
        product.physical_properties.minimum_shipping_date;

      // Attributes.
      const productVariations = product.attributes;
      const attributesList = this.setAttributes(productVariations);
      const attributeSelectionMap = attributesList.map(
        (item) => DEFAULT_ATTRIBUTE_INDEX,
      );

      // Return object.
      return {
        ...SELECTED_PRODUCT_MODEL,
        slug: product.slug,
        attributesList,
        attributeSelectionMap,
        ...this.setQuantity(DEFAULT_QUANTITY),
        ...this.setProductProperties(attributeSelectionMap),
        ...this.setShippingDate(DEFAULT_SHIPPING_DATE),
      };
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  /**
   * setAttributes()
   * Gets a list of product attributes and parse them into an array of unique variations
   * @param {Array<AttributeType>} attributes : List of product attributes
   * @return {Array<AttributeOptionType>}
   */
  setAttributes = (
    attributes: Array<AttributeType>,
  ): Array<AttributeOptionType> => {
    // Transform nested attributes list into a flat array.
    const attributesList = this._parseAttributeTypesIntoSelectOptions(
      attributes,
    );

    // Update selected product
    this._selectedProduct = { ...this._selectedProduct, attributesList };
    return attributesList;
  };

  /**
   * _parseAttributeTypesIntoSelectOptions()
   * This function gets a Product.Attributes array, and transforms it
   * into an array unique product variations and their attributes.
   * @param {array} productAttributes : List of product attributes
   * @param {array} listOfAttributes : Accumulative (recursive) list of product attributes.
   * @return {array} [{type <string>, options <array:*>, values <array:*>, ref <string>}, ...]
   */
  _parseAttributeTypesIntoSelectOptions = (
    productAttributes: Array<AttributeType> = [],
    listOfAttributes: Array<AttributeOptionType> = [],
  ): Array<AttributeOptionType> => {
    if (!productAttributes.length) return [];
    const currentIteration = listOfAttributes.length;

    // Create Options for the 'Product Variations Selectbox'
    const childAttributes: Array<AttributeType> = [];
    const optionsList: {
      options: Array<string>,
      values: Array<string>,
    } = productAttributes.reduce(
      (optionsAccumulator, currentOption, index) => {
        // Avoid Duplicates.
        if (!optionsAccumulator.values.includes(currentOption.value)) {
          // Add new atribute to the final list.
          optionsAccumulator.options.push(currentOption.value);
          optionsAccumulator.values.push(currentOption.value);

          // If this attribute has children, add them to a list of
          // subchild attributes to use them whithin this function recursively.
          if (currentOption.hasOwnProperty('children')) {
            childAttributes.push(...currentOption.children);
          }
        }

        // Return options.
        return optionsAccumulator;
      },
      { options: [], values: [] },
    );

    // Construct Attributes Object
    const attributeType: string = productAttributes[0].attribute_type;
    const selectboxName: string = `${attributeType}${currentIteration}_sbx`;
    const attribute: AttributeOptionType = {
      type: attributeType,
      options: optionsList.options,
      values: optionsList.values,
      ref: selectboxName,
    };

    // Push current item to attributes list
    listOfAttributes.push(attribute);

    // Iterate through children attributes
    if (childAttributes.length) {
      this._parseAttributeTypesIntoSelectOptions(
        childAttributes,
        listOfAttributes,
      );
    }

    // Return List of Attributes
    return listOfAttributes;
  };

  /**
   * setProductProperties()
   * @param {array} map : A list of indexes that define the path to follow through attributes tree.
   * @return {object} Returns an object with the selected product attributes and information
   */
  setProductProperties = (map: Array<number> = []): ProductProperties => {
    try {
      const productProperties: ProductProperties = {
        uuid: null,
        price: parseFloat(this._product.price),
        price_without_discount: parseFloat(
          this._product.price_without_discount,
        ),
        discount: parseFloat(this._product.discount),
        stock: this._product.quantity,
        quantity: DEFAULT_QUANTITY,
      };

      // If the product has attributes. Search for corresponding properties.
      if (this._product.attributes.length) {
        // Iterate through all product variables tree
        // with the 'map' of selected values
        map.reduce(
          (variationAccumulator, currentVariation) => {
            //------------------------------------------------------
            // TODO: This is bad code. Just to fix a bug tha must be corrected on backend.
            if (!variationAccumulator.variations) return variationAccumulator;
            //------------------------------------------------------

            // Get product variations.
            const element = variationAccumulator.variations[currentVariation];

            // If current attribute has unique stock and price, select this information.
            if (element.attribute_stock !== null) {
              productProperties.stock = element.attribute_stock.quantity;
              productProperties.price = parseFloat(
                element.attribute_stock.price,
              );
              productProperties.price_without_discount = parseFloat(
                element.attribute_stock.price_without_discount,
              );
              productProperties.discount = parseFloat(
                element.attribute_stock.discount,
              );
            }

            // If this is the last level tree, we should use attribute id.
            if (element.hasOwnProperty('attribute_uuid')) {
              productProperties.uuid = element.attribute_uuid;
            }
            //
            return { variations: element.children };
          },
          { variations: this._product.attributes },
        );

        // If selected product is 'made on demand'
        // it must use the 'main product quantity'.
        if (this._product.productMadeOnDemand) {
          productProperties.stock = this._product.quantity;
        }
      }

      // Return Information to populate Selected Product
      this._selectedProduct = {
        ...this._selectedProduct,
        ...productProperties,
        attributeSelectionMap: map,
      };
      return productProperties;
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  /**
   * setShippingSchedule()
   * Update selected product shipping schedule
   * @param {number} scheduleID : Shipping schedule id
   * @return {number}
   */
  setShippingSchedule = (
    scheduleID: number,
    currentTime: Object = DayJs(),
  ): number => {
    const shippingSchedule = this._product.physical_properties.shipping_schedules.find(
      (schedule) => schedule.id === scheduleID,
    );

    //First, search if shipping schedule id exist
    if (shippingSchedule === undefined) {
      throw new Error(
        `ShippingScheduleError: Invalid shipping schedule id: ${scheduleID}`,
      );
    }

    // If shipping date is today, verify that schipping schedule is valid
    if (
      DayJs(this._selectedProduct.shipping_date).isSame(DayJs(), 'day') &&
      !isValidShippingScheduleTime(
        shippingSchedule.limit_to_order,
        DayJs(currentTime).format('HH:mm:ss'),
        this._selectedProduct.shipping_date,
      )
    ) {
      throw new Error(
        `ShippingScheduleError: Shipping schedule limit to order has already past(${shippingSchedule.limit_to_order}) : ${scheduleID}`,
      );
    }

    this._selectedProduct = {
      ...this._selectedProduct,
      shipping_schedule: scheduleID,
    };
    return scheduleID;
  };

  /**
   * setShippingDate()
   * Update selected product shipping date
   * @param {string} shipping_date : Shipping schedule date
   * @return {object} A list with shipping date details and options
   */
  setShippingDate = (
    shipping_date: string,
    currentTime: Object = DayJs(),
  ): ShippingDateType => {
    // Not defined
    if (!shipping_date) {
      throw new Error('ShippingDateError: Shipping date not defined');
    }
    // Past dates
    if (DayJs(shipping_date).isBefore(DayJs(), 'day')) {
      throw new Error('ShippingDateError: Invalid Shipping date');
    }
    // Days past product´s maximum_shipping_date
    if (
      DayJs(shipping_date).isAfter(
        DayJs(this._product.physical_properties.maximum_shipping_date),
        'day',
      )
    ) {
      throw new Error(
        `ShippingDateError: Shipping must be before: ${this._product.physical_properties.maximum_shipping_date}`,
      );
    }
    // Days before product´s minimum_shipping_date
    if (
      DayJs(shipping_date).isBefore(
        DayJs(this._product.physical_properties.minimum_shipping_date),
      )
    ) {
      throw new Error(
        `ShippingDateError: Shipping date must be on or after: ${this._product.physical_properties.minimum_shipping_date}`,
      );
    }

    // Search for available shipping schedules for this shipoping_date
    const availableShippingSchedules: Array<ShippingScheduleType> = this._product.physical_properties.shipping_schedules.filter(
      (schedule: ShippingScheduleType) =>
        isValidShippingScheduleTime(
          schedule.limit_to_order,
          DayJs(currentTime).format('HH:mm:ss'),
          shipping_date,
        ),
    );

    // Throw an error if there´s no shipping schedules available
    let shipping_schedule = null;
    let shippingDetails = {
      availableShippingSchedules,
      shipping_schedule,
      shipping_date,
    };

    // Update selected product
    this._selectedProduct = { ...this._selectedProduct, ...shippingDetails };

    // Set default shipping schedule if there´s any available for this date,
    if (availableShippingSchedules.length) {
      shippingDetails = {
        availableShippingSchedules,
        shipping_schedule: this.setShippingSchedule(
          availableShippingSchedules[0].value,
          DayJs(currentTime),
        ),
        shipping_date,
      };

      // Update selected product with a default shipping schedule
      this._selectedProduct = { ...this._selectedProduct, ...shippingDetails };
    }

    return shippingDetails;
  };

  /**
   * setQuantity()
   * Update selected product shipping date
   * @param {string} shipping_date : Shipping schedule date
   * @return {object} An object literal with new products quantity
   */
  setQuantity = (quantity: number): number => {
    // We can make some validations here...
    let newQuantity = quantity < 1 ? 1 : quantity;
    newQuantity =
      newQuantity > this.getSelectedProduct().stock
        ? this.getSelectedProduct().stock
        : newQuantity;

    // Update selected product
    this._selectedProduct = { ...this._selectedProduct, quantity: newQuantity };
    return newQuantity;
  };

  /**
   * getAttributeSelectionMap()
   * Get users attribute choices for product
   * @return {array} An array containing attribute indexes.
   */
  getAttributeSelectionMap = (): Array<number> => {
    return this._selectedProduct.attributeSelectionMap;
  };

  /**
   * getSelectedProduct()
   * Get elected product
   * @return {SelectedProductType}
   */
  getSelectedProduct = (): SelectedProductType => {
    return this._selectedProduct;
  };
}
