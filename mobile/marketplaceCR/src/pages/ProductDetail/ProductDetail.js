import React, { useRef, Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DayJs from 'dayjs';
import {
  Image,
  View,
  TouchableOpacity,
  Platform,
  ScrollView,
  Text,
} from 'react-native';
import Swiper from 'react-native-swiper';
import ZoomImage from 'react-native-zoom-image';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import RNPickerSelect from 'react-native-picker-select';
import * as Sentry from '@sentry/react-native';

import { PRODUCT_TYPES } from '../../redux/constants/app.constants';
import { storeActions, shoppingCartActions } from '../../redux/actions';
import Loader from '../../components/Loader';
import ProductsList from '../../components/ProductsList';
import { formatDate, formatDateToUTC } from '../../utils/date';
import Counter from '../../components/Counter';
import BadgeRating from '../../components/BadgeRating';
import PageRibbon from '../../components/PageRibbon';
import ReviewsList from '../../components/StoreScreen/Reviews/ReviewsList';
import ProductDetailManager, {
  SELECTED_PRODUCT_MODEL,
} from './ProductDetailManager';
import type {
  ProductDetailType,
  SelectedProductType,
} from './ProductDetailManager';
import { getDaysBetweenDates, isValidWorkingDay } from '../../utils/date';
import done from '../../images/icon/done.png';
import visaIMG from '../../images/assets/VISALogo--blue.png';

//Styles
import { style, pickerSelectStyles } from './style';
import { calendarTheme } from '../../assets';

// DS
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';

//will be added to the ds
import { CRRowLeft, CRRowBoth } from '../../components/Layout';
import { CRBottomButton } from '../../components/CRBottomButton';
import { CRTabWrapper, CRTab } from '../../components/CRTabs';
import { styles } from '../../components/ProductsListStyle';

import { CRIcon } from '../../components/CRIcon';

//Google Analytics
import analytics from '@react-native-firebase/analytics';

// Calendar configuration
LocaleConfig.locales.es = {
  monthNames: [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ],
  monthNamesShort: [
    'Enr.',
    'Feb.',
    'Mar.',
    'Abr.',
    'May.',
    'Jun.',
    'Jul.',
    'Ago.',
    'Sept.',
    'Oct.',
    'Nov.',
    'Dic.',
  ],
  dayNames: [
    'Domingo',
    'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado',
  ],
  dayNamesShort: ['Dom.', 'Lun.', 'Mar.', 'Mier.', 'Jue.', 'Vie.', 'Sab.'],
  today: 'Hoy',
};
LocaleConfig.defaultLocale = 'es';

declare var globalThis: any;

//Check if slider has bullets and adjusts the height
export const slideHeight = (productPhotos: number): number => {
  const heightBullets = 370;
  const heightNoBullets = 320;
  if (productPhotos > 1) {
    return heightBullets;
  } else {
    return heightNoBullets;
  }
};

// Counter Icons
const minusIcon = (isMinusDisabled, touchableDisabledColor, touchableColor) => {
  return (
    <CRIcon
      name="minus--small"
      size={45}
      color={isMinusDisabled ? touchableDisabledColor : touchableColor}
    />
  );
};
const plusIcon = (isPlusDisabled, touchableDisabledColor, touchableColor) => {
  return (
    <CRIcon
      name="plus--small"
      size={45}
      color={isPlusDisabled ? touchableDisabledColor : touchableColor}
    />
  );
};

/**
 * shuffleArray()
 * Suffles the contents of an array.
 * @param {array:*} array
 */
function shuffleArray(array: Array<any>): Array<any> {
  let i: number = array.length - 1;
  for (; i > 0; i--) {
    const j: number = Math.floor(Math.random() * (i + 1));
    const temp: Array<any> = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

/**
 * ProductDetail : Class Component
 */
type ProductDetailProps = {|
  navigation: { getParam: Function },
  fetchProductDetail: Function,
  fetchProducts: Function,
  updateCartProduct: Function,
  product: ProductDetailType,
  productsList: Array<ProductDetailType>,
  reviews: { results: Array<{}>, count: number, rating: number },
  pendingReviews: { results: Array<{}>, count: number, rating: number },
|};
type ProductDetailState = {
  shouldRenderProduct: boolean,
  shouldRenderReviews: boolean,
  shouldRenderMore: boolean,
  selectedProduct: SelectedProductType,
  add: boolean,
  addColor: string,
};
export class ProductDetail extends Component<
  ProductDetailProps,
  ProductDetailState,
> {
  productManager: ProductDetailManager;
  bestSeller: boolean = false;

  constructor(props) {
    super(props);

    const { navigation, route } = props;
    const { itemStoreSlug, itemProductSlug } = route.params;
    this.storeSlug = itemStoreSlug;
    this.productSlug = itemProductSlug;
    this.history = '';
    this._swiper = null;
    this.state = {
      shouldRenderProduct: true,
      shouldRenderReviews: false,
      shouldRenderMore: false,
      selectedProduct: SELECTED_PRODUCT_MODEL,
      add: false,
      addColor: 'colorMain300',
      indexSlider: 0,
    };
  }

  componentWillUnmount() {
    this.props = {};
    this.setState({});
    // console.log('SAliendo');
  }

  /*   UNSAFE_componentWillReceiveProps(nextProps) {
    const { navigation, route } = nextProps;
    console.log("NextProps:", nextProps)
    const { itemStoreSlug, itemProductSlug } = route.params;
    console.log("RouteParams: ", route.params)
    if (itemProductSlug && this.history !== itemProductSlug) {
      this.productSlug = itemProductSlug;
      console.log("productSlug: ", this.productSlug)
      this.history = itemProductSlug;
      console.log("history: ", this.history)
      this.setState({ ...this.state });
      this.loadNewContent();
    }
  } */

  loadNewContent() {
    // Fetch product detail.
    this.props
      .fetchProductDetail(this.storeSlug, this.productSlug)
      .then((response) => {
        this.productManager = new ProductDetailManager(this.props.product);
        this.setState({
          ...this.state,
          selectedProduct: this.productManager.getSelectedProduct(),
        });
      });

    // Fetch other products from the same store.
    this.props.fetchProducts(this.storeSlug);
  }

  /**
   * componentDidMount()
   */
  async componentDidMount() {
    await this.loadNewContent();
    await this.getReviews(this.storeSlug, this.productSlug);
    await this.props.getPendingProductReviews(this.storeSlug, this.productSlug);
    await analytics().setCurrentScreen('ProductDetailScreen');
    await analytics().logEvent('ProductDetailScreen');
    await this.viewItemAnalytics();
  }

  async viewItemAnalytics() {
    //logViewItem - Firebase Analytics
    await analytics().logViewItem({
      value: Number(this.props.product.price_without_discount),
      currency: this.props.product.currency.value,
      items: [
        {
          item_brand: this.props.product.store.name,
          item_id: this.props.product.id.toString(),
          item_name: this.props.product.name,
          //item_category: 'round necked t-shirts',
        },
      ],
    });
  }

  /**
   * showProduct()
   * Updates state component, to show product detail tab.
   */
  showProduct = (): void => {
    this.setState({
      ...this.state,
      shouldRenderProduct: true,
      shouldRenderReviews: false,
      shouldRenderMore: false,
    });
  };

  /**
   * showReviews()
   * Updates state component, to show product reviews tab.
   */
  showReviews = (): void => {
    this.setState({
      ...this.state,
      shouldRenderReviews: true,
      shouldRenderProduct: false,
      shouldRenderMore: false,
    });
  };

  /**
   * showMore()
   * Updates state component, to show other product information tab.
   */
  showMore = (): void => {
    this.setState({
      ...this.state,
      shouldRenderMore: true,
      shouldRenderProduct: false,
      shouldRenderReviews: false,
    });
  };

  /**
   * showAddToCartConfirmation()
   * Shows a UI modal window to notificate users of addToCart success.
   */
  showAddToCartConfirmation = (selectedProduct) => {
    if (this.state.add) {
      return (
        <View style={styles.doneContainer}>
          <Image source={done} style={styles.done} />
          <CRText
            variant={typography.subtitle3}
            color={colors.colorWhite}
            align="center"
          >
            ¡Añadimos este producto a tu carrito!
          </CRText>
        </View>
      );
    } else {
      return (
        <CRText
          variant={typography.subtitle3}
          color={colors.colorWhite}
          align="center"
        >
          {`Agregar ${selectedProduct.quantity} al carrito | $${parseFloat(
            selectedProduct.price * selectedProduct.quantity,
          ).toFixed(2)} MXN`}
        </CRText>
      );
    }
  };

  /**
   * addToCart()
   * Adds a product to the shopping cart into redux store
   * and sends it to backend through API
   */
  addToCart = async (): void => {
    const { selectedProduct } = this.state;
    // Configure product
    const product = {
      note: '',
      product: selectedProduct.slug,
      quantity: parseInt(selectedProduct.quantity, 10),
      attribute: selectedProduct.uuid ? selectedProduct.uuid.uuid : null,
      physical_properties: {
        shipping_schedule: selectedProduct.shipping_schedule,
        shipping_date: selectedProduct.shipping_date,
      },
    };
    console.log('Producto: ', product);
    //logAddToCart - Firebase Analytics
    analytics().logAddToCart({
      value: Number(selectedProduct.price_without_discount),
      currency: this.props.product.currency.value,
      items: [
        {
          item_brand: this.props.product.store.name,
          item_id: this.props.product.id.toString(),
          item_name: this.props.product.name,
          //item_category: 'round necked t-shirts',
          //item_variant: '',
        },
      ],
    });

    // Show UI status notification
    this.setState({
      ...this.state,
      add: true,
      addColor: 'colorGreen300',
    });

    /**
     * TODO: Validate if this product is already on the shopping cart.
     * If so, we could notify users or just add one more unit to the cart.
     */
    /*let compare = JSON.stringify(product);

    if (currentProduct === compare) {
  
      Alert.alert(
        'Añadir nuevo producto',
        'Ya tienes este producto en tu carrito, si quieres añadir más actualiza la cantidad.',
        [
          {
            text: 'Aceptar',
          },
        ],
      );
    } else {
      currentProduct = compare;*/
    /**
     * END TODO
     */

    // Add Product to ShoppingCart
    await this.props
      .updateCartProduct(product)
      .catch((e) => Sentry.captureException(e));

    // Wait some time before hidding UI status notification
    setTimeout(() => {
      this.setState({
        ...this.state,
        add: false,
        addColor: 'colorMain300',
      });
    }, 3000);
  };

  /**
   * onQuantityChange()
   * Update product quantity on Manager and UI view.
   * @param {int} quantity | Integer Number.
   */
  onQuantityChange = (quantity: number): void => {
    // Update product manager
    this.productManager.setQuantity(quantity);

    // Update view
    this.setState({
      ...this.state,
      selectedProduct: this.productManager.getSelectedProduct(),
    });
  };

  /**
   * onAttributeChange()
   * Attribute change handler
   * @param {int} option | New Option
   */
  onAttributeChange = (option: { value: number, name: string }): void => {
    const { selectedProduct } = this.state;

    // Find and update corresponding attributes selected map with the values.
    const attributeSelectionMap = selectedProduct.attributesList.map(
      (item, i) =>
        item.ref === option.name
          ? option.value
          : this.productManager.getAttributeSelectionMap()[i],
    );

    // Get matching product & update component
    this.productManager.setProductProperties(attributeSelectionMap);
    this.setState({
      ...this.state,
      selectedProduct: this.productManager.getSelectedProduct(),
    });
  };

  /**
   * onDeliveryScheduleChange()
   * @param {string} schedule | Delivery schedule
   */
  onScheduleChange = (schedule: number): void => {
    this.productManager.setShippingSchedule(schedule);

    // Update selected product properties
    this.setState({
      ...this.state,
      selectedProduct: this.productManager.getSelectedProduct(),
    });
  };

  /**
   * onCalendarDayChange()
   * @param {string} selectedDate | Selected Date from calendar
   */
  onCalendarDayChange = (selectedDate: { dateString: string }): void => {
    this.productManager.setShippingDate(selectedDate.dateString);

    // Update selected delivery date.
    this.setState({
      ...this.state,
      selectedProduct: this.productManager.getSelectedProduct(),
    });
  };

  getReviews = (store, product, page) => {
    this.props.getProductReviews(
      {
        store_slug: store,
        product_slug: product,
      },
      `?page=1&page_size=10`,
    );
  };

  /**
   * render()
   * @returns JSX
   */
  render() {
    const {
      product,
      productsList,
      reviews,
      pendingReviews,
      navigation,
    } = this.props;
    const {
      selectedProduct,
      shouldRenderProduct,
      shouldRenderReviews,
      shouldRenderMore,
    } = this.state;

    // Define props
    const isIos: boolean = Platform.OS === 'ios';
    const isAndroid: boolean = Platform.OS === 'android';
    //Remodified for the bottom cart bar
    const SPACER_SIZE_IOS: number = 80;
    const SPACER_SIZE_ANDROID: number = 90;
    const shuffledProducts: Array<any> = shuffleArray(productsList);
    const placeholder = {
      label: 'Selecciona una Opción: ',
      value: 0,
      name: '',
      color: '#9EA0A4',
    };

    // Dates Configuration
    let minimumShippingDate: Object = DayJs();
    let maximumShippingDate: Object = DayJs();
    let markedDates = {};
    if (product.loaded) {
      // Define minimum/maximun shipping dates.
      minimumShippingDate = product.physical_properties.minimum_shipping_date;
      maximumShippingDate = product.physical_properties.maximum_shipping_date;

      // Get a list of days between a date interval.
      const datesRange: Array<string> = getDaysBetweenDates(
        DayJs().format('YYYY-MM-DD'),
        DayJs(maximumShippingDate).format('YYYY-MM-DD'),
      );

      // Define the status of each day between selected dates range.
      // This is used to mark and control dates interaction on calendar.
      markedDates = datesRange.reduce((datesAccumulator, currentDate) => {
        const enableDate = isValidWorkingDay(currentDate, product);
        return {
          ...datesAccumulator,
          [currentDate]: {
            disabled: !enableDate,
            disableTouchEvent: !enableDate,
          },
        };
      }, {});
    }
    // Return JSX
    return (
      <View style={style.productDetailWrapper}>
        {!product.loaded ? (
          <Loader />
        ) : (
          <View>
            <ScrollView>
              <View style={style.container}>
                {/* STORE HEADER */}
                <TouchableOpacity
                  onPress={() => {
                    // navigation.dispatch(resetAction),
                    // navigation.navigate('StoreScreen', {
                    //   itemStore: product.store.slug,
                    // });

                    globalThis.ReactApplication.ActiveScreen(
                      'StoreScreen',
                      {
                        itemStore: product.store.slug,
                      },
                      navigation,
                    );
                  }}
                >
                  <CRRowBoth addedStyle={style.storeHeader}>
                    <Image
                      source={{ uri: product.store.photo.small }}
                      style={style.storePhoto}
                    />

                    <CRText
                      color={colors.colorDark100}
                      variant={typography.paragraph}
                    >
                      {product.store.name}
                    </CRText>
                  </CRRowBoth>
                </TouchableOpacity>
                {/* END: STORE HEADER */}

                {/* PRODUCT GALLERY */}
                <Swiper
                  horizontal={true}
                  style={style.wrapper}
                  // dotStyle={style.bullet}
                  // activeDotStyle={style.bulletActive}
                  showsButtons={false}
                  loop={false}
                  height={slideHeight(product.photos.length)}
                  ref={(swiper) => {
                    this._swiper = swiper;
                  }}
                  index={this.state.indexSlider}
                  showsPagination={false}
                  // autoplay={true}
                >
                  {product.photos.map((item, i) => (
                    <View key={`${item.id}`} style={style.bannerContainer}>
                      <ZoomImage
                        source={{ uri: item.photo.medium }}
                        imgStyle={{ width: '100%', height: '100%' }}
                        style={style.imageStyle}
                        enableScaling={true}
                      />
                      <View style={style.badgeStyle}>
                        <BadgeRating product={product} />
                      </View>
                      <View style={style.bestSellerStyle}>
                        {this.bestSeller && <PageRibbon />}
                      </View>
                    </View>
                  ))}
                </Swiper>
                {product.photos.length > 1 && (
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {product.photos.map((item, i) => (
                      <TouchableOpacity
                        key={i}
                        onPress={() =>
                          this.setState({
                            ...this.state,
                            indexSlider: i,
                          })
                        }
                        style={style.touchThumb}
                      >
                        <Image
                          source={{ uri: item.photo.medium }}
                          style={style.thumb}
                        ></Image>
                      </TouchableOpacity>
                    ))}
                  </ScrollView>
                )}

                {/* END: PRODUCT GALLERY */}

                {/* PRODUCT NAME */}
                <CRRowBoth>
                  <CRText
                    variant={typography.subtitle}
                    color={colors.colorDark200}
                    align="center"
                  >
                    {product.name}
                  </CRText>
                </CRRowBoth>
                {/* END: PRODUCT NAME */}
                {selectedProduct.attributesList.length === 0 ? (
                  <Fragment>
                    {/*PRICE --NO ATTRIBUTES*/}
                    {selectedProduct.discount > 0 ? (
                      <CRRowBoth>
                        <CRText
                          variant={typography.subtitle2}
                          color={'#1A1F71'}
                          align="center"
                        >
                          <Text
                            style={{
                              textDecorationLine: 'line-through',
                              textDecorationStyle: 'solid',
                            }}
                          >
                            $
                            {parseFloat(
                              selectedProduct.price_without_discount,
                            ).toFixed(2)}{' '}
                            {product.currency.value}
                          </Text>
                          <CRText
                            variant={typography.subtitle2}
                            color={'#1A1F71'}
                            align="center"
                          >
                            {/* {' '}Sólo con <Image source={visaIMG} style={style.visa} /> */}
                          </CRText>
                        </CRText>
                      </CRRowBoth>
                    ) : (
                      <View />
                    )}

                    <CRRowBoth>
                      <CRText
                        variant={typography.subtitle}
                        color={colors.colorsDark300}
                        align="center"
                      >
                        ${parseFloat(selectedProduct.price).toFixed(2)}{' '}
                        {product.currency.value}
                      </CRText>
                    </CRRowBoth>

                    {/*STOCK --NO ATTRIBUTES*/}
                    <CRRowBoth>
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark100}
                        align="center"
                      >
                        IVA incluido
                      </CRText>
                    </CRRowBoth>

                    {selectedProduct.stock === 0 ? (
                      <Fragment />
                    ) : (
                      /*COUNTER --NO ATTRIBUTES*/
                      <CRRowBoth addedStyle={style.counter}>
                        <Counter
                          start={1}
                          minusIcon={minusIcon}
                          plusIcon={plusIcon}
                          max={selectedProduct.stock}
                          min={1}
                          onChange={this.onQuantityChange}
                        />
                      </CRRowBoth>
                    )}
                    <CRRowBoth>
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark100}
                        align="center"
                      >
                        Inventario disponible:{' '}
                        <CRText
                          variant={typography.paragraph}
                          color={colors.colorMain300}
                        >
                          {selectedProduct.stock}
                        </CRText>
                      </CRText>
                    </CRRowBoth>
                  </Fragment>
                ) : (
                  /*PRICE --WITH ATTRIBUTES*/
                  <Fragment>
                    {/* {product.discount > 0 ? (
                      <CRRowBoth>
                        <CRText
                          variant={typography.paragraph}
                          color={colors.colorAqua300}
                          align="center"
                        >
                          <Text
                            style={{
                              //textDecorationLine: 'line-through',
                              textDecorationStyle: 'solid',
                            }}
                          >
                            $
                            {parseFloat(
                              selectedProduct.price,
                            ).toFixed(2)}{' '}
                            {product.currency.value}
                          </Text>
                          <CRText
                            variant={typography.bold}
                            color={colors.colorAqua300}
                            align="center"
                          >
                            {' '}
                            -{parseFloat(product.discount).toFixed(0)}% Sólo con Visa{' '}
                          </CRText>
                        </CRText>
                      </CRRowBoth>
                    ) : (
                      <View />
                    )} */}
                    <CRRowBoth>
                      <CRText
                        variant={typography.subtitle}
                        color={colors.colorsDark300}
                        align="center"
                      >
                        <Text
                          style={{
                            //textDecorationLine: 'line-through',
                            textDecorationStyle: 'solid',
                          }}
                        >
                          $
                          {parseFloat(
                            selectedProduct.price_without_discount,
                          ).toFixed(2)}{' '}
                          {product.currency.value}
                        </Text>
                      </CRText>
                    </CRRowBoth>
                    {product.discount > 0 ? (
                      <CRRowBoth>
                        <CRText
                          variant={typography.subtitle2}
                          color={'#1A1F71'}
                          align="center"
                        >
                          <Text
                            style={{
                              //textDecorationLine: 'line-through',
                              textDecorationStyle: 'solid',
                            }}
                          >
                            ${parseFloat(selectedProduct.price).toFixed(2)}{' '}
                            {product.currency.value}
                          </Text>
                          <CRText
                            variant={typography.subtitle2}
                            color={'#1A1F71'}
                            align="center"
                          >
                            {' '}
                            -{parseInt(product.discount)}%
                          </CRText>
                        </CRText>
                      </CRRowBoth>
                    ) : (
                      <View />
                    )}

                    {/* ATTRIBUTES */}
                    <View style={style.pickerWrapper}>
                      {selectedProduct.attributesList.map((attribute, i) => (
                        <View key={`${attribute.ref}`}>
                          <RNPickerSelect
                            placeholder={placeholder}
                            items={attribute.options.map((item, j) => ({
                              key: `${item}${j}`,
                              label: `${attribute.type}: ${item}`,
                              value: { name: attribute.ref, value: j },
                            }))}
                            onValueChange={this.onAttributeChange}
                            style={{
                              ...pickerSelectStyles,
                              iconContainer: {
                                top: 13,
                                right: 12,
                              },
                            }}
                            value={{
                              name: attribute.ref,
                              value: this.productManager.getAttributeSelectionMap()[
                                i
                              ],
                            }}
                            Icon={() => {
                              return (
                                <CRIcon
                                  name="angle--down--rounded"
                                  size={20}
                                  color="gray"
                                />
                              );
                            }}
                          />
                        </View>
                      ))}
                    </View>
                    {/* END: ATTRIBUTES */}

                    {/* STOCK */}
                    <CRRowBoth>
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark100}
                        align="center"
                      >
                        Inventario disponible:{' '}
                        <CRText
                          variant={typography.paragraph}
                          color={colors.colorMain300}
                        >
                          {selectedProduct.stock}
                        </CRText>
                      </CRText>
                    </CRRowBoth>

                    {selectedProduct.stock === 0 ? (
                      <View style={style.containerNoStock} />
                    ) : (
                      <View style={style.containerStock}>
                        <Counter
                          start={1}
                          minusIcon={minusIcon}
                          plusIcon={plusIcon}
                          max={selectedProduct.stock}
                          min={1}
                          onChange={this.onQuantityChange}
                        />
                      </View>
                    )}
                    {/* END: STOCK */}
                  </Fragment>
                )}
              </View>

              {/* SHIPPING OPTIONS */}
              <View style={style.shippingContainer}>
                {/* SHIPPING DATES */}
                <Fragment>
                  <CRRowBoth style={style.txtDetail}>
                    <CRText
                      variant={typography.subtitle2}
                      color={colors.colorDark300}
                      align="center"
                    >
                      Elige el día de entrega
                    </CRText>
                  </CRRowBoth>
                  <CRRowBoth>
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorDark100}
                      align="center"
                    >
                      Tiempo de Elaboración:{' '}
                      {product.physical_properties.fabrication_time} dias.
                    </CRText>
                  </CRRowBoth>

                  {/* VACATIONS */}
                  {product.store.is_on_vacation && (
                    <View style={style.containerInfoDeliveryDate}>
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark300}
                        align="center"
                      >
                        Estamos de vacaciones, volvemos el:{' '}
                      </CRText>
                      {product.store.vacations_ranges.map((item, i) => (
                        <CRText
                          variant={typography.bold}
                          color={colors.colorDark300}
                          align="center"
                          key={i}
                        >
                          {formatDate(formatDateToUTC(new Date(item.end)))}
                        </CRText>
                      ))}
                    </View>
                  )}
                  {/*END: VACATIONS */}

                  {/* CALENDAR */}
                  <CRRowBoth style={style.calendarContainer}>
                    <Calendar
                      style={style.calendar}
                      theme={calendarTheme}
                      hideExtraDays
                      minDate={minimumShippingDate}
                      maxDate={maximumShippingDate}
                      onDayPress={this.onCalendarDayChange}
                      markedDates={{
                        ...markedDates,
                        [selectedProduct.shipping_date]: {
                          selected: true,
                          disableTouchEvent: true,
                        },
                      }}
                    />
                  </CRRowBoth>
                  {/*END CALENDAR*/}
                </Fragment>
                {/* END: SHIPPING DATES */}

                {/* SHIPPING SCHEDULES */}
                <Fragment>
                  <CRRowLeft addedStyle={style.logoContainerShiping}>
                    {selectedProduct.availableShippingSchedules.map(
                      (schedule) => (
                        <TouchableOpacity
                          key={schedule.id}
                          style={
                            schedule.value === selectedProduct.shipping_schedule
                              ? style.btnShippingActive
                              : style.btnShipping
                          }
                          onPress={() => this.onScheduleChange(schedule.value)}
                        >
                          <CRText
                            variant={typography.paragraph}
                            color={
                              schedule.value ===
                              selectedProduct.shipping_schedule
                                ? colors.colorWhite
                                : colors.colorDark100
                            }
                            align="center"
                          >
                            {schedule.schedules.delivery}
                          </CRText>
                        </TouchableOpacity>
                      ),
                    )}
                  </CRRowLeft>
                </Fragment>
                {/* END OF SHIPPING SCHEDULES */}
              </View>
              {/* END: SHIPPING OPTIONS */}

              {/* TAB NAV BAR */}
              <Fragment>
                <View>
                  <CRTabWrapper>
                    <CRTab
                      title="Detalle"
                      isActive={shouldRenderProduct}
                      onPress={() => this.showProduct()}
                    />
                    <CRTab
                      title="Reseñas"
                      isActive={shouldRenderReviews}
                      onPress={() => this.showReviews()}
                    />
                    <CRTab
                      title="Más productos"
                      isActive={shouldRenderMore}
                      onPress={() => this.showMore()}
                    />
                  </CRTabWrapper>
                  <View style={style.divider} />
                </View>
              </Fragment>
              {/* END: TAB NAV BAR */}

              {/* Description */}
              {shouldRenderProduct && (
                <View style={style.descContainer}>
                  <CRRowBoth addedStyle={style.description}>
                    <CRText
                      variant={typography.subtitle2}
                      color={colors.colorDark300}
                      align="left"
                    >
                      Detalle del Producto
                    </CRText>
                  </CRRowBoth>
                  <CRRowBoth addedStyle={style.description}>
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorDark100}
                      align="left"
                    >
                      {product.description}
                    </CRText>
                  </CRRowBoth>
                  <CRRowBoth addedStyle={style.productDimensions}>
                    <CRText
                      variant={typography.subtitle3}
                      color={colors.colorDark300}
                      align="left"
                    >
                      Dimensiones del Producto
                    </CRText>
                  </CRRowBoth>
                  <CRRowBoth addedStyle={style.description}>
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorDark100}
                      align="left"
                    >
                      {product.physical_properties.size.display_name}
                    </CRText>
                  </CRRowBoth>
                </View>
              )}
              {/* END: Description & Physical Properties */}

              {/* REVIEWS */}
              {shouldRenderReviews && (
                <View style={style.descContainer}>
                  <ReviewsList
                    reviews={reviews.results}
                    pendingReviews={pendingReviews.results}
                    reviewsCount={reviews.count}
                    rating={reviews.rating}
                    purchase={{}}
                  />
                  {isAndroid && (
                    <View style={{ height: SPACER_SIZE_ANDROID }} />
                  )}
                </View>
              )}
              {/* END: REVIEWS */}

              {/* MORE PRODUCTS */}
              {shouldRenderMore && (
                <View style={style.descContainer}>
                  <CRRowBoth>
                    <CRText
                      variant={typography.subtitle2}
                      color={colors.colorDark300}
                      align="center"
                    >
                      Más productos de {product.store.name}
                    </CRText>
                  </CRRowBoth>
                  <CRRowLeft>
                    {productsList.length > 0 ? (
                      <ProductsList
                        products={shuffledProducts.slice(0, 10)}
                        navigation={navigation}
                        {...this.props}
                      />
                    ) : (
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark100}
                      >
                        Aún no hay productos en esta sección
                      </CRText>
                    )}
                  </CRRowLeft>
                </View>
              )}
              {/* END: MORE PRODUCTS */}

              {/* SPACERS */}
              {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
              {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
              {/* END: SPACERS */}
            </ScrollView>

            {/* FOOTER */}
            <CRBottomButton
              onPress={this.addToCart}
              backgroundColor={this.state.addColor}
              attitude={selectedProduct.stock === 0 ? 'disabled' : ''}
              customContainerBtn={false}
            >
              {this.showAddToCartConfirmation(selectedProduct)}
              {/* <TouchableOpacity
                      style={style.btnAddToCart}
                      activeOpacity={0.5}
                      onPress={this.addToCart}
                    >
                      <View
                        style={{
                          paddingVertical: 15,
                          paddingHorizontal: 10,
                          flexDirection: 'row',
                          justifyContent: 'space-around',
                          alignItems: 'center',
                        }}
                      >
                        <Icon name={'ios-cart'} size={26} color={colors.colorWhite}>
                          <CRText variant={typography.subtitle3} color={colors.colorWhite} align='center'>
                            {' '}
                            Agregar al Carrito
                          </CRText>
                        </Icon>
                      </View>
                    </TouchableOpacity>
              */}
            </CRBottomButton>
            {/*            <CRBottomButton
              onPress={this.addToCart}
              backgroundColor={this.state.addColor}
              attitude={selectedProduct.stock === 0 ? 'disabled' : ''}
            >
              {this.showAddToCartConfirmation(selectedProduct)}
              {console.log("Producto Dos: ",selectedProduct)}

              <TouchableOpacity
                      style={style.btnAddToCart}
                      activeOpacity={0.5}
                      onPress={this.addToCart}
                    >
                      <View
                        style={{
                          paddingVertical: 15,
                          paddingHorizontal: 10,
                          flexDirection: 'row',
                          justifyContent: 'space-around',
                          alignItems: 'center',
                        }}
                      >
                        <Icon name={'ios-cart'} size={26} color={colors.colorWhite}>
                          <CRText variant={typography.subtitle3} color={colors.colorWhite} align='center'>
                            {' '}
                            Agregar al Carrito
                          </CRText>
                        </Icon>
                      </View>
                    </TouchableOpacity>
            </CRBottomButton>
              */}
            {/* END: FOOTER */}
          </View>
        )}
      </View>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { store, app } = state;
  const product: ProductDetailType = state.store.productDetail;

  return {
    product,
    store: state.store.data,
    productsList:
      store.productsList.results && store.productsList.results.length
        ? store.productsList.results.filter(
            (p) => p.product_type.value === PRODUCT_TYPES.PHYSICAL,
          )
        : [],
    deliveryMorning: app.pickupSchedules[0],
    deliveryEvening: app.pickupSchedules[1],
    reviews: state.store.reviews.product,
    pendingReviews: state.store.pending_reviews.product,
  };
}

function mapDispatchToProps(dispatch) {
  const {
    fetchProductDetail,
    fetchProducts,
    getProductReviews,
    getPendingProductReviews,
  } = storeActions;
  const { updateCartProduct } = shoppingCartActions;

  return bindActionCreators(
    {
      fetchProductDetail,
      fetchProducts,
      updateCartProduct,
      getProductReviews,
      getPendingProductReviews,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);

//
