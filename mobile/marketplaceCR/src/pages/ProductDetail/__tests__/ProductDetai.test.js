/**
 * @jest-environment jsdom
 */
// import 'module:react-native';
// import React from 'react';

// import renderer from 'react-test-renderer';

// it('renders correctly', () => {
//   const tree = renderer.create(<ProductDetail />).toJSON();
//   expect(tree).toMatchSnapshot();
// });
import React from 'react';
import { mount, shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';

import { ProductDetail } from '../ProductDetail';

const middlewares = [thunk]; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {};

const actions = {
  // This would fetch a resource on the network.
  fetchProductDetail: (storeSlug, productSlug) => async (
    dispatch,
    getState,
    api,
  ) => {
    // Dispatch Action
    // dispatch({
    //   type: storeActionsType.FETCH_PRODUCT_DETAIL_REQUEST,
    // });
    const promesa = await new Promise((res, rej) => {
      res({ test: 'flight' });
    });
    return promesa;
  },
};
const reducer = (state = { list: [] }, action) => {
  switch (action.type) {
    case 'RECEIVE_LIST':
      return { ...state, list: action.list };
    default:
      return state;
  }
};

describe('<ProductDetail />', () => {
  let wrapper;

  beforeEach(() => {
    //moxios.install();
    const store = mockStore(initialState);

    wrapper = mount(
      <ProductDetail
        navigation={{
          getParam: (e) => {},
        }}
        fetchProductDetail={store.dispatch(actions.fetchProductDetail)}
        fetchProducts={(e) => {}}
        product={{}}
        productsList={[]}
        reviews={{}}
        pendingReviews={{}}
      />,
      { context: { store } },
    );
  });
  afterEach(() => {
    //moxios.uninstall(axiosInstance);
    //moxios.uninstall();
  });

  //   it('Render component <ProductDetail />', () => {
  //     expect(wrapper).not.toBeNull();
  //   });

  it('Render component <ProductDetail />', () => {
    // moxios.wait(() => {

    //   const request = moxios.requests.mostRecent();
    //   request.respondWith({
    //     status: 200,
    //     response: { userProfileMock: 'asdasdasd' },
    //   });
    // });

    expect(wrapper).not.toBeNull();
  });
});
