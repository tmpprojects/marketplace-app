export default {
  id: 13783,
  attributes: [
    {
      id: 313472,
      value: 'Atún',
      children: [
        {
          id: 313473,
          value: 'Nombre de festejado',
          children: [
            {
              id: 313474,
              value: 'Cono estrellas',
              children: [
                {
                  id: 313475,
                  value: 'Verde',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263624,
                    uuid: '44YQaC_F~kpJbayHxHSR5J',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313476,
                  value: 'Morado',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263625,
                    uuid: 'VQiyH9q20J_XQhkJnZ~qgV',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313477,
                  value: 'Rojo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263626,
                    uuid: '30cBNw0uhzSP9rV0i1aiJq',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313478,
                  value: 'Amarillo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263627,
                    uuid: 'lIJJWklCHVT1xTotW~fiIG',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313479,
                  value: 'Azul',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263628,
                    uuid: 'QziGdo3qxZ51JZT8VdIgFu',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313480,
                  value: 'Rosa',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263629,
                    uuid: 'U8YZR6Ch~oTt8vf8TJmtA_',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
              attribute_type: 'personalizado',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
            },
            {
              id: 313481,
              value: 'Cono Happy Birthday',
              children: [
                {
                  id: 313482,
                  value: 'Verde',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263630,
                    uuid: 'jKT5W7BNYSWtz0ZZJpNsb4',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313483,
                  value: 'Morado',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263631,
                    uuid: '9wW5npLThbI5yAyEdZOvSK',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313484,
                  value: 'Rojo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263632,
                    uuid: 'KjJgj1aHd4VtHhT5N0TS0j',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313485,
                  value: 'Amarillo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263633,
                    uuid: 'yOvjBH8Urys4icVie_GAJ2',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313486,
                  value: 'Azul',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263634,
                    uuid: '35~1l1ENva6HmJ4Mp5nq72',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313487,
                  value: 'Rosa',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263635,
                    uuid: 'myEnIaTJsqN7NQOG6D04Qo',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
              attribute_type: 'personalizado',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
            },
          ],
          attribute_type: 'personalizado',
          attribute_uuid: null,
          attribute_stock: null,
          attribute_extradata: null,
        },
      ],
      attribute_type: 'sabor',
      attribute_uuid: null,
      attribute_stock: null,
      attribute_extradata: null,
    },
    {
      id: 313488,
      value: 'Pollo',
      children: [
        {
          id: 313489,
          value: 'Nombre de festejado',
          children: [
            {
              id: 313490,
              value: 'Cono estrellas',
              children: [
                {
                  id: 313491,
                  value: 'Verde',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263636,
                    uuid: 'REsgWuEc9hANXAwOaq4qnv',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313492,
                  value: 'Morado',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263637,
                    uuid: 'MPq4E7l~beczxz0t_OXsy8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313493,
                  value: 'Rojo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263638,
                    uuid: '6HrvUXNDYYYH1Lam_rRptP',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313494,
                  value: 'Amarillo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263639,
                    uuid: '6XO7Au7djLEY57mUO4PY0g',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313495,
                  value: 'Azul',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263640,
                    uuid: 'YrpXrF7CO4EAMJQyJyQ3iS',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313496,
                  value: 'Rosa',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263641,
                    uuid: '9uVgY0XJC5Xnx6~1jd8gcx',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
              attribute_type: 'personalizado',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
            },
            {
              id: 313497,
              value: 'Cono Happy Birthday',
              children: [
                {
                  id: 313498,
                  value: 'Verde',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263642,
                    uuid: 'AbgzWRtxweVDeDsf4DrpXC',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313499,
                  value: 'Morado',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263643,
                    uuid: 'HOE1iWFRA9Gv7QZZdwRL_E',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313500,
                  value: 'Rojo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263644,
                    uuid: 'mIOO8k410PXf1stRYOBFHZ',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313501,
                  value: 'Amarillo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263645,
                    uuid: 'BST2M8uPU~DuZSfgnsYpN3',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313502,
                  value: 'Azul',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263646,
                    uuid: '6Qa8fQcvL0vC8lc8VqDJST',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313503,
                  value: 'Rosa',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263647,
                    uuid: 'wie~hpTlD87clYFycW7G8k',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
              attribute_type: 'personalizado',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
            },
          ],
          attribute_type: 'personalizado',
          attribute_uuid: null,
          attribute_stock: null,
          attribute_extradata: null,
        },
      ],
      attribute_type: 'sabor',
      attribute_uuid: null,
      attribute_stock: null,
      attribute_extradata: null,
    },
    {
      id: 313504,
      value: 'Res',
      children: [
        {
          id: 313505,
          value: 'Nombre de festejado',
          children: [
            {
              id: 313506,
              value: 'Cono estrellas',
              children: [
                {
                  id: 313507,
                  value: 'Verde',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263648,
                    uuid: 'SvDeg71UffQVNcwtrZlsJo',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313508,
                  value: 'Morado',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263649,
                    uuid: 'hKbFLHB6_OqIOPr1ObuXOO',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313509,
                  value: 'Rojo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263650,
                    uuid: 'RXcaoISf3Up_uysyIWdpDO',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313510,
                  value: 'Amarillo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263651,
                    uuid: 'BuOabPGnB11kpSrSdMJHyo',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313511,
                  value: 'Azul',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263652,
                    uuid: '9OdufCxf2Ve3Fc7iI2y1H4',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313512,
                  value: 'Rosa',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263653,
                    uuid: 'cy3e99unyAEpNn2QhkvYRj',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
              attribute_type: 'personalizado',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
            },
            {
              id: 313513,
              value: 'Cono Happy Birthday',
              children: [
                {
                  id: 313514,
                  value: 'Verde',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263654,
                    uuid: '0v3nfPqVnGt7HkyAIqzPEQ',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313515,
                  value: 'Morado',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263655,
                    uuid: 'E377C3Ky6CBeCkNwm9IaVH',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313516,
                  value: 'Rojo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263656,
                    uuid: 'LsuOUR7Sb1_pM8S9oeJgIE',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313517,
                  value: 'Amarillo',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263657,
                    uuid: 'EXazPdcRiZyVNRHxhGzdAt',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313518,
                  value: 'Azul',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263658,
                    uuid: 'ErL_OXNYerJy22MiIIzwXh',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 313519,
                  value: 'Rosa',
                  attribute_type: 'color',
                  attribute_uuid: {
                    id: 263659,
                    uuid: 'mZx7UuWB6TUO9PaEIH8KVY',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
              attribute_type: 'personalizado',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
            },
          ],
          attribute_type: 'personalizado',
          attribute_uuid: null,
          attribute_stock: null,
          attribute_extradata: null,
        },
      ],
      attribute_type: 'sabor',
      attribute_uuid: null,
      attribute_stock: null,
      attribute_extradata: null,
    },
  ],
  attributes_change_price: false,
  call_to_action_label: '',
  category: [75, 32],
  created: '2019-06-29T14:38:54.112526-05:00',
  currency: { value: 'MXN', display_name: 'MXN - Peso mexicano' },
  description:
    'Celebra a tu mejor amigo con productos 100% naturales sin conservadores, sal o azúcar. El paquete incluye:\r\n\r\n1 pastel en forma de hueso\r\n1 hueso de huellitas con helio\r\n1 gorro de festejado\r\n\r\nSelecciona el color de orilla del pastel y no olvides agregar el nombre del festejado, para personalizar tu pedido.',
  edited: '2019-07-01T13:31:16.375925-05:00',
  excerpt: '',
  event_properties: null,
  is_for_men: false,
  is_for_women: false,
  name: 'Paquete básico',
  photos: [
    {
      id: 33205,
      file: 'a2478e25ee3123e9471bed6264d049af.JPG',
      photo: {
        big:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/a2478e25ee3123e9471bed6264d049af-thumbnail-1120x1120-90.JPG',
        original:
          'https://canastarosa.s3.amazonaws.com/media/market/product/a2478e25ee3123e9471bed6264d049af.JPG',
        medium:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/a2478e25ee3123e9471bed6264d049af-thumbnail-560x560-90.JPG',
        small:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/a2478e25ee3123e9471bed6264d049af-crop-c0-5__0-5-280x280-90.JPG',
      },
      order: 0,
    },
    {
      id: 33206,
      file: 'c4cfb9bf28130fab331742659b6715ad.jpg',
      photo: {
        big:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/c4cfb9bf28130fab331742659b6715ad-thumbnail-1120x1120-90.jpg',
        original:
          'https://canastarosa.s3.amazonaws.com/media/market/product/c4cfb9bf28130fab331742659b6715ad.jpg',
        medium:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/c4cfb9bf28130fab331742659b6715ad-thumbnail-560x560-90.jpg',
        small:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/c4cfb9bf28130fab331742659b6715ad-crop-c0-5__0-5-280x280-90.jpg',
      },
      order: 0,
    },
    {
      id: 33208,
      file: 'e6619f52d485675997e75b7793d4e562.jpg',
      photo: {
        big:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/e6619f52d485675997e75b7793d4e562-thumbnail-1120x1120-90.jpg',
        original:
          'https://canastarosa.s3.amazonaws.com/media/market/product/e6619f52d485675997e75b7793d4e562.jpg',
        medium:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/e6619f52d485675997e75b7793d4e562-thumbnail-560x560-90.jpg',
        small:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/e6619f52d485675997e75b7793d4e562-crop-c0-5__0-5-280x280-90.jpg',
      },
      order: 0,
    },
    {
      id: 33209,
      file: 'abe7575f3b66c63cdd3770b4dc50dce5.JPG',
      photo: {
        big:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/abe7575f3b66c63cdd3770b4dc50dce5-thumbnail-1120x1120-90.JPG',
        original:
          'https://canastarosa.s3.amazonaws.com/media/market/product/abe7575f3b66c63cdd3770b4dc50dce5.JPG',
        medium:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/abe7575f3b66c63cdd3770b4dc50dce5-thumbnail-560x560-90.JPG',
        small:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/abe7575f3b66c63cdd3770b4dc50dce5-crop-c0-5__0-5-280x280-90.JPG',
      },
      order: 0,
    },
    {
      id: 33210,
      file: '640a20d1bcbcf34d56434d892d4eafa3.JPG',
      photo: {
        big:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/640a20d1bcbcf34d56434d892d4eafa3-thumbnail-1120x1120-90.JPG',
        original:
          'https://canastarosa.s3.amazonaws.com/media/market/product/640a20d1bcbcf34d56434d892d4eafa3.JPG',
        medium:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/640a20d1bcbcf34d56434d892d4eafa3-thumbnail-560x560-90.JPG',
        small:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/640a20d1bcbcf34d56434d892d4eafa3-crop-c0-5__0-5-280x280-90.JPG',
      },
      order: 0,
    },
    {
      id: 33211,
      file: '31476da03af7731663453b3b607dc1d1.JPG',
      photo: {
        big:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/31476da03af7731663453b3b607dc1d1-thumbnail-1120x1120-90.JPG',
        original:
          'https://canastarosa.s3.amazonaws.com/media/market/product/31476da03af7731663453b3b607dc1d1.JPG',
        medium:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/31476da03af7731663453b3b607dc1d1-thumbnail-560x560-90.JPG',
        small:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/31476da03af7731663453b3b607dc1d1-crop-c0-5__0-5-280x280-90.JPG',
      },
      order: 0,
    },
    {
      id: 33212,
      file: '37865172af61119d1b63a604703eb063.jpg',
      photo: {
        big:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/37865172af61119d1b63a604703eb063-thumbnail-1120x1120-90.jpg',
        original:
          'https://canastarosa.s3.amazonaws.com/media/market/product/37865172af61119d1b63a604703eb063.jpg',
        medium:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/37865172af61119d1b63a604703eb063-thumbnail-560x560-90.jpg',
        small:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/37865172af61119d1b63a604703eb063-crop-c0-5__0-5-280x280-90.jpg',
      },
      order: 0,
    },
  ],
  physical_properties: {
    fabrication_time: 2,
    is_available: { value: 2, display_name: 'Disponible Bajo Pedido' },
    life_time: 1,
    minimum_shipping_date: '2020-05-05',
    maximum_shipping_date: '2020-08-09',
    shipping: [],
    shipping_schedules: [
      {
        id: 2,
        name: 'Mañana',
        limit_to_order: '08:00:00',
        collection_start: '09:00:00',
        collection_end: '13:00:00',
        delivery_start: '10:00:00',
        delivery_end: '14:00:00',
      },
      {
        id: 4,
        name: 'Tarde',
        limit_to_order: '13:00:00',
        collection_start: '15:00:00',
        collection_end: '19:00:00',
        delivery_start: '16:00:00',
        delivery_end: '20:00:00',
      },
    ],
    size: { value: 2, display_name: 'Pequeño' },
    weight: '0.000',
    zones: [{ name: 'Area Metropolitana', slug: 'area-metropolitana' }],
  },
  price: '350.00',
  product_type: { value: 'physical', display_name: 'Objeto Físico' },
  quantity: 10,
  reviews: { count: 0, rating: 0 },
  section: 3210,
  sku: '',
  slug: 'paquete-chico-81008111',
  status: { value: 'public', display_name: 'Público (Visible para todos)' },
  store: {
    id: 483,
    cover: {
      big:
        'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/cover/cce658e61fec8509e74f4e342db2fa39-crop-c0-5__0-5-2560x537-90.jpg',
      medium:
        'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/cover/cce658e61fec8509e74f4e342db2fa39-crop-c0-5__0-5-1280x268-90.jpg',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/cce658e61fec8509e74f4e342db2fa39.jpg',
      thumbnail:
        'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/cover/cce658e61fec8509e74f4e342db2fa39-crop-c0-5__0-5-360x224-90.jpg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/cover/cce658e61fec8509e74f4e342db2fa39-crop-c0-5__0-5-980x205-90.jpg',
    },
    is_on_vacation: false,
    name: 'Rehilete Repostería',
    photo: {
      small:
        'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/photo/9f2c88ac0f50b67d86ab2a120550f2f0-crop-c0-5__0-5-480x480-90.jpg',
      medium:
        'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/photo/9f2c88ac0f50b67d86ab2a120550f2f0-crop-c0-5__0-5-680x680-90.jpg',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/9f2c88ac0f50b67d86ab2a120550f2f0.jpg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/photo/9f2c88ac0f50b67d86ab2a120550f2f0-crop-c0-5__0-5-960x960-90.jpg',
    },
    slogan: 'Repostería fina para mascotas',
    slug: 'rehilete-reposteria',
    vacations_ranges: [],
    work_schedules: [
      {
        week_day: { value: 5, display_name: 'Sábado' },
        start: '10:00:00',
        end: '14:00:00',
      },
      {
        week_day: { value: 4, display_name: 'Viernes' },
        start: '11:00:00',
        end: '18:00:00',
      },
      {
        week_day: { value: 3, display_name: 'Jueves' },
        start: '11:00:00',
        end: '18:00:00',
      },
      {
        week_day: { value: 2, display_name: 'Miércoles' },
        start: '11:00:00',
        end: '18:00:00',
      },
      {
        week_day: { value: 1, display_name: 'Martes' },
        start: '11:00:00',
        end: '18:00:00',
      },
      {
        week_day: { value: 0, display_name: 'Lunes' },
        start: '11:00:00',
        end: '18:00:00',
      },
    ],
  },
  tags: [],
  work_schedules: [
    {
      week_day: { value: 5, display_name: 'Sábado' },
      start: '10:00:00',
      end: '14:00:00',
    },
    {
      week_day: { value: 4, display_name: 'Viernes' },
      start: '11:00:00',
      end: '18:00:00',
    },
    {
      week_day: { value: 3, display_name: 'Jueves' },
      start: '11:00:00',
      end: '18:00:00',
    },
    {
      week_day: { value: 2, display_name: 'Miércoles' },
      start: '11:00:00',
      end: '18:00:00',
    },
    {
      week_day: { value: 1, display_name: 'Martes' },
      start: '11:00:00',
      end: '18:00:00',
    },
    {
      week_day: { value: 0, display_name: 'Lunes' },
      start: '11:00:00',
      end: '18:00:00',
    },
  ],
};
