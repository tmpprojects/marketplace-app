/**
 * @jest-environment jsdom
 */
import DayJs from 'dayjs';

import { getProductDetail } from '../../../redux/selectors/store.selectors';
import type {
  SelectedProductType,
  ShippingDateType,
} from '../../../ProductTypes';
import ProductDetailManager from '../ProductDetailManager';
import PRODUCT_MOCK from './product-mock';

const formattedProduct = getProductDetail(PRODUCT_MOCK);
const attributesListMock = [
  {
    type: 'sabor',
    ref: 'sabor0_sbx',
    values: ['Atún', 'Pollo', 'Res'],
    options: ['Atún', 'Pollo', 'Res'],
  },
  {
    type: 'personalizado',
    ref: 'personalizado1_sbx',
    values: ['Nombre de festejado'],
    options: ['Nombre de festejado'],
  },
  {
    type: 'personalizado',
    ref: 'personalizado2_sbx',
    values: ['Cono estrellas', 'Cono Happy Birthday'],
    options: ['Cono estrellas', 'Cono Happy Birthday'],
  },
  {
    type: 'color',
    ref: 'color3_sbx',
    values: ['Verde', 'Morado', 'Rojo', 'Amarillo', 'Azul', 'Rosa'],
    options: ['Verde', 'Morado', 'Rojo', 'Amarillo', 'Azul', 'Rosa'],
  },
];

describe('ProductDetailManager Class', () => {
  let Manager: ProductDetailManager;
  const attributeSelectionMap: Array<number> = [0, 0, 0, 0];
  let selectedProductMock: SelectedProductType;
  let shippingDate = DayJs().add(3, 'day'); // <-- Delivery in 3 days from today
  let minimumShippingDate: string = shippingDate
    .subtract(1, 'day')
    .format('YYYY-MM-DD');

  // Setup
  beforeEach(() => {
    Manager = new ProductDetailManager({
      ...formattedProduct,
      physical_properties: {
        ...formattedProduct.physical_properties,
        minimum_shipping_date: minimumShippingDate, // <-- Minimun shipping date is 1 day before delivery date
      },
    });
    selectedProductMock = {
      uuid: attributeSelectionMap.reduce(
        (childAttribute, childIndex, index) => {
          if (index === 0) {
            return formattedProduct.attributes[childIndex];
          }
          return childAttribute.children[childIndex];
        },
        {},
      ).attribute_uuid,
      slug: formattedProduct.slug,
      price: parseFloat(formattedProduct.price),
      quantity: 1,
      stock: formattedProduct.quantity,
      shipping_date: Manager.getSelectedProduct().shipping_date,
      shipping_schedule:
        formattedProduct.physical_properties.shipping_schedules[0].id,
      attributeSelectionMap,
      attributesList: attributesListMock,
      availableShippingSchedules:
        formattedProduct.physical_properties.shipping_schedules,
    };
  });

  //
  it('Creates instance of ProductDetailManager correctly', () => {
    expect(Manager).not.toBeNull();
  });

  /**
   * PRODUCT SETUP
   */
  describe('PRODUCT SETUP: ', () => {
    it('Parses attributes tree into a flat list', () => {
      const attributesList = Manager._parseAttributeTypesIntoSelectOptions(
        formattedProduct.attributes,
      );
      expect(attributesList).toStrictEqual(attributesListMock);
    });
    it('Gets a flattened attributes-tree indexes map to selectedProduct properties', () => {
      expect(Manager.getAttributeSelectionMap()).toEqual(attributeSelectionMap);
    });
    it('Can set attributes with different depth-levels', () => {
      let attributes;
      let attributesMock;

      // Try with an empty list. (No attributes/variations)
      attributes = [];
      attributesMock = [];
      expect(Manager.setAttributes(attributes)).toStrictEqual(attributesMock);

      // Try with one attribute
      attributes = [
        {
          id: 313485,
          value: 'Amarillo',
          attribute_type: 'color',
          attribute_uuid: {
            id: 263633,
            uuid: 'yOvjBH8Urys4icVie_GAJ2',
            is_visible: true,
          },
          attribute_stock: null,
          attribute_extradata: null,
        },
        {
          id: 234678,
          value: 'Azul',
          attribute_type: 'color',
          attribute_uuid: {
            id: 135678,
            uuid: 'BasddH8fUryf_4icasd',
            is_visible: true,
          },
          attribute_stock: null,
          attribute_extradata: null,
        },
      ];
      attributesMock = [
        {
          type: 'color',
          ref: 'color0_sbx',
          values: ['Amarillo', 'Azul'],
          options: ['Amarillo', 'Azul'],
        },
      ];
      expect(Manager.setAttributes(attributes)).toStrictEqual(attributesMock);

      // Try with nested attributes
      attributes = [
        {
          id: 313485,
          value: 'Amarillo',
          attribute_type: 'color',
          attribute_uuid: null,
          attribute_stock: null,
          attribute_extradata: null,
          children: [
            {
              id: 123456,
              value: 'Grande',
              attribute_type: 'talla',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
              children: [
                {
                  id: 345678,
                  value: 'Algodón',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 134563,
                    uuid: 'Ury_yOvjB_s4yOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 876543,
                  value: 'Franela',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 192836,
                    uuid: 'OvjB_s4Ury_yyOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
            },
          ],
        },
        {
          id: 176567,
          value: 'Azul',
          attribute_type: 'color',
          attribute_uuid: {
            id: 456864,
            uuid: 'ddH8fUryf_4icBasasd',
            is_visible: true,
          },
          attribute_stock: null,
          attribute_extradata: null,
          children: [
            {
              id: 876567,
              value: 'Grande',
              attribute_type: 'talla',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
              children: [
                {
                  id: 654567,
                  value: 'Algodón',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 357976,
                    uuid: 's4yOvjBH8Ury_yOvjB_',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 9876435,
                  value: 'Franela',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 765345,
                    uuid: 'y_yyOvjOvjB_s4UrBH8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
            },
          ],
        },
      ];
      attributesMock = [
        {
          type: 'color',
          ref: 'color0_sbx',
          values: ['Amarillo', 'Azul'],
          options: ['Amarillo', 'Azul'],
        },
        {
          type: 'talla',
          ref: 'talla1_sbx',
          values: ['Grande'],
          options: ['Grande'],
        },
        {
          type: 'tela',
          ref: 'tela2_sbx',
          values: ['Algodón', 'Franela'],
          options: ['Algodón', 'Franela'],
        },
      ];
      expect(Manager.setAttributes(attributes)).toStrictEqual(attributesMock);
    });
  });

  /**
   * PRODUCT INTERACTION
   */
  describe('PRODUCT INTERACTION: ', () => {
    it('Gets selected product properties from a specific attribute selection', () => {
      let selectionMap;
      let productPropertiesMock;

      // Sets a new product variation selection to get corresponding attribute properties.
      selectionMap = [0, 0, 1, 2];
      productPropertiesMock = {
        uuid: selectionMap.reduce((childAttribute, childIndex, index) => {
          if (index === 0) {
            return formattedProduct.attributes[childIndex];
          }
          return childAttribute.children[childIndex];
        }, {}).attribute_uuid,
        price: parseFloat(formattedProduct.price),
        quantity: 1,
        stock: formattedProduct.quantity,
      };
      expect(Manager.setProductProperties(selectionMap)).toStrictEqual(
        productPropertiesMock,
      );

      // Expect selectedProduct object to be updated correctly.
      const selectedProduct: SelectedProductType = {
        ...selectedProductMock,
        ...productPropertiesMock,
        attributeSelectionMap: selectionMap,
      };
      expect(Manager.getSelectedProduct()).toStrictEqual(selectedProduct);
    });

    it('Gets a default selected product', () => {
      expect(Manager.getSelectedProduct()).toStrictEqual(selectedProductMock);
    });

    /**
     * UPDATES QUANTIT.
     */
    describe('UPDATES QUANTITY', () => {
      it('Updates quantity', () => {
        expect(Manager.setQuantity(5)).toBe(5);
      });

      it('Does not exceed product stock', () => {
        expect(Manager.setQuantity(900)).toBe(formattedProduct.quantity);
      });

      it('Is greater than 1 unit', () => {
        expect(Manager.setQuantity(0)).toBe(1);
      });
    });

    /**
     * ATTRIBUTES DON´T MODIFY PRODUCT PROPS.
     */
    describe('ATTRIBUTES THAT DON´T MODIFY PRODUCT PROPS.', () => {
      let selectionMap = [0, 0, 1];
      let attributes = [
        {
          id: 313485,
          value: 'Amarillo',
          attribute_type: 'color',
          attribute_uuid: null,
          attribute_stock: null,
          attribute_extradata: null,
          children: [
            {
              id: 123456,
              value: 'Grande',
              attribute_type: 'talla',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
              children: [
                {
                  id: 345678,
                  value: 'Algodón',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 134563,
                    uuid: 'Ury_yOvjB_s4yOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 876543,
                  value: 'Franela',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 192836,
                    uuid: 'OvjB_s4Ury_yyOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
            },
          ],
        },
      ];
      let productPropertiesMock = {
        quantity: 1,
        uuid: selectionMap.reduce((childAttribute, childIndex, index) => {
          if (index === 0) {
            return attributes[childIndex];
          }
          return childAttribute.children[childIndex];
        }, {}).attribute_uuid,
        price: formattedProduct.price,
        stock: formattedProduct.quantity,
      };
      const NewProduct = new ProductDetailManager({
        ...formattedProduct,
        attributes,
        productMadeOnDemand: false,
      });

      it('Get attribute uuid and general product price/stock', () => {
        expect(NewProduct.setProductProperties(selectionMap)).toStrictEqual(
          productPropertiesMock,
        );
      });

      it('Quantity does not exceed general product stock', () => {
        expect(NewProduct.setQuantity(500)).toBe(formattedProduct.quantity);
      });
    });

    /**
     * ATTRIBUTES MODIFY PRICE AND STOCK
     */
    describe('ATTRIBUTES THAT MODIFY PRICE AND STOCK. ', () => {
      const attributes = [
        {
          id: 313485,
          value: 'Amarillo',
          attribute_type: 'color',
          attribute_uuid: null,
          attribute_stock: null,
          attribute_extradata: null,
          children: [
            {
              id: 123456,
              value: 'Grande',
              attribute_type: 'talla',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
              children: [
                {
                  id: 345678,
                  value: 'Algodón',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 134563,
                    uuid: 'Ury_yOvjB_s4yOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: {
                    quantity: 23,
                    price: 300,
                  },
                  attribute_extradata: null,
                },
                {
                  id: 876543,
                  value: 'Franela',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 192836,
                    uuid: 'OvjB_s4Ury_yyOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: {
                    quantity: 70,
                    price: 200,
                  },
                  attribute_extradata: null,
                },
              ],
            },
          ],
        },
      ];
      const selectionMap = [0, 0, 1];
      const attributeDetails = selectionMap.reduce(
        (childAttribute, childIndex, index) => {
          if (index === 0) {
            return attributes[childIndex];
          }
          return childAttribute.children[childIndex];
        },
        {},
      );

      it('Get product´s attribute uuid, stock and price.', () => {
        // Sets a new product variation selection to get corresponding attribute properties.
        const NewProduct = new ProductDetailManager({
          ...formattedProduct,
          attributes,
          productMadeOnDemand: false,
        });
        expect(NewProduct.setProductProperties(selectionMap)).toStrictEqual({
          quantity: 1,
          uuid: attributeDetails.attribute_uuid,
          price: attributeDetails.attribute_stock.price,
          stock: attributeDetails.attribute_stock.quantity,
        });
      });

      it('Quantity does not exceed product attribute´s stock', () => {
        // Sets a new product variation selection to get corresponding attribute properties.
        const NewProduct = new ProductDetailManager({
          ...formattedProduct,
          attributes,
          productMadeOnDemand: false,
        });
        NewProduct.setProductProperties(selectionMap);
        expect(NewProduct.setQuantity(500)).toBe(
          attributeDetails.attribute_stock.quantity,
        );
      });

      /**
       * MADE ON DEMAND
       */
      describe('PRODUCT MADE ON DEMAND. ', () => {
        // Sets a new product variation selection to get corresponding attribute properties.
        const ProductOnDemandManager = new ProductDetailManager({
          ...formattedProduct,
          attributes,
          productMadeOnDemand: true,
        });

        it('Gets product´s main stock (ignore attribute stock if found).', () => {
          productPropertiesMock = {
            quantity: 1,
            uuid: attributeDetails.attribute_uuid,
            price: attributeDetails.attribute_stock.price,
            // !!!If product is made on demand, it should take the 'general product' stock.
            stock: formattedProduct.quantity,
          };
          expect(
            ProductOnDemandManager.setProductProperties(selectionMap),
          ).toStrictEqual(productPropertiesMock);
        });
      });
    });

    /**
     * WITHOUT ATTRIBUTES
     */
    describe('PRODUCT WITHOUT ATTRIBUTES', () => {
      it('Gets product´s stock, price, quantity (and ignore attribute uuid).', () => {
        // Sets a new product variation selection to get corresponding attribute properties.
        const NewProduct = new ProductDetailManager({
          ...formattedProduct,
          attributes: [],
          productMadeOnDemand: false,
        });
        expect(NewProduct.setProductProperties()).toStrictEqual({
          quantity: 1,
          uuid: null,
          price: formattedProduct.price,
          stock: formattedProduct.quantity,
        });
      });
    });

    /**
     * CHANGES DELIVERY DETAILS
     */
    describe('SETS DELIVERY DETAILS', () => {
      const NewProduct = new ProductDetailManager({
        ...formattedProduct,
        physical_properties: {
          ...formattedProduct.physical_properties,
          minimum_shipping_date:
            formattedProduct.physical_properties.minimum_shipping_date,
          shipping_schedules: [
            {
              ...formattedProduct.physical_properties.shipping_schedules[0],
              limit_to_order: '08:00:00', // <== Watch for limit time.
            },
            ...formattedProduct.physical_properties.shipping_schedules.slice(1),
          ],
        },
      });

      it('Changes delivery date', () => {
        const nextDate = DayJs(NewProduct.getSelectedProduct().shipping_date)
          .add(2, 'day')
          .format('YYYY-MM-DD');
        const shippingDateMock: ShippingDateType = {
          shipping_date: nextDate,
          availableShippingSchedules: NewProduct.getSelectedProduct()
            .availableShippingSchedules,
          shipping_schedule: NewProduct.getSelectedProduct()
            .availableShippingSchedules[0].value,
        };
        expect(NewProduct.setShippingDate(nextDate)).toStrictEqual(
          shippingDateMock,
        );
      });

      it('Throws and error if it receives past dates (relative from `today`).', () => {
        const yesterday = DayJs().subtract(1, 'day');
        expect(() => Manager.setShippingDate(yesterday)).toThrow(Error);
      });

      it('Throws and error if it receives a date past maximum_shipping_date.', () => {
        const oneYear = DayJs().add(1, 'year');
        expect(() => Manager.setShippingDate(oneYear)).toThrow(Error);
      });
      it('Throws and error if it receives a date before minimum_shipping_date.', () => {
        const beforeMinimumShippingDate = DayJs(minimumShippingDate).subtract(
          1,
          'day',
        );
        expect(() =>
          NewProduct.setShippingDate(beforeMinimumShippingDate),
        ).toThrow(Error);
      });

      it('Changes shipping schedule.', () => {
        const newShippingSchedule = 2;
        expect(Manager.setShippingSchedule(newShippingSchedule)).toStrictEqual(
          newShippingSchedule,
        );
      });
      it('Throws a error if it receives an invalid shipping schedule id.', () => {
        expect(() => Manager.setShippingSchedule(123)).toThrow(Error);
      });
      it('Throws and error if shipping schedule has already past today.', () => {
        const NewProduct = new ProductDetailManager({
          ...formattedProduct,
          physical_properties: {
            ...formattedProduct.physical_properties,
            minimum_shipping_date: DayJs().format('YYYY-MM-DD'), // <== Today.
            shipping_schedules: [
              {
                ...formattedProduct.physical_properties.shipping_schedules[0],
                limit_to_order: '08:00:00', // <== Watch for limit time.
              },
              ...formattedProduct.physical_properties.shipping_schedules.slice(
                1,
              ),
            ],
          },
        });
        expect(() =>
          NewProduct.setShippingSchedule(
            2,
            DayJs().hour(9).minute(0).second(0), // <== Fake current time AFTER schedule limit time.
          ),
        ).toThrow(Error);
      });
    });
  });
});
