import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { _customSetData, _setData, _getData } from '../../utils/asyncStorage';
import { CommonActions } from '@react-navigation/native';
import * as Sentry from '@sentry/react-native';
import {
  View,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  Button,
  Platform,
} from 'react-native';
import { field } from '../../components/FieldsForm/FieldsFormStyles';
import { styles } from './LoginScreenStyles';
import { required, email, password } from '../../utils/formValidators';
import { userActions } from '../../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRIcon } from '../../components/CRIcon';
import Icon from 'react-native-vector-icons/Ionicons';

//Google Analytics
import analytics from '@react-native-firebase/analytics';
declare var globalThis: any;

const renderField = ({
  keyboardType,
  meta: { touched, error, warning },
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
  formDataRedux,
  formSubmit,
}) => {
  const onSubmitEditing = function (nativeEvent) {
    if (
      formDataRedux.values.email &&
      formDataRedux.values.email !== null &&
      formDataRedux.values.email !== '' &&
      formDataRedux.values.password &&
      formDataRedux.values.password !== null &&
      formDataRedux.values.password !== ''
    ) {
      if (!formDataRedux.syncErrors) {
        formSubmit();
      }
    }
  };
  const isAndroid = Platform.OS === 'android';

  return (
    <View>
      <TextInput
        style={field.inputForm}
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray300}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        {...restInput}
        secureTextEntry={secureTextEntry}
        autoCapitalize={'none'}
        onSubmitEditing={({ nativeEvent }) => onSubmitEditing(nativeEvent)}
      />
      {(touched && error && (
        <View style={field.formErrors}>
          <CRText variant={typography.caption} color={colors.colorRed300}>
            {error}
          </CRText>
        </View>
      )) ||
        (warning && (
          <View style={field.formErrors}>
            <CRText variant={typography.caption} color={colors.colorYellow300}>
              {warning}
            </CRText>
          </View>
        ))}
    </View>
  );
};

export class LoginForm extends Component {
  constructor({ navigation }) {
    super();
    this.state = {
      showPass: true,
      press: false,
      success: false,
      userInput: null,
      value: null,
      user_name: '',
      token: '',
      profile_pic: '',
      email: '',
      userInfo: null,
      gettingLoginStatus: true,
    };
    // Bind Methods
    this.formSubmit = this.formSubmit.bind(this);
    // this.set = this.set.bind(this);
    // this.get = this.get.bind(this);
    // this.remove = this.remove.bind(this);
  }

  async componentDidMount() {
    await analytics().setCurrentScreen('LoginScreen');
    await analytics().logEvent('LoginScreen');
    this.props.initialize({
      email: (await _getData()).emailcache,
      // password: (await _getData()).passwordcache,
    });
  }

  showPass = () => {
    if (this.state.press === false) {
      this.setState({ showPass: false, press: true });
    } else {
      this.setState({ showPass: true, press: false });
    }
  };

  async formSubmit(formData) {
    try {
      const { email: mail, password: pwd } = formData;
      //this.set();
      // Try to log user in
      return this.props
        .loginUser(mail, pwd)
        .then(async (response) => {
          // await _setData(response.data);
          await _setData(response.data);
          await _customSetData('email', mail);
          await _customSetData('password', pwd);

          await this.setState({
            success: true,
          });

          await analytics().logLogin({
            method: 'Login User/Password',
          });

          //this.props.navigation.navigate('SplashScreenReload');
          globalThis.ReactApplication.ActiveScreen(
            'SplashScreenReload',
            {},
            this.props.navigation,
          );
        })
        .catch((e) => {
          Sentry.captureException(e);
          const message =
            // error.response.data.non_field_errors ||
            'Se produjo un error. Por favor intenta de nuevo más tarde.';
          throw new SubmissionError({
            _error: message,
          });
        });
    } catch (e) {
      Sentry.captureException(e);
    }
  }
  // changes = () => {

  // };

  render() {
    const { handleSubmit, error, pristine, submitting, valid } = this.props;
    try {
      return (
        <SafeAreaView style={styles.wrapper}>
          <KeyboardAwareScrollView
            contentContainerStyle={styles.scrollViewStyle}
            resetScrollToCoords={{ x: 0, y: 0 }}
            scrollEnabled
          >
            <View style={styles.inputContainer}>
              <Field
                name="email"
                keyboardType="email-address"
                placeholder="Correo electrónico"
                component={renderField}
                validate={[required, email]}
                autoFocus
                type="text"
                formSubmit={handleSubmit(this.formSubmit)}
                {...this.props}
              />
            </View>
            <View style={styles.inputContainer}>
              <Field
                name="password"
                keyboardType="default"
                placeholder="Contraseña"
                secureTextEntry={this.state.showPass}
                component={renderField}
                validate={[required]}
                type="password"
                //normalize={cleanPassword}
                onChange={(text) => {
                  this.setState({ userInput: text });
                }}
                formSubmit={handleSubmit(this.formSubmit)}
                {...this.props}
              />
              <TouchableOpacity
                style={field.btnEye}
                onPress={this.showPass.bind(this)}
              >
                <Icon
                  name={this.state.press === false ? 'ios-eye-off' : 'ios-eye'}
                  size={22}
                  color={colors.colorGray300}
                  activeOpacity={0.5}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={handleSubmit(this.formSubmit)}
              style={
                pristine || submitting || !valid
                  ? field.buttonSquareDisabled
                  : field.buttonSquare
              }
            >
              <CRText
                variant={typography.button}
                color={
                  pristine || submitting || !valid
                    ? colors.colorGray300
                    : colors.colorMain300
                }
                align="center"
              >
                Iniciar sesión
              </CRText>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.btnPass}
              activeOpacity={0.5}
              onPress={() => {
                // this.props.navigation.navigate('ForgetPassScreen')
                globalThis.ReactApplication.ActiveScreen(
                  'ForgetPassScreen',
                  {},
                  this.props.navigation,
                );
              }}
            >
              {error && (
                <View style={field.formErrors}>
                  <CRText
                    variant={typography.caption}
                    color={colors.colorRed300}
                    align="center"
                    style={styles.submitErrors}
                  >
                    {error}
                  </CRText>
                </View>
              )}
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark100}
                align="center"
              >
                ¿Olvidaste tu contraseña?
              </CRText>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                globalThis.ReactApplication.ActiveScreen(
                  'SignUpScreen',
                  {},
                  this.props.navigation,
                );

                //this.props.navigation.navigate('SignUpScreen')
              }}
            >
              <CRText
                variant={typography.bold}
                color={colors.colorDark300}
                align="center"
              >
                Crear una cuenta
              </CRText>
            </TouchableOpacity>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      );
    } catch (e) {
      Sentry.captureException(e);
    }
  }
}

LoginForm = reduxForm({
  form: 'login', //Un identificador unico para este form
})(LoginForm);

// Redux Map Functions
function mapStateToProps(state) {
  return {
    cookies: state.user.cookies,
    formDataRedux: state.form.login,
  };
}
function mapDispatchToProps(dispatch) {
  const {
    loginUser,
    getCurrentUser,
    listAddresses,

    getUserCards,
  } = userActions;

  return bindActionCreators(
    {
      loginUser,
      getCurrentUser,
      listAddresses,
      getUserCards,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
