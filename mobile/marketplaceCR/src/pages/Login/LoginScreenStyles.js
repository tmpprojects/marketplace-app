import { StyleSheet, Dimensions } from 'react-native';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

export const styles = StyleSheet.create({
  wrapper: {
    flexGrow: 1,
    backgroundColor: colors.colorWhite,
  },
  scrollViewStyle: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    alignItems: 'center',
  },
  btnPass: {
    marginTop: 30,
    marginBottom: 8,
  },
  submitErrors: {
    marginBottom: 10,
  },
  facebookButton: {
    width: 200,
    height: 47,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
    borderRadius: 3,
    backgroundColor: colors.facebookColor,
    ...typography.txtBold,
  },
  googleButton: {
    width: 200,
    height: 47,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
    borderRadius: 3,
  },
  googleButtonAND: {
    display: 'none',
  },
});
