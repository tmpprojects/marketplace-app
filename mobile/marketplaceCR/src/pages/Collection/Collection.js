import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, ScrollView, Text, Image, SafeAreaView } from 'react-native';
import { styles } from './CollectionStyles';
import { collectionActions } from './../../redux/actions';
import { CRProductCard, CRCard } from '../../components/CRCard';
import { CRThumb } from '../../components/CRThumb';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRRowLeft, CRRowFull } from '../../components/Layout';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { Spinner } from '../../utils/animations/Spinner';
import { typography } from '@canastarosa/ds-theme/typography';

import { formatTitle } from '../../utils/formatTitle';
import ZipCodeObserver from '../../components/ZipCodeObserver/ZipCodeObserver';

//Google Analytics
import analytics from '@react-native-firebase/analytics';

let stores = [];
let products = [];
let articles = [];

declare var globalThis: any;

function ScrollCard(props) {
  const { navigation, type, contentCollection } = props;
  return (
    <CRRowFull>
      <CRRowLeft addedStyle={styles.title}>
        <CRText variant={typography.subtitle2} color={colors.colorDark300}>
          {formatTitle(contentCollection.name.toLowerCase())}
        </CRText>
      </CRRowLeft>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={styles.carrousel}
      >
        {contentCollection.items.map((item, index) => {
          if (type === 'product') {
            return (
              <CRProductCard
                key={index}
                thumbImg={{ uri: item.product.photo.small }}
                productTitle={item.product.name}
                store={item.product.store.name}
                price={item.product.price}
                priceWithoutDiscount={item.product.price_without_discount}
                shippingMethod={
                  item.product.physical_properties.shipping_methods
                }
                //shippingMethod={[]}
                rating={
                  item.product.mean_product_score > 0 &&
                  item.product.mean_product_score.toFixed(1)
                }
                marginBottom={10}
                onPress={() => {
                  // navigation.navigate('ProductDetail', {
                  //   itemProductSlug: item.product.slug,
                  //   itemStoreSlug: item.product.store.slug,
                  // });

                  // navigation.navigate('ProductDetail', {
                  //   itemProductSlug: item.product.slug,
                  //   itemStoreSlug: item.product.store.slug,
                  // });

                  globalThis.ReactApplication.ActiveScreen(
                    'ProductDetail',
                    {
                      itemProductSlug: item.product.slug,
                      itemStoreSlug: item.product.store.slug,
                    },
                    props.navigation,
                  );
                }}
              />
            );
          } else {
            return (
              <View key={index}>
                <CRCard
                  key={index}
                  size="mini"
                  marginBottom={10}
                  onPress={() => {
                    // navigation.navigate('StoreScreen', {
                    //   itemStore: item.store.slug,
                    // });
                    // navigation.navigate('StoreScreen', {
                    //   itemStore: item.store.slug,
                    // });

                    globalThis.ReactApplication.ActiveScreen(
                      'StoreScreen',
                      {
                        itemStore: item.store.slug,
                      },
                      props.navigation,
                    );
                  }}
                >
                  <View style={styles.storeLogoWrapper}>
                    <CRThumb
                      source={{ uri: item.store.photo.small }}
                      imgHeight={100}
                      wrapperStyle={styles.storeLogo}
                      resizeMode="contain"
                    />
                  </View>
                </CRCard>
                <View style={styles.storeName}>
                  <CRText variant={typography.caption} align="center">
                    {item.store.name}
                  </CRText>
                </View>
              </View>
            );
          }
        })}
        {/*Right most margin*/}
        <CRRowLeft />
      </ScrollView>
    </CRRowFull>
  );
}

class Collection extends PureComponent {
  async loadContent() {
    await this.props.getCollection(this.slug);

    let { content } = this.props.collection;
    if (!Boolean(content)) {
      content = {
        listings: [],
        name: '',
      };
    }
    const stores = [];
    const products = [];
    const articles = [];
    if (Object.keys(content).length) {
      content.listings.forEach((a) => {
        // Depending of the type of listings, it will assign in the correct variable.
        switch (a.type) {
          case 'store':
            stores.push(a);
            break;
          case 'product':
            // Saves only the first item of the type of listings.
            products.push(a);
            break;
          case 'article':
            // Saves only the first item of the type of listings.
            articles.push(a);
            break;
          default:
        }
      });

      this.setState({
        stores,
        products,
        articles,
        loading: false,
      });
    }
  }
  constructor(props) {
    super(props);
    const { navigation, route } = props;
    const { slug } = route.params;
    this.state = {
      stores: [],
      products: [],
      articles: [],
      loading: true,
    };
    this.slug = slug;
    this.loadContent();
  }

  componentWillUnmount() {
    this.setState({});
    this.props = {};
  }

  render() {
    analytics().setCurrentScreen('CollectionScreen');
    analytics().logEvent('CollectionScreen');
    let { content } = this.props.collection;
    if (!Boolean(content)) {
      content = {
        listings: [],
        name: '',
      };
    }
    if (!this.state.loading) {
      return (
        <React.Fragment>
          <ZipCodeObserver
            observe="Collection"
            isFocused={() => {
              return this.props.navigation.isFocused();
            }}
            collection={this.slug}
            navigation={this.props.navigation}
          />
          <ScrollView style={styles.collectionWrapper}>
            <View style={styles.bannerContainer}>
              <Image
                source={{
                  uri: content.banner.small,
                }}
                style={styles.banner}
              />
              <CRText variant={typography.title} color={colors.colorWhite}>
                {formatTitle(content.name.toLowerCase())}
              </CRText>
            </View>
            {this.state.stores.map((currentCollection, index) => (
              <View style={styles.separation} key={index}>
                <ScrollCard
                  type="store"
                  contentCollection={currentCollection}
                  {...this.props}
                  key={index}
                />
              </View>
            ))}
            {this.state.products.map((currentCollection, index) => (
              <View style={styles.separation} key={index}>
                <ScrollCard
                  type="product"
                  contentCollection={currentCollection}
                  {...this.props}
                  key={index}
                />
              </View>
            ))}
          </ScrollView>
        </React.Fragment>
      );
    } else {
      return (
        <View style={styles.containerLoading}>
          <View style={styles.loader}>
            <Spinner light={true} />
            <Text style={styles.message}>Cargando...</Text>
          </View>
        </View>
      );
    }
  }
}

// Redux Map Functions
function mapStateToProps(state) {
  // const customerOrders = getCustomerOrdersList(state);
  // return {
  //   orders: customerOrders,
  // };
  return {
    collection: state.collection,
  };
}

const mapDispatchToProps = (dispatch) => {
  const { getCollection } = collectionActions;
  return bindActionCreators(
    {
      getCollection,
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Collection);
