import { StyleSheet, Platform } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';
import { typography, buttons } from '../../assets';
import { color } from 'react-native-reanimated';
import { Colors } from 'react-native/Libraries/NewAppScreen';
export const styles = StyleSheet.create({
  separation: {
    marginBottom: 30,
  },
  bannerContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    marginBottom: 30,
    backgroundColor: colors.colorDark300,
  },
  title: {
    marginTop: 20,
    marginBottom: 10,
  },
  subtitle: {
    fontSize: 22,
  },
  banner: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    opacity: 0.5,
    marginBottom: 60,
  },
  float: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  carrousel: { paddingLeft: 10 },
  storeLogoWrapper: {
    alignItems: 'center',
  },
  storeLogo: {
    width: 100,
    marginVertical: 5,
    borderRadius: 5,
  },
  storeName: {
    width: 100,
    alignItems: 'center',
    marginHorizontal: 5,
  },
  collectionWrapper: {
    backgroundColor: '#FAF7F5',
    marginBottom: Platform.OS === 'android' ? 50 : 60,
  },
  containerBtnAdd: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  btnAddToCart: {
    ...buttons.squareCart,
    width: '100%',
    height: 60,
  },
  txtAddToCart: {
    textAlign: 'center',
    ...typography.txtRegular,
    color: colors.colorWhite,
    fontSize: 18,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#323232',
  },
  containerLoading: {
    backgroundColor: colors.colorWhite,
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 70,
  },
  loader: {
    marginTop: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  message: {
    ...typography.txtLight,
    color: colors.colorGray400,
    textAlign: 'center',
    marginTop: 80,
  },
});
