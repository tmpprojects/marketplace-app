import { StyleSheet, Dimensions } from 'react-native';
import { typography } from '../../assets';
import { colors } from '@canastarosa/ds-theme/colors';

const { height: HEIGHT } = Dimensions.get('window');

export const styles = StyleSheet.create({
  header: {
    marginVertical: 20,
  },
  categoriesListContainer: {
    backgroundColor: '#FAF7F5',
    height: HEIGHT - 200,
  },

  categoryContainer: {
    flex: 2,
    height: 'auto',
    margin: 7,
    backgroundColor: colors.colorWhite,
    borderRadius: 5,
    shadowColor: colors.colorGray400,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  photoCategory: {
    width: '100%',
    height: 135,
    flex: 1,
    resizeMode: 'cover',
    marginVertical: 5,
  },
  category: {
    ...typography.txtSemiBold,
    textAlign: 'center',
    padding: 15,
  },
  arrowBtn: {
    top: 20,
    left: 15,
  },
  iconArrow: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    transform: [{ rotate: '180deg' }],
  },
});
