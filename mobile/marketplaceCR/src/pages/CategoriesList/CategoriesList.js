import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  TouchableOpacity,
  View,
  ScrollView,
  FlatList,
  Platform,
  Text,
} from 'react-native';

import { styles } from './CategoriesListStyle';
//DS
import { colors } from '@canastarosa/ds-theme/colors';
import { typography } from '@canastarosa/ds-theme/typography';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import CRCategoryCard from '../../components/CRCard/CRCategoryCard/CRCategoryCard';
import { CRRowLeft } from '../../components/Layout';
import { CRTitle } from '../../components/CRTitle';
import { formatTitle } from '../../utils/formatTitle';
import ZipCodeObserver from '../../components/ZipCodeObserver/ZipCodeObserver';
//Google Analytics
import analytics from '@react-native-firebase/analytics';
declare var globalThis: any;
export class CategoriesList extends Component {
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    await analytics().setCurrentScreen('CategoriesListScreen');
    await analytics().logEvent('CategoriesListScreen');
  }

  render() {
    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 30;
    const SPACER_SIZE_ANDROID = 30;
    const { categories } = this.props;
    const { navigation, route } = this.props;
    return (
      <View style={styles.categoriesListContainer}>
        <ZipCodeObserver
          observe="CategoriesList"
          isFocused={() => {
            return this.props.navigation.isFocused();
          }}
          navigation={this.props.navigation}
        />
        <ScrollView>
          <CRTitle title="Categorías" />
          <CRRowLeft>
            <FlatList
              data={categories}
              numColumns={2}
              renderItem={({ item, index }) => (
                <CRCategoryCard
                  key={index}
                  category={formatTitle(item.name.toLowerCase())}
                  thumbImg={item.photo.original}
                  size="half"
                  marginBottom={10}
                  onPress={() => {
                    //<Feed the back button
                    //<The symbol $ indicates that subsections exist
                    globalThis.ReactApplication.BackScreen.children = [];
                    globalThis.ReactApplication.ActiveScreen(
                      'CatalogScreen',
                      {
                        categoryName: item.name,
                        categorySlug: item.slug,
                      },
                      this.props.navigation,
                    );
                    //>Feed the back button
                    // this.props.navigation.navigate('CatalogScreen', {
                    //   categorySlug: item.slug,
                    //   categoryName: item.name,
                    // });
                  }}
                />
              )}
              keyExtractor={(item, index) => String(index)}
            />
          </CRRowLeft>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  const { app } = state;
  return {
    categories: app.categories.categories,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesList);
