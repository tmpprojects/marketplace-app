import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../assets';

const { width: WIDTH } = Dimensions.get('window');

export const styles = StyleSheet.create({
  wrapper: {
    flexGrow: 1,
    backgroundColor: colors.colorWhite,
  },
  scrollViewStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  message: {
    width: WIDTH - 120,
    marginBottom: 30,
  },
});
