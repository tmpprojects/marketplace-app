import React from 'react';
import { mount } from 'enzyme';

import ForgetPassScreen from './ForgetPassScreen';

describe('<ForgetPassScreen />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(<ForgetPassScreen />);
  });

  it('Render component <ForgetPassScreen />', () => {
    expect(wrapper).not.toBeNull();
  });
});
