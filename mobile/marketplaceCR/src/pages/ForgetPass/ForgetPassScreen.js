import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { userActions } from '../../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
  View,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  Alert,
} from 'react-native';
import * as Sentry from '@sentry/react-native';
import {
  required,
  email,
  normalizeToLowerCaseAndTrim,
} from '../../utils/formValidators';

import { field } from '../../components/FieldsForm/FieldsFormStyles';
import { styles } from './ForgetPassScreenStyles';
//DS
import { CRTitle } from '../../components/CRTitle';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
declare var globalThis: any;
const renderField = ({
  keyboardType,
  meta: { touched, error, warning },
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
}) => {
  return (
    <View>
      <TextInput
        style={field.inputForm}
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray300}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        {...restInput}
        secureTextEntry={secureTextEntry}
      />
      {(touched && error && (
        <View style={field.formErrors}>
          <CRText variant={typography.caption} color={colors.colorRed300}>
            {error}
          </CRText>
        </View>
      )) ||
        (warning && (
          <View style={field.formErrors}>
            <CRText variant={typography.caption} color={colors.colorYellow300}>
              {warning}
            </CRText>
          </View>
        ))}
    </View>
  );
};
export class ForgetPassForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
    };

    // Bind scope to methods
    this.formSubmit = this.formSubmit.bind(this);
  }

  formSubmit(formData) {
    return this.props
      .passwordRecover(formData)
      .then((response) => {
        this.setState({
          success: true,
        });
      })
      .catch((e) => {
        Alert.alert('Ocurrio un problema: ', e.response.data.detail);
        Sentry.captureException(e);
        throw new SubmissionError({
          _error: 'No existe el correo.',
        });
      });
  }

  render() {
    const { handleSubmit, pristine, submitting, valid } = this.props;
    const { success } = this.state;

    return (
      <SafeAreaView style={styles.wrapper}>
        <KeyboardAwareScrollView
          contentContainerStyle={styles.scrollViewStyle}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled
        >
          <View style={styles.formContainer}>
            <View style={styles.title}>
              <CRText
                variant={typography.subtitle2}
                color={colors.colorDark200}
                align="center"
              >
                Restablecer contraseña
              </CRText>
            </View>
            <View style={styles.message}>
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark300}
                align="center"
              >
                Ingresa tu correo para enviarte un link y cambiar tu contraseña
              </CRText>
            </View>
            <Field
              name="email"
              keyboardType="default"
              component={renderField}
              label="Email"
              type="text"
              placeholder="Escribe tu correo"
              normalize={normalizeToLowerCaseAndTrim}
              validate={[required, email]}
            />
          </View>

          {success &&
            globalThis.ReactApplication.ActiveScreen(
              'MessageMailScreen',
              {},
              this.props.navigation,
            )}

          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={
                pristine || submitting || !valid
                  ? field.buttonSquareDisabled
                  : field.buttonSquare
              }
              onPress={handleSubmit(this.formSubmit)}
              activeOpacity={0.5}
            >
              <CRText
                variant={typography.button}
                color={
                  pristine || submitting || !valid
                    ? colors.colorGray300
                    : colors.colorMain300
                }
                align="center"
              >
                Enviar
              </CRText>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

ForgetPassForm = reduxForm({
  form: 'password_recover',
})(ForgetPassForm);

// Redux Map Functions
function mapStateToProps(state) {
  return {
    user: state.user,
  };
}
function mapDispatchToProps(dispatch) {
  const { passwordRecover } = userActions;
  return bindActionCreators(
    {
      passwordRecover,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(ForgetPassForm);
