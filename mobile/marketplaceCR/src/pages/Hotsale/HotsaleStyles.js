import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';
import { typography, buttons } from '../../assets';

export const styles = StyleSheet.create({
  imgContainer: {
    height: 80,
    width: 220,
  },
  bannerContainer: {
    display: 'flex',
    width: '100%',
    backgroundColor: '#F4783A',
    borderBottomColor: '#E9254F',
    borderBottomWidth: 4,
    alignItems: 'center',
    padding: 5,
  },
  listContainer: {
    display: 'flex',
    width: '100%',
    backgroundColor: '#f0f0f0',
    paddingTop: 10,
    paddingBottom: 10,
  },
  discountContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    backgroundColor: '#f0f0f0',
    padding: 5,
  },
  optionPercentageButton: {
    backgroundColor: 'red',
    padding: 5,
  },
  optionPercentage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    height: 35,
    ...typography.txtSemiBold,
  },
  dropdownContainer: {
    flex: 1,
    padding: 10,
  },
  listCard: {
    width: 210,
    height: 55,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  dropdown: {
    height: 40,
  },
});
