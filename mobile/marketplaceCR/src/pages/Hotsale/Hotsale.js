import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, ScrollView, Text, Image, TouchableOpacity } from 'react-native';
import { CRHomeSectionCard } from '../../components/CRCard';
import CRCategoryCard from '../../components/CRCard/CRCategoryCard/CRCategoryCard';
import hotsaleBanner from './../../images/hotsale/bigbanner.png';
import { appActions } from '../../redux/actions';

import VirtualListContainer from '../../components/VirtualList/VirtualListContainer/VirtualListContainer';
import { list } from './CategoriesList.js';
import { styles } from './HotsaleStyles';

////

let maxItemsPerPage = 24;
let maxItemsInfiniteLoading = 24;
let infiniteItems = [];
//let activeRange = 0;
let slugHistory = '';
let filterHistory = '';
let currentPage = 1;
let currentSlug = 'belleza-1';
let dynamicLoading = false;

class Hotsale extends PureComponent {
  constructor(props) {
    super(props);
    this.initValues = {
      showItems: [],
      activeSlug: currentSlug,
      disabledInfinite: false,
      request: this.props.categoryResults,
      percentage: 25,
      config: {
        disabled: false,
      },
    };
    this.state = this.initValues;
  }
  async componentDidMount() {
    //await this.onFilterSubmit(currentSlug);
    this.initContent();
  }

  async initContent() {
    infiniteItems = [];

    // Build search query and perform content fetch
    await this.onFilterSubmit(currentSlug);

    // If we´ve got results, update state
    if (this.props.categoryResults.results) {
      this.setupInfiniteItems(this.props.categoryResults.results);
    }
  }

  setupInfiniteItems = (results: Array<{}>): void => {
    // Add elements to inifite list
    results.map((element) => infiniteItems.push(element));

    // Update state
    this.setState({
      ...this.state,
      disabledInfinite: results.length < 4 ? true : false, // <-- Disable inifinite scroll if we too few elements
      showItems: infiniteItems,
    });
  };

  onChangeCategory = async (slug: string) => {
    await this.resetResults();
    this.setState(
      {
        ...this.state,
        activeSlug: slug,
      },
      () => {
        this.onFilterSubmit(slug);
      },
    );
  };
  onChangePercentage = async (percent: number) => {
    await this.resetResults();
    this.setState(
      {
        ...this.state,
        percentage: percent,
      },
      () => {
        this.onFilterSubmit();
      },
    );
  };

  resetResults() {
    currentPage = 1;
    infiniteItems = [];
    this.setState({
      ...this.state,
      showItems: [],
      percentage: 25,
    });
  }

  onFilterSubmit = (
    categorySlug: string = this.state.activeSlug,
    page: number = currentPage,
    itemsPerPage: number = maxItemsPerPage,
    percentage: number = this.state.percentage,
  ): void => {
    if (categorySlug !== slugHistory) {
      infiniteItems.length = 0;
      currentPage = 1;
      //filterHistory = '';

      this.setState({
        ...this.state,
      });
      if (categorySlug) {
        slugHistory = categorySlug;
      }
    }

    // Build query
    const searchQueries: string = buildQuery({
      slug: categorySlug,
      page,
      percentage,
      pageSize: itemsPerPage,
    });

    // Fetch filtered content

    this.fetchContent(searchQueries, categorySlug);
  };

  /**
   * fetchContent()
   * Retrieves results from the API. Call redux actions.
   * @param {string} query | Query string to send to API
   */
  fetchContent = async (query, slug) => {
    dynamicLoading = true;
    await Promise.all([this.props.getCategoryResults(query, slug)]);
    dynamicLoading = false;
  };
  showMore = () => {
    //if (Number(calc) !== Number(activeRange) && !dynamicLoading) {
    if (!dynamicLoading && !this.state.disabledInfinite) {
      currentPage++;
      this.onFilterSubmit(
        this.state.activeSlug,
        currentPage,
        maxItemsInfiniteLoading,
      );
    }
  };

  componentWillReceiveProps(nextProps) {
    const { categoryResults } = nextProps;

    // If results have changed, render list again.
    if (
      categoryResults.results &&
      categoryResults.results !== this.props.categoryResults.results
    ) {
      this.setupInfiniteItems(categoryResults.results);
    }
  }

  render() {
    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 120;
    const SPACER_SIZE_ANDROID = 50;
    const { navigation } = this.props;
    const { showItems, request, disabledInfinite } = this.state;

    const config = {
      products: this.state.showItems,
      screen: this.state.sendScreen,
      navigation: this.props.navigation,
      type: 'products',
      showMore: (type) => this.showMore(type),
      request: false,
    };

    return (
      <View>
        <View style={styles.bannerContainer}>
          <Image source={hotsaleBanner} style={styles.imgContainer}></Image>
        </View>

        <View style={styles.listContainer}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {list.map((item, index) => {
              return (
                <View style={styles.listCard}>
                  <CRCategoryCard
                    key={index}
                    category={item.name}
                    thumbImg={item.img}
                    size="half"
                    marginBottom={-1}
                    onPress={() => {
                      this.onChangeCategory(item.slug);
                    }}
                  />
                </View>
              );
            })}
          </ScrollView>
        </View>

        <View style={styles.containerProduct}>
          <VirtualListContainer
            config={config}
            disabled={this.state.config.disabled}
          />
          {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
          {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
        </View>
      </View>
    );
  }
}

const buildQuery = (hotsaleParams): string => {
  let discountRange = '';

  if (hotsaleParams.percentage === 25) {
    discountRange = 'percentage_discount_from=0&percentage_discount_to=25';
  } else if (hotsaleParams.percentage === 30) {
    discountRange = 'percentage_discount_from=30&percentage_discount_to=39';
  } else if (hotsaleParams.percentage === 40) {
    discountRange = 'percentage_discount_from=40&percentage_discount_to=49';
  } else if (hotsaleParams.percentage === 50) {
    discountRange = 'percentage_discount_from=50&percentage_discount_to=100';
  }

  return `?category__slug=${hotsaleParams.slug}&page=${hotsaleParams.page}&page_size=${hotsaleParams.pageSize}&has_discount=true&${discountRange}`;
};

// Redux Map Functions
function mapStateToProps(state) {
  const { app } = state;
  return {
    categoryResults: app.categoryResults.results,
    categories: app.categories.categories,
    categoryLoad: app.categoryResults,
  };
}
function mapDispatchToProps(dispatch) {
  const { getCategoryResults } = appActions;
  return bindActionCreators({ getCategoryResults }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Hotsale);
