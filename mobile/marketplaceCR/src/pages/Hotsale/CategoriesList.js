export const list = [
  {
    name: 'Belleza',
    slug: 'belleza-1',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Belleza.png',
    key: 1,
  },
  {
    name: 'Diseño y otros productos',
    slug: 'diseno-otros',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Diseno.png',
    key: 2,
  },
  {
    name: 'Bebidas',
    slug: 'comida-bebidas-2',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Bebidas.png',
    key: 3,
  },
  {
    name: 'Gourmet',
    slug: 'gourmet',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Gourmet.png',
    key: 4,
  },
  {
    name: 'Fitness',
    slug: 'mujer-fitness-3',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Fitness.png',
    key: 5,
  },
  {
    name: 'Joyería y Accesorios',
    slug: 'accesorios-y-joyeria-1',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Accesorios.png',
    key: 6,
  },
  {
    name: 'Postres',
    slug: 'comida-postres-2',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Postres.png',
    key: 7,
  },
  {
    name: 'Salud',
    slug: 'salud',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Salud.png',
    key: 8,
  },
  {
    name: 'Flores y Regalos',
    slug: 'Flores-y-Regalos',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Regalos.png',
    key: 9,
  },
  {
    name: 'Juegos y Juguetes',
    slug: 'juegos-juguetes',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Juegos.png',
    key: 10,
  },
  {
    name: 'Ropa y Calzado',
    slug: 'ropa-y-calzado-1',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Calzado.png',
    key: 11,
  },
  {
    name: 'Comida Saludable',
    slug: 'saludable',
    img:
      'https://canastarosa-app.s3.us-east-2.amazonaws.com/categoryImg/Saludable.png',
    key: 12,
  },
];
