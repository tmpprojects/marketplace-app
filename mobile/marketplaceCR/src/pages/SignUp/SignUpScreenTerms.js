import React, { Component } from 'react';
import Modal from 'react-native-modal';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';

import { styles } from './SignUpScreenStyles';
import Icon from 'react-native-vector-icons/Ionicons';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRRowBoth } from '../../components/Layout';

export default class SignUpScreenTerms extends Component {
  constructor(props) {
    super(props);
    //this.state = useState({isModalVisible: props.isModalVisible});
  }

  toggleModal = () => {
    //Close modal
    this.props.toggleModal(false);
  };

  render() {
    return (
      <Modal
        isVisible={this.props.isModalVisible}
        style={styles.containerModal}
      >
        <View style={styles.btnContainerModal}>
          <TouchableOpacity
            onPress={this.toggleModal}
            style={styles.btnClose}
            activeOpacity={0.5}
          >
            <Icon name="ios-close" style={styles.closeTermsIcon} />
          </TouchableOpacity>
          <ScrollView style={styles.containerScroll}>
            <CRRowBoth addedStyle={styles.innerItems}>
              <CRText variant={typography.subtitle} color={colors.colorDark200}>
                TÉRMINOS Y CONDICIONES
              </CRText>
            </CRRowBoth>
            <CRRowBoth addedStyle={styles.innerItems}>
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark100}
              >
                Última actualización: septiembre 12, 2019
              </CRText>
            </CRRowBoth>
            <CRRowBoth addedStyle={styles.innerItems}>
              <CRText
                variant={typography.subtitle3}
                color={colors.colorDark200}
              >
                USO DEL SERVICIO PARA COMPRADORES
              </CRText>
            </CRRowBoth>
            <CRRowBoth addedStyle={styles.innerItems}>
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark100}
              >
                El presente Contrato establece los términos y condiciones de
                Canasta Rosa SAPI de CV, identificado en lo sucesivo bajo el
                nombre comercial de CANASTA ROSA que regulan el acceso y uso del
                sitio web www.canastarosa.com (en adelante EL SITIO) y todos los
                sitios web adicionales de CANASTA ROSA (los Sitios web de
                CANASTA ROSA). Cualquier persona que haga uso de EL SITIO o de
                los servicios que presta CANASTA ROSA deberá sujetarse a los
                presentes términos y condiciones junto con las demás políticas
                establecidas por CANASTA ROSA. {'\n'}
              </CRText>
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark100}
              >
                Cualquier persona que no acepte estos términos y condiciones,
                mismos que tienen un carácter de obligatorio deberá de
                abstenerse de utilizar El SITIO y/o los servicios prestados por
                CANASTA ROSA.{'\n'}
              </CRText>
              <CRText
                variant={typography.paragraph}
                color={colors.colorDark100}
              >
                Los servicios prestados por CANASTA ROSA sólo podrán ser
                utilizados por personas que cuenten con la capacidad legal para
                contratar.
              </CRText>
            </CRRowBoth>
          </ScrollView>
        </View>
      </Modal>
    );
  }
}
