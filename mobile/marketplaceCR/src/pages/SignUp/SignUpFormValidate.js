import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, SafeAreaView } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { userActions } from '../../redux/actions';
import { Spinner } from '../../utils/animations/Spinner';
import { _customSetData, _setData } from '../../utils/asyncStorage';
import * as Sentry from '@sentry/react-native';
//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

const { width: WIDTH } = Dimensions.get('window');

export class SignUpFormValidate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
    };
    this._listenInterval;
  }

  listenValidation() {
    try {
      this._listenInterval = setInterval(() => {
        if (this.props.verifyStatus === 'waiting') {
          this.props.verifyEmailAccount(this.props.userVerify);
        } else {
          if (this.props.verifyStatus === 'done') {
            clearInterval(this._listenInterval);
            this.setNewUser();
          }
        }
      }, 3000);
    } catch (e) {
      Sentry.captureException(e);
    }
  }

  async setNewUser() {
    try {
      //await _setData(this.props.cookies);
      //await clearInterval(this._listenInterval);

      // await _customSetData('email', this.props.userVerify.email);
      // await _customSetData('password', this.props.userVerify.password);

      // this.props.navigation.navigate('SplashScreen');
      this.props
        .loginUser(this.props.userVerify.email, this.props.userVerify.password)
        .then(async (response) => {
          // await _setData(response.data);
          await _customSetData('email', this.props.userVerify.email);
          await _customSetData('password', this.props.userVerify.password);
          await _setData(response.data);
          //this.props.navigation.navigate('SplashScreenReload');

          globalThis.ReactApplication.ActiveScreen(
            'SplashScreenReload',
            {},
            this.props.navigation,
          );
        })
        .catch((e) => {
          //console.log(e);
          Sentry.captureException(e);
        });
    } catch (e) {
      //console.log(e);
      Sentry.captureException(e);
    }
  }

  componentDidMount() {
    this.listenValidation();
  }

  componentWillUnmount() {
    clearInterval(this._listenInterval);
  }

  render() {
    try {
      return (
        <View style={styles.formContainer}>
          <View style={styles.message}>
            <View style={styles.title}>
              <CRText
                variant={typography.subtitle2}
                color={colors.colorDark200}
                align="center"
              >
                Esperando validación
              </CRText>
            </View>
            <CRText
              variant={typography.paragraph}
              color={colors.colorDark200}
              align="center"
            >
              Te enviamos un correo de activación a{' '}
              {this.props.userVerify.email}, no olvides revisar tu SPAM.
            </CRText>
          </View>
          <View style={styles.loader}>
            <Spinner />
          </View>
        </View>
      );
    } catch (e) {
      Sentry.captureException(e);
    }
  }
}

// Redux Map Functions
function mapStateToProps(state) {
  try {
    return {
      userVerify: {
        email: state.form.singup.values.email,
        password: state.form.singup.values.password,
      },
      verifyStatus: state.user.verifyStatus,
      cookies: state.user.cookies,
    };
  } catch (e) {
    return {
      userVerify: {
        email: '',
        password: '',
      },
      verifyStatus: state.user.verifyStatus,
      cookies: state.user.cookies,
    };
  }
}
function mapDispatchToProps(dispatch) {
  const {
    verifyEmailAccount,
    autoLogin,
    loginUser,
    getCurrentUser,
  } = userActions;
  return bindActionCreators(
    {
      verifyEmailAccount,
      autoLogin,
      getCurrentUser,
      loginUser,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(SignUpFormValidate);

const styles = StyleSheet.create({
  loader: {
    height: 100,
  },
  formContainer: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.colorWhite,
  },
  title: {
    marginBottom: 10,
  },
  message: {
    width: WIDTH - 120,
  },
});
