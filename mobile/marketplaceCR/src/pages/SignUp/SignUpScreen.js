import React, { Component } from 'react';
import { Field, reduxForm, SubmissionError, change, untouch } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Sentry from '@sentry/react-native';
import {
  View,
  TouchableOpacity,
  TextInput,
  Alert,
  SafeAreaView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CheckBox } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import { userActions } from '../../redux/actions';
import { userRegisterErrorTypes } from '../../redux/constants/user.constants';
import SignUpScreenTerms from './SignUpScreenTerms';
import { field } from '../../components/FieldsForm/FieldsFormStyles';
import { styles } from './SignUpScreenStyles';
import {
  required,
  email,
  password,
  password_confirmation,
  name,
  lastName,
} from '../../utils/formValidators';
import { VerifyAccountForm } from '../VerifyAccount/VerifyAccountForm';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
//Google Analytics
import analytics from '@react-native-firebase/analytics';
declare var globalThis: any;

const renderField = ({
  keyboardType,
  meta: { touched, error, warning },
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
}) => {
  return (
    <View>
      <TextInput
        style={field.inputForm}
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray300}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        {...restInput}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
      />
      {(touched && error && (
        <View style={field.formErrors}>
          <CRText variant={typography.caption} color={colors.colorRed300}>
            {error}
          </CRText>
        </View>
      )) ||
        (warning && (
          <View style={field.formErrors}>
            <CRText variant={typography.caption} color={colors.colorYellow300}>
              {warning}
            </CRText>
          </View>
        ))}
    </View>
  );
};

export class SingUpForm extends Component {
  constructor() {
    super();
    this.state = {
      showPass: true,
      press: false,
      success: false,
    };
    this.formSubmit = this.formSubmit.bind(this);
  }

  state = {
    isModalVisible: false,
  };

  async componentDidMount() {
    await analytics().setCurrentScreen('SignUpScreen');
    await analytics().logEvent('SignUpScreen');
  }

  componentWillUnmount() {}

  toggleModal = (receibe) => {
    receibe
      ? this.setState({ isModalVisible: true })
      : this.setState({ isModalVisible: false });
  };

  showPass = () => {
    if (this.state.press === false) {
      this.setState({ showPass: false, press: true });
    } else {
      this.setState({ showPass: true, press: false });
    }
  };
  /*
   * FORM FUNCTIONS
   */

  formSubmit(formData) {
    //const email = formData.email;
    //const query = `validates?email=${email}`;
    // Register user

    this.props
      .registerUser(formData)
      .then(() => {
        //this.props.navigation.navigate('SignUpFormValidate');
        globalThis.ReactApplication.ActiveScreen(
          'SignUpFormValidate',
          {},
          this.props.navigation,
        );

        analytics().logSignUp({
          method: 'SignUp in app',
        });
      })
      .catch((error) => {
        Sentry.captureException(error);
        if (error.response.data.email) {
          this.props.dispatch(change('singup', 'email', ''));

          //reset the field's error
          this.props.dispatch(untouch('singup', 'email'));
          Alert.alert('El correo ya estaba registrado, intenta con otro.');
        }
        const uiMessage = error.response || userRegisterErrorTypes;
        // throw new SubmissionError({
        //   _error: uiMessage,
        // });
      });
    // return ( && );

    /*
        const uiMessage = error.response || userRegisterErrorTypes;
          throw new SubmissionError({
            _error: uiMessage,
          });
      */
    //   //
  }
  render() {
    const { handleSubmit, pristine, submitting, valid } = this.props;
    const { success } = this.state;

    return (
      <SafeAreaView style={styles.wrapper}>
        {!success ? (
          <KeyboardAwareScrollView
            contentContainerStyle={styles.scrollViewStyle}
            resetScrollToCoords={{ x: 0, y: 0 }}
            scrollEnabled
          >
            <View>
              <Field
                name="first_name"
                fieldName="firstName"
                keyboardType="default"
                placeholder="Nombre(s)"
                component={renderField}
                validate={[required, name]}
              />
              <Field
                name="last_name"
                fieldName="lastName"
                keyboardType="default"
                placeholder="Apellidos"
                component={renderField}
                validate={[required, lastName]}
              />
              <Field
                name="email"
                keyboardType="default"
                placeholder="Correo Electrónico"
                component={renderField}
                validate={[required, email]}
              />
              <View>
                <Field
                  name="password"
                  keyboardType="default"
                  placeholder="Contraseña"
                  secureTextEntry={this.state.showPass}
                  type="password"
                  component={renderField}
                  validate={[required, password]}
                />
                <TouchableOpacity
                  style={field.btnEye}
                  onPress={this.showPass.bind(this)}
                >
                  <Icon
                    name={
                      this.state.press === false ? 'ios-eye-off' : 'ios-eye'
                    }
                    size={22}
                    color={colors.colorGray300}
                    activeOpacity={0.5}
                  />
                </TouchableOpacity>
              </View>
              <View>
                <Field
                  name="confirmPassword"
                  keyboardType="default"
                  placeholder="Confirmar Contraseña"
                  type="password"
                  secureTextEntry={this.state.showPass}
                  component={renderField}
                  validate={[required, password, password_confirmation]}
                />
                <TouchableOpacity
                  style={field.btnEye}
                  onPress={this.showPass.bind(this)}
                >
                  <Icon
                    name={
                      this.state.press === false ? 'ios-eye-off' : 'ios-eye'
                    }
                    size={22}
                    color={colors.colorGray300}
                    activeOpacity={0.5}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.inputContainer}>
              <View style={styles.containerCheckbox}>
                <CheckBox
                  checkedIcon={
                    <Icon name={'md-checkbox'} style={styles.checkedIcon} />
                  }
                  uncheckedIcon={
                    <Icon
                      name={'md-square-outline'}
                      style={styles.uncheckedIcon}
                    />
                  }
                  checked={this.state.checked}
                  onPress={() =>
                    this.setState({
                      checked: !this.state.checked,
                    })
                  }
                />
                <CRText
                  variant={typography.paragraph}
                  color={colors.colorDark200}
                >
                  Acepto los
                </CRText>
                <TouchableOpacity
                  style={styles.btnTerms}
                  onPress={this.toggleModal}
                  activeOpacity={0.5}
                >
                  <CRText variant={typography.bold} color={colors.colorDark200}>
                    Términos y Condiciones
                  </CRText>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={
                  pristine || submitting || !valid || !this.state.checked
                    ? field.buttonSquareDisabled
                    : field.buttonSquare
                }
                activeOpacity={0.5}
                onPress={handleSubmit(this.formSubmit)}
                disabled={!this.state.checked}
              >
                <CRText
                  variant={typography.button}
                  color={
                    pristine || submitting || !valid || !this.state.checked
                      ? colors.colorGray300
                      : colors.colorMain300
                  }
                  align="center"
                >
                  Registrarme
                </CRText>
              </TouchableOpacity>
            </View>
            <View style={styles.inputContainer}>
              <TouchableOpacity
                style={styles.btnLogin}
                activeOpacity={0.5}
                onPress={() => {
                  //this.props.navigation.navigate('LoginScreen');
                  globalThis.ReactApplication.ActiveScreen(
                    'LoginScreen',
                    {},
                    this.props.navigation,
                  );
                }}
              >
                <CRText
                  variant={typography.bold}
                  color={colors.colorDark200}
                  align="center"
                >
                  Iniciar sesión
                </CRText>
              </TouchableOpacity>
            </View>

            <SignUpScreenTerms
              isModalVisible={this.state.isModalVisible}
              toggleModal={this.toggleModal}
            />
          </KeyboardAwareScrollView>
        ) : (
          <VerifyAccountForm
            navigation={this.props.navigation}
            user={this.props.user.payload}
            validateCode={this.props.validateCode}
          />
        )}
      </SafeAreaView>
    );
  }
}

SingUpForm = reduxForm({
  form: 'singup',
})(SingUpForm);

// Redux Map Functions
function mapStateToProps(state) {
  return {
    user: state.user,
  };
}
function mapDispatchToProps(dispatch) {
  const { registerUser, validateCode } = userActions;
  return bindActionCreators(
    {
      registerUser,
      // verifyAccount,
      validateCode,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(SingUpForm);
