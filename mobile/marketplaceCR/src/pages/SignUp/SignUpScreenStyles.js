import { StyleSheet, Dimensions } from 'react-native';
import { colors, typography, buttons } from '../../assets';

const { width: WIDTH } = Dimensions.get('window');

export const styles = StyleSheet.create({
  wrapper: {
    flexGrow: 1,
    backgroundColor: colors.colorWhite,
  },
  scrollViewStyle: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    alignItems: 'center',
  },
  buttonContainer: {
    alignItems: 'center',
  },
  btnLogin: {
    marginTop: 30,
  },

  containerCheckbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: -20,
  },
  checkedIcon: {
    color: '#E63976',
    fontSize: 26,
  },
  uncheckedIcon: {
    color: '#979797',
    fontSize: 26,
  },
  btnTerms: { marginLeft: 5 },
  closeTermsIcon: {
    color: colors.colorGray400,
    fontSize: 40,
    marginTop: 30,
    marginRight: 20,
  },
  txtTitleTerms: {
    ...typography.txtBold,
    color: colors.black300,
    fontSize: 24,
    marginLeft: 20,
  },
  txtTitleAct: {
    ...typography.txtLight,
    color: colors.black300,
    marginLeft: 20,
    marginTop: 24,
  },
  txtTitleUse: {
    ...typography.txtBold,
    color: colors.black300,
    fontSize: 16,
    marginLeft: 20,
    marginTop: 27,
  },
  txtTerms: {
    ...typography.txtLight,
    color: colors.black300,
    fontSize: 16,
    marginLeft: 20,
    marginRight: 15,
    marginTop: 25,
  },
  headerElement: {
    marginTop: 100,
  },
  formErrors: {
    color: colors.warningColor,
    ...typography.txtLight,
    fontSize: 14,
    marginHorizontal: 25,
  },
  //SIGNUP TERMS STYLES
  containerModal: {
    margin: 0,
    marginTop: 60,
  },
  btnContainerModal: {
    flex: 1,
    backgroundColor: colors.colorWhite,
  },
  btnClose: {
    alignItems: 'flex-end',
  },
  containerScroll: {
    flex: 1,
  },
  innerItems: {
    marginHorizontal: 20,
  },
});
