import React, { Component } from 'react';
import { View, Image, Text, Platform, Linking, Alert } from 'react-native';
import { styles } from './PaymentResultStyle';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ordersActions, userActions } from '../../redux/actions';
import imgSuccess from '../../images/illustration/success.png';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

import { resetAction } from '../../utils/resetActions';
import { CRRowBoth } from '../../components/Layout';
//Google Analytics
import analytics from '@react-native-firebase/analytics';
declare var globalThis: any;
export class PaymentSuccess extends Component {
  constructor(props) {
    super(props);
    const { navigation, route } = this.props;

    const {
      orderID,
      nameClient,
      emailClient,
      orderDetail,
      paymentProvider,
    } = route.params;

    this.orderID = orderID;
    this.nameClient = nameClient;
    this.emailClient = emailClient;
    this.orderDetail = orderDetail;
    this.paymentProvider = paymentProvider;

    this.state = {
      order: this.orderID,
      client: this.nameClient,
      email: this.emailClient,
      orderData: this.orderDetail,
      paymentProvider: this.paymentProvider,
      itemBrand: [],
      itemId: [],
      itemName: [],
      itemVariant: [],
      shipping: [],
    };
    this.props.getUserCards();
  }

  async componentDidMount() {
    globalThis.ReactApplication.BackScreen.disable = false;
    await analytics().setCurrentScreen('PaymentSuccessScreen');
    await analytics().logEvent('PaymentSuccessScreen');
    await this.analyticsLogPurchase();
  }

  openDetail(orderData) {
    this.props.selectOrderDetail(orderData);
    this.props.getUserCards();
    //this.props.navigation.navigate('Orders');

    globalThis.ReactApplication.ActiveScreen(
      'Orders',
      {},
      this.props.navigation,
    );
  }

  //analyticsFirebase
  async analyticsLogPurchase() {
    //ecommerce_purchase
    //*Agregar cuando se confirme una orden exitosa.
    //*OXXO y Transferencia también se registran.
    //console.log('Datos de la orden: ', this.state.orderData);
    this.state.orderData.orders.map((product, i) =>
      product.products.map(
        (productDetail, i) => (
          this.state.shipping.push(product.physical_properties.shipping_price),
          this.state.itemBrand.push(product.store.name),
          this.state.itemId.push(productDetail.id),
          this.state.itemName.push(productDetail.product.name),
          this.state.itemVariant.push(
            productDetail.attribute === null
              ? ''
              : productDetail.attribute.tree_string,
          )
        ),
      ),
    );

    await analytics().logPurchase({
      value: Number(this.state.orderData.order_price),
      currency: 'MXN',
      //coupon: '',
      //shipping: Number(this.state.shipping.join(", ")),
      transaction_id: this.state.orderData.uid,
      items: [
        {
          item_brand: this.state.itemBrand.join(', ').toString(),
          item_id: this.state.itemId.join(', ').toString(),
          item_name: this.state.itemName.join(', ').toString(),
          item_variant: this.state.itemVariant.join(', ').toString(),
          //item_category: 'round necked t-shirts',
        },
      ],
    });
  }

  render() {
    const isAndroid = Platform.OS === 'android';
    const isIos = Platform.OS === 'ios';
    return (
      <View style={styles.container}>
        {/* GRATITUDE */}
        <CRRowBoth>
          <Image source={imgSuccess} style={styles.imageSuccess} />
          <CRText
            variant={typography.subtitle2}
            color={colors.colorDark200}
            align={'center'}
          >
            ¡Gracias por tu compra
            {this.state.client !== undefined ? '\n' + this.state.client : ''}!
          </CRText>
        </CRRowBoth>
        {/* GRATITUDE */}
        <CRRowBoth>
          {/* ORDER INFO */}
          {this.state.paymentProvider === 'stripe' ? (
            <CRText
              variant={typography.caption}
              color={colors.colorDark100}
              align={'center'}
            >
              <Text style={styles.txtOrder}>Tu órden </Text>
              <CRText
                variant={typography.bold}
                color={colors.colorDark200}
                align={'center'}
              >
                <Text
                  style={styles.txtOrder}
                  onPress={() => this.openDetail(this.state.orderData)}
                >
                  {this.state.order}
                </Text>
              </CRText>{' '}
              <Text style={styles.txtOrder}>está confirmada</Text>
            </CRText>
          ) : null}
          {this.state.paymentProvider === 'paypal' ? (
            <CRText
              variant={typography.caption}
              color={colors.colorDark100}
              align={'center'}
            >
              <Text style={styles.txtOrder}>Tu órden </Text>
              <CRText
                variant={typography.bold}
                color={colors.colorDark200}
                align={'center'}
              >
                <Text
                  style={styles.txtOrder}
                  onPress={() => this.openDetail(this.state.orderData)}
                >
                  {this.state.order}
                </Text>
              </CRText>{' '}
              <Text style={styles.txtOrder}>está confirmada</Text>
            </CRText>
          ) : null}
          {this.state.paymentProvider === 'oxxo' ? (
            <CRText
              variant={typography.caption}
              color={colors.colorDark100}
              align={'center'}
            >
              <Text style={styles.txtOrder}>Tu órden </Text>
              <CRText
                variant={typography.bold}
                color={colors.colorDark200}
                align={'center'}
              >
                <Text
                  style={styles.txtOrder}
                  onPress={() => this.openDetail(this.state.orderData)}
                >
                  {this.state.order}
                </Text>
              </CRText>{' '}
              <Text style={styles.txtOrder}>
                está a la espera de recibir tu pago, acude al oxxo más cercano a
                tu domicilio con el codigo que te enviamos a tu correo
              </Text>
            </CRText>
          ) : null}
          {this.state.paymentProvider === 'bank-transfer' ? (
            <CRText
              variant={typography.caption}
              color={colors.colorDark100}
              align={'center'}
            >
              <Text style={styles.txtOrder}>Tu órden </Text>
              <CRText
                variant={typography.bold}
                color={colors.colorDark200}
                align={'center'}
              >
                <Text
                  style={styles.txtOrder}
                  onPress={() => this.openDetail(this.state.orderData)}
                >
                  {this.state.order}
                </Text>
              </CRText>{' '}
              <Text style={styles.txtOrder}>
                está a la espera de ser pagada.
              </Text>
            </CRText>
          ) : null}
          {/* ORDER INFO */}
        </CRRowBoth>
        <CRRowBoth addedStyle={styles.instructions}>
          {/* ORDER INSTRUCTIONS */}
          <CRText
            variant={typography.paragraph}
            color={colors.colorDark100}
            align={'center'}
          >
            <Text style={styles.txtDetailOrder}>
              Pronto recibirás al correo electrónico{' '}
              <Text style={styles.txtBoldInfo}>{this.state.email}</Text> {'\n'}{' '}
              todos lo detalles de tu compra.
            </Text>
          </CRText>
          {/* ORDER INSTRUCTIONS */}
        </CRRowBoth>
        <CRRowBoth addedStyle={styles.continue}>
          {/* CONTINUE */}

          <CRText
            variant={typography.caption}
            color={colors.colorDark300}
            align={'center'}
          >
            <Text style={styles.txtAndroidAppleStore}>
              ¿Te gusto la experiencia de compra?
            </Text>
          </CRText>

          <CRText
            variant={typography.caption}
            color={colors.colorMain300}
            align={'center'}
          >
            {isIos && (
              <Text
                style={styles.txtAndroidAppleStore}
                onPress={() => {
                  Linking.openURL(
                    `itms-apps://itunes.apple.com/app/id1500303864?action=write-review`,
                  ).catch((e) => {
                    Alert.alert('Abre tu App Store.');
                    Sentry.captureException(e);
                  });
                }}
              >
                Compártenos tu opinión en la App Store.{' '}
              </Text>
            )}
            {isAndroid && (
              <Text
                style={styles.txtAndroidAppleStore}
                onPress={() => {
                  Linking.openURL(
                    `market://details?id=com.canastarosa.app`,
                  ).catch((e) => {
                    Sentry.captureException(e);
                    Alert.alert('Abre tu Play Store');
                  });
                }}
              >
                Compártenos tu opinión en la Play Store.{' '}
              </Text>
            )}
          </CRText>

          <View style={styles.space}></View>
          <CRText
            variant={typography.caption}
            color={colors.colorMain300}
            align={'center'}
          >
            <Text
              style={styles.txtBackToHome}
              onPress={() => {
                this.props.getUserCards();
                // this.props.navigation.navigate('ShoppingCart');
                // this.props.navigation.navigate('HomeScreen');
                // this.props.navigation.dispatch(resetAction);

                globalThis.ReactApplication.ActiveScreen(
                  'HomeScreen',
                  {},
                  this.props.navigation,
                );
              }}
            >
              Seguir Comprando
            </Text>
          </CRText>
          {/* CONTINUE */}
        </CRRowBoth>
      </View>
    );
  }
}

// Redux Map Functions
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  const { selectOrderDetail } = ordersActions;
  return bindActionCreators(
    {
      selectOrderDetail,
      getUserCards: userActions.getUserCards,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentSuccess);
