import { StyleSheet } from 'react-native';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.colorWhite,
  },
  imageSuccess: {
    height: 190,
    width: 'auto',
    resizeMode: 'contain',
    marginBottom: 20,
  },

  instructions: {
    marginTop: 20,
  },
  continue: {
    marginTop: 28,
  },
  txtGratitude: {
    fontSize: 28,
  },
  txtOrder: {
    fontSize: 18,
  },
  txtDetailOrder: {
    fontSize: 18,
  },
  txtBackToHome: {
    fontSize: 18,
  },
  txtAndroidAppleStore: {
    fontSize: 18,
  },
  txtBoldInfo: {
    ...typography.bold,
    fontSize: 18,
  },
  space: {
    width: '100%',
    height: 30,
  },
});
