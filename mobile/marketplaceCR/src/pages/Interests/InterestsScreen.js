import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { appActions } from '../../redux/actions';

import {
  StyleSheet,
  ImageBackground,
  View,
  Text,
  SafeAreaView,
} from 'react-native';

import InterestBtn from '../../components/Welcome/ButtonsInterest';
import { colors, typography, buttons } from '../../assets';
import HeaderWelcome from '../../components/Welcome/HeaderWelcome';

class InterestsScreen extends Component {
  componentDidMount() {
    this.props.fetchInterests();
  }
  render() {
    const { interestsList } = this.state;
    return (
      <View style={styles.background}>
        <SafeAreaView style={styles.headerContainer}>
          <HeaderWelcome />
          <Text style={styles.header}>¡BIENVENIDO!</Text>
          <Text style={styles.headerInfo}>
            Queremos mostrarte los productos más relevantes e increíbles.
            Ayúdanos seleccionando tus intereses.
          </Text>
        </SafeAreaView>
        <View>
          <InterestBtn interests={interestsList} />
        </View>

        <View style={styles.linksContainer}>
          <Text
            style={styles.link}
            onPress={() => {
              //this.props.navigation.navigate('HomeScreen')

              globalThis.ReactApplication.ActiveScreen(
                'HomeScreen',
                {},
                this.props.navigation,
              );
            }}
          >
            Continuar
          </Text>
          <Text
            style={styles.linkGray}
            onPress={() => {
              //this.props.navigation.navigate('HomeScreen')

              globalThis.ReactApplication.ActiveScreen(
                'HomeScreen',
                {},
                this.props.navigation,
              );
            }}
          >
            Ahora no
          </Text>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  const { interests } = state.app;
  return {
    interestsList: interests,
  };
}

function mapDispatchToProps(dispatch) {
  const { fetchInterests } = appActions;
  return bindActionCreators({ fetchInterests }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(InterestsScreen);

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
    backgroundColor: colors.colorDark400,
  },
  headerContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 40,
  },
  header: {
    ...typography.txtExtraBold,
    fontSize: 36,
    color: colors.white,
    marginBottom: 20,
  },
  headerInfo: {
    ...typography.txtLight,
    fontSize: 18,
    width: 300,
    color: colors.white,
    textAlign: 'center',
  },

  interestsList: {
    flex: 1,
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  interestBtn: {
    margin: 10,
    ...buttons.square,
  },
  interestTitle: {
    ...typography.text,
    padding: 10,
    textAlign: 'center',
    color: colors.white,
  },

  linksContainer: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  link: {
    ...typography.txtRegular,
    fontSize: 20,
    color: colors.black300,
    marginVertical: 30,
  },
  linkGray: {
    ...typography.txtLight,
    fontSize: 16,
    color: colors.black300,
    paddingBottom: 10,
  },
});
