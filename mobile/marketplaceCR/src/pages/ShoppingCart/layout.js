import { StyleSheet, Dimensions, Platform } from 'react-native';
import { buttons } from '../../assets';

// DS
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

const { height: HEIGHT, width: WIDTH } = Dimensions.get('window');
const isAndroid = Platform.OS === 'android';
const alto = Dimensions.get('window').height;

export const layout = StyleSheet.create({
  cartContainer: {
    backgroundColor: '#FAF7F5',
    flexGrow: 1,
    height: HEIGHT - 200,
  },
  title: {
    marginTop: 16,
  },
  countProducts: {
    marginBottom: 30,
  },
  wrapper: {
    height: '100%',
  },
  productsCountContainer: {
    marginVertical: 10,
    //flex: 1,
  },
  subtotalContainer: {
    alignSelf: 'stretch',
    paddingVertical: 15,
    marginHorizontal: 10,
    paddingHorizontal: 10,
    borderRadius: 3,
  },
  ordersContainer: {
    flex: 1,
  },
  store: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    marginTop: 10,
    marginBottom: 15,
  },
  storeContainerInfo: {
    marginHorizontal: 10,
  },
  thumbnail: {
    width: 50,
    height: 52,
    borderWidth: 1,
    borderColor: colors.colorGray200,
  },
  countProductContainer: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 10,
    flex: 3,
  },
  countProduct: {
    ...typography.txtLight,
    color: colors.colorGray400,
  },

  productCartContainer: {
    borderTopColor: colors.colorGray200,
    borderTopWidth: 1,
  },
  productCartInner: {
    marginHorizontal: 10,
    marginTop: 10,
    marginBottom: 20,
  },
  productInfoBlock: {
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  detail: {
    flex: 2,
    marginLeft: 20,
  },
  productName: {
    ...typography.txtSemiBold,
  },
  attribute: {
    ...typography.txtLight,
    color: colors.colorGray400,
  },

  containerItemMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderTopColor: colors.colorGray200,
    borderTopWidth: 1,
    paddingVertical: 15,
  },
  iconItemArrow: {
    marginRight: 30,
  },
  subtotalProduct: {
    ...typography.txtLight,
  },

  subtotalOrder: {
    //marginTop: 40,
    backgroundColor: '#FAF7F5',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  subtotal: {
    ...typography.txtSemiBold,
    color: colors.colorGray400,
  },
  subtotalTitle: {
    ...typography.txtSemiBold,
    color: colors.colorGray400,
  },

  containerBtn: {
    width: '100%',
    height: HEIGHT,
    justifyContent: 'flex-end',
    position: 'absolute',
    bottom: 100,
  },
  btn: {
    ...buttons.squareCart,
    width: '100%',
    height: 80,
  },
  btnText: {
    textAlign: 'center',
    ...typography.txtRegular,
    color: colors.colorWhite,
    fontSize: 20,
  },

  //Modal stuff

  cardContainerModal: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },
  iconSchedule: {
    marginRight: 8,
  },
  checkBoxIcon: {
    color: colors.colorMain300,
  },
  unselectedCheck: {
    width: 20,
    height: 20,
    borderWidth: 2,
    borderColor: colors.colorGray400,
    borderRadius: 2,
  },
  selectedCheck: {
    width: 20,
    height: 20,
    borderWidth: 2,
    borderColor: colors.colorMain300,
    backgroundColor: colors.colorMain300,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 1,
  },

  //NOTE
  fieldNoteStyle: {
    minHeight: 30,
    width: 250,
    borderColor: colors.colorGray300,
    borderWidth: 1,
    marginTop: 10,
    borderRadius: 5,
    paddingLeft: 5,
  },

  containerSubTotalUno: {
    marginTop: isAndroid ? 50 : 110,
    marginBottom: isAndroid ? -80 : 110,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    width: '100%',
    paddingVertical: 15,
    backgroundColor: '#FAF7F5',
  },
  containerSubTotalDos: {
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    width: '100%',
    paddingVertical: 15,
    backgroundColor: '#FAF7F5',
  },
  visa: {
    height: 10,
    width: 29,
    resizeMode: 'contain',
  },
  //

  viewBottomShoppingCart: {
    bottom: isAndroid ? '-11%' : alto < 737 ? '-17%' : '-4%',
  },
});
