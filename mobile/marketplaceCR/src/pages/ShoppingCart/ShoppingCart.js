import React, { Component } from 'react';
import type { Node } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FieldArray, getFormSyncErrors } from 'redux-form';
import { Text, ScrollView, View, Platform } from 'react-native';

import type { OrderType } from '../../redux/types/OrderTypes';
import withShoppingCartUI from '../../utils/hocs/withShoppingCartUI';
import { shoppingCartActions } from '../../redux/actions';
import { formatNumberToPrice } from '../../utils/normalizePrice';
import { SHOPPING_CART_FORM_CONFIG } from '../../utils/shoppingCartUtils/shoppingCartFormConfig';
import CartOrder from '../../components/ShoppingCart/CartOrder';
import CartSummary from '../../components/ShoppingCart/CartSummary';
import EmptyShoppingCart from '../../components/EmptyShoppingCart/EmptyShoppingCart';
import AsyncStorage from '@react-native-community/async-storage';

// Styles
import { layout } from './layout';

// DS
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';

// Will be added to the DS
import { CRIcon } from '../../components/CRIcon';
import { CRRowBoth, CRRowFull, CRRowLeft } from '../../components/Layout';
import { CRCard } from '../../components/CRCard';
import { CRBottomButton } from '../../components/CRBottomButton';
import { CRButton } from '../../components/CRButton';
import { CRLoaderScreen } from '../../components/CRAnim/CRLoaderScreen';
import { CRTitle } from '../../components/CRTitle';
//Google Analytics
import analytics from '@react-native-firebase/analytics';
import { _getPaymentMethodSaved } from './../../utils/asyncStorage';
// Define height props
const isIos: boolean = Platform.OS === 'ios';
const isAndroid: boolean = Platform.OS === 'android';
//Remodified for the bottom cart bar
const SPACER_SIZE_IOS = 150;
const SPACER_SIZE_ANDROID = 180;

// Declare global scope
declare var globalThis: any;

/**
 * @desc ShoppingCart class renders the list of products on the cart
 * grouped by 'Orders' (Stores)
 */
type ShoppingCartProperties = {
  renderShippingMethods: Function,
  renderShippingDate: Function,
  fetchShoppingCart: Function,
  updateCartProduct: Function,
  closeModalWindow: Function,
  removeFromCart: Function,
  navigation: Object,
  orders: Array<OrderType>,
  unavailableOrders: Array<OrderType>,
  cartPrice: number,
  productsCount: number,
};
type ShoppingCartState = {
  modalVisible: boolean,
  modalView: Node,
};
export class ShoppingCart extends Component<
  ShoppingCartProperties,
  ShoppingCartState,
> {
  state = {
    modalVisible: false,
    modalView: null,
    last4: '',
    pmCard: '',
    infoCard: [],
    itemBrand: [],
    itemId: [],
    itemName: [],
    itemVariant: [],
  };
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    await analytics().setCurrentScreen('ShoppingCartScreen');
    await analytics().logEvent('ShoppingCartScreen');
    // Fetch shopping cart
    await this.props.fetchShoppingCart();
    await this.cartProducts();

    this.setState({
      last4: (await _getPaymentMethodSaved()).last4,
      pmCard: (await _getPaymentMethodSaved()).pmCard,
    });
    let infoCard = [...this.state.infoCard];
    //NEW CARD
    this.props.cardsList.cards.map((card, i) =>
      this.state.last4 === card.card.last4
        ? (infoCard.push({
            data: card,
            is_paying_with_saved_card: false,
            is_saving_card: false,
            name: 'Tarjeta guardada',
            origin_frontend: null,
            paySavedCard: true,
            payment_card_token: card.id,
            payment_method_id: card.card.brand,
            required_invoice: false,
            slug: 'save-card',
            valid: true,
            was_paid_with_saved_card: false,
          }),
          this.setState({ infoCard }),
          AsyncStorage.setItem(
            'paymentMethodSaved',
            JSON.stringify(this.state.infoCard[0]),
          ))
        : console.log('No'),
    );
    //SAVED CARD IN PROFILE
    /* console.log("PROFILE: ", this.props.user.profile.stripe.default_card_id)
    this.props.cardsList.cards.map((card, i) => (
      console.log("QQWQWQWQWQW: ", card.id),
      this.props.user.profile.stripe.default_card_id === card.id ?
      (
        infoCard.push({
          data: card,
          is_paying_with_saved_card: false,
          is_saving_card: false,
          name: "Tarjeta guardada", 
          origin_frontend: null,
          paySavedCard: true,
          payment_card_token: card.id,
          payment_method_id: card.card.brand,
          required_invoice: false,
          slug: "save-card",
          valid: true,
          was_paid_with_saved_card: false,
        }),
        this.setState({ infoCard }),
        AsyncStorage.setItem('paymentMethodSaved', JSON.stringify(this.state.infoCard[0])),
      )
      :
      (console.log("No"))
    )
    ) */
  }

  componentWillUnmount() {
    this.setState({});
    this.props = {};
    console.log('Quitando Componente');
  }

  showSuccess = () => {
    // this.props.navigation.navigate('Checkout', {
    //   transition: 'bottom',
    // });
    // Hack screen
    globalThis.ReactApplication.ActiveScreen(
      'Checkout___payment',
      {
        transition: 'bottom',
      },
      this.props.navigation,
    );
  };

  async cartProducts() {
    console.log('----->', this.props.orders);
    await this.props.orders.map((product, i) =>
      product.products.map(
        (productDetail, i) => (
          this.state.itemBrand.push(productDetail.product.store.name),
          this.state.itemId.push(productDetail.id),
          this.state.itemName.push(productDetail.product.name),
          this.state.itemVariant.push(
            productDetail.attribute === null
              ? ''
              : productDetail.attribute.tree_string,
          )
        ),
      ),
    );

    await analytics().logViewCart({
      value: this.props.cartPrice,
      currency: 'MXN',
      items: [
        {
          item_brand: this.state.itemBrand.join(', ').toString(),
          item_id: this.state.itemId.join(', ').toString(),
          item_name: this.state.itemName.join(', ').toString(),
          item_variant: this.state.itemVariant.join(', ').toString(),
          //item_category: 'round necked t-shirts',
        },
      ],
    });
  }

  render() {
    const { orders, unavailableOrders, cartPrice, productsCount } = this.props;
    const subtotal = (
      <CRRowBoth addedStyle={layout.subtotalContainer}>
        <View style={layout.subtotalOrder}>
          <CRText variant={typography.subtitle3} color={colors.colorDark100}>
            Subtotal
          </CRText>
          <CRText variant={typography.subtitle3}>
            {formatNumberToPrice(cartPrice)}MXN
          </CRText>
        </View>
      </CRRowBoth>
    );
    return (
      <View>
        <ScrollView style={layout.cartContainer}>
          <CRTitle title="Mi carrito" />

          {/* UNAVAILABLE ORDERS */}
          {unavailableOrders.length > 0 && (
            <CartSummary
              onPress={() => {
                //this.props.navigation.navigate('ProductNotAvailable');

                globalThis.ReactApplication.ActiveScreen(
                  'ProductNotAvailable',
                  {},
                  this.props.navigation,
                );
              }}
            />
          )}
          {/* END: UNAVAILABLE ORDERS */}
          {orders.length > 0 ? (
            <View style={layout.wrapper}>
              <CRRowBoth addedStyle={layout.productsCountContainer}>
                <CRText
                  variant={typography.parapgraph}
                  color={colors.colorDark100}
                  align="center"
                >
                  {productsCount} producto{productsCount > 1 ? 's' : ''}
                </CRText>
              </CRRowBoth>
              <CRRowBoth addedStyle={layout.ordersContainer}>
                <FieldArray
                  name="orders"
                  component={CartOrder}
                  showCalendar
                  showShippingMethods={false}
                  onShippingMethodsPressed={this.props.renderShippingMethods}
                  onShippingDatePressed={this.props.renderShippingDate}
                  closeModalWindow={this.props.closeModalWindow}
                  /*reduxFormChange={reduxFormChange}
              pickupSchedules={pickupSchedules}*/
                />
              </CRRowBoth>
              <View
                style={
                  orders.length === 1
                    ? layout.containerSubTotalUno
                    : layout.containerSubTotalDos
                }
              >
                {subtotal}
              </View>
              {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
              {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
            </View>
          ) : (
            <React.Fragment>
              {unavailableOrders.length > 0 ? <Text /> : <EmptyShoppingCart />}
            </React.Fragment>
          )}
          {/* END LAYOUT */}
        </ScrollView>
        {/* CONTINUE BUTTON */}
        {orders.length > 0 ? (
          <View style={layout.viewBottomShoppingCart}>
            <CRBottomButton
              onPress={() => {
                this.showSuccess();
              }}
              customContainerBtn={true}
            >
              <CRText
                variant={typography.subtitle3}
                color={colors.colorWhite}
                align="center"
              >
                Continuar
              </CRText>
            </CRBottomButton>
          </View>
        ) : (
          <Text />
        )}
        {/* END: CONTINUE BUTTON */}
      </View>
    );
  }
}
// Wrap component within reduxForm
ShoppingCart = withShoppingCartUI(ShoppingCart, {});

// Pass Redux state and actions to component
function mapStateToProps(state, props) {
  // Define or retrieve default form values
  const orders: Array<Object> = globalThis.ShoppingCartManager.getOrders();
  const coupon: Object = globalThis.ShoppingCartManager.getCoupon();
  const shippingAddress: Object = globalThis.ShoppingCartManager.getShippingAddress();
  const paymentMethod: Object = globalThis.ShoppingCartManager.getPaymentMethod();
  const requiresInvoice: boolean = globalThis.ShoppingCartManager.getRequiresInvoice();

  return {
    coupon,
    orders,
    paymentMethod,
    shippingAddress,
    requiresInvoice,
    user: state.user,
    cardsList: state.user.cards,
    addressList: globalThis.ShoppingCartManager.getAddresses(),
    paymentMethodsList: globalThis.ShoppingCartManager.getPaymentMethods(),
    totalPrice: globalThis.ShoppingCartManager.getCartGrandTotal(),
    cartPrice: globalThis.ShoppingCartManager.getCartProductsTotal(),
    shippingPrice: globalThis.ShoppingCartManager.getShippingPrice(),
    productsCount: globalThis.ShoppingCartManager.getCartProductsCount(),
    //unavailableOrders: globalThis.ShoppingCartManager.getUnavailableOrders(),
    formErrors: getFormSyncErrors(SHOPPING_CART_FORM_CONFIG.formName)(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    ...bindActionCreators(
      {
        removeFromCart: shoppingCartActions.removeFromCart,
        updateCartProduct: shoppingCartActions.updateCartProduct,
        fetchShoppingCart: shoppingCartActions.fetchShoppingCart,
      },
      dispatch,
    ),
  };
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);
