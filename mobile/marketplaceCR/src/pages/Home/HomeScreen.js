import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Sentry from '@sentry/react-native';
import {
  View,
  Text,
  ScrollView,
  Platform,
  Image,
  TouchableOpacity,
} from 'react-native';

import {
  userActions,
  appActions,
  shoppingCartActions,
  collectionActions,
} from '../../redux/actions';
import { PRODUCT_TYPES } from '../../redux/constants/app.constants';
import { formatTitle } from '../../utils/formatTitle';

console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed', 'Warning'];
console.disableYellowBox = true;

//DS
import { styles } from './HomeScreenStyles';
//import { CRButler } from '@canastarosa/ds-reactnative/components/CRButler';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import ZipCodeObserver from '../../components/ZipCodeObserver/ZipCodeObserver';

//Will be imported to the ds
import { CRBanner } from '../../components/HomeScreen/CRBanner';
import { CRRowBoth } from '../../components/Layout';

//import Analytics from 'appcenter-analytics';
import analytics from '@react-native-firebase/analytics';

import {
  CRProductCard,
  CRStoreCard,
  CRHomeSectionCard,
  CRCollectionCard,
} from '../../components/CRCard';
import { CRRowLeft, CRRowFull } from '../../components/Layout';
import hotsaleBanner from './../../images/hotsale/banner.png';
import nl from './../../images/assets/landings/nl.jpg';
import jal from './../../images/assets/landings/jal.jpg';

declare var globalThis: any;

export class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
    //this.remove = this.remove.bind(this);
  }

  async componentDidMount() {
    globalThis.ReactApplication.BackScreen.disable = false;
    try {
      if (!this.props.isLogged) {
        this.props.loginAsGuest();
      }
    } catch (error) {
      //alert(error);
    }
    this.props.getFeaturedBanners();
    this.props.getFeaturedProducts();
    this.props.getFeaturedStores();
    this.props.fetchShoppingCart();
    this.props.getAllCollections();
    await analytics().setCurrentScreen('HomeScreen');
    await analytics().logEvent('HomeScreen');
  }

  render() {
    let storeLandings;
    try {
      storeLandings =
        globalThis.ReactApplication.HomeScreen.store.landings.data;
    } catch (e) {
      storeLandings = [];
      Sentry.captureException(e);
    }

    const {
      stores,
      products,
      slides,
      navigation,
      route,
      allcollections,
      zipCode,
    } = this.props;
    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 10;
    const SPACER_SIZE_ANDROID = 10;

    return (
      <ScrollView style={styles.home}>
        <ZipCodeObserver
          observe="HomeScreen"
          isFocused={() => {
            return this.props.navigation.isFocused();
          }}
          navigation={this.props.navigation}
        />
        <CRBanner {...this.props} slides={slides} />

        {storeLandings.map((item, index) => {
          if (item.slug == 'landing/activacion-mty') {
            return (
              <TouchableOpacity
                onPress={() => {
                  globalThis.ReactApplication.ActiveScreen(
                    'NewStoresNLJAL',
                    {
                      stores: 'NL',
                      title: '¡Hola Monterrey!',
                    },
                    this.props.navigation,
                  );
                }}
                key={index}
              >
                <View style={styles.landingCard}>
                  <View style={styles.landingCardImage}>
                    <Image source={nl} style={styles.imageLanding} />
                  </View>
                  <View style={styles.landingCardInfo}>
                    <Text style={styles.landingCardTitleMTY}>{item.title}</Text>
                    <Text style={styles.landingCardDescriptionMTY}>
                      {item.message}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          } else {
            return (
              <TouchableOpacity
                onPress={() => {
                  globalThis.ReactApplication.ActiveScreen(
                    'NewStoresNLJAL',
                    {
                      stores: 'JAL',
                      title: '¡Hola Guadalajara!',
                    },
                    this.props.navigation,
                  );
                }}
                key={index}
              >
                <View style={styles.landingCardJal}>
                  <View style={styles.landingCardImage}>
                    <Image source={jal} style={styles.imageLanding} />
                  </View>
                  <View style={styles.landingCardInfo}>
                    <Text style={styles.landingCardTitleGDL}>{item.title}</Text>
                    <Text style={styles.landingCardDescriptionGDL}>
                      {item.message}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          }
        })}

        <CRRowFull>
          <CRRowLeft addedStyle={styles.title}>
            <CRText variant={typography.subtitle} color={colors.colorDark300}>
              Colecciones increíbles
            </CRText>
          </CRRowLeft>
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={styles.carrousel}
          >
            {allcollections && (
              <CRRowLeft addedStyle={styles.float}>
                <CRCollectionCard
                  store={formatTitle(allcollections[0].name.toLowerCase())}
                  thumbImg={allcollections[0].cover.small}
                  //storeLogo={allcollections[0].cover.small}
                  slogan={formatTitle(
                    allcollections[0].description.toLowerCase(),
                  )}
                  marginBottom={10}
                  onPress={async () => {
                    globalThis.ReactApplication.ActiveScreen(
                      'Collection',
                      {
                        slug: allcollections[0].slug,
                      },
                      this.props.navigation,
                    );
                  }}
                />
                <CRCollectionCard
                  store={formatTitle(allcollections[1].name.toLowerCase())}
                  thumbImg={allcollections[1].cover.small}
                  //storeLogo={allcollections[1].cover.small}
                  slogan={formatTitle(
                    allcollections[1].description.toLowerCase(),
                  )}
                  marginBottom={10}
                  onPress={async () => {
                    navigation.push('Collection', {
                      slug: allcollections[1].slug,
                    });
                  }}
                />
                <CRCollectionCard
                  store={formatTitle(allcollections[2].name.toLowerCase())}
                  thumbImg={allcollections[2].cover.small}
                  //storeLogo={allcollections[2].cover.small}
                  slogan={formatTitle(
                    allcollections[2].description.toLowerCase(),
                  )}
                  marginBottom={10}
                  onPress={async () => {
                    navigation.push('Collection', {
                      slug: allcollections[2].slug,
                    });
                  }}
                />
              </CRRowLeft>
            )}

            <CRCollectionCard
              store="Nuevas tiendas"
              thumbImg="https://canastarosa.s3.us-east-2.amazonaws.com/media/__sized__/featured/market/store/6e5707fa72c98fc3e18c2abdd00f0103-crop-c0-5__0-5-480x356-90.jpg"
              //storeLogo="https://canastarosa.s3.us-east-2.amazonaws.com/media/__sized__/featured/market/store/6e5707fa72c98fc3e18c2abdd00f0103-crop-c0-5__0-5-480x356-90.jpg"
              slogan="Lo nuevo en Canasta"
              marginBottom={10}
              onPress={() => {
                globalThis.ReactApplication.ActiveScreen(
                  'NewStores',
                  {},
                  this.props.navigation,
                );
              }}
            />
            {/* <CRHomeSectionCard
              name="Nuevas tiendas"
              size="half"
              thumbImg="https://canastarosa.s3.us-east-2.amazonaws.com/media/__sized__/featured/market/store/6e5707fa72c98fc3e18c2abdd00f0103-crop-c0-5__0-5-480x356-90.jpg"
              onPress={() => {
                navigation.navigate('NewStores');
              }}
            /> */}
            <CRRowLeft />
          </ScrollView>
        </CRRowFull>

        <View
          style={{
            borderBottomColor: colors.colorGray200,
            borderBottomWidth: 1,
          }}
        />

        <CRRowFull>
          <CRRowLeft addedStyle={styles.title}>
            <CRText variant={typography.subtitle} color={colors.colorDark300}>
              Selección Canasta
            </CRText>
          </CRRowLeft>
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={styles.carrousel}
          >
            {products.map((item, index) => (
              <CRProductCard
                key={index}
                thumbImg={{ uri: item.product.photo.small }}
                productTitle={item.product.name}
                store={item.product.store.name}
                price={item.product.price}
                shippingMethod={
                  item.product.physical_properties.shipping_methods
                }
                priceWithoutDiscount={item.product.price_without_discount}
                discount={item.product.discount}
                rating={
                  item.product.mean_product_score > 0 &&
                  item.product.mean_product_score.toFixed(1)
                }
                marginBottom={10}
                onPress={() => {
                  globalThis.ReactApplication.ActiveScreen(
                    'ProductDetail',
                    {
                      itemProductSlug: item.product.slug,
                      itemStoreSlug: item.product.store.slug,
                    },
                    this.props.navigation,
                  );
                }}
              />
            ))}
            {/*Right most margin*/}
            <CRRowLeft />
          </ScrollView>
        </CRRowFull>

        <View
          style={{
            borderBottomColor: colors.colorGray200,
            borderBottomWidth: 1,
          }}
        />

        <CRRowFull>
          <CRRowLeft addedStyle={styles.title}>
            <CRText variant={typography.subtitle} color={colors.colorDark300}>
              Nuestras tiendas favoritas
            </CRText>
          </CRRowLeft>
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={styles.carrouselStore}
          >
            {stores.map((item, index) => (
              <CRStoreCard
                key={index}
                store={item.store.name}
                thumbImg={item.photo.small}
                storeLogo={item.store.photo.small}
                slogan={item.store.slogan}
                rating={
                  item.store.mean_store_score > 0 &&
                  item.store.mean_store_score.toFixed(1)
                }
                marginBottom={10}
                onPress={() => {
                  globalThis.ReactApplication.ActiveScreen(
                    'StoreScreen',
                    {
                      itemStore: item.store.slug,
                    },
                    this.props.navigation,
                  );
                }}
              />
            ))}
            {/*Right most margin*/}
            <CRRowLeft />
          </ScrollView>
        </CRRowFull>
        {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
        {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
      </ScrollView>
    );
  }
}

// Redux Map Functions
function mapStateToProps(state) {
  const { app, user, collection } = state;
  return {
    slides: app.featuredBanners.banners,
    stores: app.featuredStores.stores,
    products: app.featuredProducts.products.filter(
      (p) => p.product.product_type.value === PRODUCT_TYPES.PHYSICAL,
    ),
    categories: app.categories.categories,
    isLogged: user.isLogged,
    allcollections: collection.allcollections,
    zipCode: app.zipCode,
  };
}
function mapDispatchToProps(dispatch) {
  const {
    getFeaturedBanners,
    getFeaturedProducts,
    getFeaturedStores,
    getCategories,
  } = appActions;

  const { getAllCollections } = collectionActions;

  const { loginAsGuest } = userActions;
  const { fetchShoppingCart } = shoppingCartActions;

  return bindActionCreators(
    {
      getFeaturedBanners,
      getFeaturedProducts,
      getFeaturedStores,
      getCategories,
      loginAsGuest,
      getAllCollections,
      fetchShoppingCart: fetchShoppingCart,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
