import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';
import { typography, buttons } from '../../assets';

console.log('Ancho!!!', Dimensions.get('window').width);
console.log('Alto!!!', Dimensions.get('window').height);

export const styles = StyleSheet.create({
  imageLanding: {
    width: 140,
    height: 75,
    resizeMode: 'contain',
  },
  landingCard: {
    display: 'flex',
    backgroundColor: '#2e3353',
    flexDirection: 'row',
    width: '100%',
    marginBottom: 3,
  },
  landingCardJal: {
    display: 'flex',
    backgroundColor: '#f5d7d4',
    flexDirection: 'row',
    width: '100%',
    marginTop: 0,
  },
  landingCardInfo: {
    paddingLeft: 0,
    paddingTop: 10,
    paddingRight: 150,
    width: '100%',
  },
  landingCardTitleMTY: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  landingCardDescriptionMTY: {
    color: 'white',
    fontSize: 14,
  },
  landingCardTitleGDL: {
    color: colors.colorPeach400,
    fontSize: 16,
    fontWeight: 'bold',
  },
  landingCardDescriptionGDL: {
    color: colors.colorPeach400,
    fontSize: 14,
  },

  home: {
    backgroundColor: '#FAF7F5',
  },
  title: {
    marginTop: 15,
    marginBottom: 10,
  },
  bannerContainer: {
    display: 'flex',
    width: '100%',
    backgroundColor: '#F4783A',
    borderBottomColor: '#E9254F',
    borderBottomWidth: 4,
    alignItems: 'center',
  },

  float: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  carrousel: { paddingLeft: 10 },

  carrouselStore: { paddingLeft: 10, marginBottom: 100 },

  logoContainer: {
    backgroundColor: colors.colorDark400,
    paddingVertical: 34,
  },

  containerBtnAdd: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  btnAddToCart: {
    ...buttons.squareCart,
    width: '100%',
    height: 60,
  },
  txtAddToCart: {
    textAlign: 'center',
    ...typography.txtRegular,
    color: colors.colorWhite,
    fontSize: 18,
  },
});
