import { StyleSheet } from 'react-native';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography, buttons } from '../../assets';

export const styles = StyleSheet.create({
  menuWrapper: {
    backgroundColor: '#FAF7F5',
    marginTop: 15,
  },

  breadcrumbs: {
    fontSize: 16,
    justifyContent: 'center',
    marginTop: 10,
    flexDirection: 'row',
  },
  breadcrumbsOption: {
    marginRight: 2,
    color: colors.colorGray400,
  },
  iconArrow: {
    marginHorizontal: 7,
  },
  iconArrowDown: {
    marginHorizontal: 5,
    alignSelf: 'center',
  },
  btnCategory: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subContainer: {
    //backgroundColor: colors.colorViolet100,
    flexDirection: 'column',
    //height: HEIGHT,
  },
  cover: {
    height: 80,
  },

  tagsContainer: {
    //height: 50,
  },
  containerCatalog: {
    backgroundColor: '#FAF7F5',
    flexDirection: 'column',
    paddingTop: 20,
    flex: 1,
    //justifyContent: 'space-between',
    //height: 500,
  },
  categoryResults: {
    backgroundColor: colors.colorYellow100,
    height: '100%',
  },
  categoryName: {
    ...typography.txtExtraBold,
    color: colors.colorDark300,
    textAlign: 'center',
    marginTop: 0,
    marginBottom: 0,
  },
  arrowBtn: {
    top: 20,
    left: 15,
  },
  filtersContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 15,
    marginBottom: 15,
    alignItems: 'center',
  },
  categoryResultsList: {
    marginHorizontal: 8,
  },
  results: {
    ...typography.txtLight,
    color: colors.colorGray400,
  },

  //SUBCATEGORIES LIST
  containerSubcategories: {
    marginVertical: 20,
    height: 55,
  },
  subcategory: {
    margin: 10,
    ...buttons.btnInterests,
    borderColor: 'transparent',
  },
  subcategoryActive: {
    margin: 10,
    ...buttons.btnInterests,
    backgroundColor: colors.colorMain300,
  },
  subcategoryName: {
    ...typography.txtLight,
    padding: 8,
    textAlign: 'center',
  },
  subcategoryNameActive: {
    ...typography.txtLight,
    padding: 8,
    textAlign: 'center',
    color: colors.colorWhite,
  },
  filterName: {
    ...typography.txtLight,
    padding: 8,
    textAlign: 'center',
    color: colors.colorDark300,
  },
});
