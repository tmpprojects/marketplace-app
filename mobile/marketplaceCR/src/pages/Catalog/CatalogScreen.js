import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Animated,
} from 'react-native';
import { appActions } from '../../redux/actions';
import { Filters } from '../../components/Filters/Filters';
import Loader from '../../components/Loader';
import VirtualListContainer from '../../components/VirtualList/VirtualListContainer/VirtualListContainer';
import arrow from '../../images/icon/icon_arrow_right.png';
import Tags from '../../components/Tags';
import { styles } from './CatalogScreenStyles';
import { formatTitle } from '../../utils/formatTitle';
import ZipCodeObserver from '../../components/ZipCodeObserver/ZipCodeObserver';

//DS
import { colors } from '@canastarosa/ds-theme/colors';
import { typography } from '@canastarosa/ds-theme/typography';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';

//Wil be added to the ds
import { CRIcon } from '../../components/CRIcon';
import { CRRowLeft, CRRowBoth } from '../../components/Layout';
import { CRDinamicMenu } from '../../components/CRDinamicMenu/CRDinamicMenu';

//Google Analytics
import analytics from '@react-native-firebase/analytics';

////

let maxItemsPerPage = 15;
let maxItemsInfiniteLoading = 15;
let infiniteItems = [];
//let activeRange = 0;
let slugHistory = '';
let filterHistory = '';
let currentPage = 1;
let currentSlug = '';
let dynamicLoading = false;
let cacheScreen = {};

/**
CatalogScreen Component
 */
type CatalogScreenProps = {
  navigation: { getParam: Function, goBack: Function },
  categories: Array<{ slug: string, children: Array<{}> }>,
  categoryResults: { loading: boolean, results: Array<{}> },
};
type CatalogScreenState = {
  isModalVisible: boolean,
  showItems: Array<{}>,
  sendScreen: string,
  activeSlug: string,
  disabledInfinite: boolean,
  categoryNameTitle: string,
};

declare var globalThis: any;

export class CatalogScreen extends PureComponent<
  CatalogScreenProps,
  CatalogScreenState,
> {
  categoryName: string;

  createCache(categorySlug) {
    const cacheSlug = globalThis.ReactApplication.utils.sanitizeslug(
      categorySlug,
    );
    let verify = globalThis.ReactApplication.cacheScreen[cacheSlug];
    if (!Boolean(verify)) {
      globalThis.ReactApplication.cacheScreen[cacheSlug] = {
        filter: {},
        stockelements: [],
      };
    }
    return globalThis.ReactApplication.cacheScreen[cacheSlug];
  }

  constructor(props) {
    super(props);
    const { navigation, route } = this.props;

    const { categoryName, sender, categorySlug } = route.params;

    currentSlug = categorySlug;
    const sendScreen = sender;
    this.categoryName = categoryName;

    this.initState = {
      isModalVisible: false,
      showItems: [],
      sendScreen: sendScreen,
      activeSlug: currentSlug,
      disabledInfinite: false,
      categoryNameTitle: this.categoryName,
      loading: true,
      scrolling: 0,
    };
    this.state = this.initState;
    /*************** cache ********************/
    cacheScreen = this.createCache(currentSlug);
    /*************** cache ********************/
  }

  saveCache = (filter = {}, stockelements = []) => {
    if (stockelements.length > 0) {
      console.log(
        'saveCache: (cacheScreen.stockelements.length ---------------> ',
        cacheScreen.stockelements.length,
      );
      console.log(
        'saveCache: stockelements.length---------------> ',
        stockelements.length,
      );
      if (
        Number(stockelements.length) > Number(cacheScreen.stockelements.length)
      ) {
        cacheScreen.stockelements = infiniteItems;
        cacheScreen.filter = filter;
        console.log('saveCache: Se guardo el cache --------------->');
      }
    }
    return true;
  };
  validateCache = () => {
    if (cacheScreen.stockelements.length > 0) {
      return true;
    }
    return false;
  };
  getCache = async () => {
    console.log('se esta ejecutando get cache---->', cacheScreen);

    this.setState(this.initState);
    this.setupInfiniteItems(cacheScreen.stockelements),
      // await this.setState({
      //   ...this.state,
      //   isModalVisible: false,
      //   showItems: cacheScreen.stockelements,
      //   sendScreen: sendScreen,
      //   activeSlug: currentSlug,
      //   disabledInfinite: false,
      //   loading: false,
      // });
      // dynamicLoading = false;

      console.log('this.state---->', this.state);
    // this.onFilterSubmit(
    //   currentSlug,
    //   cacheScreen.filter,
    //   cacheScreen.filter.page,
    //   maxItemsInfiniteLoading,
    // );
  };

  async componentDidMount() {
    try {
      /*
      //await this.onFilterSubmit(currentSlug);
      if (Boolean(this.validateCache())) {
        console.log('inicializa el cache algo se guardo previamente');
        this.resetResults();
        this.getCache();
        
      } else {
        console.log('Carga normal');
        this.resetResults();
        this.initContent();
      }
      */

      this.resetResults();
      this.initContent();

      //Google Analytics
      await analytics().setCurrentScreen('CatalogScreen');
      await analytics().logEvent('CatalogScreen');
    } catch (e) {
      console.log(e);
    }
  }
  componentWillUnmount() {
    //activeRange = 0;
    // slugHistory = '';
    // filterHistory = '';
    // currentPage = 1;
    // infiniteItems = [];
    // //this.setState(this.initState);
  }

  async initContent() {
    infiniteItems = [];

    // Build search query and perform content fetch
    await this.onFilterSubmit(currentSlug);

    // If we´ve got results, update stateshowMore
    if (this.props.categoryResults.results) {
      this.setState(
        this.initState,
        this.setupInfiniteItems(this.props.categoryResults.results),
      );
    }
    console.log('init content');
  }

  componentWillReceiveProps(nextProps: CatalogScreenProps) {
    const { categoryResults } = nextProps;

    // If results have changed, render list again.
    if (
      categoryResults.results &&
      categoryResults.results !== this.props.categoryResults.results
    ) {
      this.setupInfiniteItems(categoryResults.results);
    }
  }

  setupInfiniteItems = (results: Array<{}>): void => {
    // Add elements to inifite list
    this.setState({
      ...this.state,
      loading: true,
    });
    results.map((element) => infiniteItems.push(element));

    // Update state
    this.setState({
      ...this.state,
      disabledInfinite: results.length < 4 ? true : false, // <-- Disable inifinite scroll if we too few elements
      showItems: infiniteItems,
      loading: false,
    });
  };

  onChangeCategory = async (slug: string, title: string = 'Todo') => {
    //console.log('onChangeCategory', slug);
    globalThis.ReactApplication.ActiveScreen(
      'CatalogScreen',
      {
        categoryName: title,
        categorySlug: slug,
      },
      this.props.navigation,
    );
    console.log('press in subcategory');
    await this.resetResults();
    this.setState(
      {
        ...this.state,
        activeSlug: slug,
        categoryNameTitle: title,
      },
      () => {
        this.onFilterSubmit(slug, filterHistory);
      },
    );
  };

  /**
   * fetchContent()
   * Retrieves results from the API. Call redux actions.
   * @param {string} query | Query string to send to API
   */
  fetchContent = async (query, slug) => {
    dynamicLoading = true;
    await Promise.all([this.props.getCategoryResults(query, slug)]);
    dynamicLoading = false;
  };

  /**
   * onFilterSubmit()
   * @param {string} query | Filters querystring.
   */
  onFilterSubmit = (
    categorySlug: string = this.state.activeSlug,
    filters: string = filterHistory,
    page: number = currentPage,
    itemsPerPage: number = maxItemsPerPage,
  ): void => {
    if (filters) {
      filterHistory = filters;
    }

    if (categorySlug !== slugHistory) {
      infiniteItems = [];
      currentPage = 1;
      filterHistory = '';

      // this.setState({
      //   ...this.state,
      // });
      if (categorySlug) {
        slugHistory = categorySlug;
      }
    }

    // Build query
    const searchQueries: string = buildQuery({
      slug: categorySlug,
      filters,
      page,
      pageSize: itemsPerPage,
    });

    // Fetch filtered content

    (async () => {
      await this.fetchContent(searchQueries, categorySlug);

      /**************** save cache *****************/
      this.saveCache(
        {
          categorySlug,
          filters,
          page,
          itemsPerPage,
        },
        this.props.categoryResults.results,
      );
      /**************** save cache *****************/
    })();
  };

  showMore = async (calc) => {
    //if (Number(calc) !== Number(activeRange) && !dynamicLoading) {
    if (!dynamicLoading && !this.state.disabledInfinite) {
      //dynamicLoading = true;
      //activeRange = calc;
      currentPage++;
      await this.onFilterSubmit(
        this.state.activeSlug,
        filterHistory,
        currentPage,
        maxItemsInfiniteLoading,
      );
    }
  };

  resetResults() {
    currentPage = 1;
    infiniteItems = [];
    this.props.categoryResults.results = null;
    this.setState({
      ...this.state,
      showItems: [],
    });
  }

  /**
   * createBreadcrumbsTree()
   * Returns an array with the marching categories path.
   * @param {array} categories | Array of nested categories to search from.
   * @param {string} targetCategorySlug | Category String to search.
   * @param {bool} found | Indicates if category has been already found.
   * @param {array} parentPath | Array with the parent categories.
   * @returns {array} An array of categories (Model example: [{name, slug}, ...]).
   */
  createBreadcrumbsTree = (
    categories,
    targetCategorySlug,
    found = false,
    parentMap = [],
    parentPath = [],
  ) => {
    const tester = categories.reduce(
      (a, b, index) => {
        let result = a;

        // If category is not found, proceed to recursive loop.
        if (!result.found) {
          // If we've found the matching category, return current result
          if (b.slug === targetCategorySlug) {
            result = {
              path: [...result.path, { name: b.name, slug: b.slug }],
              map: [...result.map, index],
              found: true,
            };

            // else, search whithin current category children.
          } else if (b.children.length) {
            const innerCategoryPath = this.createBreadcrumbsTree(
              b.children,
              targetCategorySlug,
              false,
              [...result.map, index],
              [...result.path, { name: b.name, slug: b.slug }],
            );

            if (innerCategoryPath.found) {
              result = innerCategoryPath;
            }
          }
        }

        // Return current path.
        return result;
      },
      { found, path: parentPath, map: parentMap },
    );

    return tester;
  };

  render() {
    const {
      categories,
      categoryResults,
      categoryLoad,
      navigation,
      route,
    } = this.props;

    // Find Category tree for breadcrumbs.
    const categoryName = route.params.categoryName;
    const breadcrumbs = this.createBreadcrumbsTree(
      categories,
      this.state.activeSlug,
    );
    const breadcrumbsPath = breadcrumbs.path;

    const subcategoriesList = breadcrumbs.map.reduce(
      (childAttribute, childIndex, index) => {
        if (index === 0) {
          return categories[childIndex].children;
        }
        return childAttribute[childIndex].children;
      },
      [],
    );
    // Preloader
    let loadingContent = this.state.loading;

    ////////////////////

    //Dinamic menu constants
    const menuHeight = 90;
    const scrollY = new Animated.Value(0);
    const diffClamp = Animated.diffClamp(scrollY, 0, menuHeight);
    const translateY = diffClamp.interpolate({
      inputRange: [0, menuHeight],
      outputRange: [0, menuHeight * -1],
    });

    const config = {
      products: this.state.showItems,
      screen: this.state.sendScreen,
      category: this.state.activeSlug,
      navigation: this.props.navigation,
      refreshing: true,
      type: 'products',
      showMore: () => {
        this.showMore();
      },
      request: this.props.categoryResults,
      disabledInfinite: this.state.disabledInfinite,
    };

    // Return JSX
    return (
      <View style={styles.containerCatalog}>
        <ZipCodeObserver
          observe="CatalogScreen"
          isFocused={() => {
            return this.props.navigation.isFocused();
          }}
          categorySlug={this.state.activeSlug}
          categoryName={categoryName}
          navigation={this.props.navigation}
        />

        <CRDinamicMenu position={translateY}>
          <View style={styles.menuWrapper}>
            <CRText
              variant={typography.subtitle}
              color={colors.colorDark200}
              align="center"
            >
              {formatTitle(this.categoryName.toLowerCase())}
            </CRText>
            {/* BREADCRUMBS */}
            {breadcrumbsPath.length > 0 && (
              <CRRowBoth addedStyle={styles.breadcrumbs}>
                <TouchableOpacity
                  style={styles.btnCategory}
                  onPress={() => {
                    //this.onChangeCategory(this.currentSlug, this.categoryName);
                    this.props.navigation.goBack(null);
                  }}
                >
                  <CRIcon
                    name="categories"
                    size={15}
                    color={colors.colorGray300}
                  />
                  <CRIcon
                    name="angle--right"
                    size={10}
                    color={colors.colorGray400}
                    style={styles.iconArrow}
                  />
                </TouchableOpacity>
                {breadcrumbsPath.map((c, index) => {
                  if (c.slug !== this.state.activeSlug) {
                    return (
                      <TouchableOpacity
                        style={styles.btnCategory}
                        onPress={() => {
                          this.onChangeCategory(c.slug, c.name);
                        }}
                        key={index}
                      >
                        <Text style={styles.breadcrumbsOption}>
                          {formatTitle(c.name.toLowerCase())}
                        </Text>
                        <CRIcon
                          name="angle--right"
                          size={10}
                          color={colors.colorGray400}
                          style={styles.iconArrow}
                        />
                      </TouchableOpacity>
                    );
                  }
                })}
                <Text style={styles.breadcrumbsOption}>
                  {formatTitle(this.state.categoryNameTitle.toLowerCase())}
                </Text>
                <CRIcon
                  name="angle--down"
                  size={7}
                  color={colors.colorMain300}
                  style={styles.iconArrowDown}
                />
              </CRRowBoth>
            )}
            {!loadingContent && (
              <View style={styles.tagsContainer}>
                <View
                  style={
                    subcategoriesList.length > 0
                      ? styles.containerSubcategories
                      : null
                  }
                >
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {/* Added a margin in the left */}
                    <CRRowLeft />
                    <Tags
                      props={this.props}
                      tags={subcategoriesList}
                      onChange={this.onChangeCategory}
                      activeSlug={this.state.activeSlug}
                    />

                    {/* {subcategoriesList[0].children.map((item, i) => (
                    <TouchableOpacity
                      onSubmit={() => {}}
                      key={i}
                      style={styles.subcategory}
                      onPress={changeCategory(item.slug)}>
                      <Text style={styles.subcategoryName}>{item.name}</Text>
                    </TouchableOpacity>
                  ))} */}
                  </ScrollView>
                </View>

                <View
                  style={
                    subcategoriesList.length > 0
                      ? styles.filtersContainer
                      : [styles.filtersContainer, { marginTop: 15 }]
                  }
                >
                  <Text style={styles.results}>
                    {categoryResults.count} productos
                  </Text>
                  <Filters
                    categoryResults={categoryResults.results}
                    categoryName={categoryName}
                    categorySlug={this.state.activeSlug}
                    isenabled={true}
                    onFilterSubmit={async (e, filters) => {
                      await this.resetResults();
                      this.onFilterSubmit(e, filters);
                    }}
                  />
                </View>
              </View>
            )}
            {/* END: BREADCRUMBS */}
          </View>
        </CRDinamicMenu>
        {loadingContent ? (
          <Loader />
        ) : (
          <View style={styles.subContainer}>
            <CRRowLeft>
              <VirtualListContainer
                config={config}
                onScrollAction={(e) => {
                  scrollY.setValue(e.nativeEvent.contentOffset.y);
                  this.setState({
                    ...this.state,
                    scrolling: e.nativeEvent.contentOffset.y,
                  });
                }}
                scrolling={this.state.scrolling}
                paddingTop={
                  subcategoriesList.length > 0
                    ? menuHeight + 100
                    : menuHeight + 12
                }
              />
            </CRRowLeft>
          </View>
        )}
      </View>
    );
  }
}

const buildQuery = (categoryQuery): string => {
  return `?ordering=${categoryQuery.filters}&category__slug=${categoryQuery.slug}&page=${categoryQuery.page}&page_size=${categoryQuery.pageSize}`;
};

function mapStateToProps(state) {
  const { app } = state;
  return {
    categoryResults: app.categoryResults.results,
    categories: app.categories.categories,
    categoryLoad: app.categoryResults,
  };
}
function mapDispatchToProps(dispatch) {
  const { getCategoryResults } = appActions;
  return bindActionCreators({ getCategoryResults }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CatalogScreen);
