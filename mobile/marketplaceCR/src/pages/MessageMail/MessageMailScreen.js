import React, { Component } from 'react';
import { View, SafeAreaView, Image, TouchableOpacity } from 'react-native';

import pass from '../../images/illustration/recover_pass.png';
import { styles } from './MessageMailScreenStyles';
import { CRTitle } from '../../components/CRTitle';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
declare var globalThis: any;
export default class MessageMailScreen extends Component {
  render() {
    return (
      <SafeAreaView style={styles.background}>
        <View style={styles.messageContainer}>
          <Image source={pass} style={styles.image} />
          <CRTitle title="Revisa tu correo" />
          <CRText
            variant={typography.paragraph}
            color={colors.colorDark300}
            align="center"
          >
            Hemos enviado un correo a la cuenta que nos proporcionaste con los
            pasos a seguir para reestablecer tu contraseña
          </CRText>
        </View>
        <View style={styles.inputContainer}>
          <TouchableOpacity
            style={styles.btnLogin}
            activeOpacity={0.5}
            onPress={
              () => {
                globalThis.ReactApplication.ActiveScreen(
                  'LoginScreen',
                  {},
                  this.props.navigation,
                );
              }
              //this.props.navigation.navigate('LoginScreen')
            }
          >
            <CRText
              variant={typography.button}
              color={colors.colorMain300}
              align="center"
            >
              Aceptar
            </CRText>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}
