import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  background: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  messageContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    textAlign: 'center',
    width: 320,
  },
  icon: {
    marginVertical: 20,
  },
  inputContainer: {
    marginTop: 30,
  },
  image: {
    width: 300,
    height: 230,
    resizeMode: 'cover',
  },
});
