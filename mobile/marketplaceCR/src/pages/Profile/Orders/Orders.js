import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { userTypes } from './../../../redux/constants/index';
import { ordersActions } from './../../../redux/actions';
import { getCustomerOrdersList } from './../../../redux/reducers/orders.reducer';
import OrderCard from './../../../components/Profile/Orders/OrderCard';
import { styles } from './OrdersStyles';
import { Text, ScrollView, View } from 'react-native';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

//will be added to the ds
import { CRRowBoth, CRRowFull } from '../../../components/Layout';
import CRTabWrapper from '../../../components/CRTabs/CRTabWrapper/CRTabWrapper';
import CRTab from '../../../components/CRTabs/CRTab/CRTab';
import { CRTitle } from '../../../components/CRTitle';
import SignInCollaborate from '../../../components/SingInCollaborate/SignInCollaborate';

import Loader from '../../../components/Loader';
//Google Analytics
import analytics from '@react-native-firebase/analytics';

/* ⬇ Show component message if mistakes in child components. */
const ErrorComponent = () => {
  return (
    <Text style={styles.errorMessage}>
      ¡Ho!, tuvimos un problema al intentar cargar tus ordenes. Inténtalo más
      tarde.
    </Text>
  );
};

declare var globalThis: any;
/* ⬇ Show user orders (customer). */
const ShowOrderItems = (props) => {
  return (
    <ScrollView>
      {/* ------- CARD COMPONENT ------- */}

      <OrderCard properties={props.properties} navigation={props.navigation} />
      {/* ------- CARD COMPONENT ------- */}
    </ScrollView>
  );
};
class Orders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shouldRenderComplete: false,
      shouldRenderAll: true,
      shouldRenderInProcess: false,
      orderStatus: 'all',
      hasError: false,
    };
    this.props.getOrders(userTypes.CUSTOMER);
  }

  componentDidMount() {
    analytics().setCurrentScreen('OrdersScreen');
    analytics().logEvent('OrdersScreen');
  }
  showRenderAll = () => {
    this.setState({
      ...this.state,
      shouldRenderAll: true,
      shouldRenderComplete: false,
      shouldRenderInProcess: false,
      orderStatus: 'all',
    });
  };

  showRenderComplete = () => {
    this.setState({
      ...this.state,
      shouldRenderComplete: true,
      shouldRenderAll: false,
      shouldRenderInProcess: false,
      orderStatus: 'delivered',
    });
  };

  showRenderInProcess = () => {
    this.setState({
      ...this.state,
      shouldRenderInProcess: true,
      shouldRenderComplete: false,
      shouldRenderAll: false,
      orderStatus: 'inprogress',
    });
  };

  // Optimize performance
  shouldComponentUpdate(nextProps, nextState) {
    if (
      this.props.orders.loading === true &&
      nextProps.orders.loading === false
    ) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }

  /* ⬇ Function to prevent mistakes in child components. */
  componentDidCatch(error, info) {
    this.setState({
      ...this.state,
      hasError: true,
    });
  }
  preventErrorChild(componentResolve, componentError) {
    return this.state.hasError ? componentError : componentResolve;
  }
  /* ⬆ Function to prevent mistakes in child components. */

  render() {
    const { navigation, orders, isLogged } = this.props;
    let data = {
      orders: orders,
      items: orders.results,
      orderStatus: this.state.orderStatus,
    };
    const {
      shouldRenderAll,
      shouldRenderComplete,
      shouldRenderInProcess,
    } = this.state;
    return (
      <View style={styles.ordersWrapper}>
        {isLogged ? (
          <React.Fragment>
            <CRTitle title="Mis pedidos" />
            <CRRowFull>
              <CRTabWrapper>
                <CRTab
                  title="Mis órdenes"
                  isActive={shouldRenderAll}
                  onPress={() => this.showRenderAll()}
                />
                <CRTab
                  title="En proceso"
                  isActive={shouldRenderInProcess}
                  onPress={() => this.showRenderInProcess()}
                />
                <CRTab
                  title="Finalizados"
                  isActive={shouldRenderComplete}
                  onPress={() => this.showRenderComplete()}
                />
              </CRTabWrapper>
            </CRRowFull>
            <React.Fragment>
              {/* ⬇ Function to prevent mistakes in child components. */}
              {orders.loading === true ? (
                <Loader />
              ) : (
                {
                  ...this.preventErrorChild(
                    <ShowOrderItems
                      properties={data}
                      navigation={navigation}
                      state={this.state}
                    />,
                    <ErrorComponent />,
                  ),
                }
              )}

              {/*  ⬆ Function to prevent mistakes in child components. */}
            </React.Fragment>
          </React.Fragment>
        ) : (
          <SignInCollaborate {...this.props} />
        )}
      </View>
    );
  }
}

// Redux Map Functions
function mapStateToProps(state) {
  const customerOrders = getCustomerOrdersList(state);
  return {
    isLogged: state.user.isLogged,
    orders: customerOrders,
  };
}
function mapDispatchToProps(dispatch) {
  const { getOrders } = ordersActions;
  return bindActionCreators(
    {
      getOrders,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Orders);
