import { StyleSheet } from 'react-native';
import { typography } from '../../../assets';
import { colors } from '@canastarosa/ds-theme/colors';

export const styles = StyleSheet.create({
  ordersWrapper: {
    flex: 1,
    backgroundColor: '#FAF7F5',
  },
  orderDetailWrapper: {
    flex: 1,
    backgroundColor: '#FAF7F5',
  },
  newOrder: {
    color: colors.colorViolet300,
  },
  productOrder: {
    color: colors.colorViolet300,
  },
  shippingOrder: {
    color: colors.colorTurquiose300,
  },
  transitingOrder: {
    color: colors.colorRed300,
  },
  orderDeliveredOrder: {
    color: colors.colorGray300,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: 20,
  },
  arrowBtn: {
    marginHorizontal: 15,
    flex: 2,
  },
  title: {
    flex: 1,
  },
  titleIndicator: {
    ...typography.txtRegular,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  orderProductPrice: {
    alignItems: 'flex-end',
    padding: 10,
  },
  centerStatus: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleCard: {
    ...typography.txtRegular,
    color: colors.colorDark300,
    fontWeight: 'bold',
  },
  subTitleCard: {
    ...typography.txtRegular,
    color: colors.colorDark300,
  },
  scrollView: {
    marginBottom: 300,
  },
  paymentNotice: {
    backgroundColor: colors.colorYellow200,
    padding: 15,
    marginBottom: 20,
    borderRadius: 5,
  },
  ordersList: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,
  },
  ordersCount: {
    ...typography.txtLight,
    color: colors.colorGray400,
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 30,
  },

  //ORDER CARD
  orderDetailContainer: {
    backgroundColor: colors.colorWhite,
    padding: 30,
    marginTop: 20,
    marginBottom: 20,
  },
  listWrapper: {
    borderBottomColor: colors.colorGray200,
    borderBottomWidth: 1,
  },
  orderStore: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 5,
    paddingBottom: 10,
  },
  storeAvatar: {
    width: 45,
    height: 45,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: colors.colorGray200,
  },
  orderStoreSubtotal: {
    alignItems: 'flex-start',
    flex: 1,
  },
  orderStoreName: {
    alignItems: 'flex-start',
    paddingLeft: 10,
    flex: 1,
  },
  shippingDate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  productThumb: {
    width: 64,
    height: 66,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: colors.colorGray200,
  },
  orderQuantity: {
    borderWidth: 1,
    borderColor: colors.colorGray200,
    height: 36,
    width: 36,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
  },
  orderPrice: {
    alignItems: 'flex-end',
    paddingHorizontal: 10,
  },
  orderTotalPrice: {
    marginBottom: 35,
    marginTop: 10,
  },
  orderPriceText: {
    color: colors.colorDark300,
    fontSize: 16,
    textAlign: 'right',
  },
  orderShippingWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 20,
  },
  orderTotalProduct: {
    marginTop: 30,
  },
  titleTotalCard: {
    ...typography.txtSemiBold,
    fontSize: 20,
    textAlign: 'right',
  },
  indicator: {
    width: 6,
    height: '100%',
    backgroundColor: colors.colorMain300,
    position: 'absolute',
  },
  storeImage: {
    width: 60,
    height: 60,
  },

  order: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  orderInfo: {
    flex: 1,
  },
  orderNumber: {
    ...typography.txtSemiBold,
  },
  storeName: {
    ...typography.txtSemiBold,
    color: colors.colorGray400,
  },
  date: {
    ...typography.txtLight,
    color: colors.colorGray400,
  },
  orderDetail: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  detail: {
    marginHorizontal: 25,
  },
  price: {
    ...typography.txtBold,
    fontSize: 18,
    textAlign: 'right',
    marginBottom: 8,
  },
  btnDetails: {
    color: colors.colorGray400,
    textAlign: 'right',
  },

  //MENU

  menuContainer: {
    paddingHorizontal: 15,
    marginTop: 20,
  },
  divider: {
    borderBottomColor: colors.colorGray200,
    borderBottomWidth: 1,
    zIndex: -1,
  },
  activeDivider: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: -2,
    borderBottomColor: colors.colorTurquiose300,
    borderBottomWidth: 3,
    height: '50%',
    width: '90%',
  },
  menu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemMenu: {
    flex: 3,
    padding: 20,
  },
  name: {
    ...typography.txtLight,
    textAlign: 'center',
  },
  nameActive: {
    ...typography.txtSemiBold,
    fontSize: 16,
    color: colors.colorTurquiose300,
    textAlign: 'center',
    borderBottomWidth: 2,
    borderBottomColor: colors.colorTurquiose300,
  },
  btnReviews: {
    color: colors.colorMain300,
    ...typography.txtSemiBold,
    textAlign: 'center',
    marginTop: 15,
  },
  errorMessage: {
    color: colors.colorGray400,
    ...typography.txtLight,
    textAlign: 'center',
    marginTop: 15,
    paddingTop: 50,
    paddingHorizontal: 20,
  },

  //In Order styles

  orderIndicatorStep: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleIndicatorBase: {
    backgroundColor: colors.colorWhite,
    borderRadius: 50,
    width: 26,
    height: 26,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.colorGray200,
    marginBottom: 8,
  },
  circleIndicatorActive: {
    borderColor: colors.colorMain300,
  },
  circleIndicatorCurrent: {
    borderColor: colors.colorMain300,
    borderWidth: 8,
  },

  trackWrapper: {
    position: 'absolute',
    width: '110%',
    height: 87,
    paddingHorizontal: '15%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  orderTrack: {
    width: '100%',
    height: 2,
    borderColor: colors.colorMain300,
    borderWidth: 1,
  },
});
