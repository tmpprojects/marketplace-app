import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Image,
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Platform,
} from 'react-native';
import { styles } from './OrdersStyles';
import placeholder from '../../../../src/images/assets/empty.png';

import { formatNumberToPrice } from './../../../utils/normalizePrice';

import Moment from 'dayjs';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

//will be added to the ds
import { CRCard } from '../../../components/CRCard';
import { CRRowBoth, CRRowFull } from '../../../components/Layout';
import { CRTimeLine } from '../../../components/CRTimeline';
import { CRThumb } from '../../../components/CRThumb';
import { CRTitle } from '../../../components/CRTitle';

//Google Analytics
import analytics from '@react-native-firebase/analytics';

//let shipping = 0;

//Remodified for the bottom cart bar
const SPACER_SIZE_IOS = 90;
const SPACER_SIZE_ANDROID = 100;

declare var globalThis: any;

class OrderDetail extends Component {
  constructor(props) {
    super(props);
    const { navigation, route } = this.props;
    const { initialValue } = route.params;
    this.initialValue = initialValue;
    this.shipping = this.initialValue;
  }

  componentDidMount() {
    analytics().setCurrentScreen('OrderDetailScreen');
    analytics().logEvent('OrderDetailScreen');
  }

  shippingPrice(price) {
    this.shipping += Number(price);
  }
  subtotal(total) {
    return total;
  }

  printImage(obj, properties) {
    let objCheck = obj;
    try {
      for (const prop of properties) {
        if (prop) {
          if (objCheck[prop]) {
            objCheck = objCheck[prop];
          }
        }
      }
    } catch (e) {}
    if (objCheck.length) {
      return (
        <Image
          source={{
            uri: objCheck,
          }}
          style={styles.storeImage}
        />
      );
    } else {
      return <Image source={placeholder} style={styles.storeImage} />;
    }
  }

  ifPhoto(object) {
    if (object.length > 0) {
      return true;
    }
    return false;
  }

  render() {
    //const { navigation } = this.props;
    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';

    return (
      <SafeAreaView style={styles.orderDetailWrapper}>
        <CRRowBoth>
          <CRText
            variant={typography.subtitle2}
            color={colors.colorDark200}
            align="center"
          >
            CR-{this.props.detailOrder.id}
          </CRText>
          <CRText
            variant={typography.paragraph}
            color={colors.colorDark100}
            align="center"
          >
            Realizada:
            <CRText
              variant={typography.bold}
              color={colors.colorDark100}
              align="center"
            >
              &nbsp;
              {Moment(this.props.detailOrder.created).format('DD MMMM YYYY')}
            </CRText>
          </CRText>
        </CRRowBoth>
        <View>
          <View style={styles.centerStatus}>
            <Text style={styles.title}>
              {this.props.detailOrder.payment_status.display_name}
            </Text>
          </View>
        </View>
        <ScrollView>
          <View>
            {/*-----------------   ----------------*/}
            <View>
              {this.props.detailOrder.payment_status.value ===
              'awaiting_payment' ? (
                <CRRowBoth addedStyle={styles.paymentNotice}>
                  <CRText variant={typography.paragraph} align="center">
                    Esta orden está esperando tu pago, por favor, sigue las
                    intrucciones que llegaron a tu correo.
                  </CRText>
                </CRRowBoth>
              ) : null}
            </View>
            {this.props.detailOrder.orders.map((orderIndex, i) => {
              return (
                <CRRowBoth key={`${orderIndex.id}${i}`}>
                  <CRCard size="full" marginBottom={10}>
                    <CRRowFull addedStyle={styles.listWrapper}>
                      <View style={styles.orderStore}>
                        <View style={styles.storeAvatar}>
                          <CRThumb
                            source={{ uri: orderIndex.store.photo.small }}
                            imgHeight={45}
                          />
                        </View>
                        <View style={styles.orderStoreName}>
                          <CRText
                            variant={typography.subtitle3}
                            color={colors.colorDark300}
                          >
                            {orderIndex.store.name}
                          </CRText>
                          <CRText
                            variant={typography.paragraph}
                            color={colors.colorDark300}
                          >
                            {orderIndex.uid}
                          </CRText>
                          <View style={styles.shippingDate}>
                            {orderIndex.physical_properties
                              .shipping_method_name == 'Envíos Nacionales' ? (
                              <CRText
                                variant={typography.paragraph}
                                color={colors.colorDark100}
                              >
                                Entrega Estimada:
                              </CRText>
                            ) : (
                              <CRText
                                variant={typography.paragraph}
                                color={colors.colorDark100}
                              >
                                Entrega:
                              </CRText>
                            )}

                            <CRText
                              variant={typography.bold}
                              color={colors.colorDark100}
                            >
                              &nbsp;
                              {Moment(
                                orderIndex.physical_properties.shipping_date,
                              ).format('DD MMMM')}
                            </CRText>
                          </View>
                        </View>
                      </View>
                    </CRRowFull>
                    {/*----------------- Delivery TimeLine  ----------------*/}
                    <CRTimeLine
                      status={orderIndex.physical_properties.status.value}
                    />
                    {/*----------------- END:Delivery TimeLine  ----------------*/}
                    {/*----------------- PRODUCTS  ----------------*/}
                    {orderIndex.products.map((productIndex, i) => {
                      return (
                        <View key={`${productIndex.product.id}${i}`}>
                          <CRRowFull addedStyle={styles.listWrapper}>
                            <View style={styles.orderStore}>
                              <View style={styles.productThumb}>
                                <CRThumb
                                  source={{
                                    uri: this.ifPhoto(
                                      productIndex.product.photos,
                                    )
                                      ? productIndex.product.photos[0].photo
                                          .small
                                      : '',
                                  }}
                                  imgHeight={64}
                                />
                              </View>
                              <View style={styles.orderStoreName}>
                                <CRText variant={typography.bold}>
                                  {productIndex.product.name}
                                </CRText>
                                {/* <Text style={styles.subTitleCard}>color </Text>*/}
                              </View>
                              <View style={styles.orderQuantity}>
                                <CRText
                                  variant={typography.paragraph}
                                  align="center"
                                >
                                  {productIndex.quantity}
                                </CRText>
                              </View>
                            </View>
                            <View style={styles.orderProductPrice}>
                              <CRText
                                variant={typography.subtitle3}
                                color={colors.colorDark100}
                              >
                                {formatNumberToPrice(productIndex.total_price)}{' '}
                                MXN
                              </CRText>
                            </View>
                          </CRRowFull>
                        </View>
                      );
                    })}

                    <View style={styles.orderTotalPrice}>
                      <View style={styles.orderPrice}>
                        <View style={styles.orderShippingWrapper}>
                          <CRText
                            variant={typography.subtitle3}
                            color={colors.colorDark200}
                            align="left"
                          >
                            Envío
                            <CRText
                              variant={typography.paragraph}
                              color={colors.colorDark100}
                              align="left"
                            >
                              &nbsp;(
                              {
                                orderIndex.physical_properties
                                  .shipping_method_name
                              }
                              )&nbsp;&nbsp;&nbsp;
                            </CRText>
                          </CRText>
                          <CRText
                            variant={typography.subtitle3}
                            color={colors.colorDark200}
                            align="right"
                          >
                            {`${formatNumberToPrice(
                              orderIndex.physical_properties.shipping_price,
                            )}`}
                            {this.shippingPrice(
                              orderIndex.physical_properties.shipping_price,
                            )}{' '}
                            MXN
                          </CRText>
                        </View>
                        <CRText
                          variant={typography.subtitle3}
                          color={colors.colorDark200}
                          align="right"
                        >
                          {formatNumberToPrice(orderIndex.total_price)} MXN
                        </CRText>
                      </View>
                    </View>
                  </CRCard>
                </CRRowBoth>
              );
            })}

            {/*-----------------   ----------------*/}

            <View style={styles.orderDetailProduct}>
              <View style={styles.orderStore}>
                <View style={styles.orderStoreSubtotal}>
                  <CRText
                    variant={typography.subtitle3}
                    color={colors.colorDark200}
                    align="left"
                  >
                    Subtotal
                  </CRText>
                </View>
                <View>
                  <CRText
                    variant={typography.subtitle3}
                    color={colors.colorDark200}
                    align="right"
                  >
                    {formatNumberToPrice(
                      this.subtotal(this.props.detailOrder.order_price),
                    )}{' '}
                    MXN
                  </CRText>
                </View>
              </View>
              <View style={styles.orderStore}>
                <View style={styles.orderStoreSubtotal}>
                  <CRText
                    variant={typography.subtitle3}
                    color={colors.colorDark200}
                    align="left"
                  >
                    Descuento
                  </CRText>
                </View>
                <View>
                  <CRText
                    variant={typography.subtitle3}
                    color={colors.colorDark200}
                    align="right"
                  >
                    {formatNumberToPrice(this.props.detailOrder.discount)} MXN
                  </CRText>
                </View>
              </View>
              <View style={styles.orderStore}>
                <View style={styles.orderStoreSubtotal}>
                  <CRText
                    variant={typography.subtitle3}
                    color={colors.colorDark200}
                    align="left"
                  >
                    Envío
                  </CRText>
                </View>
                <View>
                  <CRText
                    variant={typography.subtitle3}
                    color={colors.colorDark200}
                    align="right"
                  >
                    {formatNumberToPrice(this.shipping)} MXN
                  </CRText>
                </View>
              </View>
            </View>
            {/*-----------------   ----------------*/}

            <View style={styles.orderTotalProduct}>
              <View style={styles.orderStore}>
                <View style={styles.orderStoreSubtotal}>
                  <CRText
                    variant={typography.subtitle2}
                    color={colors.colorMain300}
                    align="right"
                  >
                    Total
                  </CRText>
                </View>
                <View>
                  <CRText
                    variant={typography.subtitle2}
                    color={colors.colorMain300}
                    align="right"
                  >
                    {formatNumberToPrice(this.props.detailOrder.price)} MXN
                  </CRText>
                </View>
              </View>
            </View>
            {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
            {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
// Redux Map Functions
function mapStateToProps(state) {
  return {
    detailOrder: state.orders.detailorder,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetail);
