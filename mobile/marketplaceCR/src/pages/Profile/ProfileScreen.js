import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  Linking,
  Image,
  View,
  TouchableOpacity,
  Text,
  Platform,
  ScrollView,
  Alert,
} from 'react-native';
import {
  getBuildNumber,
  getBundleId,
  getVersion,
} from 'react-native-device-info';
import DocumentPicker from 'react-native-document-picker';
import arrow from '../../images/icon/icon_arrow_right.png';
import camera from '../../images/icon/camera.png';
import defaultProfile from './../../images/icon/profileDefault.png';
import { getUserProfile } from '../../redux/selectors/users.selectors';
import { userActions } from '../../redux/actions';
import { styles } from './ProfileScreenStyles';
import SignUpScreenTerms from '../SignUp/SignUpScreenTerms';
import SignInCollaborate from '../../components/SingInCollaborate/SignInCollaborate';
import { CRIcon } from '../../components/CRIcon';
import { Spinner } from '../../utils/animations/Spinner';
//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
//Google Analytics
import analytics from '@react-native-firebase/analytics';
declare var globalThis: any;
export class ProfileScreen extends Component {
  state = {
    isModalVisible: false,
    uploadProfile: false,
  };

  constructor(props) {
    super(props);
  }

  toggleModal = (visible) => {
    visible
      ? this.setState({ isModalVisible: true })
      : this.setState({ isModalVisible: false });
  };

  componentDidMount() {
    analytics().setCurrentScreen('ProfileScreen');
    analytics().logEvent('ProfileScreen');
  }

  async uploadImage() {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });
      /*
        res.uri,
        res.type, // mime type
        res.name,
        res.size,
      */
      var bodyFormData = new FormData();
      bodyFormData.append('profile_photo', {
        name: res.name,
        type: res.type,
        uri: res.uri,
      });
      this.props.uploadUserPhoto(bodyFormData);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }
  // If it's a SVG then use a mock PNG
  checkImageProfile = (user) => {
    if (user.profile_photo) {
      if (user.profile_photo.small) {
        const currenImage = String(user.profile_photo.small);
        const extension = currenImage.split('.').pop().toLowerCase();
        if (
          extension !== 'svg' &&
          extension !== '' &&
          extension !== null &&
          extension !== undefined
        ) {
          return { uri: user.profile_photo.small };
        } else {
          return defaultProfile;
        }
      }
    }
    return defaultProfile;
  };
  render() {
    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 30;
    const SPACER_SIZE_ANDROID = 30;
    const { navigation, user, isLogged, logout } = this.props;
    return (
      <View style={styles.profileWrapper}>
        {isLogged ? (
          <ScrollView>
            {this.props.userProfile === 'pending' && (
              <View style={styles.uploadPhotoProfile}>
                <View style={styles.uploadPhotoProfileContainer}>
                  <Text style={styles.uploadPhotoProfileTitle}>
                    ¡Cargando tu nueva foto!
                  </Text>
                  <View style={styles.uploadPhotoProfileSpinner}>
                    <Spinner light={true} />
                  </View>
                </View>
              </View>
            )}

            <React.Fragment>
              {/* <View style={styles.BtnsContainer}>
                <EditProfile />
              </View> */}
              <View style={styles.profileContainer}>
                <View style={styles.containerPhoto}>
                  <View style={styles.photoProfile}>
                    <Image
                      source={this.checkImageProfile(user)}
                      style={styles.photo}
                    />
                  </View>
                  <View style={styles.containerBtnAdd}>
                    <TouchableOpacity
                      style={styles.btnAdd}
                      onPress={() => {
                        this.uploadImage();
                      }}
                    >
                      <Image source={camera} style={styles.iconPhoto} />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.containerInfo}>
                  <Text style={styles.name}>
                    {user.first_name} {user.last_name}
                  </Text>
                  <Text style={styles.mail}>{user.email}</Text>
                </View>
              </View>

              <View style={styles.menuProfile}>
                <TouchableOpacity
                  style={styles.containerItemMenu}
                  onPress={() => {
                    //navigation.navigate('Orders');

                    globalThis.ReactApplication.ActiveScreen(
                      'Orders',
                      {},
                      navigation,
                    );
                  }}
                >
                  {/*  <CRIcon
                    name="orders"
                    size={20}
                    color={colors.colorGray300}
                    style={styles.iconItem}
                  />*/}
                  <Text style={styles.itemMenu}>Mis Pedidos</Text>
                  <Image source={arrow} style={styles.iconItemArrow} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.containerItemMenu}
                  onPress={() => {
                    //navigation.navigate('Addresses');
                    globalThis.ReactApplication.ActiveScreen(
                      'Addresses',
                      {},
                      navigation,
                    );
                  }}
                >
                  {/*   <CRIcon
                    name="home"
                    size={20}
                    color={colors.colorGray300}
                    style={styles.iconItem}
                  />*/}
                  <Text style={styles.itemMenu}>Direcciones</Text>
                  <Image source={arrow} style={styles.iconItemArrow} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.containerItemMenu}
                  onPress={() => {
                    //navigation.navigate('PaymentMethods');

                    globalThis.ReactApplication.ActiveScreen(
                      'PaymentMethods',
                      {},
                      navigation,
                    );
                  }}
                >
                  {/* <CRIcon
                    name="card"
                    size={15}
                    color={colors.colorGray300}
                    style={styles.iconItem}
                  />*/}
                  <Text style={styles.itemMenu}>Mis métodos de pago</Text>
                  <Image source={arrow} style={styles.iconItemArrow} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.containerItemMenu}
                  onPress={() => {
                    //navigation.navigate('Reviews');

                    globalThis.ReactApplication.ActiveScreen(
                      'Reviews',
                      {},
                      navigation,
                    );
                  }}
                >
                  {/* <CRIcon
                    name="star--hollow"
                    size={20}
                    color={colors.colorGray300}
                    style={styles.iconItem}
                  /> */}
                  <Text style={styles.itemMenu}>Mis Reseñas</Text>
                  <Image source={arrow} style={styles.iconItemArrow} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.containerItemMenu}
                  onPress={() =>
                    Linking.openURL('https://canastarosa.com/about-us/sell')
                  }
                >
                  {/* <CRIcon
                    name="basket"
                    size={20}
                    color={colors.colorGray300}
                    style={styles.iconItem}
                  />*/}
                  <Text style={styles.itemMenu}>
                    Abre tu tienda en Canasta Rosa
                  </Text>
                  <Image source={arrow} style={styles.iconItemArrow} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.containerItemMenu}
                  onPress={() =>
                    Linking.openURL(
                      'https://canastarosahelp.zendesk.com/hc/es-419',
                    )
                  }
                >
                  {/*<CRIcon
                    name="question"
                    size={20}
                    color={colors.colorGray300}
                    style={styles.iconItem}
                  />*/}
                  <Text style={styles.itemMenu}> Ayuda </Text>
                  <Image source={arrow} style={styles.iconItemArrow} />
                </TouchableOpacity>
              </View>
              <View style={styles.footer}>
                <View style={styles.contact}>
                  <CRText
                    variant={typography.caption}
                    color={colors.colorDark100}
                  >
                    Contáctanos a
                  </CRText>
                  <TouchableOpacity
                    onPress={() =>
                      Linking.openURL('mailto:pedidos@canastarosa.com')
                    }
                  >
                    <CRText
                      variant={typography.caption}
                      color={colors.colorDark400}
                    >
                      &nbsp;pedidos@canastarosa.com
                    </CRText>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity
                  onPress={this.toggleModal}
                  activeOpacity={0.5}
                >
                  <CRText
                    variant={typography.caption}
                    color={colors.colorDark100}
                  >
                    Términos y Condiciones
                  </CRText>
                </TouchableOpacity>
              </View>
              <View style={styles.logoutContainer}>
                <TouchableOpacity
                  style={styles.btnLogout}
                  onPress={async () => {
                    Alert.alert(
                      'Cerrar sesión',
                      '¿Estas seguro de querer cerrar la sesión?',
                      [
                        {
                          text: 'Cancel',
                          style: 'cancel',
                        },
                        {
                          text: 'OK',
                          onPress: async () => {
                            await logout();
                            //navigation.navigate('OnBoardingScreen');

                            globalThis.ReactApplication.ActiveScreen(
                              'OnBoardingScreen',
                              {},
                              navigation,
                            );
                          },
                        },
                      ],
                      { cancelable: false },
                    );
                  }}
                >
                  <CRText
                    variant={typography.caption}
                    color={colors.colorDark100}
                  >
                    Cerrar sesión
                  </CRText>
                  <View style={styles.versionNumber}>
                    <CRText
                      variant={typography.caption}
                      color={colors.colorDark100}
                    >
                      v{getVersion()}
                    </CRText>
                  </View>
                </TouchableOpacity>
              </View>
              <SignUpScreenTerms
                isModalVisible={this.state.isModalVisible}
                toggleModal={this.toggleModal}
              />
            </React.Fragment>
            {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
            {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
          </ScrollView>
        ) : (
          <SignInCollaborate {...this.props} />
        )}
      </View>
    );
  }
}
// Redux Map Functions
function mapStateToProps(state) {
  const { user } = state;
  return {
    isLogged: user.isLogged,
    user: getUserProfile(state),
    userProfile: state.user.profile.uploadphoto,
  };
}
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateUserInfo: userActions.updateUserInfo,
      updatePassword: userActions.updatePassword,
      uploadUserPhoto: userActions.uploadUserPhoto,
      logout: userActions.logout,
    },
    dispatch,
  );
export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
