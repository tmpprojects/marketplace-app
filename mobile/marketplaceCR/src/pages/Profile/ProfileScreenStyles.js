import { StyleSheet } from 'react-native';
import { typography } from '../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { ceil } from 'react-native-reanimated';

export const styles = StyleSheet.create({
  profileWrapper: {
    flex: 1,
    backgroundColor: '#FAF7F5',
  },
  versionNumber: {
    marginTop: 20,
    opacity: 0.5,
  },
  BtnsContainer: {
    position: 'absolute',
    top: 10,
    right: 15,
  },
  uploadPhotoProfileTitle: {
    color: colors.colorDark100,
  },
  uploadPhotoProfile: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 999,
    elevation: 999,
    display: 'flex',
    justifyContent: 'center',
  },
  uploadPhotoProfileContainer: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
  },
  uploadPhotoProfileSpinner: {
    height: 150,
  },
  profileContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    marginTop: 20,
    marginBottom: 10,
  },
  containerPhoto: {
    alignItems: 'center',
  },
  photoProfile: {
    width: 96,
    height: 96,
    borderRadius: 96 / 2,
    backgroundColor: colors.colorWhite,
    shadowColor: colors.colorGray400,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  photo: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 96 / 2,
  },
  containerBtnAdd: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  btnAdd: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: colors.colorWhite,
    shadowColor: colors.colorGray400,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },

  containerInfo: {
    marginLeft: 22,
    flex: 1,
  },
  name: {
    ...typography.txtRegular,
    color: colors.colorMain300,
    marginBottom: 8,
  },
  mail: {
    ...typography.txtRegular,
    fontSize: 13,
    color: colors.colorDark100,
    marginBottom: 12,
  },

  menuProfile: {
    alignItems: 'center',
  },
  containerItemMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
    paddingHorizontal: 20,
    width: '90%',
    height: 55,
    backgroundColor: colors.colorWhite,
    borderColor: colors.colorGray100,
    borderWidth: 1,
    borderRadius: 10,
  },
  iconItem: {
    flex: 1,
    marginRight: 20,
  },
  itemMenu: {
    ...typography.txtRegular,
    color: colors.colorDark400,
    flex: 8,
    alignSelf: 'center',
    marginRight: 20,
  },
  footer: {
    width: '90%',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 25,
  },
  contact: {
    flexDirection: 'row',
    marginBottom: 8,
  },
  btnHelp: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },
  iconHelp: {
    width: 12,
    height: 12,
    opacity: 0.5,
    marginRight: 5,
  },
  mailContact: {
    color: colors.colorDark300,
    ...typography.txtRegular,
    fontSize: 13,
    marginTop: 5,
  },
  logoutContainer: {
    alignItems: 'center',
  },
  btnLogout: {
    // ...buttons.square,
    // borderColor: colors.colorMain300,
    alignItems: 'center',
    marginTop: 20,
  },
  txtLogout: {
    ...typography.txtRegular,
    color: colors.colorGray400,
  },
  txtTerms: {
    ...typography.txtRegular,
    color: colors.colorDark100,
    fontSize: 13,
    marginTop: 15,
  },
});
