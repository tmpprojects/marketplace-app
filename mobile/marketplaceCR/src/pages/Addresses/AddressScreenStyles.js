import { StyleSheet } from 'react-native';
import { typography, buttons } from '../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const styles = StyleSheet.create({
  addressesWrapper: {
    flex: 1,
    backgroundColor: '#FAF7F5',
  },
  title: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  addressesList: {
    padding: 15,
  },
  message: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnAdd: {
    ...buttons.square,
    width: '90%',
    borderColor: colors.colorMain300,
    backgroundColor: colors.colorWhite,
    alignItems: 'center',
    alignSelf: 'center',
  },
  btnName: {
    ...typography.txtRegular,
    color: colors.colorMain300,
    fontSize: 18,
  },
});
