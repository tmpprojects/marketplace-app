import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import {
  Platform,
  View,
  TouchableOpacity,
  Text,
  ScrollView,
} from 'react-native';
import { styles } from './AddressScreenStyles';
import * as Sentry from '@sentry/react-native';

import withShoppingCartForm from '../../utils/hocs/withShoppingCartForm';
import type { AddressType } from '../../redux/types/AddressesTypes';
import { getAddressesList } from '../../redux/selectors/users.selectors';
import AddressModal from '../../components/Profile/Address/AddressModal/AddressModal';
import { userActions } from '../../redux/actions';
import AddressCard from '../../components/Profile/Address/AddressCard/AddressCard';
import { FadeInView } from '../../utils/animations/FadeInView';
import Loader from '../../components/Loader';

import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography } from '@canastarosa/ds-theme/typography';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { CRTitle } from '../../components/CRTitle';

//Google Analytics
import analytics from '@react-native-firebase/analytics';
declare var globalThis: any;
type AddressesWrapperPorps = {
  addAddress: Function,
  deleteAddress: Function,
  updateAddress: Function,
  listAddresses: Function,
  onUpdateAddress: Function,
  addresses: Array<AddressType>,
  selectedShippingAddress: AddressType,
};
type AddressesWrapperState = {
  shouldRenderAddressesListModal: boolean,
  shouldRenderAddressFormModal: boolean,
};
export class AddressesScreen extends Component<
  AddressesWrapperPorps,
  AddressesWrapperState,
> {
  state = {
    shouldRenderAddressesListModal: false,
    shouldRenderAddressFormModal: false,
  };

  constructor(props) {
    super(props);
  }
  componentDidMount = () => {
    this.toggleAddressFormModal(false);
    analytics().setCurrentScreen('AddressesScreen');
    analytics().logEvent('AddressesScreen');
  };

  toggleAddressFormModal = (visible: boolean = false): void => {
    visible === undefined
      ? this.setState({ shouldRenderAddressFormModal: false })
      : this.setState({ shouldRenderAddressFormModal: visible });
  };
  toggleAddressListModal = (visible: boolean = false): void => {
    visible === undefined
      ? this.setState({ shouldRenderAddressesListModal: false })
      : this.setState({ shouldRenderAddressesListModal: visible });
  };
  onAddAddress = (addressId: string): void => {
    // Close address form and list modals.
    this.toggleAddressFormModal(false);
    this.toggleAddressListModal(false);

    // Add address
    this.props.addAddress(addressId).catch((error) => {
      Sentry.captureException(error);
    });
  };

  onDeleteAddress = (addressId: string): void => {
    // Close address form and list modals.
    this.toggleAddressFormModal(false);

    // Delete address
    this.props.deleteAddress(addressId).catch((error) => null);
  };

  onUpdateAddress = (targetAddress: AddressType): void => {
    //FIX: Telephone
    // console.log('onUpdateAddress', targetAddress)

    //verify that address exists
    const validatedAddress = this.props.addressList.find(
      (a) => a.uuid === targetAddress.uuid,
    );

    // Close address form and list modals.
    this.toggleAddressListModal(false);

    // Update address
    this.props
      .updateAddress({ ...validatedAddress, ...targetAddress })
      .catch((e) => {
        Sentry.captureException(e);
      });
  };

  onAddressChange = (targetAddress: AddressType): void => {
    //FIX: Telephone
    //console.log('onAddressChange', targetAddress)

    // Update Default Pickup Address
    this.onUpdateAddress({
      ...targetAddress,
      default_address: true,
    });
  };
  onInvalidAddress = (targetAddress: AddressType) => {};

  render() {
    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 220;
    const SPACER_SIZE_ANDROID = 220;
    const { addresses = [], selectedShippingAddress } = this.props;
    return (
      <View style={styles.addressesWrapper}>
        <CRTitle title="Mis direcciones" />

        <TouchableOpacity
          style={styles.btnAdd}
          onPress={() => this.toggleAddressFormModal(true)}
          activeOpacity={0.5}
        >
          <Text style={styles.btnName}>Añadir nueva dirección de entrega</Text>
        </TouchableOpacity>

        <AddressModal
          title="Nueva dirección de entrega"
          btnAction="Guardar"
          onSubmit={this.onAddAddress}
          modalVisible={this.state.shouldRenderAddressFormModal}
          toggleModal={this.toggleAddressFormModal}
          initialValues={{}}
        />
        <ScrollView style={styles.addressesList}>
          {addresses.length === 0 ? (
            <View style={styles.message}>
              <CRText
                variant={typography.paragraph}
                color={colors.colorGray400}
                align="center"
              >
                Aún no tienes direcciones
              </CRText>
            </View>
          ) : !this.props.addressesLoading ? (
            <React.Fragment>
              <FadeInView>
                {addresses.map((address, index) => (
                  <View key={index + address.uuid}>
                    <Field
                      name="shippingAddress"
                      id={`address_${address.uuid}`}
                      toggleModal={this.toggleAddressFormModal}
                      address={address}
                      component={AddressCard}
                      value={address.uuid}
                      changeAddress={this.onAddressChange}
                      updateAddress={this.onUpdateAddress}
                      deleteAddress={this.onDeleteAddress}
                      modalVisible={this.toggleAddressListModal}
                      invalidAddress={this.onInvalidAddress}
                      active={
                        selectedShippingAddress &&
                        address.uuid === selectedShippingAddress.uuid
                      }
                    />
                  </View>
                ))}
              </FadeInView>
              {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
              {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
            </React.Fragment>
          ) : (
            <Loader />
          )}
        </ScrollView>
      </View>
    );
  }
}
AddressesScreen = withShoppingCartForm(AddressesScreen, {});

const selector = formValueSelector('shoppingOrder_form');
function mapStateToProps(state) {
  return {
    addresses: getAddressesList(state),
    selectedShippingAddress: selector(state, 'shippingAddress'),
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      listAddresses: userActions.listAddresses,
      addAddress: userActions.addAddress,
      deleteAddress: userActions.deleteAddress,
      updateAddress: userActions.updateAddress,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(AddressesScreen);
