import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { storeActions } from '../../redux/actions';

import { View, ScrollView, Platform } from 'react-native';
import { CRStoreCard } from '../../components/CRCard';
import { styles } from './NewStoresStyles';

import Loader from '../../components/Loader';
import { CRRowBoth } from '../../components/Layout';
import { CRTitle } from '../../components/CRTitle';
import ZipCodeObserver from '../../components/ZipCodeObserver/ZipCodeObserver';

//Google Analytics
import analytics from '@react-native-firebase/analytics';

declare var globalThis: any;

class NewStores extends Component {
  constructor(props) {
    super(props);
  }

  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.zipCode !== this.props.zipCode) {

  //     //Mock refresh this screen.
  //     // this.props.navigation.navigate('ChangeZipCode', {
  //     //   reloadTo: 'NewStores',
  //     // });

  //   }
  // }

  componentDidMount() {
    //Google Analytics
    analytics().setCurrentScreen('NewStoresScreen');
    analytics().logEvent('NewStoresScreen');

    this.props.getNewsStores();
  }
  showMore = () => {
    currentStorePage++;
  };

  render() {
    const { storesList, navigation, loaderStores } = this.props;
    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 10;
    const SPACER_SIZE_ANDROID = 20;
    return (
      <ScrollView style={styles.newStoresWrapper}>
        <ZipCodeObserver
          observe="NewStores"
          isFocused={() => {
            return this.props.navigation.isFocused();
          }}
          navigation={this.props.navigation}
        />
        <CRRowBoth addedStyle={styles.title}>
          <CRTitle title="Nuevas tiendas" />
        </CRRowBoth>
        {!loaderStores.loaded ? (
          <React.Fragment>
            <Loader />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <CRRowBoth>
              {storesList.map((item, index) => (
                <CRStoreCard
                  key={index}
                  size="full"
                  store={item.name}
                  thumbImg={item.photo.small}
                  storeLogo={item.photo.small}
                  rating={
                    item.mean_store_score > 0 &&
                    item.mean_store_score.toFixed(1)
                  }
                  slogan={item.slogan}
                  marginBottom={10}
                  onPress={() => {
                    // navigation.navigate('StoreScreen', {
                    //   itemStore: item.slug,
                    // });
                    globalThis.ReactApplication.ActiveScreen(
                      'StoreScreen',
                      {
                        itemStore: item.slug,
                      },
                      navigation,
                    );
                  }}
                />
              ))}

              {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
              {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
            </CRRowBoth>
          </React.Fragment>
        )}
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  const { app, store } = state;
  return {
    storesList: store.new_stores.stores.results,
    loaderStores: store.new_stores.stores,
    zipCode: app.zipCode,
  };
}

function mapDispatchToProps(dispatch) {
  const { getNewsStores } = storeActions;
  return bindActionCreators({ getNewsStores }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NewStores);
