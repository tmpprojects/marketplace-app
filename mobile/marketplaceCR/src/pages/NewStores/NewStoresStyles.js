import { StyleSheet } from 'react-native';
import { typography } from '../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const styles = StyleSheet.create({
  newStoresWrapper: {
    backgroundColor: '#FAF7F5',
  },
  containerStore: {
    flex: 1,
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: 20,
  },
  arrowBtn: {
    marginHorizontal: 15,
    flex: 2,
  },
  title: {
    ...typography.txtExtraBold,
    color: colors.colorMain300,
    textAlign: 'center',
    marginVertical: 20,
  },
  subtitle2: {
    // A subtitle but....smaller..
    color: colors.colorDark300,
    textAlign: 'left',
    fontSize: 22,
    fontFamily: 'Sarabun-SemiBold',
  },
});
