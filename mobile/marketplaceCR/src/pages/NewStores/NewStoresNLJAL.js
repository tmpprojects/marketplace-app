import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { storeActions } from '../../redux/actions';

import { View, ScrollView, Platform } from 'react-native';
import { CRStoreCard } from '../../components/CRCard';
import { styles } from './NewStoresStyles';

import Loader from '../../components/Loader';
import { CRRowBoth } from '../../components/Layout';
import { CRTitle } from '../../components/CRTitle';
import ZipCodeObserver from '../../components/ZipCodeObserver/ZipCodeObserver';

declare var globalThis: any;

class NewStoresNLJAL extends Component {
  constructor(props) {
    super(props);
    const { route } = this.props;
    const { stores, title } = route.params;
    this.preloadContent = [];
    this.title = title;
    if (stores === 'NL') {
      this.preloadContent =
        globalThis.ReactApplication.HomeScreen.store.landingNL.data;
    } else {
      this.preloadContent =
        globalThis.ReactApplication.HomeScreen.store.landingJAL.data;
    }
  }

  componentDidMount() {
    const { route } = this.props;
    const { stores, title } = route.params;
  }

  render() {
    const { storesList, navigation, route, loaderStores } = this.props;

    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 10;
    const SPACER_SIZE_ANDROID = 20;
    return (
      <ScrollView style={styles.newStoresWrapper}>
        <CRRowBoth addedStyle={styles.title}>
          <CRTitle title={this.title} />
        </CRRowBoth>

        <React.Fragment>
          <CRRowBoth>
            {this.preloadContent.map((item, index) => (
              <CRStoreCard
                key={index}
                size="full"
                store={item.name}
                thumbImg={item.photo}
                storeLogo={item.photo}
                rating={
                  item.mean_store_score > 0 && item.mean_store_score.toFixed(1)
                }
                slogan={item.slogan}
                marginBottom={10}
                onPress={() => {
                  // navigation.navigate('StoreScreen', {
                  //   itemStore: item.slug,
                  // });

                  globalThis.ReactApplication.ActiveScreen(
                    'StoreScreen',
                    {
                      itemStore: item.slug,
                    },
                    navigation,
                  );
                }}
              />
            ))}

            {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
            {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
          </CRRowBoth>
        </React.Fragment>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  const { app, store } = state;
  return {
    storesList: store.new_stores.stores.results,
    loaderStores: store.new_stores.stores,
    zipCode: app.zipCode,
  };
}

function mapDispatchToProps(dispatch) {
  const { getNewsStores } = storeActions;
  return bindActionCreators({ getNewsStores }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NewStoresNLJAL);
