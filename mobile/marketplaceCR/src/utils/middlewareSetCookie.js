export const middlewareSetCookie = (
  api: Object,
  getStatestate: Function,
): Object => {
  const reduxStore = getStatestate();

  if (
    reduxStore.user.hasOwnProperty('cookies') &&
    reduxStore.user.cookies !== undefined
  ) {
    const formatCookies = {
      ...reduxStore.user.cookies,
      'Content-Type': 'application/json',
    };
    api.defaults.headers = formatCookies;
  }

  return api;
};

export const middlewareClearCookie = (api: Object): Object => {
  api.defaults.headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  };
  return api;
};
