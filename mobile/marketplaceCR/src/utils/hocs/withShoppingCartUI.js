import { config } from '../../../env';
import React, { Component } from 'react';
import type { Node } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Alert,
  View,
  TouchableOpacity,
  Modal,
  ScrollView,
  Text,
} from 'react-native';
import { Field, formValueSelector } from 'redux-form';
import axios from 'axios';
import { WebView } from 'react-native-webview';
import * as Sentry from '@sentry/react-native';

import withShoppingCartForm from './withShoppingCartForm';
import { userActions } from '../../redux/actions';
import AddressCard from '../../components/Profile/Address/AddressCard/AddressCard';
import AddressModal from '../../components/Profile/Address/AddressModal/AddressModal';

import { CRTitle } from '../../components/CRTitle';
import { CRRowBoth, CRRowFull } from '../../components/Layout';
import { layout } from '../../pages/Checkout/layout';

// DS
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';

import { styles } from '../../routes/TopMenu/AddressesWrapperStyles';
import { CRIcon } from '../../components/CRIcon';

// Declar global scope reference
declare var globalThis: any;

type AddressType = {
  uuid: string | number,
  zip_code: string | number,
  default_address: boolean,
};

//
export default function withShoppingCartUI(
  WrappedComponent: any,
  selectData: {},
) {
  // ...and returns another component...
  type ShoppingCartUIWrappedProps = {|
    addAddress: Function,
    deleteAddress: Function,
    updateAddress: Function,
    shippingAddress: AddressType,
    addressList: Array<AddressType>,
  |};
  type ShoppingCartUIWrappedState = {
    modalView: ?Node,
    modalFull: boolean,
    modalVisible: boolean,
    showPayPalModal: boolean,
    loadingPayment: boolean,
    shouldRenderAddressFormModal: boolean,
    shouldRenderAddressesListModal: boolean,
  };
  class ShoppingCartUIWrapped extends Component<
    ShoppingCartUIWrappedProps,
    ShoppingCartUIWrappedState,
  > {
    state = {
      modalView: null,
      modalFull: false,
      modalVisible: false,
      showPayPalModal: false,
      loadingPayment: false,
      shouldRenderAddressFormModal: false,
      shouldRenderAddressesListModal: false,
    };
    _paypal_payment_details = {};

    componentDidMount() {
      // ... that takes care of the subscription...
      //DataSource.addChangeListener(this.handleChange);
    }

    componentWillUnmount() {
      //DataSource.removeChangeListener(this.handleChange);
    }

    toggleAddressFormModal = (visible: boolean = false): void => {
      this.setState({ shouldRenderAddressFormModal: visible });
    };

    toggleAddressListModal = (visible: boolean = false): void => {
      visible === undefined
        ? this.setState({ shouldRenderAddressesListModal: false })
        : this.setState({ shouldRenderAddressesListModal: visible });
    };

    onAddAddress = (addressId: string): void => {
      // Close address form and list modals.
      this.toggleAddressFormModal(false);
      this.toggleAddressListModal(false);

      // Add address
      this.props
        .addAddress(addressId)
        .then((response) => {
          this.renderAddressList([]);
        })
        .catch((e) => {
          Sentry.captureException(e);
        });
    };

    onDeleteAddress = (addressId: string): void => {
      // Close address form and list modals.
      this.toggleAddressFormModal(false);

      // Delete address
      this.props.deleteAddress(addressId).catch((e) => {
        Sentry.captureException(e);
      });
    };

    onUpdateAddress = (targetAddress: AddressType): void => {
      //verify that address exists
      const validatedAddress = this.props.addressList.find(
        (a) => a.uuid === targetAddress.uuid,
      );
      // TODO: Return an UI error to notify user about something went wrong.
      if (!validatedAddress) {
        // Handle state here...
        return false;
      }

      // Close address form and list modals.
      this.toggleAddressListModal(false);

      // Update address
      this.props
        .updateAddress({ validatedAddress, ...targetAddress })
        .catch((e) => {
          Sentry.captureException(e);
        });
    };

    changeShippingAddress = (targetAddress: AddressType): void => {
      // Update Default Pickup Address
      this.onUpdateAddress({
        uuid: targetAddress.uuid,
        default_address: true,
      });

      //
      this.closeModalWindow();
    };

    toogleModalWindow = (visible: boolean): void => {
      this.modalType = undefined;
      this.setState({
        modalVisible: visible,
      });
    };
    closeModalWindow = (): void => {
      this.modalType = undefined;
      this.toogleModalWindow(false);
    };

    changeShippingMethod = (order: Object, shippingSchedule: Object): void => {
      this.closeModalWindow();
    };
    renderShippingMethods = (shippingMethods: Array<Object>): void => {
      this.modalType = 'shippingMethodView';

      const shippingMethodsForm = (
        <CRRowBoth>
          <CRRowFull addedStyle={layout.titleModal}>
            <CRText
              variant={typography.subtitle2}
              color={colors.colorDark200}
              align="center"
            >
              Selecciona el tipo de envío
            </CRText>
          </CRRowFull>
          <View style={layout.cardsList}>{shippingMethods}</View>
        </CRRowBoth>
      );

      this.setState({
        modalFull: true,
        modalVisible: true,
        modalView: shippingMethodsForm,
      });
    };

    changeShippingSchedule = (
      order: number,
      shippingSchedule: Object,
    ): void => {
      this.closeModalWindow();
    };
    changeShippingDate = (order: number, shippingDate: string): void => {
      this.closeModalWindow();
    };
    renderShippingDate = (shippingSchedules: Array<Object>): void => {
      const shippingSchedulesForm = (
        <CRRowBoth>
          <CRRowFull addedStyle={layout.titleModal}>
            <CRText
              variant={typography.subtitle2}
              color={colors.colorDark200}
              align="center"
            >
              Selecciona el horario de entrega
            </CRText>
          </CRRowFull>
          <View style={layout.cardsList}>{shippingSchedules}</View>
        </CRRowBoth>
      );

      this.setState({
        modalFull: true,
        modalVisible: true,
        modalView: shippingSchedulesForm,
      });
    };

    renderAddressList = (addresses: Array<{}>) => {
      const { addressList, shippingAddress } = this.props;
      let addressesPromiseResolve, addressesPromiseReject;
      const addressesPromise = new Promise((resolve, reject) => {
        addressesPromiseResolve = resolve;
        addressesPromiseReject = reject;
      });

      const addressListView = (
        <View style={styles.addresesListWrapper}>
          {/* USER HAS NO STORED ADDRESSES */}
          {addressList.length === 0 ? (
            <View style={styles.messageAddress}>
              <CRIcon
                name="home"
                size={20}
                color={colors.colorGray300}
                style={styles.iconItem}
              />
              <CRText
                variant={typography.paragraph}
                color={colors.colorGray400}
                align="center"
              >
                Aún no tienes direcciones
              </CRText>
            </View>
          ) : (
            <React.Fragment>
              {/* USER HAS ADDRESSES */}
              <CRText
                variant={typography.subtitle3}
                color={colors.colorMain300}
              >
                Selecciona una dirección
              </CRText>

              {/* LIST */}
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                style={styles.addressesList}
              >
                {addressList.map((address, index) => (
                  <View key={index + address.uuid} style={styles.addressCard}>
                    <Field
                      name="shippingAddress"
                      address={address}
                      component={AddressCard}
                      id={`address_${address.uuid}`}
                      toggleModal={this.toggleAddressFormModal}
                      changeAddress={(address) => {
                        addressesPromiseResolve(address);
                        this.changeShippingAddress(address);
                      }}
                      updateAddress={this.onUpdateAddress}
                      deleteAddress={this.onDeleteAddress}
                      modalVisible={false}
                      active={
                        shippingAddress && address.uuid === shippingAddress.uuid
                      }
                      closeModalWindow={this.closeModalWindow}
                    />
                  </View>
                ))}
              </ScrollView>
              {/* END: LIST */}
            </React.Fragment>
          )}

          <TouchableOpacity
            style={styles.btnAdd}
            onPress={() => this.toggleAddressFormModal(true)}
            activeOpacity={0.5}
          >
            <CRText
              variant={typography.paragraph}
              color={colors.colorMain300}
              align="center"
            >
              Añadir nueva dirección de entrega
            </CRText>
          </TouchableOpacity>
        </View>
      );

      this.setState({
        modalFull: false,
        modalVisible: true,
        modalView: addressListView,
      });

      return addressesPromise;
    };

    renderPayPal = (_paypal_payment_details: {}) => {
      this.paypalPromise = new Promise((resolve, reject) => {
        this.paypalPromiseResolver = resolve;
        this.paypalPromiseRejecter = reject;
      });

      axios({
        method: 'post',
        url: 'https://api.sandbox.paypal.com/v1/oauth2/token',
        data: 'grant_type=client_credentials',
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          //'Access-Control-Allow-Credentials': true,
          withCredentials: true,
          //grant_type: 'client_credentials',
        },
        auth: {
          username: config.PAYPAL_USER,
          password: config.PAYPAL_PASSWORD,
        },
      }).then((paypalToken) => {
        const _paypal_payment_details_2 = {
          ..._paypal_payment_details,
          ...paypalToken.data,
        };

        //
        const paypalView = (
          <WebView
            source={{
              uri: _paypal_payment_details_2.links[1].href,
              headers: {
                Authorization: `Bearer ${_paypal_payment_details_2.access_token}`,
              },
            }}
            onNavigationStateChange={(data: Object) => {
              // Podríamos agregar payment ID y token para confirmar que la URL pertenece a nuestra transacción
              if (data.url.includes('paypal/execute-payment')) {
                this.setState({ showPayPalModal: false });
                this.paypalPromiseResolver(_paypal_payment_details_2);

                //
              } else if (data.url.includes('paypal/cancel-payment')) {
                this.setState({ showPayPalModal: false });
                this.paypalPromiseRejecter('Pago cancelado');
              } else {
                return;
              }
            }}
            //injectedJavaScript={`document.f1.submit()`}
            containerStyle={{ marginTop: 50 }}
            useWebKit
            allowUniversalAccessFromFileURLs
            mixedContentMode="always"
            originWhitelist={['*']}
            thirdPartyCookiesEnabled
            javaScriptEnabled
            domStorageEnabled
          />
        );

        //
        this.setState({
          paypalView,
          showPayPalModal: true,
          modalVisible: false,
          modalView: null,
          modalFull: true,
        });
      });

      return this.paypalPromise;
    };

    render() {
      // ... and renders the wrapped component with the fresh data!
      // Notice that we pass through any additional props
      return (
        <React.Fragment>
          <WrappedComponent
            closeModalWindow={this.closeModalWindow}
            changeShippingDate={this.changeShippingDate}
            changeShippingMethod={this.changeShippingMethod}
            changeShippingSchedule={this.changeShippingSchedule}
            renderPayPal={this.renderPayPal}
            renderAddressList={this.renderAddressList}
            renderShippingDate={this.renderShippingDate}
            renderShippingMethods={this.renderShippingMethods}
            {...this.props}
          />

          {/* PAYPAL MODAL */}
          <Modal
            visible={this.state.showPayPalModal}
            onRequestClose={() => this.setState({ showPayPalModal: false })}
          >
            {this.state.paypalView}
          </Modal>
          {/* END: PAYPAL MODAL */}

          {/* MODAL BOX */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
          >
            <View style={layout.modalView}>
              <TouchableOpacity
                style={layout.modalViewTO}
                onPress={() => {
                  this.closeModalWindow(false);
                }}
              />
              <View
                style={
                  this.state.modalFull
                    ? layout.containerModalFull
                    : layout.containerModal
                }
              >
                {/* CLOSE BUTTON */}
                <TouchableOpacity
                  onPress={() => {
                    //if (this.modalType !== 'shippingMethodView') {
                    this.closeModalWindow();
                    //}
                  }}
                  style={layout.btnCloseModal}
                  activeOpacity={0.5}
                >
                  <CRIcon name="close" style={layout.iconStyleModal} />
                </TouchableOpacity>
                {/* END: CLOSE BUTTON */}

                {/* DYNAMIC CONTENT */}
                {this.state.modalView}

                <AddressModal
                  title="Nueva dirección de entrega"
                  btnAction="Guardar"
                  onSubmit={this.onAddAddress}
                  modalVisible={this.state.shouldRenderAddressFormModal}
                  toggleModal={this.toggleAddressFormModal}
                  initialValues={{}}
                  closeModalWindow={this.closeModalWindow}
                />
                {/* END: DYNAMIC CONTENT */}
              </View>
            </View>
          </Modal>
          {/* END: MODAL BOX */}
        </React.Fragment>
      );
    }
  }

  // Pass Redux state and actions to component
  function mapStateToProps(state, props) {
    return {};
  }
  function mapDispatchToProps(dispatch) {
    return {
      dispatch,
      ...bindActionCreators(
        {
          listAddresses: userActions.listAddresses,
          addAddress: userActions.addAddress,
          deleteAddress: userActions.deleteAddress,
          updateAddress: userActions.updateAddress,
        },
        dispatch,
      ),
    };
  }

  // Export Component
  ShoppingCartUIWrapped = withShoppingCartForm(ShoppingCartUIWrapped);
  return connect(mapStateToProps, mapDispatchToProps)(ShoppingCartUIWrapped);
}
