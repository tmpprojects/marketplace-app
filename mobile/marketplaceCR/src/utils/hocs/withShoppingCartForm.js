import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, getFormSyncErrors } from 'redux-form';

import { SHOPPING_CART_FORM_CONFIG } from '../shoppingCartUtils/shoppingCartFormConfig';

// Declar global scope reference
declare var globalThis: any;

export default function withShoppingCartForm(
  WrappedComponent: any,
  selectData: ?{} = {},
) {
  // ...and returns another component...
  class ShoppingCartWrapped extends Component<{}> {
    constructor(props) {
      super(props);
      //   this.handleChange = this.handleChange.bind(this);
      //   this.state = {
      //     data: selectData(DataSource, props),
      //   };
    }

    componentDidMount() {
      // ... that takes care of the subscription...
      //DataSource.addChangeListener(this.handleChange);
    }

    componentWillUnmount() {
      //DataSource.removeChangeListener(this.handleChange);
    }

    handleChange() {
      //   this.setState({
      //     data: selectData(DataSource, this.props),
      //   });
    }

    render() {
      // ... and renders the wrapped component with the fresh data!
      // Notice that we pass through any additional props
      return <WrappedComponent {...this.props} />;
    }
  }

  // Wrap component within reduxForm
  ShoppingCartWrapped = reduxForm({
    ...SHOPPING_CART_FORM_CONFIG.config,
  })(ShoppingCartWrapped);

  type ShoppingCartFormType = {};

  // Pass Redux state and actions to component
  function mapStateToProps(state, props) {
    // Define or retrieve default form values
    const orders: Array<Object> = globalThis.ShoppingCartManager.getOrders();
    const unavailableOrders: Array<{}> = globalThis.ShoppingCartManager.getUnavailableOrders();
    const coupon: Object = globalThis.ShoppingCartManager.getCoupon();
    const shippingAddress: Object = globalThis.ShoppingCartManager.getShippingAddress();
    const paymentMethod: Object = globalThis.ShoppingCartManager.getPaymentMethod();
    const requiresInvoice: boolean = globalThis.ShoppingCartManager.getRequiresInvoice();
    const selectedShippingAddress: ?string = shippingAddress
      ? shippingAddress.uuid
      : null;
    const clientData: Object = {
      email: state.user.profile ? state.user.profile.email : '', //globalThis.ShoppingCartManager.getClientData().email,
      phone: state.user.profile ? state.user.profile.mobile : '',
      client_name: state.user.profile ? state.user.profile.first_name : '',
      client_lastName: state.user.profile ? state.user.profile.last_name : '',
    };
    const initialValues: ShoppingCartFormType = {
      orders,
      clientData,
      unavailableOrders,
      coupon: coupon,
      paymentMethod: paymentMethod,
      requiresInvoice,
      selectedShippingAddress,
      ...(shippingAddress ? { shippingAddress: shippingAddress } : {}),
    };

    return {
      coupon,
      orders,
      paymentMethod,
      shippingAddress,
      requiresInvoice,
      unavailableOrders,
      user: state.user,

      orderHasShippingErrors: globalThis.ShoppingCartManager.hasShippingAddressErrors(),
      orderHasPaymentErrors: globalThis.ShoppingCartManager.hasPaymentMethodErrors(),

      addressList: globalThis.ShoppingCartManager.getAddresses(),
      paymentMethodsList: globalThis.ShoppingCartManager.getPaymentMethods(),
      totalPrice: globalThis.ShoppingCartManager.getCartGrandTotal(),
      totalPriceWithDiscount: globalThis.ShoppingCartManager.getCartGrandTotalWithDiscount(),
      cartPrice: globalThis.ShoppingCartManager.getCartProductsTotal(),
      cartPriceWithDiscount: globalThis.ShoppingCartManager.getCartProductsTotalWithDiscount(),
      shippingPrice: globalThis.ShoppingCartManager.getShippingPrice(),
      productsCount: globalThis.ShoppingCartManager.getCartProductsCount(),
      unavailableProducts: globalThis.ShoppingCartManager.getUnavailableProducts(),
      formErrors: getFormSyncErrors(SHOPPING_CART_FORM_CONFIG.formName)(state),
      initialValues,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
      dispatch,
      ...bindActionCreators({}, dispatch),
    };
  }

  // Export Component
  return connect(mapStateToProps, mapDispatchToProps)(ShoppingCartWrapped);
}
