import DayJs from 'dayjs';

const MONTH_NAMES = [
  'Enero',
  'febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre',
];
export const WEEK_OBJECT = [
  { name: 'Lunes', value: 0, description: '' },
  { name: 'Martes', value: 1, description: '' },
  { name: 'Miércoles', value: 2, description: '' },
  { name: 'Jueves', value: 3, description: '' },
  { name: 'Viernes', value: 4, description: '' },
  { name: 'Sábado', value: 5, description: '' },
  { name: 'Domingo', value: 6, description: '' },
];

/*------------------------------------------------
//    FORMAT DATE UTILITY
------------------------------------------------*/
export const formatDate = function (date) {
  const day = date.getDate();
  const monthIndex = date.getMonth();
  const year = date.getFullYear();
  return `${day} ${MONTH_NAMES[monthIndex]} ${year}`;
};
export const formatDateYYMMDD = function (date) {
  const day = date.getDate();
  let monthIndex = date.getMonth() + 1;
  monthIndex = monthIndex < 10 ? `0${monthIndex}` : `${monthIndex}`;
  const year = date.getFullYear();
  return `${year}-${monthIndex}-${day}`;
};
export const formatDateToUTC = (date) =>
  new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate());

/*------------------------------------------------
//   CONVERT TIME FROM 24hr TO 12hr FORMAT
------------------------------------------------*/
export const formatHour24To12 = function (time) {
  const timeArray = time
    .split(':')
    .map((a) => (isNaN(parseInt(a, 10)) ? 0 : parseInt(a, 10)));

  return timeArray.reduce((a, b, i) => {
    let formattedTime = b;
    if (i === 0) {
      formattedTime = b > 12 ? b - 12 : b;
    }
    formattedTime = formattedTime > 9 ? formattedTime : `0${formattedTime}`;
    return `${a}${formattedTime}${i < timeArray.length - 1 ? ':' : ''}`;
  }, '');
};
export const addHourPeriodSuffix = function (time) {
  let periodSuffix = 'am';
  const timeComponents = time.split(':');
  let hour = parseInt(timeComponents.shift(), 10);

  // Verify hour period
  if (hour > 12) {
    periodSuffix = 'pm';
    hour -= 12;
  }

  // Format and return new hour
  hour = hour > 9 ? hour : `0${hour}`;
  return `${[hour, ...timeComponents].join(':')}${periodSuffix}`;
};

/**
 * isValidWorkingDay()
 * Takes a date and verifies weather this is a valid working/shipping day.
 * working/shipping date.
 * @param {string} date | 'YYYY-MM-DD'
 * @param {obj} product | Product Object.
 * @returns bool
 */
export const isValidWorkingDay = (
  date: string | Date | Object,
  product: null | Object,
): boolean => {
  // Date is mandatory.
  if (!date) {
    return false;
  }

  // Properties and Variables.
  const targetDate: Object = DayJs(date);
  const weekDay: number = targetDate.day();

  /**
   * ------------------------------------------------------------
   * 1. First Apply validations form Company Level rules.
   * TODO: This SHOULD be managed on backend as much as possible!
   * ---- --------------------------------------------------------
   */
  // Holidays abnd Festivities
  if (targetDate.isSame(`${targetDate.year()}-4-1`, 'day')) return false; // Labours Day
  if (targetDate.isSame(`${targetDate.year()}-11-24`, 'day')) return false; // Noche Buena
  if (targetDate.isSame(`${targetDate.year()}-11-25`, 'day')) return false; // Xmas
  if (targetDate.isSame(`${targetDate.year()}-11-31`, 'day')) return false; // New Years Eve
  if (targetDate.isSame(`${targetDate.year()}-0-1`, 'day')) return false; // NewYear

  // Disable All Sundays
  if (weekDay === 0) return false;

  /**
   * ------------------------------------------------------------
   * 2. We apply product specific shipping date rules.
   * If we´ve received a product param,
   * we must search for product´s availability shipping details.
   * ------------------------------------------------------------
   */
  if (product) {
    const {
      work_schedules,
      physical_properties: physicalProperties,
      vacations_ranges,
    } = product;
    const {
      minimum_shipping_date: minimumShippingDate,
      maximum_shipping_date: maximumShippingDate,
      deactivated_delivery_days: deactivatedDays,
    } = physicalProperties;

    // Mark date as invalid if it´s before the minimum shipping date
    if (DayJs(date).isBefore(minimumShippingDate, 'YYYY-MM-DD', true)) {
      return false;
    }

    // Mark date as invalid if its after the maximum shipping date
    if (DayJs(date).isAfter(maximumShippingDate, 'YYYY-MM-DD', true)) {
      return false;
    }

    if (deactivatedDays) {
      const {
        'express-car': expressCar,
        'express-moto': expressMoto,
        standard,
      } = deactivatedDays;

      let isDeactivated = false;

      if (standard) {
        standard.forEach((day) => {
          isDeactivated = targetDate.isSame(day, 'day');
        });
      }
      if (expressCar) {
        expressCar.forEach((day) => {
          isDeactivated = targetDate.isSame(day, 'day');
        });
      }
      if (expressMoto) {
        expressMoto.forEach((day) => {
          isDeactivated = targetDate.isSame(day, 'day');
        });
      }

      if (isDeactivated) {
        return false;
      }
    }

    // Check if store has vacations planned.
    if (vacations_ranges && vacations_ranges.length) {
      let isOnVacation = false; // Vacation flag
      vacations_ranges.forEach((vacation) => {
        if (!isOnVacation) {
          isOnVacation = targetDate.isSame(vacation.start, 'day');
          isOnVacation = isOnVacation || targetDate.isSame(vacation.end, 'day');
          isOnVacation =
            isOnVacation || targetDate.isBetween(vacation.start, vacation.end);
        }
      });

      // Mark this date as invalid if the store is on vacations.
      if (isOnVacation) {
        return false;
      }
    }

    // Verify if the store opens this this week day.
    const weekdayIndex = weekDay === 0 ? 6 : weekDay - 1;
    const workingDay = work_schedules.find(
      (schedule) => schedule.value === weekdayIndex,
    );
    if (!workingDay) return false;
  }

  // Otherwise, this is a validshipping day
  return true;
};

/**
 * isValidShippingScheduleTime()
 * Obtenemos una fecha y un horario.
 * Aplicamos regalas de negocio para verificar
 * si el horario es válido para la fecha indicada,
 * @param {string} time | Horario en formato 'hh:mm:ss'
 * @param {string} targetDate | 'YYYY-MM-DD' Fecha sobre la que verificaremos la disponibilidad de horario.
 * @returns bool
 */
export const isValidShippingScheduleTime = (
  limitTime: string,
  currentTime: string,
  targetDate: string,
): boolean => {
  if (!limitTime || !currentTime || !targetDate) return false;

  // Si la fecha de entrega es para hoy,
  // verificamos que el horario de entrega no haya pasado aún.
  if (DayJs(targetDate).isSame(DayJs(), 'day')) {
    // Convertimos el horario (string 'hh:mm:ss') en un objeto Date
    // para poder compararlo con otras fechas.
    const limitTimeParts = limitTime.split(':');
    const scheduleLimitTime = DayJs()
      .hour(limitTimeParts[0])
      .minute(limitTimeParts[1])
      .second(limitTimeParts[2]);

    const currentTimeParts = currentTime.split(':');
    const scheduleCurrentTime = DayJs()
      .hour(currentTimeParts[0])
      .minute(currentTimeParts[1])
      .second(currentTimeParts[2]);

    // ¿Ya pasó el horario que estamos buscando?
    return scheduleCurrentTime.isBefore(scheduleLimitTime);
  }
  return true;
};

/**
 * getDaysBetweenDates()
 * Takes two dates as an input and returns an array of days between them.
 * @param {string} startDate | Start Date in format 'YYYY-MM-DD'.
 * @param {string} endDate | End Date in format 'YYYY-MM-DD'.
 * @returns {array:string} 'YYYY-MM-DD'
 */
export const getDaysBetweenDates = function (
  startDate: string,
  endDate: string,
): Array<string> {
  const dates = [];
  let currDate = DayJs(startDate).startOf('day');
  let lastDate = DayJs(endDate).startOf('day');

  while (currDate.diff(lastDate) < 1) {
    dates.push(currDate.format('YYYY-MM-DD'));
    currDate = currDate.add(1, 'day');
  }
  return dates;
};
