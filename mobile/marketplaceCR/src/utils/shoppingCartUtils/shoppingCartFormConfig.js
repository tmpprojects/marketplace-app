import { validateShoppingForm } from './validateShoppingCartForm';

const cartFormName = 'shoppingOrder_form';
export const SHOPPING_CART_FORM_CONFIG = {
  formName: cartFormName,
  config: {
    form: cartFormName,
    destroyOnUnmount: false, // <---- Preserve data. Dont destroy form on unmount
    enableReinitialize: true, // <---- Allow updates when 'initValues' prop change
    keepDirtyOnReinitialize: false, // <---- Prevent 'dirty' fields to update
    updateUnregisteredFields: true, // <---- Update unregistered fields.
    forceUnregisterOnUnmount: true, // <---- Unregister fields on unmount
    //onSubmit: a => { return new Promise((resolve, reject) => console.log('submitted.')); },
    validate: () => {
      return globalThis.ShoppingCartManager.validateOrder();
    },
  },
};
