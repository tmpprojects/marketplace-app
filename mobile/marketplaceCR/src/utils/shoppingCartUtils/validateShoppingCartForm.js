import { SHIPPING_METHODS } from '../../redux/constants/app.constants';

export const validateShoppingForm = (values) => {
  const errors = {};
  if (!values) {
    errors.emptyForm = 'Form must not be empty';
    return errors;
  }

  // if (!values.products || !values.products.length) {
  //     errors.products = 'No Products on Cart'
  // }
  let requiresAddress = true;
  if (values.hasOwnProperty('orders')) {
    requiresAddress = values.orders.find((o) =>
      o.physical_properties.selected_shipping_method
        ? o.physical_properties.selected_shipping_method.slug !==
          SHIPPING_METHODS.PICKUP_POINT.slug
        : false,
    );
  }

  // if (requiresAddress) {
  //     errors.shippingMethod = 'Shipping Method Required';
  // }
  if (requiresAddress && !values.selectedShippingAddress) {
    errors.shippingMethod = 'Selecciona una dirección';
  }

  if (!values.shippingAddress && requiresAddress) {
    errors.shippingAddress = 'Shipping Address Required';
  }

  if (!values.paymentMethod || !values.paymentMethod.valid) {
    errors.paymentMethod = 'Valid Payment Method Required';
  }

  //
  if (values.gift) {
    if (!values.giftReceiver.name) {
      errors.giftReceiver = {};
      errors.giftReceiver.name = 'Campo obligatorio.';
    }
    if (!values.giftReceiver.phone) {
      errors.giftReceiver = {};
      errors.giftReceiver.phone = 'Campo obligatorio.';
    }
    if (!values.giftReceiver.name) {
      errors.giftReceiver = {};
      errors.giftReceiver.name = 'Campo obligatorio.';
    }
  }

  //
  if (!values.clientData) {
    errors.clientData = 'Información del cliente obligatoria.';
  } else {
    if (!values.clientData.client_name) {
      errors.clientData = {};
      errors.clientData.client_name = 'Campo obligatorio.';
    }
    if (!values.clientData.client_lastName) {
      errors.clientData = {};
      errors.clientData.client_lastName = 'Campo obligatorio.';
    }
    if (!values.clientData.phone) {
      errors.clientData = {};
      errors.clientData.phone = 'Campo obligatorio.';
    }
    if (!values.clientData.email) {
      errors.clientData = {};
      errors.clientData.email = 'Campo obligatorio.';
    }
  }

  return errors;
};

export const shippingSubmit = () => {
  //return false;
  //history.push("/checkout/payment")
};
export const paymentSubmit = () => {
  //return false;
  //history.push("/checkout/payment")
};
export const confirmationSubmit = () => {
  //window.location = "/checkout/review"
  //return false;
  //history.push("/checkout/payment")
};
