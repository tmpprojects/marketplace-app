/**
 * @jest-environment jsdom
 */
import DayJs from 'dayjs';

import type { OrderGroupType } from '../../../OrderGroupTypes';
import { ShoppingCartManager, ClientDataError } from './helpers'; //./ShoppingCartManager';

import { getProductDetail } from '../../redux/selectors/store.selectors';
// import type {
//   SelectedProductType,
//   ShippingDateType,
// } from '../../../ProductTypes';
import PRODUCT_MOCK from '../../pages/ProductDetail/__tests__/product-mock';

const cartProduct = getProductDetail(PRODUCT_MOCK);

describe('ShoppingCartManager Class', () => {
  let ShoppingCart: ShoppingCartManager;

  // Setup
  beforeEach(() => {
    ShoppingCart = new ShoppingCartManager();
  });

  it('Selects a shipping address when updating user address list.', () => {
    const address = {
      uuid: 'abc123',
      num_ext: '315', // Campo obligatorio
      num_int: null,
      city: 'Miguel Hidalgo',
      state: 'CDMX',
      phone: '55394856', // Datos inválidos
      client_name: 'John Smith',
      zip_code: '11800', // Datos inválidos
      street_address: 'Montes Urales',
      neighborhood: 'Las lomas',
    };
    ShoppingCart.setAddresses([address]);
    expect(ShoppingCart.getShippingAddress()).toEqual(address);

    const algo = [0].find((address, index) => index === [0].length - 1);
    expect(algo).toEqual(0);
  });

  //
  it('Creates instance of ShoppingCartManager correctly', () => {
    expect(1).not.toBeNull();
  });

  describe('WHEN SETTING GUEST USER INFORMATION: ', () => {
    const ShoppingCart = new ShoppingCartManager();
    const cartProducts = [
      {
        id: 393420,
        attribute: null,
        created: '2020-05-07T20:12:16.786418-05:00',
        edited: '2020-05-07T20:12:16.786437-05:00',
        note: '',
        physical_properties: {
          shipping_date: '2020-05-11',
          shipping_schedule: {
            id: 2,
            name: 'Mañana',
            collection_start: '09:00:00',
            collection_end: '13:00:00',
            delivery_start: '10:00:00',
            delivery_end: '14:00:00',
          },
        },
        product: {
          id: 69345,
          name: 'Caja de Galletas Mother´s Day',
          slug: 'caja-de-galletas-mother-s-day-11098811',
          price: '240.00',
          quantity: 1,
          store: {
            name: 'Macarena Alta Repostería',
            photo: {
              medium:
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/photo/e507a4d283ad2c9b59ea35a4cf769879-crop-c0-5__0-5-680x680.png',
              big:
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/photo/e507a4d283ad2c9b59ea35a4cf769879-crop-c0-5__0-5-960x960.png',
              small:
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/photo/e507a4d283ad2c9b59ea35a4cf769879-crop-c0-5__0-5-480x480.png',
              original:
                'https://canastarosa.s3.amazonaws.com/media/market/store/photo/e507a4d283ad2c9b59ea35a4cf769879.png',
            },
            shipping_schedules: [
              {
                id: 2,
                name: 'Mañana',
                collection_start: '09:00:00',
                collection_end: '13:00:00',
                delivery_start: '10:00:00',
                delivery_end: '14:00:00',
              },
              {
                id: 4,
                name: 'Tarde',
                collection_start: '15:00:00',
                collection_end: '19:00:00',
                delivery_start: '16:00:00',
                delivery_end: '20:00:00',
              },
            ],
            slug: 'macarena-alta-reposteria',
            vacations_ranges: [
              { start: '2019-07-29', end: '2019-08-02' },
              { start: '2019-09-12', end: '2019-09-17' },
              { start: '2019-11-14', end: '2019-11-18' },
              { start: '2019-12-24', end: '2020-01-05' },
              { start: '2020-03-03', end: '2020-03-07' },
            ],
            work_schedules: [
              {
                week_day: { value: 6, display_name: 'Domingo' },
                start: '09:00:00',
                end: '17:30:00',
              },
              {
                week_day: { value: 5, display_name: 'Sábado' },
                start: '08:30:00',
                end: '17:30:00',
              },
              {
                week_day: { value: 4, display_name: 'Viernes' },
                start: '08:30:00',
                end: '17:30:00',
              },
              {
                week_day: { value: 3, display_name: 'Jueves' },
                start: '08:30:00',
                end: '17:30:00',
              },
              {
                week_day: { value: 2, display_name: 'Miércoles' },
                start: '08:30:00',
                end: '17:30:00',
              },
              {
                week_day: { value: 1, display_name: 'Martes' },
                start: '08:30:00',
                end: '17:30:00',
              },
              {
                week_day: { value: 0, display_name: 'Lunes' },
                start: '08:30:00',
                end: '17:30:00',
              },
            ],
          },
          photos: [
            {
              id: 153518,
              file: 'f7a36247f0bf54d9a80962279602ab91.jpg',
              photo: {
                small:
                  'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/f7a36247f0bf54d9a80962279602ab91-crop-c0-5__0-5-280x280-90.jpg',
                medium:
                  'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/f7a36247f0bf54d9a80962279602ab91-thumbnail-560x560-90.jpg',
                big:
                  'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/f7a36247f0bf54d9a80962279602ab91-thumbnail-1120x1120-90.jpg',
                original:
                  'https://canastarosa.s3.amazonaws.com/media/market/product/f7a36247f0bf54d9a80962279602ab91.jpg',
              },
              order: 0,
            },
            {
              id: 153521,
              file: '634be2d212731cf40ecaa100927d2e2a.jpg',
              photo: {
                small:
                  'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/634be2d212731cf40ecaa100927d2e2a-crop-c0-5__0-5-280x280-90.jpg',
                medium:
                  'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/634be2d212731cf40ecaa100927d2e2a-thumbnail-560x560-90.jpg',
                big:
                  'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/634be2d212731cf40ecaa100927d2e2a-thumbnail-1120x1120-90.jpg',
                original:
                  'https://canastarosa.s3.amazonaws.com/media/market/product/634be2d212731cf40ecaa100927d2e2a.jpg',
              },
              order: 1,
            },
          ],
          physical_properties: {
            fabrication_time: 1,
            is_available: { value: 2, display_name: 'Disponible Bajo Pedido' },
            life_time: 0,
            minimum_shipping_date: '2020-05-11',
            maximum_shipping_date: '2020-08-15',
            shipping: [
              {
                delivery_date: '2020-05-11',
                description: '',
                excerpt: '',
                is_available: true,
                minimum_delivery_date: '2020-05-11',
                name: 'Express Moto',
                price: 69.0,
                slug: 'express-moto',
                why_is_not_available: null,
              },
              {
                delivery_date: '2020-05-11',
                description: '',
                excerpt: '',
                is_available: true,
                minimum_delivery_date: '2020-05-11',
                name: 'Express Automóvil',
                price: 99.0,
                slug: 'express-car',
                why_is_not_available: null,
              },
            ],
            size: { value: 1, display_name: 'Sobre' },
          },
        },
        quantity: 1,
        unit_price: 240.0,
      },
    ];
    const shippingAddresses = [
      {
        uuid: 'abc123',
        num_ext: '315',
        num_int: null,
        city: 'Miguel Hidalgo',
        state: 'CDMX',
        phone: '55394856',
        client_name: 'John Smith',
        zip_code: '11800',
        street_address: 'Montes Urales',
        neighborhood: 'Las lomas',
      },
    ];
    const validPaymentMethod = {
      valid: true,
      slug: 'oxxo',
      payment_method_id: 'oxxo',
    };
    ShoppingCart.setPaymentMethod(validPaymentMethod);
    ShoppingCart.updateCart(cartProducts);

    //
    it('Validates the order when setting a shipping address.', () => {
      ShoppingCart.setShippingAddress(shippingAddresses[0]);
      expect(ShoppingCart.validateOrder()).toEqual({});
    });
    it('Validates the order when setting a list of addresses.', () => {
      ShoppingCart.setShippingAddress();
      ShoppingCart.setAddresses(shippingAddresses);
      expect(ShoppingCart.validateOrder()).toEqual({});
    });
    // it('Gets client information based on shipping address.', () => {
    //   const shippingAddress = {
    //     phone: '55394856',
    //     client_name: 'John Smith',
    //   };
    //   expect(ShoppingCart.getClientData()).toStrictEqual(shippingAddress);
    // });
  });

  describe('WHEN SETTING CLIENT INFORMATION: ', () => {
    const ShoppingCart = new ShoppingCartManager();
    it('Receives an object with errors if client information is not set correctly.', () => {
      expect(ShoppingCart.hasClientDataErrors()).not.toBeNull();
    });
    it('Expects an error if we attempt to set invalid or incomplete client information.', () => {
      const clientData = () =>
        ShoppingCart.setClientData({
          name: 'asdasasd',
          telephone: 'asd',
          lastName: 'asdsad',
          //email: 'invalid or missing email',
        });
      expect(clientData).toThrow(ClientDataError);
    });
    it('Receives new data if we set valid client information.', () => {
      const clientData = {
        name: 'John',
        lastName: 'Smith',
        telephone: '5530458595',
        email: 'test@canastarosa.com',
      };
      expect(ShoppingCart.setClientData(clientData)).toStrictEqual(clientData);
    });
  });

  describe('WHEN SETTING A SHIPPING ADDRESS: ', () => {
    const ShoppingCart = new ShoppingCartManager();
    it('Receives an object with errors if shipping address is not set correctly.', () => {
      expect(ShoppingCart.hasShippingAddressErrors()).not.toBeNull();
    });
    it('Expects an error if we attempt to set invalid or incomplete shipping address.', () => {
      const shippingAddress = () =>
        ShoppingCart.setShippingAddress({
          uuid: 'string',
          num_ext: null, // Campo obligatorio
          num_int: 'string',
          city: 'string',
          state: 'string',
          phone: 'teléfono inválido', // Datos inválidos
          client_name: 'string',
          zip_code: '11800-abc-123', // Datos inválidos
          street_address: 'string',
          neighborhood: 'string',
        });
      expect(shippingAddress).toThrow(Error);
    });
    it('Receives new data if we set a valid shipping address.', () => {
      const shippingAddress = {
        uuid: 'abc123',
        num_ext: '315',
        num_int: null,
        city: 'Miguel Hidalgo',
        state: 'CDMX',
        phone: '55394856',
        client_name: 'John Smith',
        zip_code: '11800',
        street_address: 'Montes Urales',
        neighborhood: 'Las lomas',
      };
      expect(ShoppingCart.setShippingAddress(shippingAddress)).toStrictEqual(
        shippingAddress,
      );
    });
    it('Receives an object of errors if static validateAddress() utility gets an invalid address.', () => {
      const shippingAddress = {
        uuid: 'abc123',
        num_ext: '315',
        num_int: null,
        city: 'Miguel Hidalgo',
        state: 'CDMX',
        phone: '55394856',
        client_name: 'John Smith',
        //zip_code: '11800', // Campo obligatorio
        street_address: 'Montes Urales',
        neighborhood: 'Las lomas',
      };
      expect(
        ShoppingCartManager.validateAddress(shippingAddress),
      ).not.toBeNull();
    });
    it('Receives no errors if static validateAddress() utility gets valid information.', () => {
      const shippingAddress = {
        uuid: 'abc123',
        num_ext: '315', // Campo obligatorio
        num_int: null,
        city: 'Miguel Hidalgo',
        state: 'CDMX',
        phone: '55394856', // Datos inválidos
        client_name: 'John Smith',
        zip_code: '11800', // Datos inválidos
        street_address: 'Montes Urales',
        neighborhood: 'Las lomas',
      };
      expect(
        ShoppingCartManager.validateAddress(shippingAddress),
      ).toStrictEqual(null);
    });
  });

  describe('WHEN SETTING A PAYMENT METHOD: ', () => {
    const ShoppingCart = new ShoppingCartManager();
    const validPaymentMethod = {
      valid: true,
      slug: 'oxxo',
      payment_method_id: 'oxxo',
    };

    it('Receives an object with errors if payment method is not set correctly.', () => {
      expect(ShoppingCart.hasPaymentMethodErrors()).not.toBeNull();
    });
    it('Expects an error if we dont set a payment `slug`.', () => {
      const { slug, ...invalidPaymentMethod } = validPaymentMethod; // Remove required property.
      const paymentError = () =>
        ShoppingCart.setPaymentMethod(invalidPaymentMethod);
      expect(paymentError).toThrow(Error);
    });
    it('Expects an error if we dont set the property `valid` to a boolean value.', () => {
      const { valid, ...invalidPaymentMethod } = validPaymentMethod; // Remove required property.
      const paymentError = () =>
        ShoppingCart.setPaymentMethod(invalidPaymentMethod);
      expect(paymentError).toThrow(Error);
    });
    it('Expects an error if we dont set a `payment_method_id`.', () => {
      const {
        payment_method_id, // Remove required property.
        ...invalidPaymentMethod
      } = validPaymentMethod;
      const paymentError = () =>
        ShoppingCart.setPaymentMethod(invalidPaymentMethod);
      expect(paymentError).toThrow(Error);
    });

    describe('WHEN PAYING WITH CREDIT CARDS: ', () => {
      const validCreditCardPaymentMethod = {
        ...validPaymentMethod,
        slug: 'stripe', // Define a credit card payment type.
        payment_card_token: '000xxx000xxx', // Define a card tokene.
      };
      it('Expects an error if we dont set a `payment_card_token` when using stripe.', () => {
        const invalidPaymentMethod = {
          ...validCreditCardPaymentMethod,
          slug: 'stripe', // Define a credit card payment type.
          payment_card_token: undefined, // Remove required property.
        };
        const paymentError = () =>
          ShoppingCart.setPaymentMethod(invalidPaymentMethod);
        expect(paymentError).toThrow(Error);
      });
      it('Expects an error if we dont set a `payment_card_token` when using mercado-pago.', () => {
        const invalidPaymentMethod = {
          ...validCreditCardPaymentMethod,
          slug: 'mercado-pago', // Define a credit card payment type.
          payment_card_token: undefined, // Remove required property.
        };
        const paymentError = () =>
          ShoppingCart.setPaymentMethod(invalidPaymentMethod);
        expect(paymentError).toThrow(Error);
      });
      it('Expects an error if we dont set a `payment_card_token` when using a saved card.', () => {
        const invalidPaymentMethod = {
          ...validCreditCardPaymentMethod,
          slug: 'save-card', // Define a credit card payment type.
          payment_card_token: undefined, // Remove required property.
        };
        const paymentError = () =>
          ShoppingCart.setPaymentMethod(invalidPaymentMethod);
        expect(paymentError).toThrow(Error);
      });
    });

    describe('NON DIGITAL PAYMENTS: ', () => {
      let PhysicalShoppingCart;
      const validPhysicalPayment = {
        ...validPaymentMethod,
        slug: 'bank-transfer', // Define a non digital payment.
        valid: true,
      };

      // Setup
      beforeEach(() => {
        PhysicalShoppingCart = new ShoppingCartManager();
      });
      afterEach(() => {
        PhysicalShoppingCart = null;
      });

      describe('BANK TRANSFER: ', () => {
        it('Expects an error if we dont explicitly set the property `valid` as true.', () => {
          const invalidPaymentMethod = {
            ...validPhysicalPayment,
            slug: 'bank-transfer',
            valid: false, // Set an invalid value.
          };
          const paymentError = () =>
            PhysicalShoppingCart.setPaymentMethod(invalidPaymentMethod);
          expect(paymentError).toThrow(Error);
        });
        it('Dont return any error when payment method is set correctly.', () => {
          PhysicalShoppingCart.setPaymentMethod({
            ...validPhysicalPayment,
            slug: 'bank-transfer',
          });
          const paymentError = PhysicalShoppingCart.hasPaymentMethodErrors();
          expect(paymentError).toEqual(null);
        });
      });

      describe('OXXO: ', () => {
        it('Expects an error if we dont explicitly set the property `valid` as true.', () => {
          const invalidPaymentMethod = {
            ...validPhysicalPayment,
            slug: 'oxxo',
            valid: false, // Set an invalid value.
          };
          const paymentError = () =>
            PhysicalShoppingCart.setPaymentMethod(invalidPaymentMethod);
          expect(paymentError).toThrow(Error);
        });
        it('Dont return any error when payment method is set correctly.', () => {
          PhysicalShoppingCart.setPaymentMethod({
            ...validPhysicalPayment,
            slug: 'oxxo',
          });
          const paymentError = PhysicalShoppingCart.hasPaymentMethodErrors();
          expect(paymentError).toEqual(null);
        });
      });

      describe('CASH: ', () => {
        it('Expects an error if we dont explicitly set the property `valid` as true.', () => {
          const invalidPaymentMethod = {
            ...validPhysicalPayment,
            slug: 'cash',
            valid: false, // Set an invalid value.
          };
          const paymentError = () =>
            PhysicalShoppingCart.setPaymentMethod(invalidPaymentMethod);
          expect(paymentError).toThrow(Error);
        });
        it('Dont return any error when payment method is set correctly.', () => {
          PhysicalShoppingCart.setPaymentMethod({
            ...validPhysicalPayment,
            slug: 'cash',
          });
          const paymentError = PhysicalShoppingCart.hasPaymentMethodErrors();
          expect(paymentError).toEqual(null);
        });
      });
    });
  });

  describe('WHEN SETTING A COUPON: ', () => {
    const validCoupon = {
      code: 'ABC123',
    };
    it('Receives no error if we dont set any or give non valid coupon.', () => {
      expect(ShoppingCart.setCoupon()).toEqual(null);
      expect(ShoppingCart.setCoupon({})).toEqual(null);
      expect(ShoppingCart.setCoupon(123)).toEqual(null);
    });
    it('Receives coupon details if we set a correct coupon.', () => {
      expect(ShoppingCart.setCoupon(validCoupon)).toEqual(validCoupon);
    });
    it('Expects an error if we dont set a coupon code.', () => {
      const { code, ...invalidCoupon } = validCoupon; // <-- Remove required property.
      const couponError = () =>
        ShoppingCart.setCoupon({
          ...invalidCoupon,
          otherProps: 'values',
        });
      expect(couponError).toThrow(Error);
    });

    /**
     * TODO: TEST FOR APPLYING DISCOUNT WHEN SETTING A VALID COUPON
     */
  });

  const cartProducts = [
    {
      id: 393420,
      attribute: null,
      created: '2020-05-07T20:12:16.786418-05:00',
      edited: '2020-05-07T20:12:16.786437-05:00',
      note: '',
      physical_properties: {
        shipping_date: '2020-05-11',
        shipping_schedule: {
          id: 2,
          name: 'Mañana',
          collection_start: '09:00:00',
          collection_end: '13:00:00',
          delivery_start: '10:00:00',
          delivery_end: '14:00:00',
        },
      },
      product: {
        id: 69345,
        name: 'Caja de Galletas Mother´s Day',
        slug: 'caja-de-galletas-mother-s-day-11098811',
        price: '240.00',
        quantity: 1,
        store: {
          name: 'Macarena Alta Repostería',
          photo: {
            medium:
              'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/photo/e507a4d283ad2c9b59ea35a4cf769879-crop-c0-5__0-5-680x680.png',
            big:
              'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/photo/e507a4d283ad2c9b59ea35a4cf769879-crop-c0-5__0-5-960x960.png',
            small:
              'https://canastarosa.s3.amazonaws.com/media/__sized__/market/store/photo/e507a4d283ad2c9b59ea35a4cf769879-crop-c0-5__0-5-480x480.png',
            original:
              'https://canastarosa.s3.amazonaws.com/media/market/store/photo/e507a4d283ad2c9b59ea35a4cf769879.png',
          },
          shipping_schedules: [
            {
              id: 2,
              name: 'Mañana',
              collection_start: '09:00:00',
              collection_end: '13:00:00',
              delivery_start: '10:00:00',
              delivery_end: '14:00:00',
            },
            {
              id: 4,
              name: 'Tarde',
              collection_start: '15:00:00',
              collection_end: '19:00:00',
              delivery_start: '16:00:00',
              delivery_end: '20:00:00',
            },
          ],
          slug: 'macarena-alta-reposteria',
          vacations_ranges: [
            { start: '2019-07-29', end: '2019-08-02' },
            { start: '2019-09-12', end: '2019-09-17' },
            { start: '2019-11-14', end: '2019-11-18' },
            { start: '2019-12-24', end: '2020-01-05' },
            { start: '2020-03-03', end: '2020-03-07' },
          ],
          work_schedules: [
            {
              week_day: { value: 6, display_name: 'Domingo' },
              start: '09:00:00',
              end: '17:30:00',
            },
            {
              week_day: { value: 5, display_name: 'Sábado' },
              start: '08:30:00',
              end: '17:30:00',
            },
            {
              week_day: { value: 4, display_name: 'Viernes' },
              start: '08:30:00',
              end: '17:30:00',
            },
            {
              week_day: { value: 3, display_name: 'Jueves' },
              start: '08:30:00',
              end: '17:30:00',
            },
            {
              week_day: { value: 2, display_name: 'Miércoles' },
              start: '08:30:00',
              end: '17:30:00',
            },
            {
              week_day: { value: 1, display_name: 'Martes' },
              start: '08:30:00',
              end: '17:30:00',
            },
            {
              week_day: { value: 0, display_name: 'Lunes' },
              start: '08:30:00',
              end: '17:30:00',
            },
          ],
        },
        photos: [
          {
            id: 153518,
            file: 'f7a36247f0bf54d9a80962279602ab91.jpg',
            photo: {
              small:
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/f7a36247f0bf54d9a80962279602ab91-crop-c0-5__0-5-280x280-90.jpg',
              medium:
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/f7a36247f0bf54d9a80962279602ab91-thumbnail-560x560-90.jpg',
              big:
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/f7a36247f0bf54d9a80962279602ab91-thumbnail-1120x1120-90.jpg',
              original:
                'https://canastarosa.s3.amazonaws.com/media/market/product/f7a36247f0bf54d9a80962279602ab91.jpg',
            },
            order: 0,
          },
          {
            id: 153521,
            file: '634be2d212731cf40ecaa100927d2e2a.jpg',
            photo: {
              small:
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/634be2d212731cf40ecaa100927d2e2a-crop-c0-5__0-5-280x280-90.jpg',
              medium:
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/634be2d212731cf40ecaa100927d2e2a-thumbnail-560x560-90.jpg',
              big:
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/634be2d212731cf40ecaa100927d2e2a-thumbnail-1120x1120-90.jpg',
              original:
                'https://canastarosa.s3.amazonaws.com/media/market/product/634be2d212731cf40ecaa100927d2e2a.jpg',
            },
            order: 1,
          },
        ],
        physical_properties: {
          fabrication_time: 1,
          is_available: { value: 2, display_name: 'Disponible Bajo Pedido' },
          life_time: 0,
          minimum_shipping_date: '2020-05-11',
          maximum_shipping_date: '2020-08-15',
          shipping: [
            {
              delivery_date: '2020-05-11',
              description: '',
              excerpt: '',
              is_available: true,
              minimum_delivery_date: '2020-05-11',
              name: 'Express Moto',
              price: 69.0,
              slug: 'express-moto',
              why_is_not_available: null,
            },
            {
              delivery_date: '2020-05-11',
              description: '',
              excerpt: '',
              is_available: true,
              minimum_delivery_date: '2020-05-11',
              name: 'Express Automóvil',
              price: 99.0,
              slug: 'express-car',
              why_is_not_available: null,
            },
          ],
          size: { value: 1, display_name: 'Sobre' },
        },
      },
      quantity: 1,
      unit_price: 240.0,
    },
  ];
  describe('WHEN SETTING NEW PRODUCTS INTO THE SHOPPING CART: ', () => {
    it('Receives an empty array when passing no products.', () => {
      expect(ShoppingCart.updateCart()).toEqual([]);
    });
    it('Returns an array of orders when we update the cart.', () => {
      expect(ShoppingCart.updateCart(cartProducts)).toEqual(
        expect.arrayContaining([expect.objectContaining({})]),
      );
    });

    it('Returns an array of orders when pass a list of products.', () => {
      expect(ShoppingCart.setProductsByStore(cartProducts)).toEqual(
        expect.arrayContaining([expect.objectContaining({})]),
      );
    });
  });

  it('Returns true if the shopping cart has a valid order.', () => {
    ShoppingCart.setClientData({
      name: 'John',
      lastName: 'Smith',
      telephone: '5530458595',
      email: 'test@canastarosa.com',
    });
    ShoppingCart.setShippingAddress({
      uuid: 'abc123',
      num_ext: '315', // Campo obligatorio
      num_int: null,
      city: 'Miguel Hidalgo',
      state: 'CDMX',
      phone: '55394856', // Datos inválidos
      client_name: 'John Smith',
      zip_code: '11800', // Datos inválidos
      street_address: 'Montes Urales',
      neighborhood: 'Las lomas',
    });
    ShoppingCart.setPaymentMethod({
      valid: true,
      slug: 'oxxo',
      payment_method_id: 'oxxo',
    });
    ShoppingCart.updateCart(cartProducts);
    expect(ShoppingCart.hasValidOrder()).toEqual(true);
  });

  /** 
   * PRODUCT SETUP
   
  describe('PRODUCT SETUP: ', () => {
    it('Parses attributes tree into a flat list', () => {
      const attributesList = Manager._parseAttributeTypesIntoSelectOptions(
        formattedProduct.attributes,
      );
      expect(attributesList).toStrictEqual(attributesListMock);
    });
    it('Gets a flattened attributes-tree indexes map to selectedProduct properties', () => {
      expect(Manager.getAttributeSelectionMap()).toEqual(attributeSelectionMap);
    });
    it('Can set attributes with different depth-levels', () => {
      let attributes;
      let attributesMock;

      // Try with an empty list. (No attributes/variations)
      attributes = [];
      attributesMock = [];
      expect(Manager.setAttributes(attributes)).toStrictEqual(attributesMock);

      // Try with one attribute
      attributes = [
        {
          id: 313485,
          value: 'Amarillo',
          attribute_type: 'color',
          attribute_uuid: {
            id: 263633,
            uuid: 'yOvjBH8Urys4icVie_GAJ2',
            is_visible: true,
          },
          attribute_stock: null,
          attribute_extradata: null,
        },
        {
          id: 234678,
          value: 'Azul',
          attribute_type: 'color',
          attribute_uuid: {
            id: 135678,
            uuid: 'BasddH8fUryf_4icasd',
            is_visible: true,
          },
          attribute_stock: null,
          attribute_extradata: null,
        },
      ];
      attributesMock = [
        {
          type: 'color',
          ref: 'color0_sbx',
          values: ['Amarillo', 'Azul'],
          options: ['Amarillo', 'Azul'],
        },
      ];
      expect(Manager.setAttributes(attributes)).toStrictEqual(attributesMock);

      // Try with nested attributes
      attributes = [
        {
          id: 313485,
          value: 'Amarillo',
          attribute_type: 'color',
          attribute_uuid: null,
          attribute_stock: null,
          attribute_extradata: null,
          children: [
            {
              id: 123456,
              value: 'Grande',
              attribute_type: 'talla',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
              children: [
                {
                  id: 345678,
                  value: 'Algodón',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 134563,
                    uuid: 'Ury_yOvjB_s4yOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 876543,
                  value: 'Franela',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 192836,
                    uuid: 'OvjB_s4Ury_yyOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
            },
          ],
        },
        {
          id: 176567,
          value: 'Azul',
          attribute_type: 'color',
          attribute_uuid: {
            id: 456864,
            uuid: 'ddH8fUryf_4icBasasd',
            is_visible: true,
          },
          attribute_stock: null,
          attribute_extradata: null,
          children: [
            {
              id: 876567,
              value: 'Grande',
              attribute_type: 'talla',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
              children: [
                {
                  id: 654567,
                  value: 'Algodón',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 357976,
                    uuid: 's4yOvjBH8Ury_yOvjB_',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 9876435,
                  value: 'Franela',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 765345,
                    uuid: 'y_yyOvjOvjB_s4UrBH8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
            },
          ],
        },
      ];
      attributesMock = [
        {
          type: 'color',
          ref: 'color0_sbx',
          values: ['Amarillo', 'Azul'],
          options: ['Amarillo', 'Azul'],
        },
        {
          type: 'talla',
          ref: 'talla1_sbx',
          values: ['Grande'],
          options: ['Grande'],
        },
        {
          type: 'tela',
          ref: 'tela2_sbx',
          values: ['Algodón', 'Franela'],
          options: ['Algodón', 'Franela'],
        },
      ];
      expect(Manager.setAttributes(attributes)).toStrictEqual(attributesMock);
    });
  });*/

  /**
   * PRODUCT INTERACTION
   
  describe('PRODUCT INTERACTION: ', () => {
    it('Gets selected product properties from a specific attribute selection', () => {
      let selectionMap;
      let productPropertiesMock;

      // Sets a new product variation selection to get corresponding attribute properties.
      selectionMap = [0, 0, 1, 2];
      productPropertiesMock = {
        uuid: selectionMap.reduce((childAttribute, childIndex, index) => {
          if (index === 0) {
            return formattedProduct.attributes[childIndex];
          }
          return childAttribute.children[childIndex];
        }, {}).attribute_uuid,
        price: parseFloat(formattedProduct.price),
        quantity: 1,
        stock: formattedProduct.quantity,
      };
      expect(Manager.setProductProperties(selectionMap)).toStrictEqual(
        productPropertiesMock,
      );

      // Expect selectedProduct object to be updated correctly.
      const selectedProduct: SelectedProductType = {
        ...selectedProductMock,
        ...productPropertiesMock,
        attributeSelectionMap: selectionMap,
      };
      expect(Manager.getSelectedProduct()).toStrictEqual(selectedProduct);
    });

    it('Gets a default selected product', () => {
      expect(Manager.getSelectedProduct()).toStrictEqual(selectedProductMock);
    });

    /**
     * UPDATES QUANTIT.
     
    describe('UPDATES QUANTITY', () => {
      it('Updates quantity', () => {
        expect(Manager.setQuantity(5)).toBe(5);
      });

      it('Does not exceed product stock', () => {
        expect(Manager.setQuantity(900)).toBe(formattedProduct.quantity);
      });

      it('Is greater than 1 unit', () => {
        expect(Manager.setQuantity(0)).toBe(1);
      });
    });

    /**
     * ATTRIBUTES DON´T MODIFY PRODUCT PROPS.
     
    describe('ATTRIBUTES THAT DON´T MODIFY PRODUCT PROPS.', () => {
      let selectionMap = [0, 0, 1];
      let attributes = [
        {
          id: 313485,
          value: 'Amarillo',
          attribute_type: 'color',
          attribute_uuid: null,
          attribute_stock: null,
          attribute_extradata: null,
          children: [
            {
              id: 123456,
              value: 'Grande',
              attribute_type: 'talla',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
              children: [
                {
                  id: 345678,
                  value: 'Algodón',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 134563,
                    uuid: 'Ury_yOvjB_s4yOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
                {
                  id: 876543,
                  value: 'Franela',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 192836,
                    uuid: 'OvjB_s4Ury_yyOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: null,
                  attribute_extradata: null,
                },
              ],
            },
          ],
        },
      ];
      let productPropertiesMock = {
        quantity: 1,
        uuid: selectionMap.reduce((childAttribute, childIndex, index) => {
          if (index === 0) {
            return attributes[childIndex];
          }
          return childAttribute.children[childIndex];
        }, {}).attribute_uuid,
        price: formattedProduct.price,
        stock: formattedProduct.quantity,
      };
      const NewProduct = new ShoppingCartManager({
        ...formattedProduct,
        attributes,
        productMadeOnDemand: false,
      });

      it('Get attribute uuid and general product price/stock', () => {
        expect(NewProduct.setProductProperties(selectionMap)).toStrictEqual(
          productPropertiesMock,
        );
      });

      it('Quantity does not exceed general product stock', () => {
        expect(NewProduct.setQuantity(500)).toBe(formattedProduct.quantity);
      });
    });

    /**
     * ATTRIBUTES MODIFY PRICE AND STOCK
     
    describe('ATTRIBUTES THAT MODIFY PRICE AND STOCK. ', () => {
      const attributes = [
        {
          id: 313485,
          value: 'Amarillo',
          attribute_type: 'color',
          attribute_uuid: null,
          attribute_stock: null,
          attribute_extradata: null,
          children: [
            {
              id: 123456,
              value: 'Grande',
              attribute_type: 'talla',
              attribute_uuid: null,
              attribute_stock: null,
              attribute_extradata: null,
              children: [
                {
                  id: 345678,
                  value: 'Algodón',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 134563,
                    uuid: 'Ury_yOvjB_s4yOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: {
                    quantity: 23,
                    price: 300,
                  },
                  attribute_extradata: null,
                },
                {
                  id: 876543,
                  value: 'Franela',
                  attribute_type: 'tela',
                  attribute_uuid: {
                    id: 192836,
                    uuid: 'OvjB_s4Ury_yyOvjBH8',
                    is_visible: true,
                  },
                  attribute_stock: {
                    quantity: 70,
                    price: 200,
                  },
                  attribute_extradata: null,
                },
              ],
            },
          ],
        },
      ];
      const selectionMap = [0, 0, 1];
      const attributeDetails = selectionMap.reduce(
        (childAttribute, childIndex, index) => {
          if (index === 0) {
            return attributes[childIndex];
          }
          return childAttribute.children[childIndex];
        },
        {},
      );

      it('Get product´s attribute uuid, stock and price.', () => {
        // Sets a new product variation selection to get corresponding attribute properties.
        const NewProduct = new ShoppingCartManager({
          ...formattedProduct,
          attributes,
          productMadeOnDemand: false,
        });
        expect(NewProduct.setProductProperties(selectionMap)).toStrictEqual({
          quantity: 1,
          uuid: attributeDetails.attribute_uuid,
          price: attributeDetails.attribute_stock.price,
          stock: attributeDetails.attribute_stock.quantity,
        });
      });

      it('Quantity does not exceed product attribute´s stock', () => {
        // Sets a new product variation selection to get corresponding attribute properties.
        const NewProduct = new ShoppingCartManager({
          ...formattedProduct,
          attributes,
          productMadeOnDemand: false,
        });
        NewProduct.setProductProperties(selectionMap);
        expect(NewProduct.setQuantity(500)).toBe(
          attributeDetails.attribute_stock.quantity,
        );
      });

      /**
       * MADE ON DEMAND
       
      describe('PRODUCT MADE ON DEMAND. ', () => {
        // Sets a new product variation selection to get corresponding attribute properties.
        const ProductOnDemandManager = new ShoppingCartManager({
          ...formattedProduct,
          attributes,
          productMadeOnDemand: true,
        });

        it('Gets product´s main stock (ignore attribute stock if found).', () => {
          productPropertiesMock = {
            quantity: 1,
            uuid: attributeDetails.attribute_uuid,
            price: attributeDetails.attribute_stock.price,
            // !!!If product is made on demand, it should take the 'general product' stock.
            stock: formattedProduct.quantity,
          };
          expect(
            ProductOnDemandManager.setProductProperties(selectionMap),
          ).toStrictEqual(productPropertiesMock);
        });
      });
    });

    /**
     * WITHOUT ATTRIBUTES
     
    describe('PRODUCT WITHOUT ATTRIBUTES', () => {
      it('Gets product´s stock, price, quantity (and ignore attribute uuid).', () => {
        // Sets a new product variation selection to get corresponding attribute properties.
        const NewProduct = new ShoppingCartManager({
          ...formattedProduct,
          attributes: [],
          productMadeOnDemand: false,
        });
        expect(NewProduct.setProductProperties()).toStrictEqual({
          quantity: 1,
          uuid: null,
          price: formattedProduct.price,
          stock: formattedProduct.quantity,
        });
      });
    });

    /**
     * CHANGES DELIVERY DETAILS
     
    describe('SETS DELIVERY DETAILS', () => {
      const NewProduct = new ShoppingCartManager({
        ...formattedProduct,
        physical_properties: {
          ...formattedProduct.physical_properties,
          minimum_shipping_date:
            formattedProduct.physical_properties.minimum_shipping_date,
          shipping_schedules: [
            {
              ...formattedProduct.physical_properties.shipping_schedules[0],
              limit_to_order: '08:00:00', // <== Watch for limit time.
            },
            ...formattedProduct.physical_properties.shipping_schedules.slice(1),
          ],
        },
      });

      it('Changes delivery date', () => {
        const nextDate = DayJs(NewProduct.getSelectedProduct().shipping_date)
          .add(2, 'day')
          .format('YYYY-MM-DD');
        const shippingDateMock: ShippingDateType = {
          shipping_date: nextDate,
          availableShippingSchedules: NewProduct.getSelectedProduct()
            .availableShippingSchedules,
          shipping_schedule: NewProduct.getSelectedProduct()
            .availableShippingSchedules[0].value,
        };
        expect(NewProduct.setShippingDate(nextDate)).toStrictEqual(
          shippingDateMock,
        );
      });

      it('Throws and error if it receives past dates (relative from `today`).', () => {
        const yesterday = DayJs().subtract(1, 'day');
        expect(() => Manager.setShippingDate(yesterday)).toThrow(Error);
      });

      it('Throws and error if it receives a date past maximum_shipping_date.', () => {
        const oneYear = DayJs().add(1, 'year');
        expect(() => Manager.setShippingDate(oneYear)).toThrow(Error);
      });
      it('Throws and error if it receives a date before minimum_shipping_date.', () => {
        const beforeMinimumShippingDate = DayJs(minimumShippingDate).subtract(
          1,
          'day',
        );
        expect(() =>
          NewProduct.setShippingDate(beforeMinimumShippingDate),
        ).toThrow(Error);
      });

      it('Changes shipping schedule.', () => {
        const newShippingSchedule = 2;
        expect(Manager.setShippingSchedule(newShippingSchedule)).toStrictEqual(
          newShippingSchedule,
        );
      });
      it('Throws a error if it receives an invalid shipping schedule id.', () => {
        expect(() => Manager.setShippingSchedule(123)).toThrow(Error);
      });
      it('Throws and error if shipping schedule has already past today.', () => {
        const NewProduct = new ShoppingCartManager({
          ...formattedProduct,
          physical_properties: {
            ...formattedProduct.physical_properties,
            minimum_shipping_date: DayJs().format('YYYY-MM-DD'), // <== Today.
            shipping_schedules: [
              {
                ...formattedProduct.physical_properties.shipping_schedules[0],
                limit_to_order: '08:00:00', // <== Watch for limit time.
              },
              ...formattedProduct.physical_properties.shipping_schedules.slice(
                1,
              ),
            ],
          },
        });
        expect(() =>
          NewProduct.setShippingSchedule(
            2,
            DayJs().hour(9).minute(0).second(0), // <== Fake current time AFTER schedule limit time.
          ),
        ).toThrow(Error);
      });
    });
  });*/
});
