import Moment from 'moment';
import DayJs from 'dayjs';
import {
  SHIPPING_METHODS,
  PAYMENT_METHODS,
  COUPON_TYPES,
} from '../../redux/constants';
import {
  formatWorkSchedules,
  formatShippingSchedules,
} from '../../redux/reducers';

// import { getStoreDetail } from '../../redux/selectors/store.selectors';
// import type {
//   OrderGroupType,
//   OrderClientDataType,
// } from '../../redux/types/OrderGroupTypes';
import { isValidShippingScheduleTime } from '../../utils/date';

//
export const SHOPPING_CART_MODEL = {
  uuid: null,
  slug: '',
  price: 0,
  quantity: 1,
  attributeSelectionMap: [],
  attributesList: [],
  availableShippingSchedules: [],
  shipping_date: '',
  shipping_schedule: '',
};

export class ShoppingCartError extends Error {
  constructor(params, details = {}) {
    // Pass remaining arguments (including vendor specific ones) to parent constructor
    super(params);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ShoppingCartError);
    }

    this.name = 'ShoppingCartError';
    this.details = details;
    this.date = new Date();
  }
}
export class ClientDataError extends ShoppingCartError {
  constructor(params, details = {}) {
    // Pass remaining arguments (including vendor specific ones) to parent constructor
    super(params);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ClientDataError);
    }

    this.name = 'ShoppingCartError.ClientDataError';
    //this.details = details;
    //this.date = new Date();
  }
}

type ClientDataType = {
  name: string,
  lastName: string,
  email: string,
  telephone: string,
};
type ShippingAddressType = {
  uuid: string,
  num_ext: string,
  num_int: string,
  city: string,
  state: string,
  phone: string,
  client_name: string,
  zip_code: string,
  street_address: string,
  neighborhood: string,
};
type PaymentMethodType = {|
  valid: boolean,
  name: ?string,
  slug: ?string, // mercado-pago | stripe | paypal | oxxo | bank-transfer | cash | etc...
  payment_method_id: ?string, // visa | amex | oxxo | mastercard | etc...
  payment_card_token: ?string,
  is_saving_card: boolean,
  is_paying_with_saved_card: boolean,
  was_paid_with_saved_card: boolean,
  required_invoice: boolean,
  origin_frontend: ?string, // mobile | web
|};
type CouponType = {|
  code: string,
  type: ?string,
  valid: boolean,
  discount: number,
  statusMessage: string,
|};
type CartProductType = {|
  slug: string,
  product: {
    quantity: number,
    physical_properties: {
      is_available: { value: number },
      minimum_shipping_date: string,
    },
  },
  remainingStock: { stock: number },
  productMadeOnDemand: boolean,
  attribute: { stock: number },
  quantity: number,
  note: ?string,
  price: number | number,
  maximum_shipping_date: string,
  minimum_shipping_date: string,
  physical_properties: {
    shipping_schedule: string,
    shipping_date: string,
  },
  work_schedules: [],
|};

export const COUPON_MODEL: CouponType = {
  code: '',
  type: null,
  discount: 0,
  valid: false,
  statusMessage: '',
};

export const couponErrorTypes = {
  IVALID_CODE: {
    code: 0,
    label: 'code',
    message: 'invalid; Código Invalido',
    ui_message: 'No es válido el cupón para esta orden.',
  },
  UNKNOWN_CODE: {
    code: 0,
    label: 'code',
    message: 'unknown; Código desconocido',
    ui_message: 'Código Inválido.',
  },
  CHARACTERES_CODE: {
    code: 0,
    label: 'code',
    message: 'Asegúrese de que este campo no tenga más de 11 caracteres.',
    ui_message: 'Verifica el número de caracteres',
  },
};
export const PAYMENT_METHOD_DEFAULT: PaymentMethodType = {
  valid: false,
  name: '',
  slug: null,
  payment_method_id: null,
  payment_card_token: null,
  is_saving_card: false,
  is_paying_with_saved_card: false,
  was_paid_with_saved_card: false,
  required_invoice: false,
  origin_frontend: null,
};
export const PAYMENT_METHOD_MERCADO_PAGO: PaymentMethodType = {
  ...PAYMENT_METHOD_DEFAULT,
  slug: PAYMENT_METHODS.MERCADO_PAGO.slug,
  name: PAYMENT_METHODS.MERCADO_PAGO.name,
  //otros
};
export const PAYMENT_METHOD_STRIPE: PaymentMethodType = {
  ...PAYMENT_METHOD_DEFAULT,
  slug: PAYMENT_METHODS.STRIPE.slug,
  name: PAYMENT_METHODS.STRIPE.name,
};
export const PAYMENT_METHOD_PAYPAL: PaymentMethodType = {
  ...PAYMENT_METHOD_DEFAULT,
  valid: true,
  slug: PAYMENT_METHODS.PAYPAL.slug,
  name: PAYMENT_METHODS.PAYPAL.name,
};
export const PAYMENT_METHOD_OXXO: PaymentMethodType = {
  ...PAYMENT_METHOD_DEFAULT,
  valid: true,
  slug: PAYMENT_METHODS.OXXO.slug,
  name: PAYMENT_METHODS.OXXO.name,
};
export const PAYMENT_METHOD_BANK_TRANSFER: PaymentMethodType = {
  ...PAYMENT_METHOD_DEFAULT,
  valid: true,
  slug: PAYMENT_METHODS.BANK_TRANSFER.slug,
  name: PAYMENT_METHODS.BANK_TRANSFER.name,
};
export const PAYMENT_METHOD_CASH: PaymentMethodType = {
  ...PAYMENT_METHOD_DEFAULT,
  valid: true,
  slug: PAYMENT_METHODS.CASH.slug,
  name: PAYMENT_METHODS.CASH.name,
};
export const PAYMENT_METHOD_SAVED_CARD: PaymentMethodType = {
  ...PAYMENT_METHOD_DEFAULT,
  slug: PAYMENT_METHODS.SAVED_CARD.slug,
  name: PAYMENT_METHODS.SAVED_CARD.name,
};

export const cleanupObject = (targetObject = {}, index = 0): {} => {
  const objectKeys = Object.keys(targetObject);
  let cleanObject = {};

  //
  if (typeof targetObject === 'object' && objectKeys.length) {
    objectKeys.map((key) => {
      if (targetObject[key]) {
        if (
          typeof targetObject[key] === 'object' &&
          Object.keys(targetObject[key]).length
        ) {
          const test = cleanupObject(targetObject[key], index + 1);
          cleanObject[key] = test;
          return test;
        }
        cleanObject[key] = targetObject[key];
      }
    });
  }
  return cleanObject;
};

/**
 * ShoppingCartManager | Class
 */
export class ShoppingCartManager {
  _clientData: ?ClientDataType;
  _shippingAddress: ?ShippingAddressType;
  _paymentMethod: ?PaymentMethodType;
  _coupon: ?CouponType;
  _products: Array<CartProductType>;
  _addressesList: Array<ShippingAddressType>;
  _giftReceiverData: ?{};

  constructor(
    cart: Array<CartProductType> = [],
    shippingMethods = [],
    shippingSchedules = [],
  ) {
    this._clientData = {};
    this._coupon = COUPON_MODEL;
    this._shippingAddress = null;
    this._paymentMethod = PAYMENT_METHOD_DEFAULT;
    this._products = cart;
    this._addressesList = [];

    this._init = false;
    this._isGift = false;
    this._giftReceiverData = null;
    this._selectedShippingAddress = {};
    this._billingAddress = {};
    this._selectedBillingAddress = {};
    this._requiresInvoice = false;
    this._orders = [];
    this._orderGroups = [];
    this._shippingMethods = shippingMethods;
    this._deliverySchedules = shippingSchedules;
    this._paymentMethods = {
      [PAYMENT_METHOD_OXXO.slug]: PAYMENT_METHOD_OXXO,
      [PAYMENT_METHOD_STRIPE.slug]: PAYMENT_METHOD_STRIPE,
      [PAYMENT_METHOD_PAYPAL.slug]: PAYMENT_METHOD_PAYPAL,
      [PAYMENT_METHOD_MERCADO_PAGO.slug]: PAYMENT_METHOD_MERCADO_PAGO,
      [PAYMENT_METHOD_BANK_TRANSFER.slug]: PAYMENT_METHOD_BANK_TRANSFER,
      [PAYMENT_METHOD_CASH.slug]: PAYMENT_METHOD_CASH,
      [PAYMENT_METHOD_SAVED_CARD.slug]: PAYMENT_METHOD_SAVED_CARD,
    };
    this._selectedPaymentMethod = undefined;
    this._selectedCreditCard = undefined;

    //
    this.updateCart(this._products);
    this._init = true; // Position this at the end of constructor!.
  }

  static SHOPPING_CART_MANAGER_ERRORS = {
    INVALID_SHIPPING_ADDRESS: 'INVALID_SHIPPING_ADDRESS',
  };

  hasValidOrder = (): ?{ [string]: string } => {
    const errors = {};

    if (this.hasClientDataErrors()) {
      errors.clientData = this.hasClientDataErrors();
    }
    if (this.hasGiftErrors()) {
      errors.giftDetails = this.hasGiftErrors();
    }
    if (this.hasShippingAddressErrors()) {
      errors.shippingAddress = this.hasShippingAddressErrors();
    }
    if (this.hasPaymentMethodErrors()) {
      errors.shippingAddress = this.hasPaymentMethodErrors();
    }
    if (this.hasOrderErrors()) {
      errors.orders = this.hasOrderErrors();
    }

    //
    return !Object.keys(errors).length;
  };
  validateOrder = (): ?{ [string]: string } => {
    const errors = {};
    let hasErrors = false;

    if (this.hasClientDataErrors()) {
      hasErrors = true;
      errors.clientData = this.hasClientDataErrors();
    }
    if (this.hasGiftErrors()) {
      hasErrors = true;
      errors.giftDetails = this.hasGiftErrors();
    }
    if (this.hasShippingAddressErrors()) {
      hasErrors = true;
      errors.shippingAddress = this.hasShippingAddressErrors();
    }
    if (this.hasPaymentMethodErrors()) {
      hasErrors = true;
      errors.paymentMethod = this.hasPaymentMethodErrors();
    }
    if (this.hasOrderErrors()) {
      hasErrors = true;
      errors.orders = this.hasOrderErrors();
    }

    //
    return hasErrors ? (errors ? errors : {}) : {};
  };

  /**-----------------------------------
   * CLIENT DATA
   -----------------------------------*/
  hasClientDataErrors = (): ?{} => {
    let clientDataErrors: {
      error: ?string,
      name: ?string,
      lastName: ?string,
      telephone: ?string,
      email: ?string,
    } = {
      error: null,
      name: null,
      lastName: null,
      telephone: null,
      email: null,
    };
    let hasErrors: boolean = false;
    const clientDetails = this.getClientData();

    // Required fields validation.
    if (!clientDetails) {
      hasErrors = true;
      clientDataErrors.error = 'Client data missing';
    } else {
      // if (!clientDetails.name) {
      //   hasErrors = true;
      //   clientDataErrors.name = 'Client name missing';
      // }
      // if (!clientDetails.lastName) {
      //   hasErrors = true;
      //   clientDataErrors.lastName = 'Client last name missing';
      // }

      //FIX: telephone
      if (!clientDetails.telephone) {
        //console.log('Client phone missing', clientDetails);
        //If the address has a phone
        if (this.getShippingAddress()) {
          try {
            this._clientData.telephone = this.getShippingAddress().phone;
          } catch (e) {}
        } else {
          hasErrors = true;
          clientDataErrors.telephone = 'Client phone missing';
        }
      }

      if (!clientDetails.email) {
        hasErrors = true;
        clientDataErrors.email = 'Client email missing';
      }
    }

    //
    // return !hasErrors ? null : this.cleanupObject(clientDataErrors);
    return !hasErrors ? null : cleanupObject(clientDataErrors);
  };
  setClientData(
    clientData: {
      first_name: ?string,
      last_name: ?string,
      email: ?string,
      mobile: ?string,
    } = {
      first_name: null,
      last_name: null,
      email: null,
      mobile: null,
    },
  ): ?ClientDataType {
    // Search for empty params.
    if (!clientData) {
      return {};
    }
    // console.log('clientData', clientData);
    // console.log('getShippingAddress', this.getShippingAddress());

    // Construct data.
    const data = {
      name: clientData.first_name,
      lastName: clientData.last_name,
      email: clientData.email,
      telephone: clientData.mobile,
    };
    //FIX: Telephone

    this._clientData = data;

    // Validate client information
    const clientErrors = false; //this.hasClientDataErrors();
    if (!clientErrors) {
      return this._clientData;
    }

    // If data has errors, reset and throw an error
    this._clientData = null;
    throw new ClientDataError('ClientDataError', { type: clientErrors });
  }
  getClientData = function (): ?ClientDataType {
    return this._clientData;
  };

  /***** REFACTOR THIS METHODS */
  getAddresses = function () {
    return this._addressesList;
  };
  setAddresses = (
    addresses: Array<ShippingAddressType> = [],
  ): Array<ShippingAddressType> => {
    this._addressesList = addresses;

    // Try to find a default address
    const defaultAddress: ?ShippingAddressType = this._addressesList[0];

    //
    this.setShippingAddress(defaultAddress);
    return this._addressesList;
  };

  /**-----------------------------------
   * SHIPPING ADDRESS
   -----------------------------------*/
  /***** REFACTOR THIS METHOD */
  verifySippingAddress = (address) => {
    /*
    FIX: SENTRY
    withWarning is not defined

    
    return new Promise((resolve, reject) => {
      const reason = new Error('Address has an invalid format.');
      //const ModalWithWarning = withWarning(AddressManager);

      this.props.openModalBox(() => (
        <ModalWithWarning
          showErrorsOnInit={true}
          initialValues={address}
          onSubmit={(a) => {
            this.props.updateAddress(a).then((r) => {
              this.props.closeModalBox();
              setTimeout(() => resolve(a), 500);
            });
          }}
          onCancel={() => reject(reason)}
        />
      ));
    });
    */
    //
    // return new Promise((resolve, reject) => {
    //   const reason = new Error('Address has an invalid format.');
    //   const ModalWithWarning = withWarning(AddressManager);
    //   this.props.openModalBox(() => (
    //     <ModalWithWarning
    //       showErrorsOnInit={true}
    //       initialValues={address}
    //       onSubmit={(a) => {
    //         this.props.updateAddress(a).then((r) => {
    //           this.props.closeModalBox();
    //           setTimeout(() => resolve(a), 500);
    //         });
    //       }}
    //       onCancel={() => reject(reason)}
    //     />
    //   ));
    // });
  };
  static validateAddress = (
    address: ?ShippingAddressType,
  ): ?{ [string]: string } => {
    let hasErrors = false;
    const errors = {};

    //
    if (!address) {
      hasErrors = true;
      errors.error = 'Dirección de envío obligatoria.';
    } else {
      if (!address.zip_code || address.zip_code.length !== 5) {
        hasErrors = true;
        errors.zip_code = 'Código postal inválido.';
      }
      if (!address.num_ext || !address.num_ext.length) {
        hasErrors = true;
        errors.num_ext = 'Número exterior obligatorio.';
      }
      if (address.street_address) {
        if (address.street_address.length < 2) {
          hasErrors = true;
          errors.street_address = 'Nombre de la calle obligatorio.';
        }
      }
      if (address.neighborhood.length < 2) {
        hasErrors = true;
        errors.neighborhood = 'Nombre de la colonia obligatorio.';
      }
      if (address.city.length <= 2) {
        hasErrors = true;
        errors.city = 'Estado o delegación obligatorios.';
      }
      if (address.state.length < 2) {
        hasErrors = true;
        errors.state = 'Nombre del estado obligatorio.';
      }
      if (address.phone.length < 8) {
        hasErrors = true;
        errors.phone = 'Teléfono de la persona quien recibe obligatorio.';
      }
      if (address.client_name.length < 4) {
        hasErrors = true;
        errors.client_name = 'Nombre de la persona que recibe obligatorio.';
      }
    }

    //
    return hasErrors ? errors : null;
  };
  hasShippingAddressErrors = (): ?{ [string]: string } => {
    const errors = ShoppingCartManager.validateAddress(this._shippingAddress);
    return !errors ? null : errors;
  };
  setShippingAddress = (
    address: ?ShippingAddressType,
  ): ?ShippingAddressType => {
    try {
      if (!address) {
        this._shippingAddress = null;
        return this._shippingAddress;
      }
      let formattedAddress = address;

      //
      this._shippingAddress = formattedAddress;

      //
      const addressErrors = this.hasShippingAddressErrors();
      if (!addressErrors) {
        return this._shippingAddress;
      }

      //

      this._shippingAddress = null;
      throw new ShoppingCartError('ShoppingCartError.ShippingAddressError', {
        details: addressErrors,
      });
    } catch (e) {}

    // Try to set address explicitly passed on parameters.
    /*this._shippingAddress = this._addressesList.find((address) => {
      // Set selected address.
      if (newAddress && newAddress.uuid === address.uuid) {
        return newAddress;
      }
    });

    // If there was no predefined address,
    // try to gett current selected shipping address.
    if (!this._shippingAddress) {
      this._shippingAddress = this._addressesList.find((address) => {
        // Set selected address.
        if (
          this._selectedShippingAddress &&
          this._selectedShippingAddress === address.uuid
        ) {
          return address;
        }
      });
    }

    // If selected address was not found.
    // Attempt to find a default option.
    if (!this._shippingAddress) {
      this._shippingAddress = this._addressesList.find(
        (address, index, list) => {
          // Ssearch for default address
          if (address.default_address) {
            return address.default_address;

            // If the address is neither, the 'selected' or the 'default' one,
            // choose the last address of the list.
          } else if (index === list.length - 1 && !address.default_address) {
            return true;
          }

          //
          return false;
        },
      );
    }

    //
    this._selectedShippingAddress = this._shippingAddress.uuid;
    return this._shippingAddress;*/
  };
  getShippingAddress = function (): ?ShippingAddressType {
    return this._shippingAddress;
  };

  /**-----------------------------------
   * PAYMENT METHODS
   -----------------------------------*/
  hasPaymentMethodErrors = (): ?{
    [string]: any,
  } => {
    let paymentMethodErrors: {
      error: ?string,
      valid: ?string,
      payment_method_id: ?string,
      payment_provider: {
        name: ?string,
        payment_card_token: ?string,
      },
    } = {
      error: null,
      valid: null,
      payment_method_id: null,
      payment_provider: {
        name: null,
        payment_card_token: null,
      },
    };
    let hasErrors: boolean = false;
    const paymentDetails = this.getPaymentMethod();

    // Required fields validation.
    if (!paymentDetails) {
      hasErrors = true;
      paymentMethodErrors.error = 'Método de pago obligatorio';
    } else {
      // Some payment methods (like cards and paypal), requires a previous validation.
      // This flag must be set depending on each payment method flow.
      // To avoid passing invalid orders to backend, this property es mandatory
      // in order to pass shopping cart form validation.
      if (paymentDetails.valid === undefined) {
        hasErrors = true;
        paymentMethodErrors.valid = 'Por favor, valida tu un método de pago.';
      }

      // We need a payment_method_id.
      // This are defined manually or by provided by 3rd party payment providers.
      // Some example values are: visa | master | amex | oxxo | paypal
      if (paymentDetails.payment_method_id === undefined) {
        hasErrors = true;
        paymentMethodErrors.payment_method_id =
          'Por favor, indica el id de proveedor de pago.';
      }

      // slug is mandatory.
      // This is the payment id used on our database.
      // Note: Frontend and Backend handle different payment flows based on this attribute.
      if (!paymentDetails.slug) {
        hasErrors = true;
        paymentMethodErrors.payment_provider.name =
          'Por favor, especifica un método de pago.';

        // Validate payment method based on payment slug.
      } else {
        // TODO: Podemos verificar que el valor slug se válido,
        // si lo comparamos contra el listado de métodos de pago.
        // ...logic here...
        // END TODO.

        // Each one has different properies, so we must handle every case individually.
        switch (paymentDetails.slug) {
          // CREDIT CARD RULES
          case PAYMENT_METHOD_STRIPE.slug:
          case PAYMENT_METHOD_MERCADO_PAGO.slug:
          case PAYMENT_METHOD_SAVED_CARD.slug: {
            if (!paymentDetails.payment_card_token) {
              hasErrors = true;
              paymentMethodErrors.payment_provider.payment_card_token =
                'No se encontró el token de la tarjeta.';
            }
            break;
          }

          // NON DIGITAL PAYMENTS
          case PAYMENT_METHOD_OXXO.slug:
          case PAYMENT_METHOD_CASH.slug:
          case PAYMENT_METHOD_BANK_TRANSFER.slug: {
            if (!paymentDetails.valid) {
              hasErrors = true;
              paymentMethodErrors.valid =
                'Este método de pago debe ser declarado explicitamente como válido.';
            }
            break;
          }
        }
      }
    }

    //
    return !hasErrors ? null : cleanupObject(paymentMethodErrors);
  };
  setPaymentMethod = (paymentMethod: PaymentMethodType): ?PaymentMethodType => {
    //
    this._paymentMethod = paymentMethod;
    const errors = this.hasPaymentMethodErrors();
    if (!errors) {
      return this._paymentMethod;
    }

    //
    this._paymentMethod = null;
    throw new ShoppingCartError('ShoppingCartError.PaymentMethodError', {
      type: errors,
    });
  };
  getPaymentMethod = function (): ?PaymentMethodType {
    return this._paymentMethod;
  };

  //
  getPaymentMethods = function () {
    return this._paymentMethods;
  };
  setPaymentMethods = (paymentMethods) => {
    this._paymentMethods = paymentMethods;
  };
  getSelectedCreditCard = function () {
    return this._selectedCreditCard;
  };
  setSelectedCreditCard = (cardID: string) => {
    this._selectedCreditCard = cardID;
    return this._selectedCreditCard;
  };
  getRequiresInvoice = function () {
    return this._requiresInvoice;
  };
  setRequiresInvoice = (value: boolean = false) => {
    this._requiresInvoice = value;
    return this._requiresInvoice;
  };

  /**-----------------------------------
   * COUPON SETTINGS
   -----------------------------------*/
  hasCouponErrors = (): ?{} => {
    const errors: { code: ?string } = { code: null };
    let hasErrors: boolean = false;
    const coupon = this.getCoupon();

    if (coupon) {
      if (!coupon.code) {
        hasErrors = true;
        errors.code = 'Código de cupón obligatorio.';
      }
    }

    return hasErrors ? errors : null;
  };
  setCoupon = (couponDetails: CouponType): ?CouponType => {
    if (
      !couponDetails ||
      !typeof couponDetails ||
      !Object.keys(couponDetails).length
    ) {
      return null;
    }

    this._coupon = couponDetails;
    const errors = this.hasCouponErrors();
    if (!errors) {
      return this._coupon;
    }

    //
    this._coupon = null;
    throw new ShoppingCartError('ShoppingCartError.CouponError', {
      types: errors,
    });
  };
  getCoupon = (): ?CouponType => {
    return this._coupon;
  };

  /**-----------------------------------
   * CART LEVEL METHODS
   -----------------------------------*/
  updateCart(products: Array<CartProductType> = []): Array<{}> {
    this._products = this.formatCartProducts(products);
    this._orders = this.setProductsByStore(this._products);
    this._orderGroups = this.setOrders(this._orders);
    return this._orderGroups;
  }
  resetCart() {
    this._products = this.formatCartProducts([]);
    this._orders = this.setProductsByStore(this._products);
    this._orderGroups = this.setOrders(this._orders);
  }
  getCart() {
    return this._orderGroups;
  }
  updateProduct(id, product) {}

  /* PRODUCTS */
  formatCartProducts = (
    products: Array<CartProductType> = [],
  ): Array<CartProductType> => {
    return products.map((product) => {
      const { product: productDetails, ...productCartProperties } = product;

      // Verify if this product is made on demand
      const productMadeOnDemand =
        productDetails.physical_properties.is_available.value === 2;

      // Validate remaining prouct stock.
      let remainingStock = productDetails.quantity;
      // Product attributes may affect stock and price,
      if (productCartProperties.attribute) {
        // if the current product is NOT made on demand,
        // we should use 'general product properties' stock.
        if (!productMadeOnDemand) {
          remainingStock = productCartProperties.attribute.stock
            ? productCartProperties.attribute.stock
            : productDetails.quantity;
        }
      }

      // Format dates
      const minimumShippingDate = Moment(
        productDetails.physical_properties.minimum_shipping_date,
        'YYYY-MM-DD',
        true,
      ).toDate();
      const maximumShippingDate = Moment(minimumShippingDate)
        .add(2, 'M')
        .toDate();

      // Return product details
      return {
        product: productDetails,
        ...productCartProperties,
        remainingStock,
        productMadeOnDemand,
        slug: productDetails.slug,
        attribute: productCartProperties.attribute,
        quantity: productCartProperties.quantity, //|| 1,
        note: productCartProperties.note || '',
        price: productCartProperties.unit_price_without_discount,
        maximum_shipping_date: maximumShippingDate,
        minimum_shipping_date: minimumShippingDate,
        physical_properties: {
          ...productCartProperties.physical_properties,
          shipping_schedule: formatShippingSchedules([
            productCartProperties.physical_properties.shipping_schedule,
          ])[0],
          shipping_date: Moment(
            productCartProperties.physical_properties.shipping_date,
          ).toDate(),
        },
        work_schedules: formatWorkSchedules(
          productDetails.store.work_schedules,
        ),
      };
    });
  };
  getCartProducts = (): Array<CartProductType> => {
    return this._products;
  };

  /* STORES */
  getProductsByStore = function () {
    return this._orders;
  };
  setProductsByStore = (formattedProducts) => {
    return formattedProducts.reduce((a, b) => {
      let productArray = a;
      // Create a new empty cart product model
      const newProduct = {
        ...b,
        note: b.note,
        quantity: b.quantity,
      };

      // Try to find current product´s store (newProduct) on stores list (a)
      const storeIndex = productArray.findIndex(
        (item) => item.store.slug === b.product.store.slug,
      );

      // If store is not found, add it to the list
      if (storeIndex === -1) {
        productArray = [
          ...productArray,
          {
            //shipping_method: shippingMethod,
            store: {
              ...b.product.store,
              work_schedules: formatWorkSchedules(
                b.product.store.work_schedules,
              ),
            },
            products: [newProduct],
          },
        ];

        // if store is found, add current product (newProduct) to product list
      } else {
        const product = {
          ...productArray[storeIndex],
          products: [...productArray[storeIndex].products, newProduct],
        };
        productArray = [
          ...productArray.slice(0, storeIndex),
          product,
          ...productArray.slice(storeIndex + 1),
        ];
      }

      return productArray;
    }, []);
  };

  /**-----------------------------------
   * DELIVERY SCHEDULES
   -----------------------------------*/
  getDeliverySchedules = function () {
    return this._deliverySchedules;
  };
  setDeliverySchedules = (schedules) => {
    this._deliverySchedules = schedules;
  };
  getShippingMethods = function () {
    return this._shippingMethods;
  };
  setShippingMethods = (shippingMethods) => {
    this._shippingMethods = shippingMethods;
  };
  getOrderShippingMethod = function (orderIndex) {
    return this._orderGroups[orderIndex].physical_properties
      .selected_shipping_method;
  };
  setOrderShippingMethod = (orderIndex, newShippingMethod) => {
    this._orderGroups = this._orderGroups.map((order, i) => {
      if (i === orderIndex) {
        return {
          ...order,
          physical_properties: {
            ...order.physical_properties,
            shipping_price: newShippingMethod.price,
            shipping_date: newShippingMethod.delivery_date,
            selected_shipping_method: newShippingMethod,
          },
        };
      }
      return order;
    });
  };

  getShippingPrice = () => {
    const coupon = this._coupon;
    const orders = this.getOrders();
    const ordersShippingPrice = orders.reduce(
      (a, item) => a + parseFloat(item.physical_properties.shipping_price, 10),
      0,
    );

    let freeShippingCoupon = false;
    if (
      coupon.valid &&
      (coupon.type === COUPON_TYPES.FREE_SHIPPING ||
        coupon.type === COUPON_TYPES.FREE_SHIPPING_ALL ||
        coupon.type === COUPON_TYPES.FREE_SHIPPING_UNIQUE ||
        coupon.type === COUPON_TYPES.FREE_SHIPPING_STORE ||
        coupon.type === COUPON_TYPES.FREE_SHIPPING_GENERAL_STORE ||
        coupon.type === COUPON_TYPES.FREE_SHIPPING_AND_PERCENTUAL)
    ) {
      freeShippingCoupon = true;
    }

    return (
      parseFloat(ordersShippingPrice.toFixed(2), 10) *
      (freeShippingCoupon ? 0 : 1)
    );
  };

  /** */
  getBillingAddress = function () {
    return this._billingAddress;
  };
  setBillingAddress = (newAddress = null) => {
    this._billingAddress = this._addressesList.find((address) => {
      // Set selected address.
      if (newAddress && newAddress.uuid === address.uuid) {
        return newAddress;
      }
    });

    // If selected address was not found.
    // Attempt to find a default option.
    if (!this._billingAddress) {
      this._billingAddress = this._addressesList.find(
        (address, index, list) => {
          // Ssearch for default address
          if (address.default_address) {
            return address.default_address;

            // If the address is neither, the 'selected' or the 'default' one,
            // choose the last address of the list.
          } else if (index === list.length - 1 && !address.default_address) {
            return true;
          }

          //
          return false;
        },
      );
    }

    //
    this._selectedBillingAddress = this._billingAddress.uuid;
    return this._billingAddress;
  };

  /**-----------------------------------
   * GIFT SETTINGS
   -----------------------------------*/
  hasGiftErrors = (): ?{} => {
    const errors: { code: ?string } = { code: null };
    let hasErrors: boolean = false;
    const giftDetails = this.getGiftReceiverData();

    if (giftDetails) {
      if (!giftDetails.name) {
        hasErrors = true;
        errors.name = 'Nombre de quien recibe el regalo obligatorio.';
      }
      if (!giftDetails.phone) {
        hasErrors = true;
        errors.phone = 'Teléfono de quien recibe el regalo obligatorio.';
      }
    }

    return hasErrors ? errors : null;
  };
  getGiftReceiverData = function () {
    return this._giftReceiverData;
  };
  setGiftReceiverData(giftReceiverData: ?{}) {
    if (!giftReceiverData) {
      this._giftReceiverData = null;
    } else {
      this._giftReceiverData = giftReceiverData;
    }

    //
    return this._giftReceiverData;
  }

  requiresShipping = function () {
    // Verify if any shipping method requires an address.
    return this.getShippingMethods().find((sm) => {
      if (
        sm.slug === SHIPPING_METHODS.EXPRESS.slug ||
        sm.slug === SHIPPING_METHODS.EXPRESS_BIKE.slug ||
        sm.slug === SHIPPING_METHODS.EXPRESS_CAR.slug ||
        sm.slug === SHIPPING_METHODS.STANDARD.slug
      ) {
        return true;
      }
      return false;
    });
  };

  /**
   * completeOrder()
   * @params <object> order | Information to process an order group for backend.
   * @param <object> orderGroup
   */
  completeOrder = (): Promise<Object> => {
    const client = 'app-marketplace';
    const email = this.getClientData().email;
    const coupon = this.getCoupon();
    const orders = this.getOrders();
    const giftReceiver = this.getGiftReceiverData();
    const paymentMethod = this.getPaymentMethod();
    const shippingAddress = this.getShippingAddress();
    const requiresInvoice = false;

    const orderValidationPromise = new Promise((resolve): any => {
      // Construct outter order object for Backend
      const orderGroup: OrdeGroupType = {
        client,
        email,
        orders: orders.reduce((o, order) => {
          if (!order.products.length) {
            return [...o];
          }

          //
          return [
            ...o,
            {
              ...order,
              store: order.store.slug,
              products: order.products.map((product) => {
                const p = {
                  ...product,
                  product: product.slug,
                };
                if (product.attribute) {
                  p.attribute = product.attribute.uuid;
                }
                return p;
              }),
              physical_properties: {
                shipping_date: Moment(
                  order.physical_properties.shipping_date,
                ).format('YYYY-MM-DD'),
                shipping_method:
                  order.physical_properties.selected_shipping_method.slug,
                shipping_schedule:
                  order.physical_properties.shipping_schedule.id,
              },
            },
          ];
        }, []),
        payment_provider: paymentMethod.slug,
        required_invoice: requiresInvoice,
        coupon: coupon.code,
        shipping_address: {},
        billing_address: {},
        telephone: '',
        name: '',
        receiver_name: null,
        receiver_telephone: null,
        receiver_note: null,
      };

      // Verify if shipping method requires an address.
      if (this.requiresShipping()) {
        // Validate shipping address.
        if (ShoppingCartManager.validateAddress(shippingAddress) === null) {
          orderGroup.shipping_address = {
            street: shippingAddress.street_address,
            neighborhood: shippingAddress.neighborhood,
            city: shippingAddress.city,
            state: shippingAddress.state,
            zip_code: shippingAddress.zip_code,
            num_ext: shippingAddress.num_ext,
            num_int: shippingAddress.num_int,
            latitude: shippingAddress.latitude,
            longitude: shippingAddress.longitude,
            telephone: shippingAddress.telephone,
          };

          orderGroup.billing_address = orderGroup.shipping_address;
          orderGroup.telephone = shippingAddress.phone;
          orderGroup.name = shippingAddress.client_name; //;

          // Gift information
          let receiverName: null | string = null;
          let receiverNote: null | string = null;
          let receiverTelephone: null | string = null;
          if (giftReceiver) {
            try {
              receiverName = giftReceiver.name;
              receiverTelephone = giftReceiver.phone;
              receiverNote = giftReceiver?.note;
            } catch (error) {
              throw new ShoppingCartError(
                'Gift orders must contain a valid name and phone number ' /*{
                type:
                  ShoppingCartManager.SHOPPING_CART_MANAGER_ERRORS
                    .INVALID_SHIPPING_ADDRESS,
              }*/,
              );
            }
          }
          orderGroup.receiver_name = receiverName;
          orderGroup.receiver_telephone = receiverTelephone;
          orderGroup.receiver_note = receiverNote;

          //
          resolve(orderGroup);
        } else {
          throw new ShoppingCartError('Invalid Shipping Address', {
            type:
              ShoppingCartManager.SHOPPING_CART_MANAGER_ERRORS
                .INVALID_SHIPPING_ADDRESS,
          });
        }
      }

      //
      resolve(orderGroup);
    });

    // Return handler
    return orderValidationPromise;
  };

  /** */
  getUnavailableProducts = () => {
    let ordersByShipping = this._orderGroups;
    ordersByShipping = ordersByShipping.reduce(
      (list, p) => [...list, ...p.unavailableProducts],
      [],
    );
    return ordersByShipping;
  };

  getUnavailableOrders = () => {
    let ordersByShipping = this._orderGroups;
    ordersByShipping = ordersByShipping.reduce((unavailableOrders, order) => {
      const { unavailableProducts, ...restOrder } = order;
      if (unavailableProducts.length) {
        return [
          { ...restOrder, products: unavailableProducts },
          ...unavailableOrders,
        ];
      }
      return unavailableOrders;
    }, []);
    return ordersByShipping;
  };

  getCartNumberOfShippings = function () {
    return this.getOrders().length;
  };
  getCartProductsCountTotal = function () {
    // This should ONLY return 'available' products.
    return this._products.reduce((c, p) => c + p.quantity, 0);
  };
  getCartProductsCount = function () {
    // This should ONLY return 'available' products.
    return this.getOrders().reduce((a, order) => {
      const productsInOrder = order.products.reduce(
        (c, p) => c + p.quantity,
        0,
      );
      return a + productsInOrder;
    }, 0);
  };
  getCartProductsTotal = () => {
    const orders = this.getOrders();
    const products = orders
      ? orders.reduce((p, o) => [...p, ...o.products], [])
      : [];
    const productsTotal = products
      .map(
        (item) =>
          parseFloat(item.unit_price, 10) * parseFloat(item.quantity, 10),
      )
      .reduce((a, b) => a + b, 0);
    return productsTotal ? parseFloat(productsTotal.toFixed(2), 10) : 0;
  };

  getCartProductsTotalWithDiscount = () => {
    const orders = this.getOrders();
    const products = orders
      ? orders.reduce((p, o) => [...p, ...o.products], [])
      : [];
    const productsTotal = products
      .map(
        (item) =>
          parseFloat(item.unit_price, 10) * parseFloat(item.quantity, 10),
      )
      .reduce((a, b) => a + b, 0);
    return productsTotal ? parseFloat(productsTotal.toFixed(2), 10) : 0;
  };

  getCartGrandTotal = () => {
    const couponDetails = this._coupon;
    let grandTotal =
      parseFloat(this.getCartProductsTotal(), 10) +
      parseFloat(this.getShippingPrice(), 10);

    if (couponDetails.valid) {
      const discountAmount = parseFloat(couponDetails.discount, 10);
      switch (couponDetails.type) {
        case COUPON_TYPES.PERCENTUAL:
        case COUPON_TYPES.FREE_SHIPPING_AND_PERCENTUAL:
          grandTotal -= grandTotal * discountAmount;
          break;
        case COUPON_TYPES.QUANTITY:
          grandTotal -= discountAmount;
          if (grandTotal < 0) {
            return 0;
          }
          break;
        default:
      }
    }

    return parseFloat(grandTotal.toFixed(2));
  };

  getCartGrandTotalWithDiscount = () => {
    const couponDetails = this._coupon;
    let grandTotal =
      parseFloat(this.getCartProductsTotalWithDiscount(), 10) +
      parseFloat(this.getShippingPrice(), 10);

    if (couponDetails.valid) {
      const discountAmount = parseFloat(couponDetails.discount, 10);
      switch (couponDetails.type) {
        case COUPON_TYPES.PERCENTUAL:
        case COUPON_TYPES.FREE_SHIPPING_AND_PERCENTUAL:
          grandTotal -= grandTotal * discountAmount;
          break;
        case COUPON_TYPES.QUANTITY:
          grandTotal -= discountAmount;
          if (grandTotal < 0) {
            return 0;
          }
          break;
        default:
      }
    }

    return parseFloat(grandTotal.toFixed(2));
  };

  /** */
  getAvailableOrders = function () {
    let orderGroups = this._orderGroups;
    orderGroups = orderGroups.filter(
      (o) => o.physical_properties.shipping_methods.length,
    );
    orderGroups = orderGroups.filter((o) => o.products.length);
    return orderGroups;
  };
  hasOrderErrors = () => {
    const errors: {
      orders: ?string | Array<{}>,
      products: ?string | Array<{}>,
    } = { orders: null, products: null };
    let hasErrors = false;
    const orders = this.getOrders();

    // Order group must have at least one order(store).
    if (!orders.length) {
      hasErrors = true;
      errors.orders = 'La orden debe contener al menos una tienda.';
    } else {
      // Search for errors inside every order.
      const orderErrors = orders.filter((order) => {
        // Product level validation
        if (!order.products.length) {
          return {
            products: 'La orden requiere al menos un producto. disponible',
          };

          // Search inside every product
        } else {
          const productErrors = order.products.filter((product) => {
            if (product.quantity < 1) {
              return { quantity: 'Producto sin inventario disponible' };
            }
          });
          hasErrors = hasErrors || productErrors.length;
          errors.products = productErrors;
        }

        // physical properties validation
        if (!order.physical_properties) {
          return {
            physical_properties:
              'La orden requiere fechas, horario y método de envío.',
          };
        } else {
          if (!order.physical_properties.shipping_date) {
            return {
              shipping_date: 'Por favor, especifica una fecha de entrega.',
            };
          }
          if (!order.physical_properties.shipping_schedule) {
            return {
              shipping_schedule: 'Por favor, elige un horario de entrega.',
            };
          }
          if (
            !order.physical_properties.shipping_methods ||
            !order.physical_properties.shipping_methods.length
          ) {
            return {
              shipping_methods:
                'Este producto no cuenta con métodos de entrega para esta zona.',
            };
          }
          if (!order.physical_properties.selected_shipping_method) {
            return {
              selected_shipping_method: 'Por favor, elige un método de envío.',
            };
          }
        }
      });

      //
      hasErrors = hasErrors || orderErrors.length;
      errors.orders = orderErrors;
    }

    return hasErrors ? cleanupObject(errors) : null;
  };
  getOrders = (): Array<{}> => {
    return this.getAvailableOrders();
  };
  setOrders = (orders: array) => {
    const cartStores = orders;
    const platformPickupSchedules = this._deliverySchedules;
    const platformShippingMethods = this._shippingMethods;
    const PRODUCT_MODEL = {
      product: {
        physical_properties: {
          shipping: {},
          shipping_date: {},
          shipping_schedule: {
            id: {},
          },
        },
      },
    };

    return cartStores.reduce((orders, store, storeIndex) => {
      // Loop through each store products.
      // This will return an array of stores (with products)
      // based on distinct shipping dates/schedules.
      let ordersByShipping = store.products.reduce(
        (a, product = PRODUCT_MODEL) => {
          let returnArray = a;

          // Store/Order Model.
          const basicStoreModel = {
            ...store,
            physical_properties: {
              shipping_methods: product.product.physical_properties.shipping.map(
                (s) => ({
                  ...s,
                  price: parseInt(s.price, 10),
                }),
              ),
              shipping_date: Moment(
                product.physical_properties.shipping_date,
                'YYYY-MM-DD',
                true,
              ).format('YYYY-MM-DD'),
              shipping_schedule: product.physical_properties.shipping_schedule,
            },
          };

          // Try to find distinct shipping dates
          const foundDateIndex = returnArray.findIndex(
            (p) =>
              p.physical_properties.shipping_date ===
              Moment(product.physical_properties.shipping_date).format(
                'YYYY-MM-DD',
              ),
          );

          // If we found a distinct date, add product to list
          if (foundDateIndex === -1) {
            returnArray = [
              ...returnArray,
              {
                ...basicStoreModel,
                products: [product],
              },
            ];

            // if shipping date already exists...
          } else {
            // Try to find distinct schedules
            const foundScheduleIndex = returnArray.findIndex(
              (d) =>
                d.physical_properties.shipping_schedule.id ===
                product.physical_properties.shipping_schedule.id,
            );

            // If the schedule is not found, add item to list
            if (foundScheduleIndex === -1) {
              returnArray = [
                ...returnArray,
                {
                  ...basicStoreModel,
                  products: [product],
                },
              ];

              // If this products doesn´t have a distinct shipping schedule,
              // only add product to list
            } else {
              returnArray = [
                ...returnArray.slice(0, foundDateIndex),
                {
                  ...returnArray[foundDateIndex],
                  products: [...returnArray[foundDateIndex].products, product],
                },
                ...returnArray.slice(foundDateIndex + 1),
              ];
            }
          }

          // Return list of distinct shippings
          return returnArray;
        },
        [],
      );

      // Make some adjustments to the order
      let ordersFromForm;
      /*let ordersFromForm = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName)(
                state,
                'orders'
            );
            ordersFromForm =
                ordersFromForm && ordersFromForm.length ? ordersFromForm : undefined;*/

      ordersByShipping = ordersByShipping.map((o, index) => {
        // Slice products list based on shipping availability (per product).
        let availableProducts = o.products.filter(
          (p) =>
            p.product.physical_properties.shipping.filter((s) => s.is_available)
              .length,
        );
        let unavailableProducts = o.products.filter(
          (p) => availableProducts.findIndex((a) => a.slug === p.slug) === -1,
        );

        // Get unique shipping methods for the whole order
        let availableShippingMethods = availableProducts.reduce((a, p) => {
          const shippingMethod = p.product.physical_properties.shipping.filter(
            (s) => !a.find((x) => s.slug === x.slug && s.is_available),
          );
          return [...a, ...shippingMethod];
        }, []);

        // Filter Shipping Methods.
        // If the order has at least one product that needs to be sent
        // by car (and car shipping is available), the whole order should be sent by car.
        const hasCarShipping = availableShippingMethods.find(
          (s) => s.slug === SHIPPING_METHODS.EXPRESS_CAR.slug && s.is_available,
        );

        // If NOT all products in this order have 'bike shipping',
        // disable this shipping method
        if (hasCarShipping && availableProducts.length > 1) {
          const allProductsHaveBikeShipping = availableProducts.reduce(
            (a, p) => {
              return !a
                ? false
                : p.product.physical_properties.shipping.find(
                    (sm) =>
                      sm.slug === SHIPPING_METHODS.EXPRESS_BIKE.slug &&
                      sm.is_available,
                  );
            },
            true,
          );

          if (!allProductsHaveBikeShipping) {
            availableShippingMethods = availableShippingMethods.map((a) =>
              a.slug === SHIPPING_METHODS.EXPRESS_BIKE.slug
                ? {
                    ...a,
                    is_available: false,
                  }
                : a,
            );
          }
        }

        // Verify Product Availablility Again
        // Slice products list based on shipping availability (per product).
        availableProducts = availableProducts.filter(
          (p) =>
            p.product.physical_properties.shipping.filter(
              (s) =>
                availableShippingMethods.filter((sm) => sm.slug === s.slug)
                  .length,
            ).length,
        );
        unavailableProducts = o.products.filter(
          (p) => availableProducts.findIndex((a) => a.slug === p.slug) === -1,
        );

        // Get Selected shipping method.
        let selectedShippingMethod;
        let userSelectedShippingMethod;
        // (try to retrieve selected shipping methos from redux form)
        if (
          this._orderGroups[storeIndex] &&
          this._init &&
          availableShippingMethods.length
        ) {
          selectedShippingMethod = this._orderGroups[storeIndex]
            .physical_properties.selected_shipping_method;

          // selectedShippingMethod = availableShippingMethods.find(sm =>
          //     ordersFromForm[storeIndex]
          //     ? sm.slug === userSelectedShippingMethod.slug &&
          //         userSelectedShippingMethod.is_available
          //     : false
          // );
        }
        selectedShippingMethod =
          selectedShippingMethod ||
          availableShippingMethods.find((sm) => sm.is_available);

        // Define default order shipping price
        // First get the highest shipping price as default.
        let shippingPrice = 0;
        //shippingPrice = platformShippingMethods.reduce((a, b) => Math.max(a, b.price), 0);

        // If the order has products, get the price of the first element
        // * All products on the same order have the same shipping price.
        if (selectedShippingMethod && availableProducts.length) {
          shippingPrice = availableProducts.reduce((sm, p) => {
            if (!sm) {
              return availableShippingMethods.find(
                (s) => s.slug === selectedShippingMethod.slug,
              );
            }
            return sm;
          }, null);
          shippingPrice = shippingPrice ? shippingPrice.price : 0;
        }

        // If the selected method does not exist in the product, adjust it to the one.
        // Se añade una validación previa antes del fix, por si algún producto no tiene metodo de envio
        // y viene como undefined, no haga tronar a todo el carrito, es solo un if-else, para validar esa parte.
        if (selectedShippingMethod === undefined) {
        } else {
          let validateSM = selectedShippingMethod.name;
          let verifySM = false;
          let availableSM = false;
          if (this._orderGroups[storeIndex] !== undefined) {
            this._orderGroups[storeIndex].products.map((obj) => {
              obj.product.physical_properties.shipping.map((sm_name) => {
                if (validateSM === sm_name.name) {
                  verifySM = true;
                }
                if (!availableSM) {
                  availableSM = sm_name;
                }
              });
            });
            if (!verifySM) {
              selectedShippingMethod = availableSM;
            }
          }
        }

        let minimumShippingDate = Moment(); //
        const maximumShippingDate = Moment(minimumShippingDate).add(2, 'M');
        if (
          ordersFromForm &&
          ordersFromForm[storeIndex] &&
          ordersFromForm[storeIndex].minimum_shipping_date
        ) {
          minimumShippingDate = Moment(
            ordersFromForm[storeIndex].minimum_shipping_date,
          );
        }

        // Get shipping date depending on the selected shipping method.
        let shippingDate = Moment();
        if (selectedShippingMethod && availableProducts.length) {
          shippingDate = Moment(
            availableProducts[0].physical_properties.shipping_date,
          );
          minimumShippingDate = Moment(
            selectedShippingMethod.minimum_delivery_date,
          );
        }
        if (shippingDate.isBefore(minimumShippingDate)) {
          shippingDate = minimumShippingDate;
        }

        // Sort remaining available shipping methods by priority.
        const shippingMethodsOrderSequence = [
          SHIPPING_METHODS.EXPRESS_CAR,
          SHIPPING_METHODS.EXPRESS_BIKE,
          SHIPPING_METHODS.STANDARD,
          SHIPPING_METHODS.PICKUP_POINT,
        ];
        availableShippingMethods = availableShippingMethods.sort((a, b) => {
          const aKey = shippingMethodsOrderSequence.findIndex(
            (sm) => sm.slug === a.slug,
          );
          const bKey = shippingMethodsOrderSequence.findIndex(
            (sm) => sm.slug === b.slug,
          );
          return aKey - bKey;
        });

        // Get unique shipping methods for the whole order
        const shippingSchedules = formatShippingSchedules(
          platformPickupSchedules.filter((shippingSchedule) => {
            return isValidShippingScheduleTime(
              shippingSchedule.collection_start,
              DayJs().format('HH:mm:ss'),
              Moment(shippingDate).format('YYYY-MM-DD'),
            );
          }),
        );

        // Compose and return Order.
        return {
          ...o,
          products: availableProducts,
          unavailableProducts,
          physical_properties: {
            ...o.physical_properties,
            shipping_date: Moment(shippingDate).format('YYYY-MM-DD'),
            shipping_methods: availableShippingMethods,
            shipping_schedules: shippingSchedules,
            selected_shipping_method: selectedShippingMethod,
            shipping_price: shippingPrice,
            minimum_shipping_date: minimumShippingDate,
            maximum_shipping_date: maximumShippingDate,
          },
        };
      });

      // Return list of orders by store and shipping
      return [...orders, ...ordersByShipping];
    }, []);
  };
}
