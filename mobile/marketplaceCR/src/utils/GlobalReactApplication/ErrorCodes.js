const Error = {
  L001: {
    type: 'landing',
    context: 'No está disponible el recurso o se movió de ubicación',
    message: 'No pudimos cargar esta sección',
    internal: undefined,
  },
  L002: {
    type: 'landing',
    context: 'No se pudo conectar al servidor o currio un error inesperado',
    message: 'Por ahora no podemos mostrarte esta sección, vuelve pronto.',
    internal: undefined,
  },
};

export { Error };
