import { Error } from './ErrorCodes';
import { useRoute } from '@react-navigation/native';
declare var globalThis: any;

const allsections = [];
const allsectionsprops = [];

const rootScreens = [
  '_HomeScreen',
  '_CategoriesList',
  '_ShoppingCart',
  '_Orders',
  '_Profile',
];
// Screens disable tracking
const noTracking = ['SelectPayment', 'Checkout'];

const getResource = (url = '') => {
  try {
    return fetch(url)
      .then((response) => response.json())
      .then((data) => {
        return {
          error: false,
          data: data,
        };
      })
      .catch((e) => {
        Error.L001.internal = e;
        return {
          error: Error.L001,
          data: [],
        };
      });
  } catch (e) {
    Error.L002.internal = e;
    return {
      error: Error.L002,
      data: [],
    };
  }
};

const SendError = (
  screen = '',
  func: '',
  desc: '',
  email: '',
  globaldata = {},
) => {
  fetch(
    'https://auth.canastarosa.com/sentry',

    {
      method: 'POST', // or 'PUT'
      body: JSON.stringify({
        screen,
        func,
        desc,
        email,
        globaldata,
      }), // data can be `string` or {object}!
      headers: {
        'Content-Type': 'application/json',
      },
    },
  )
    .then((response) => response.json())
    .then((data) => {
      console.log('Send sentry.');
    })
    .catch((e) => {
      console.log('Error send sentry.');
    });
};

const configurationLandings = (getURL) => {
  return getResource(getURL);
};

const configurationFilters = (getURL) => {
  return getResource(getURL);
};

const sanitizeSlug = (slug) => {
  return slug.replace(/[^a-zA-Z0-9]/gi, '');
};

const GlobalReactApplication = async () => {
  if (!Boolean(globalThis.ReactApplication)) {
    globalThis.ReactApplication = {
      //Section of screen.
      // back: screen back, to: open screen, params: new open screen, navigation: object navigation.
      cacheScreen: {
        exampleScreen: {
          name: '',
          filter: '',
          stockelements: [],
        },
      },
      ActiveScreen: function (
        screen = '',
        params = {},
        nav = {},
        restart = false,
      ) {
        try {
          const globalBackScreen = globalThis.ReactApplication.BackScreen;

          if (!noTracking.includes(screen)) {
            if (screen === 'Checkout___payment') {
              screen = 'Checkout';
            }
            //Dynamic screen for Product
            if (screen === 'ProductDetail') {
              const customScreen = `${screen}___${sanitizeSlug(
                params.itemProductSlug,
              )}`;
              globalBackScreen.allsectionsprops[customScreen] = params;
              globalBackScreen.allsections.push(customScreen);
              //Dynamic screen for Catalog
            } else if (screen === 'CatalogScreen') {
              const customScreen = `${screen}___${sanitizeSlug(
                params.categoryName,
              )}`;
              globalBackScreen.allsectionsprops[customScreen] = params;
              globalBackScreen.allsections.push(customScreen);
            } else if (screen === 'SearchResult') {
              const customScreen = `${screen}___${sanitizeSlug(
                params.itemSearch,
              )}`;
              globalBackScreen.allsectionsprops[customScreen] = params;
              globalBackScreen.allsections.push(customScreen);
            } else {
              //Normal screen
              globalBackScreen.allsections.push(screen);
              globalBackScreen.allsectionsprops[screen] = params;
            }
            allsections.push(screen);

            //Check if root screens
            if (rootScreens.includes(screen)) {
              const globalBackScreen = globalThis.ReactApplication.BackScreen;
              globalBackScreen.allsections = [screen];
              globalBackScreen.allsectionsprops = [];
            }

            if (restart) {
              globalBackScreen.allsectionsprops = [];
              globalBackScreen.allsections = ['HomeScreen'];
            }

            console.log('All actions ---- > ', globalBackScreen.allsections);
            // console.log(
            //   'All sections props ---- > ',
            //   globalBackScreen.allsectionsprops,
            // );
          }

          nav.navigate(screen, params);
        } catch (e) {
          console.log('navigation error.', e);
        }
      },
      BackScreen: {
        allsections: [],
        allsectionsprops: [],
        disable: false,
        reset: function () {
          const globalBackScreen = globalThis.ReactApplication.BackScreen;
          globalBackScreen.allsectionsprops = [];
          globalBackScreen.allsections = ['HomeScreen'];
          globalBackScreen = false;
        },
      },
      SentryCatch: function (
        screen = '',
        func: '',
        desc: '',
        email: '',
        globaldata = {},
      ) {
        SendError(screen, func, desc, email, globaldata);
      },
      ButtonMenu: {
        tab1: true,
        tab2: false,
        tab3: false,
      },
      HomeScreen: {
        functions: {},
        store: {
          landings: await configurationLandings(
            'https://canastarosa.s3.us-east-2.amazonaws.com/temp/static-listings/home_landings.json?cache=1',
          ),
          landingJAL: await configurationLandings(
            'https://canastarosa.s3.us-east-2.amazonaws.com/temp/static-listings/stores_ja.json',
          ),
          landingNL: await configurationLandings(
            'https://canastarosa.s3.us-east-2.amazonaws.com/temp/static-listings/stores_nl.json',
          ),
        },
      },
      LoginScreen: {},
      RecoveryScreen: {},
      NewAccountScreen: {},
      ProductScreen: {},
      StoreScreen: {},
      ProfileScreen: {},
      utils: {
        sanitizeslug: sanitizeSlug,
        filter: await configurationFilters(
          'https://canastarosa-app.s3.us-east-2.amazonaws.com/json/filterwords.json',
        ),
        checkFilter: function (text) {
          const filter = globalThis.ReactApplication.utils.filter.data.filter;
          console.log('----------> data', filter);
          for (let word of filter) {
            if (text.toLowerCase().includes(word.toLowerCase())) {
              return true;
            }
          }
        },
      },
    };
    console.log('globalThis.ReactApplication', globalThis.ReactApplication);
  }
};

/*

let filter = [
  "oms",
  "coronav",
  "covid",
  "cuarentena",
  "pandemia",
  "contagio",
  "enfermo",
  "fda",
  "oms",
  "antivirus",
  "esporas",
  "virus",
  "bacteria",
  "ah1",
  "influenza",
  "vacuna",
  "nfs",
  "influenza",
];

let example =
  "playera hermosa OMS 19 en serigrafia";

function validateFilter() {
  for (let word of filter) {
    if ((example.toLowerCase()).includes(word.toLowerCase())) {
      return true;
    }
  }
}
// block if true... 
console.log(validateFilter());


*/

export { GlobalReactApplication };
