import React, { Component } from 'react';
import { View, Animated, Easing, StyleSheet } from 'react-native';
import spinnerPNG from './../../../src/images/spinners/spinner8.png';
import spinnerPNG2 from './../../../src/images/spinners/spinner8-light.png';

export class Spinner extends Component {
  constructor() {
    super();
    this.spinValue = new Animated.Value(0);
  }
  componentDidMount() {
    this.spin();
  }
  spin() {
    this.spinValue.setValue(0);
    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => this.spin());
  }

  render() {
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    return (
      <View style={styles.container}>
        <Animated.Image
          style={{
            ...styles.spinnerSize,
            transform: [{ rotate: spin }],
          }}
          source={this.props.light ? spinnerPNG2 : spinnerPNG}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  spinnerSize: {
    width: 60,
    height: 60,
  },
});
