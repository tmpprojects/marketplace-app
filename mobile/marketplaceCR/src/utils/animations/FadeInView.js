import React, { useState, useEffect } from 'react';
import { Animated } from 'react-native';

export const FadeInView = (props) => {
  const [fadeAnim] = useState(new Animated.Value(0));
  // const [fadeAnimScale] = useState(new Animated.Value(0.8));

  useEffect(() => {
    const setDelay = () => {
      const delayItem = 200;
      const newValue = props.num * delayItem;
      return newValue;
    };
    /* Configuration */
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 1000,
      //easing: Easing.easeIn,
      delay: setDelay(),
      useNativeDriver: true,
    }).start();

    // Animated.timing(fadeAnimScale, {
    //   toValue: 1,
    //   easing: Easing.easeIn,
    //   delay: setDelay(),
    //   duration: 1000,
    // }).start();
  }, [fadeAnim, props.num]);

  return (
    <Animated.View
      style={{
        opacity: fadeAnim,
        // transform: [{scaleX: fadeAnimScale}, {scaleY: fadeAnimScale}],
      }}
    >
      {props.children}
    </Animated.View>
  );
};
