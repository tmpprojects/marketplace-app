import AsyncStorage from '@react-native-community/async-storage';

// Save the session
export const _setData = async (session) => {
  try {
    if (session !== '' && session !== null && session !== undefined) {
      await AsyncStorage.setItem('session', JSON.stringify(session));
    }
  } catch (error) {}
};

// Save the custom data
export const _customSetData = async (name, value) => {
  try {
    await AsyncStorage.setItem(name, JSON.stringify(value));

    if (name === 'email') {
      await AsyncStorage.setItem('emailcache', JSON.stringify(value));
    } else if (name === 'password') {
      await AsyncStorage.setItem('passwordcache', JSON.stringify(value));
    }
  } catch (error) {}
};

// Save the custom data
export const _customGetData = async (name) => {
  try {
    let customValue = await AsyncStorage.getItem(name);
    return JSON.parse(customValue);
  } catch (error) {
    return false;
  }
};

// Get the session
export const _getData = async () => {
  try {
    let session = await AsyncStorage.getItem('session');
    let email = await AsyncStorage.getItem('email');
    let password = await AsyncStorage.getItem('password');
    let asyncversion = await AsyncStorage.getItem('asyncversion');

    let emailCache = await AsyncStorage.getItem('emailcache');
    let passwordCache = await AsyncStorage.getItem('passwordcache');

    return {
      session: JSON.parse(session),
      email: JSON.parse(email),
      password: JSON.parse(password),
      emailcache: JSON.parse(emailCache),
      passwordcache: JSON.parse(passwordCache),
      asyncversion: JSON.parse(asyncversion),
    };
  } catch (error) {}
};
// Get the payment method saved
export const _getPaymentMethodSaved = async () => {
  try {
    let paymentMethodSaved = await AsyncStorage.getItem('paymentMethodSaved');
    let last4 = await await AsyncStorage.getItem('last4');
    let pmCard = await await AsyncStorage.getItem('pmCard');

    return {
      paymentMethodSaved: JSON.parse(paymentMethodSaved),
      last4: JSON.parse(last4),
      pmCard: JSON.parse(pmCard),
    };
  } catch (error) {}
};
// Clear all data
export const _clearData = async () => {
  try {
    await AsyncStorage.removeItem('session');
    await AsyncStorage.removeItem('email');
    await AsyncStorage.removeItem('password');
    await AsyncStorage.removeItem('paymentMethodSaved');
    await AsyncStorage.removeItem('last4');
    await AsyncStorage.removeItem('pmCard');
    //await AsyncStorage.removeItem('asyncversion');
    await AsyncStorage.removeItem('isStored');
    // await AsyncStorage.clear();
  } catch (error) {}
};

export const _clearVersion = async () => {
  try {
    await AsyncStorage.removeItem('asyncversion');
  } catch (error) {}
};
