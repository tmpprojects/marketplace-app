import axios from 'axios';
//import config from 'react-native-config';
import { config } from '../../env';

export const axiosInstance = axios.create({
  baseURL: config.HOST_BACKEND_LOCAL,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});
