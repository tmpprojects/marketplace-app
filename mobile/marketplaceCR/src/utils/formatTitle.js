export const formatTitle = (string) =>
  string.charAt(0).toUpperCase().concat(string.substring(1, string.length));
