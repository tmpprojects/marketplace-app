import { StackActions, NavigationActions } from 'react-navigation';

export const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'MainMenu' })],
});

export const resetActionCollection = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })],
});
