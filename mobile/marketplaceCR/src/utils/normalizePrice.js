export const formatNumberToPrice = (number) => {
  if (number) {
    const currentPrice = number.toString().split('.');

    const cents =
      currentPrice.length > 1 ? currentPrice[1].replace(/[^\d]/g, '') : '00';
    const intPrice = currentPrice[0].replace(/[^\d]/g, '');
    const formattedPrice = intPrice.replace(/./g, (c, i, a) =>
      i && (a.length - i) % 3 === 0 ? ',' + c : c,
    );

    return `\$${formattedPrice}.${cents}`;
  } else {
    return '';
  }
};

export const formatPriceToNumber = (price: number): number => {
  if (price) {
    const totalPrice = price.toString().split('.');
    const cents =
      totalPrice.length > 1 ? totalPrice[1].replace(/[^\d]/g, '') : '00';
    const intPrice = totalPrice[0].replace(/[^\d]/g, '');
    return parseInt(`${intPrice}.${cents}`, 10);
  } else {
    return 0;
  }
};
