import { StyleSheet, Dimensions } from 'react-native';
import colors from '@canastarosa/ds-theme/colors/js/variables';
const { height: HEIGHT, width: WIDTH } = Dimensions.get('window');

export const stylesLoadInit = StyleSheet.create({
  fixedElement: {
    flex: 1,
    width: WIDTH,
    height: 2000,
    backgroundColor: 'white',
    opacity: 0.9,
    position: 'absolute',
    zIndex: 999,
  },
  gifSpinner: {
    width: 100,
    height: 100,
  },
  loadingText: {
    //...typography.txtExtraBold,
    textAlign: 'center',
    color: colors.colorGray400,
    fontSize: 16,
    marginTop: -120,
  },
  version: {
    //...typography.txtExtraBold,
    textAlign: 'center',
    color: colors.colorGray400,
    fontSize: 16,
    position: 'absolute',
    bottom: 30,
    width: '100%',
  },
  loader: {
    height: HEIGHT - 350,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconLoader: {
    width: 100,
    height: 100,
  },
  wait: {
    color: colors.colorDark100,
  },
});
