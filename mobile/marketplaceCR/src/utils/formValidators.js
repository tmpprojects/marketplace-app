export const required = (value) => (value ? undefined : 'Campo obligatorio');

export const minLength = (min) => (value) =>
  value && value.length < min ? `Mínimo ${min} caracteres.` : undefined;

export const maxLength = (max) => (value) =>
  value && value.length > max ? `Máximo ${max} o menos caracteres.` : undefined;

export const number = (value) =>
  value && isNaN(Number(value)) ? 'Debe ser un número.' : undefined;

export const email = (value) =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Email inválido'
    : undefined;

export const storeReferrerCode = (value) =>
  value && /[^A-Za-z0-9-]/gi.test(value) ? 'Caracteres no válidos.' : undefined;

export const password_strong = (value) =>
  value && /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(value)
    ? 'Por favor elige una contraseña más segura.'
    : undefined;

export const password = (value) =>
  value && !/^[0-9a-zA-Z._-]{8,15}$/.test(value)
    ? 'Elige entre 8 y 15 caracteres alfanuméricos.'
    : undefined;

export const password_confirmation = (value, allValues) =>
  value && value !== allValues.password
    ? 'Las contraseñas no coinciden.'
    : undefined;

export const cleanPassword = (value, previousValue) => {
  return value.trim();
};

export const name = (value) =>
  value && value.length < 3 ? 'Escribe tu nombre.' : undefined;

export const lastName = (value) =>
  value && value.length < 3 ? 'Escribe tus apellidos.' : undefined;

export const numIntExt = (value) =>
  value && !/^[0-9a-zA-Z._-]/gi.test(value)
    ? 'El número tiene caracteres no válidos.'
    : undefined;

export const normalizeToLowerCaseAndTrim = (value, previousValue) => {
  //Sentry fix
  //undefined is not an object (evaluating 'n.toLowerCase')
  if (Boolean(value)) {
    return value.toLowerCase().trim();
  } else {
    return '';
  }
};
