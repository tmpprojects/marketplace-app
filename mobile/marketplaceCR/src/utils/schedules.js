import { WEEK_OBJECT, formatHour24To12, addHourPeriodSuffix } from './date';

export const formatWorkSchedules = (schedules) => {
  const orderedSchedules = schedules.map((schedule) => ({
    value: schedule.week_day.value,
    name: WEEK_OBJECT.find((a) => a.value === schedule.week_day.value).name,
    open: {
      name: formatHour24To12(schedule.start.split(':').slice(0, -1).join(':')),
      value: schedule.start.split(':').slice(0, -1).join(':'),
    },
    close: {
      name: formatHour24To12(schedule.end.split(':').slice(0, -1).join(':')),
      value: schedule.end.split(':').slice(0, -1).join(':'),
    },
  }));

  return orderedSchedules.sort((a, b) => {
    if (a.value > b.value) {
      return 1;
    } else if (a.value < b.value) {
      return -1;
    }
    return 0;
  });
};

export const formatShippingSchedules = (schedules) => {
  const classNames = ['morning', 'evening', 'afternoon'];

  // Order schedules by hour
  const orderedSchedules = schedules.sort((a, b) => {
    if (a > b) {
      return 1;
    } else if (a < b) {
      return -1;
    }
    return 0;
  });

  // return formatted schedules
  return orderedSchedules.map((schedule, index) => {
    // Format time display.
    const pickupStart = addHourPeriodSuffix(
      schedule.collection_start.split(':').slice(0, -1).join(':'),
    );
    const pickupEnd = addHourPeriodSuffix(
      schedule.collection_end.split(':').slice(0, -1).join(':'),
    );
    const deliveryStart = addHourPeriodSuffix(
      schedule.delivery_start.split(':').slice(0, -1).join(':'),
    );
    const deliveryEnd = addHourPeriodSuffix(
      schedule.delivery_end.split(':').slice(0, -1).join(':'),
    );

    // Return schedules list.
    return {
      id: schedule.id,
      value: schedule.id,
      className: classNames[index],
      schedules: {
        limit_to_order: schedule.limit_to_order,
        pickupStart,
        pickupEnd,
        pickup: `${schedule.name} ${pickupStart} - ${pickupEnd}`,
        deliveryStart,
        deliveryEnd,
        delivery: `${schedule.name} ${deliveryStart} - ${deliveryEnd}`,
      },
    };
  });
};
