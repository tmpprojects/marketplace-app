import { StyleSheet, Platform } from 'react-native';

const isAndroid = Platform.OS === 'android';

export const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    marginTop: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    marginTop: 16,
  },
  message: {
    marginTop: 30,
  },
  image: {
    width: 320,
    height: 190,
    resizeMode: 'cover',
  },
});
