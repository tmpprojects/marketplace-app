import React from 'react';
import { View, Image } from 'react-native';

import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';

import search from '../../images/illustration/doll_search.png';
import { styles } from './EmptyShoppingCartStyles';

const EmptyShoppingCart = (props) => {
  return (
    <View style={styles.container}>
      <Image source={search} style={styles.image} />
      <View style={styles.message}>
        <CRText
          variant={typography.subtitle3}
          color={colors.colorDark300}
          align="center"
        >
          No hay nada en tu carrito.
        </CRText>
        <CRText
          variant={typography.paragraph}
          color={colors.colorDark300}
          align="center"
        >
          ¡Anímate a descubrir algo increíble!
        </CRText>
      </View>
    </View>
  );
};

export default EmptyShoppingCart;
