import React, { PureComponent } from 'react';
import * as Sentry from '@sentry/react-native';
import {
  View,
  VirtualizedList,
  Text,
  TouchableOpacity,
  Platform,
  Image,
} from 'react-native';

import { styles } from './VirtualListContainerStyles';
import { Spinner } from '../../../utils/animations/Spinner';
import DoubleProducts from './DoubleProducts';
import { CRStoreCard } from '../../CRCard';
import notFound from './../../../images/illustration/doll_search.png';

import { CRIcon } from '../../CRIcon';
import { colors } from '@canastarosa/ds-theme/colors';
import CRCategoryCard from '../../../components/CRCard/CRCategoryCard/CRCategoryCard';
import { CRTitle } from '../../../components/CRTitle';
import CardReview from './../../Profile/Reviews/CardReview';
declare var globalThis: any;
export default class VirtualListContainer extends PureComponent {
  constructor(props) {
    super(props);
    this._virtualizedList = React.createRef();
    this.state = {
      doubleColumProducts: [],
      onScrollAction: this.props.onScroll,
    };
    this.historyContent;
  }
  componentDidMount() {
    this.generateDoubleColum(this.props.config.products);
    console.log('this._virtualizedList', this);
    //this._virtualizedList.scrollToIndex({animated: true,index:10});
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      if (nextProps.config.products) {
        this.generateDoubleColum(nextProps.config.products);
      }
    }
  }

  handleScroll = (event) => {
    if (this.props.config.showMore) {
      this.props.config.showMore(this.props.config.type);
    }
  };

  setScore = (num, max = 5) => {
    try {
      let score = [];
      if (num > 5) {
        num = 5;
      }
      for (let i = 0; i < num; i++) {
        score.push(
          <CRIcon name="star" size={16} color={colors.colorYellow300} />,
        );
      }
      let remaining = max - Number(num);
      if (remaining) {
        for (let i = 0; i < remaining; i++) {
          score.push(
            <CRIcon
              name="star--hollow"
              size={16}
              color={colors.colorYellow300}
            />,
          );
        }
      }

      return score;
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  renderFooter = () => {
    try {
      if (this.props.config.disabledInfinite) {
        return <View style={styles.containerEmpty}></View>;
      }

      if (
        this.props.config.request.count === 0 &&
        this.props.config.request.results.length === 0
      ) {
        return (
          <View style={styles.containerSpinner}>
            <Image source={notFound} style={styles.notFound}></Image>
            <Text style={styles.messageNotfound}>
              ¡Ho!, no encontramos resultados.
            </Text>
          </View>
        );
      } else if (!this.props.disabled) {
        return (
          <View style={styles.containerSpinner}>
            <Spinner style={styles.gifSpinner} light={true} />
          </View>
        );
      } else {
        return (
          <View style={styles.containerSpinner}>
            <Spinner style={styles.gifSpinner} light={true} />
          </View>
        );
      }
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  getItem = (data, index) => {
    return data[index];
  };

  getItemCount = (data) => {
    return data.length;
  };

  generateDoubleColum = (products) => {
    try {
      if (this.props.config.type === 'products') {
        let tempDoubleColumProducts = [];
        let startIndex = 0;
        let endIndex = 2;
        if (products) {
          products.map((item, index) => {
            var newArray = products.slice(startIndex, endIndex);
            startIndex += 2;
            endIndex += 2;
            if (!newArray.length) {
              return false;
            } else {
              tempDoubleColumProducts.push(newArray);
            }
          });
          this.setState({
            doubleColumProducts: tempDoubleColumProducts,
          });
        }
      } else if (this.props.config.type === 'stores') {
        this.setState({
          doubleColumProducts: this.props.config.stores,
        });
      } else if (
        this.props.config.type === 'reviewsCompleted' ||
        this.props.config.type === 'reviewsPending'
      ) {
        this.setState({
          doubleColumProducts: this.props.config.products,
        });
      }
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  validateJson = (item, values) => {
    try {
      let structure = item;
      let valid = true;

      for (let verify of values) {
        if (structure[verify]) {
          structure = structure[verify];
        } else {
          valid = false;
        }
      }

      if (valid) {
        return structure;
      } else {
        return '';
      }
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  render() {
    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 150;
    const SPACER_SIZE_ANDROID = 150;

    try {
      return (
        <View style={styles.productListContainer}>
          <VirtualizedList
            ref={this._virtualizedList}
            //data={products}
            onScroll={this.props.onScrollAction}
            bounces={false}
            data={this.state.doubleColumProducts}
            //initialNumToRender={12}
            initialNumToRender={2}
            style={{
              paddingTop: this.props.paddingTop ? this.props.paddingTop : 0,
            }}
            renderItem={({ item, index }) => {
              try {
                if (this.props.config.type === 'stores') {
                  return (
                    <CRStoreCard
                      key={index}
                      store={item.name}
                      size="full"
                      thumbImg={item.cover.small}
                      storeLogo={item.photo.small}
                      slogan={item.slogan}
                      rating={
                        item.mean_store_score > 0 &&
                        item.mean_store_score.toFixed(1)
                      }
                      marginBottom={10}
                      onPress={() => {
                        //this.props.config.navigation.navigate('StoreScreen', {
                        // itemStore: item.slug,
                        //});

                        globalThis.ReactApplication.ActiveScreen(
                          'StoreScreen',
                          {
                            itemStore: item.slug,
                          },
                          this.props.config.navigation,
                        );
                      }}
                    />
                  );
                } else if (this.props.config.type === 'products') {
                  return (
                    <TouchableOpacity key={index}>
                      <DoubleProducts
                        navigation={this.props.config.navigation}
                        items={item}
                      ></DoubleProducts>
                    </TouchableOpacity>
                  );
                } else if (this.props.config.type === 'reviewsCompleted') {
                  return (
                    <CardReview review={item} type={'completed'}></CardReview>
                  );
                } else if (this.props.config.type === 'reviewsPending') {
                  return (
                    <CardReview
                      review={item}
                      type={'pending'}
                      reloadReviews={this.props.reloadReviews}
                    ></CardReview>
                  );
                }
              } catch (e) {
                Sentry.captureException(e);

                return null;
              }
              return null;
            }}
            keyExtractor={(item, index) => String(index)}
            getItemCount={this.getItemCount}
            getItem={this.getItem}
            ListFooterComponent={this.renderFooter}
            onEndReached={this.handleScroll}
            onEndReachedThreshold={0.6}
          />
          {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
          {isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} />}
        </View>
      );
    } catch (e) {
      Sentry.captureException(e);
    }
  }
}
