import { StyleSheet, Dimensions } from 'react-native';
import { typography, buttons } from '../../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

const { height: HEIGHT } = Dimensions.get('window');

export const styles = StyleSheet.create({
  productListContainer: {
    width: '100%',
    height: HEIGHT - 50,
    //backgroundColor: 'red',
  },
  notFound: {
    height: 170,
    width: 280,
    marginTop: 30,
  },
  priceAndRanking: {
    height: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 10,
  },
  flatListProducts: {
    backgroundColor: colors.colorGreen100,
    paddingBottom: 30,
  },
  message: {
    ...typography.txtLight,
    color: colors.colorGray400,
    textAlign: 'center',
    marginTop: 80,
  },
  messageNotfound: {
    ...typography.txtLight,
    color: colors.colorGray400,
    textAlign: 'center',
    marginTop: 20,
    fontSize: 20,
  },
  productsList: {
    paddingHorizontal: 15,
  },
  productContainer: {
    width: 'auto',
    height: 'auto',
    marginHorizontal: 10,
    marginVertical: 15,
    backgroundColor: colors.colorWhite,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  productData: {
    justifyContent: 'space-between',
    flex: 1,
  },
  productName: {
    marginBottom: 0,
  },
  productStore: {
    marginTop: 0,
  },
  imgWrapper: { width: '35%', borderRadius: 5 },
  product: {
    ...typography.txtLight,
    textTransform: 'capitalize',
    fontSize: 16,
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  price: {
    ...typography.txtSemiBold,
    color: colors.colorDark300,
    fontSize: 16,
    marginTop: 10,
  },
  rating: {
    ...typography.txtRegular,
    color: colors.colorDark300,
  },
  icon: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
  },
  storeName: {
    ...typography.txtExtraBold,
    fontSize: 16,
    fontWeight: '100',
    color: colors.colorGray400,
    textTransform: 'capitalize',
  },
  subcategoryActive: {
    margin: 10,
    ...buttons.btnInterests,
    backgroundColor: colors.colorMain300,
  },
  subcategoryNameActive: {
    ...typography.txtLight,
    padding: 8,
    textAlign: 'center',
    color: colors.colorWhite,
  },
  gifSpinner: {
    width: 100,
    height: 100,
  },
  containerSpinner: {
    paddingTop: 50,
    alignItems: 'center',
    paddingBottom: 200,
  },
  containerEmpty: {
    paddingTop: 100,
    alignItems: 'center',
    paddingBottom: 100,
  },
});
