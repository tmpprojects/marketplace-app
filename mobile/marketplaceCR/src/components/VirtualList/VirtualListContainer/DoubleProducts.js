import React, { Component } from 'react';
import { useNavigationState } from '@react-navigation/native';

import { View } from 'react-native';

import { styles } from './../../ProductsListStyle';

//DS
import { CRProductCard } from './../../CRCard/CRProductCard';

export default class DoubleProducts extends Component {
  validateJson = (item, values) => {
    let structure = item;
    let valid = true;
    for (let verify of values) {
      if (structure[verify]) {
        structure = structure[verify];
      } else {
        valid = false;
      }
    }

    if (valid) {
      return structure;
    } else {
      return '';
    }
  };
  constructor(props) {
    super(props);
    // const state = useNavigationState(state => state);
    // const routeName = (state.routeNames[state.index]);
  }
  render() {
    return (
      <View style={styles.doubleProducts}>
        {this.props.items &&
          this.props.items.map((item, index) => (
            <View style={styles.doubleProduct} key={index}>
              <CRProductCard
                thumbImg={{ uri: item.photo.small }}
                productTitle={item.name}
                product={this.validateJson(item, ['name'])}
                store={this.validateJson(item, ['store', 'name'])}
                price={item.price}
                shippingMethod={item.physical_properties.shipping_methods}
                //shippingMethod={[]}
                priceWithoutDiscount={item.price_without_discount}
                discount={item.discount}
                rating={
                  item.mean_product_score > 0 &&
                  item.mean_product_score.toFixed(1)
                }
                // all={item}
                // type={this.props.config.type}
                size="half"
                marginBottom={10}
                onPress={async () => {
                  // this.props.navigation.navigate('ProductDetail', {
                  //   itemProductSlug: item.slug,
                  //   itemStoreSlug: item.store.slug,
                  // });

                  globalThis.ReactApplication.ActiveScreen(
                    'ProductDetail',
                    {
                      itemProductSlug: item.slug,
                      itemStoreSlug: item.store.slug,
                    },
                    this.props.navigation,
                  );
                }}
              />
            </View>
          ))}
      </View>
    );
  }
}
