// function trunc(text) {
//     const maxLegth = 55;
//     return text.length > maxLegth ? `${text.substr(0, maxLegth)}...` : text;
//   }

//   const ProductsCard = ({
//     product,
//     store,
//     price,
//     priceDiscount,
//     discount,
//     photo,
//     screen,
//     all,
//     rating,
//     onPress,
//   }) => {

//     return (
//       <CRCard size="full" onPress={onPress}>
//         <View style={styles.productContainer}>
//           <CRThumb source={{ uri: photo }} wrapperStyle={styles.imgWrapper} />
//           <View style={styles.productData}>
//             <View>
//               <CRRowBoth addedStyle={styles.productName}>
//                 <CRText variant={typography.bold} color={colors.colorDark300}>
//                   {trunc(product)}
//                 </CRText>
//                 {/* screen === 'category' ||
//               (screen === 'NO-ID' && (
//               )) */}
//               </CRRowBoth>
//               <CRRowBoth addedStyle={styles.productStore}>
//                 <CRText variant={typography.caption} color={colors.colorDark100}>
//                   {trunc(store)}
//                 </CRText>
//               </CRRowBoth>
//               {discount > 0 ? (
//                 <CRRowBoth>
//                   <CRText
//                     variant={typography.paragraph}
//                     color={colors.colorRed300}
//                   >
//                     Antes:{' '}
//                     <Text
//                       style={{
//                         textDecorationLine: 'line-through',
//                         textDecorationStyle: 'solid',
//                       }}
//                     >
//                       {formatNumberToPrice(priceDiscount)} MXN
//                     </Text>
//                     <Text> {parseInt(discount)}%</Text>
//                   </CRText>
//                 </CRRowBoth>
//               ) : null}
//               <CRRowBoth>
//                 <CRText
//                   variant={typography.paragraph}
//                   color={colors.colorDark100}
//                 >
//                   {formatNumberToPrice(price)}MXN
//                 </CRText>
//               </CRRowBoth>
//             </View>
//             {rating > 0 ? (
//               <View style={styles.priceAndRanking}>
//                 <CRText
//                   variant={typography.paragraph}
//                   color={colors.colorDark100}
//                 >
//                   {rating}
//                   <CRIcon name="star" size={16} color={colors.colorYellow300} />
//                 </CRText>
//               </View>
//             ) : (
//               <Text />
//             )}
//           </View>
//         </View>
//       </CRCard>
//     );
//   };
