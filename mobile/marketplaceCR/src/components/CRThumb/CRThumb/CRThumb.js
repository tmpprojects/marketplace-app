import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { default as style } from './style';
import placeHolderDefault from '../../../images/assets/placeholder.jpg';

class CRThumb extends Component {
  //Default state
  state = {
    finalImg: placeHolderDefault,
    imgHeight: 130,
    resizeMode: 'contain',
    wrapperStyle: {},
    finalplaceHolder: placeHolderDefault,
  };

  constructor(props) {
    super(props);

    this.state = {
      imgHeight: this.props.imgHeight ? this.props.imgHeight : 130,
      resizeMode: this.props.resizeMode ? this.props.resizeMode : 'cover',
      wrapperStyle: this.props.wrapperStyle,
      finalplaceHolder: this.props.placeHolder
        ? this.props.placeHolder
        : placeHolderDefault,
      finalImg: this.props.source
        ? this.props.source
        : this.props.finalplaceHolder,
    };
  }

  loadPlaceHolder() {
    this.setState({ finalImg: this.state.finalplaceHolder });
  }
  render() {
    return (
      <View
        style={[
          style.thumb,
          { height: this.state.imgHeight },
          this.state.wrapperStyle,
        ]}
      >
        <Image
          style={[style.thumbImg, { resizeMode: this.state.resizeMode }]}
          source={this.state.finalImg}
          onError={() => this.loadPlaceHolder()}
        />
      </View>
    );
  }
}

export default CRThumb;
