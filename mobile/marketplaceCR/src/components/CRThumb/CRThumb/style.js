import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  thumb: {
    width: '100%',
    borderTopEndRadius: 3,
    borderTopStartRadius: 3,
    overflow: 'hidden',
    height: 157,
    justifyContent: 'center',
  },
  thumbImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
});
