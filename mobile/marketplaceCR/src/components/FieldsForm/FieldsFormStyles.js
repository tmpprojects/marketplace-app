import { StyleSheet, Dimensions } from 'react-native';
import { buttons } from '../../assets';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

const { width: WIDTH } = Dimensions.get('window');

export const field = StyleSheet.create({
  inputForm: {
    width: WIDTH - 120,
    height: 50,
    paddingLeft: 10,
    borderBottomColor: colors.colorGray300,
    borderTopColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderWidth: 1,
    color: colors.colorDark300,
    marginHorizontal: 25,
    marginVertical: 8,
    ...typography.txtLight,
    fontSize: 16,
    textAlign: 'left',
  },
  textForm: {
    ...typography.txtLight,
    fontSize: 18,
    color: colors.colorDark300,
    width: 300,
    marginBottom: 60,
    textAlign: 'center',
  },
  btnEye: {
    position: 'absolute',
    top: 25,
    right: 37,
  },
  buttonSquare: {
    ...buttons.square,
    // borderRadius: 50 / 2,
    // backgroundColor: colors.colorMain300,
    borderColor: colors.colorMain300,
  },
  buttonSquareDisabled: {
    ...buttons.squareDisabled,
    borderColor: colors.colorGray300,
    // borderRadius: 50 / 2,
    // backgroundColor: colors.colorGray200,
    // borderColor: colors.colorGray200,
  },
  formErrors: {
    marginHorizontal: 25,
  },
});
