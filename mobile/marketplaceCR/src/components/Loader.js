import React, { Component } from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import { Spinner } from './../utils/animations/Spinner';
const { height: HEIGHT } = Dimensions.get('window');

export default class Loader extends Component {
  render() {
    return (
      <View style={styles.loader}>
        <Spinner style={styles.gifSpinner} light={true} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loader: {
    height: HEIGHT - 350,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconLoader: {
    width: 100,
    height: 100,
  },
});
