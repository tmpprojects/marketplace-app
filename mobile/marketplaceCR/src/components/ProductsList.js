import React, { Component } from 'react';
import { CommonActions } from '@react-navigation/native';

import { View, FlatList, Text, Platform } from 'react-native';

import { styles } from './ProductsListStyle';

//DS
import { CRProductCard } from './CRCard/CRProductCard';
import { resetAction } from '../utils/resetActions';
declare var globalThis: any;
export default class ProductsList extends Component {
  handleLoadMore = () => {};

  render() {
    const isIos = Platform.OS === 'ios';
    const SPACER_SIZE = 34;
    const { products, screen } = this.props;
    return (
      <View style={styles.productsList}>
        {products.length === 0 && (
          <Text style={styles.message}>
            Aún no hay productos en esta sección
          </Text>
        )}
        <FlatList
          data={products}
          numColumns={2}
          renderItem={({ item, index }) => (
            <CRProductCard
              thumbImg={{ uri: item.photo.small }}
              productTitle={item.name}
              store={screen === 'category' && item.store.name}
              price={item.price}
              shippingMethod={item.physical_properties.shipping_methods}
              //shippingMethod={[]}
              priceWithoutDiscount={item.price_without_discount}
              discount={item.discount}
              rating={
                item.mean_product_score > 0 &&
                item.mean_product_score.toFixed(1)
              }
              size="half"
              marginBottom={10}
              onPress={() => {
                //this.props.navigation.dispatch(resetAction);
                // this.props.navigation.navigate('ProductDetail', {
                //   itemProductSlug: item.slug,
                //   itemStoreSlug: item.store.slug,
                // });

                globalThis.ReactApplication.ActiveScreen(
                  'ProductDetail',
                  {
                    itemProductSlug: item.slug,
                    itemStoreSlug: item.store.slug,
                  },
                  this.props.navigation,
                );
              }}
            />
          )}
        />
        {isIos && <View style={{ height: SPACER_SIZE }} />}
      </View>
    );
  }
}
