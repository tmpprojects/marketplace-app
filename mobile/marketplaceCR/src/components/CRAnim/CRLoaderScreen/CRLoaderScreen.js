import React from 'react';
import { Text, View, SafeAreaView, Image } from 'react-native';
import { default as style } from './style';
import { Spinner } from '../../../utils/animations/Spinner';
import imgSuccess from '../../../images/illustration/success.png';

const CRLoaderScreen = ({ loaderMessage }) => {
  return (
    <View style={style.loaderScreen}>
      <View style={style.loaderScreenElements}>
        <Image source={imgSuccess} style={style.imageSuccess} />
        <Spinner style={style.loaderSpinner} />
        <Text style={style.loaderMessage}>Espera un momento...</Text>
        <Text>{loaderMessage}</Text>
      </View>
    </View>
  );
};

export default CRLoaderScreen;
