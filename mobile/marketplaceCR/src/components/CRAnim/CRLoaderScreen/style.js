import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  loaderScreen: {
    backgroundColor: colors.colorWhite,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'center',
  },
  loaderScreenElements: {
    height: 400,
    width: '100%',

    display: 'flex',
    alignItems: 'center',
  },
  loaderMessage: {
    fontWeight: 'bold',
  },
  loaderSpinner: {},
  imageSuccess: {
    height: 180,
    width: '90%',
    resizeMode: 'contain',
    marginBottom: 10,
  },
});
