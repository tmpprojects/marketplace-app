import React, { Component } from 'react';
import { Text } from 'react-native';

class CRAnimPayment extends Component {
  state = { pleaseDisplayMe: 0 };

  componentDidMount() {
    setInterval(() => {
      this.setState({ pleaseDisplayMe: this.state.pleaseDisplayMe + 1 });
    }, 50);
  }

  render() {
    return <Text>{this.state.pleaseDisplayMe}</Text>;
  }
}

export default CRAnimPayment;
