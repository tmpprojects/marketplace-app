import React, { Component } from 'react';
import type { Node } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FieldArray, Field } from 'redux-form';
import { View, Alert, TouchableOpacity, Image, Platform } from 'react-native';
import { CheckBox } from 'react-native-elements';
import DayJs from 'dayjs';
import { Calendar, LocaleConfig } from 'react-native-calendars';

import { getDaysBetweenDates, isValidWorkingDay } from '../../utils/date';
import { SHIPPING_METHODS } from '../../redux/constants/app.constants';
import { shoppingCartActions } from '../../redux/actions';
import { formatNumberToPrice } from '../../utils/normalizePrice';
import type { OrderType } from '../../redux/types/OrderTypes';
import CartProduct from './CartProduct';

// Will be added to the DS
import { CRCard } from '../CRCard';
import { CRRowLeft, CRRowBoth, CRRowFull } from '../../components/Layout';
import { calendarTheme } from '../../assets';

// DS
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { layout } from '../../pages/ShoppingCart/layout';
import arrow from '../../images/icon/icon_arrow_right.png';

//Will be added to the ds
import { CRThumb } from '../CRThumb';
import { CRIcon } from '../CRIcon';

declare var globalThis: any;

// Setup calendar
LocaleConfig.locales.es = {
  monthNames: [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ],
  monthNamesShort: [
    'Enr.',
    'Feb.',
    'Mar.',
    'Abr.',
    'May.',
    'Jun.',
    'Jul.',
    'Ago.',
    'Sept.',
    'Oct.',
    'Nov.',
    'Dic.',
  ],
  dayNames: [
    'Domingo',
    'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado',
  ],
  dayNamesShort: ['Dom.', 'Lun.', 'Mar.', 'Mier.', 'Jue.', 'Vie.', 'Sab.'],
  today: 'Hoy',
};
LocaleConfig.defaultLocale = 'es';
let undefinedMethod = false;

type CartOrderProps = {
  fields: { map: Function, get: Function },
  dispatch: Function,
  updateCartProduct: Function,
  closeModalWindow: Function,
  onRenderShippingMethods: Function,
  changeShippingMethod: Function,
  onShippingMethodsPressed: Function,
  onRenderShippingDate: Function,
  showShippingSchedules: Boolean,
  showShippingMethods: Boolean,
};

export class CartOrder extends Component<CartOrderProps> {
  changeShippingSchedule = (order: Object, shippingSchedule: Object): void => {
    this.props.onChangeShippingSchedule(order, shippingSchedule);

    // this.props.change(
    //   `orders[${orderIndex}].physical_properties.shipping_price`,
    //   shippingMethod.price,
    // );

    // Update Shopping Cart Product
    // We need to do this for every product on the order,
    // since the API expects delivery date and schedules per product.
    // * This must change in the future.
    order.products.forEach((product) =>
      this.props.updateCartProduct({
        slug: product.slug,
        product: product.product.slug,
        quantity: product.quantity,
        note: product.note,
        attribute: product.attribute ? product.attribute.uuid : null,
        physical_properties: {
          ...product.physical_properties,
          shipping_date: DayJs(
            product.physical_properties.shipping_date,
          ).format('YYYY-MM-DD'),
          shipping_schedule: shippingSchedule.id,
        },
      }),
    );
  };

  changeShippingMethod = (orderIndex: number, shippingMethod: Object): void => {
    this.props.onChangeShippingMethod(orderIndex, shippingMethod);

    // Sync Shopping Cart
    this.props.fetchShoppingCart();
    globalThis.ShoppingCartManager.setOrderShippingMethod(
      orderIndex,
      shippingMethod,
    );
    this.props.change(
      `orders[${orderIndex}].physical_properties.shipping_price`,
      shippingMethod.price,
    );
    this.props.change(
      `orders[${orderIndex}].physical_properties.shipping_date`,
      shippingMethod.delivery_date,
    );
  };

  changeShippingDate = (
    order: OrderType,
    shippingDate: { dateString: string },
  ): void => {
    this.props.onChangeShippingDate(order, shippingDate);

    // Update Shopping Cart Product
    // We need to do this for every product on the order,
    // since the API expects delivery date and schedules per product.
    // * This must change in the future.
    order.products.forEach((product) =>
      this.props.updateCartProduct({
        slug: product.slug,
        product: product.product.slug,
        quantity: product.quantity,
        note: product.note,
        attribute: product.attribute ? product.attribute.uuid : null,
        physical_properties: {
          ...product.physical_properties,
          shipping_date: DayJs(shippingDate.dateString).format('YYYY-MM-DD'),
          shipping_schedule: product.physical_properties.shipping_schedule.id,
        },
      }),
    );
  };

  showAllShippingMethods(order, index, item, onRenderShippingMethods) {
    const shippingMethods = order.physical_properties.shipping_methods.map(
      (sm) => {
        return (
          <CRRowFull key={sm.slug}>
            <TouchableOpacity onPress={() => null}>
              <CRCard size="full">
                <View style={layout.cardContainerModal}>
                  <Field
                    name={`${item}.physical_properties.selected_shipping_method`}
                    value={JSON.stringify(sm)}
                    onChange={(e) => this.changeShippingMethod(index, e)}
                    component={({ input, ...rest }) => {
                      return (
                        <CheckBox
                          checkedIcon={
                            <View style={layout.selectedCheck}>
                              <CRIcon
                                name={'check'}
                                size={12}
                                color={colors.colorWhite}
                              />
                            </View>
                          }
                          uncheckedIcon={
                            <View style={layout.unselectedCheck}></View>
                          }
                          checked={
                            JSON.stringify(sm) === JSON.stringify(input.value)
                          }
                          onPress={() => input.onChange(sm)}
                        />
                      );
                    }}
                  >
                    {sm.name}
                  </Field>
                  <CRText
                    variant={typography.paragraph}
                    color={colors.colorDark100}
                  >
                    {` ${sm.name} `}
                    <CRText color={colors.colorDark300}>{`${formatNumberToPrice(
                      sm.price,
                    )}MXN`}</CRText>
                  </CRText>
                </View>
              </CRCard>
            </TouchableOpacity>
          </CRRowFull>
        );
      },
    );
    onRenderShippingMethods(shippingMethods);
  }

  render(): Array<Node> {
    const {
      fields,
      dispatch,
      onRenderShippingDate,
      onRenderShippingMethods,
      showShippingSchedules,
      showShippingMethods,
    } = this.props;
    const isIos = Platform.OS === 'ios';
    const SPACER_SIZE_IOS = 180;

    // Format Orders
    return fields.map((item, index): Object => {
      const order = fields.get(index);

      //fix01

      const methodsAvailable = order.physical_properties.shipping_methods;
      const productCount = order.products.reduce((a, b) => a + b.quantity, 0);
      const selectedShippingSchedule =
        order.physical_properties.shipping_schedule;

      let selectedShippingMethod =
        order.physical_properties.selected_shipping_method;

      const selectedShippingMethodIsValid = methodsAvailable.filter(
        (item) => item.name == selectedShippingMethod.name,
      ).length
        ? true
        : false;

      //To detect if there are any previous shipping methods
      //Fix

      if (!selectedShippingMethodIsValid) {
        if (onRenderShippingMethods) {
          if (!undefinedMethod) {
            // this.showAllShippingMethods(
            //   order,
            //   index,
            //   item,
            //   onRenderShippingMethods,
            // );
            // undefinedMethod = true;
          }
        }
      } else {
        undefinedMethod = false;
      }

      let isStandardShipping = true;
      let minimumShippingDate;
      let maximumShippingDate;
      if (selectedShippingMethod) {
        isStandardShipping =
          selectedShippingMethod.slug === SHIPPING_METHODS.STANDARD.slug;

        // Define minimum/maximun shipping dates.
        minimumShippingDate =
          order.physical_properties.selected_shipping_method
            .minimum_delivery_date;
        maximumShippingDate = DayJs(
          order.physical_properties.maximum_shipping_date,
        ).format('YYYY-MM-DD');
      }
      // Render JSX
      return (
        <CRCard
          key={`${order.store.slug}-
            ${order.physical_properties.shipping_date}-
            ${order.physical_properties.shipping_schedule.id}`}
          size="full"
          marginBottom={10}
          attitude="disabled"
        >
          <View style={layout.store}>
            <View style={layout.thumbnail}>
              <CRThumb
                source={{ uri: order.store.photo.small }}
                imgHeight={50}
                resizeMode="contain"
              />
            </View>
            <View style={layout.countProductContainer}>
              <CRText variant={typography.bold}>{order.store.name}</CRText>
              <CRText variant={typography.caption} color={colors.colorDark100}>
                {productCount} Producto{productCount > 1 ? 's' : ''}
              </CRText>
            </View>
          </View>

          <FieldArray
            name={`${item}.products`}
            component={CartProduct}
            dispatch={dispatch}
          />

          {/* MÉTODO DE ENVÍO */}
          {showShippingMethods && (
            <TouchableOpacity
              style={layout.containerItemMenu}
              onPress={() => {
                this.showAllShippingMethods(
                  order,
                  index,
                  item,
                  onRenderShippingMethods,
                );
              }}
            >
              <CRRowLeft>
                <CRText
                  variant={typography.caption}
                  color={colors.colorDark100}
                >
                  Tipo de Envio:{' '}
                </CRText>
                {selectedShippingMethod.price === 0 ? (
                  <CRText
                    variant={typography.paragraph}
                    color={colors.colorDark300}
                  >
                    {`${selectedShippingMethod.name} `} 0.00 MXN
                  </CRText>
                ) : (
                  <CRText
                    variant={typography.paragraph}
                    color={colors.colorDark300}
                  >
                    {`${selectedShippingMethod.name}  ${formatNumberToPrice(
                      selectedShippingMethod.price,
                    )}`}{' '}
                    MXN
                  </CRText>
                )}
              </CRRowLeft>
              <View style={layout.iconItemArrow}>
                <CRIcon
                  name={'angle--right'}
                  size={16}
                  color={colors.colorDark100}
                />
              </View>
            </TouchableOpacity>
          )}
          {/* END: MÉTODO DE ENVÍO */}

          {/* SHIPPING DATE/SCHEDULES */}
          {showShippingMethods && (
            <TouchableOpacity
              style={layout.containerItemMenu}
              onPress={() => {
                // Get a list of days between a date interval.
                const datesRange = getDaysBetweenDates(
                  DayJs().format('YYYY-MM-DD'),
                  DayJs(maximumShippingDate).format('YYYY-MM-DD'),
                );

                // Define the status of each day between selected dates range.
                // This is used to mark and control dates interaction on calendar.
                const markedDates = datesRange.reduce(
                  (datesAccumulator, currentDate) => {
                    const enableDate = isValidWorkingDay(currentDate, {
                      work_schedules: order.store.work_schedules,
                      physical_properties: {
                        //...field.physical_properties,
                        minimum_shipping_date: minimumShippingDate,
                        maximum_shipping_date: maximumShippingDate,
                      },
                      vacations_ranges: order.store.vacations_ranges,
                    });

                    return {
                      ...datesAccumulator,
                      [currentDate]: {
                        disabled: !enableDate,
                        disableTouchEvent: !enableDate,
                      },
                    };
                  },
                  {},
                );

                const shippingSchedules = (
                  <View>
                    <CRRowBoth>
                      <Calendar
                        theme={calendarTheme}
                        hideExtraDays
                        minDate={minimumShippingDate}
                        maxDate={maximumShippingDate}
                        onDayPress={(value) =>
                          this.changeShippingDate(order, value)
                        }
                        markedDates={{
                          ...markedDates,
                          [order.physical_properties.shipping_date]: {
                            selected: true,
                            disableTouchEvent: true,
                          },
                        }}
                      />
                    </CRRowBoth>

                    {order.physical_properties.shipping_schedules.map(
                      (schedule) => {
                        return (
                          <CRRowFull>
                            {showShippingSchedules && (
                              <CRCard key={schedule.id} size="full">
                                <View style={layout.cardContainerModal}>
                                  <Field
                                    name={`${item}.physical_properties.shipping_schedule`}
                                    onChange={(e) =>
                                      this.changeShippingSchedule(order, e)
                                    }
                                    component={({ input, ...rest }) => (
                                      <CheckBox
                                        checkedIcon={
                                          <View style={layout.selectedCheck}>
                                            <CRIcon
                                              name={'check'}
                                              size={12}
                                              color={colors.colorWhite}
                                            />
                                          </View>
                                        }
                                        uncheckedIcon={
                                          <View
                                            style={layout.unselectedCheck}
                                          ></View>
                                        }
                                        checked={schedule.id === input.value.id}
                                        onPress={() => {
                                          input.onChange(schedule);
                                        }}
                                      />
                                    )}
                                  >
                                    {schedule.schedules.delivery}
                                  </Field>
                                  <CRText
                                    variant={typography.paragraph}
                                    color={colors.colorDark300}
                                  >
                                    {schedule.schedules.delivery}
                                  </CRText>
                                </View>
                              </CRCard>
                            )}
                          </CRRowFull>
                        );
                      },
                    )}
                    {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
                  </View>
                );

                onRenderShippingDate(shippingSchedules);
              }}
            >
              <CRRowLeft>
                <CRText
                  variant={typography.caption}
                  color={colors.colorDark100}
                >
                  Entrega:{' '}
                </CRText>
                <CRText
                  variant={typography.paragraph}
                  color={colors.colorDark300}
                >
                  {`${DayJs(order.physical_properties.shipping_date).format(
                    'D MMMM',
                  )}`}{' '}
                  {`${
                    !isStandardShipping
                      ? selectedShippingSchedule.schedules.delivery
                      : ''
                  }`}
                </CRText>
              </CRRowLeft>
              <View style={layout.iconItemArrow}>
                <CRIcon
                  name={'angle--right'}
                  size={16}
                  color={colors.colorDark100}
                />
              </View>
            </TouchableOpacity>
          )}
          {/* END: SHIPPING DATE/SCHEDULES */}
        </CRCard>
      );
    });
  }
}

// Pass Redux state and actions to component
function mapStateToProps(state: Object, props: Object): Object {
  return {};
}

function mapDispatchToProps(dispatch: Function): Object {
  return {
    dispatch,
    ...bindActionCreators(
      {
        updateCartProduct: shoppingCartActions.updateCartProduct,
      },
      dispatch,
    ),
  };
}
CartOrder.defaultProps = {
  showShippingSchedules: true,
  showShippingMethods: true,
};

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CartOrder);
