import React, { Component } from 'react';
import type { Node } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, change } from 'redux-form';
import {
  View,
  Alert,
  TextInput,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import DayJs from 'dayjs';

import type { ProductType } from '../../redux/types/ProductTypes';
import { formatNumberToPrice } from '../../utils/normalizePrice';
import { shoppingCartActions } from '../../redux/actions';

//will be added to DS
import { CRCounterCart } from '../../components/CRCounter';
// DS
import { layout } from '../../pages/ShoppingCart/layout';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { CRThumb } from '../CRThumb';
import { CRIcon } from '../CRIcon';
//
import placeHolderDefault from '../../images/assets/placeholder.jpg';
//Google Analytics
import analytics from '@react-native-firebase/analytics';
import visaIMG from '../../images/assets/VISALogo--blue.png';

const minusIcon = (isMinusDisabled, touchableDisabledColor, touchableColor) => {
  return (
    <CRIcon
      name="minus"
      size={28}
      color={isMinusDisabled ? touchableDisabledColor : touchableColor}
    />
  );
};
const plusIcon = (isPlusDisabled, touchableDisabledColor, touchableColor) => {
  return (
    <CRIcon
      name="plus"
      size={28}
      color={isPlusDisabled ? touchableDisabledColor : touchableColor}
    />
  );
};

//
type CartProductProps = {
  meta: Object,
  fields: { map: Function, get: Function },
  dispatch: Function,
  updateCartProduct: Function,
  removeCartProduct: Function,
};
type CartProductState = {
  showOrderNoteWindow: boolean,
};

class CartProduct extends Component<CartProductProps, CartProductState> {
  state = {
    showOrderNoteWindow: false,
  };
  /**
   * onQuantityChange
   */
  onQuantityChange = (
    value: number,
    product: ProductType,
    item: string,
  ): void => {
    const { dispatch, meta } = this.props;

    // Evaluate new quantity
    if (value < 1) {
      Alert.alert(
        '¿Quieres eliminar este producto?',
        'La acción no podrá deshacerse',
        [
          {
            text: 'Cancel',
            onPress: () => {
              dispatch(change(meta.form, `${item}.quantity`, 1));
            },
            style: 'cancel',
          },
          {
            text: 'OK',
            onPress: () => {
              this.removeProduct(product);
            },
          },
        ],
        { cancelable: false },
      );
    } else {
      this.updateProduct({ ...product, quantity: value });
    }
  };

  updateProduct = (product: ProductType) => {
    /**
     * updateProduct()
     * Updates the shopping cart information after updating a product within the cart.
     * @param {ProductType} product : Updated Product
     */

    // Update product on DB.
    this.props.updateCartProduct({
      slug: product.slug,
      product: product.product.slug,
      quantity: product.quantity,
      note: product.note,
      attribute: product.attribute ? product.attribute.uuid : null,
      physical_properties: {
        ...product.physical_properties,
        shipping_date: DayJs(product.physical_properties.shipping_date).format(
          'YYYY-MM-DD',
        ),
        shipping_schedule: product.physical_properties.shipping_schedule.id,
      },
    });
  };

  removeProduct = (product: ProductType) => {
    /**
     * removeProduct()
     * @desc Updates the shopping cart after removing a product
     * @param {ProductType} product | Product Object
     */

    this.props.removeCartProduct({
      ...product,
      attribute: product.attribute ? product.attribute.uuid : null,
    });
    analytics().logRemoveFromCart({
      value: product.unit_price_without_discount,
      currency: 'MXN',
      items: [
        {
          item_brand: product.product.store.name,
          item_id: product.id.toString(),
          item_name: product.product.name,
          item_variant:
            product.attribute === null ? '' : product.attribute.tree_string,
          //item_category: 'round necked t-shirts',
        },
      ],
    });
  };

  openOrderNoteWindow = (): void => {
    /**
     * openOrderNoteWindow()
     * @desc Updates state to show order note input text
     */
    this.setState({ showOrderNoteWindow: true });
  };

  render(): Array<Node> {
    const { showOrderNoteWindow } = this.state;
    const { fields } = this.props;

    // REturn JSX
    return fields.map((item, index) => {
      const product = fields.get(index);
      const cart = this.props.cart;
      const { product: productDetails } = product;
      return (
        <View style={layout.productCartContainer} key={product.id}>
          <View style={layout.productCartInner}>
            <View style={layout.productInfoBlock}>
              <View style={layout.thumbnail}>
                {productDetails.photos.length > 0 ? (
                  <CRThumb
                    source={{ uri: productDetails.photos[0].photo.small }}
                    imgHeight={50}
                    resizeMode="contain"
                  />
                ) : (
                  <CRThumb
                    source={placeHolderDefault}
                    imgHeight={50}
                    resizeMode="contain"
                  />
                )}
              </View>

              <View style={layout.detail}>
                <CRText varaint={typography.bold}>
                  {productDetails.name} {/*`x ${product.quantity}`*/}
                </CRText>
                {product.attribute && (
                  <CRText
                    variant={typography.paragraph}
                    color={colors.colorDark100}
                  >
                    {product.attribute.tree_string
                      .split(',')
                      .map(
                        (attribute) =>
                          `${attribute
                            .charAt(0)
                            .toUpperCase()}${attribute.slice(1).toLowerCase()}`,
                      )
                      .join(', ')}
                  </CRText>
                )}
              </View>

              <Field
                name={`${item}.quantity`}
                value={product.quantity}
                onChange={(value) =>
                  this.onQuantityChange(value, product, item)
                }
                component={({ input, ...rest }) => {
                  return (
                    <CRCounterCart
                      start={input.value}
                      minusIcon={minusIcon}
                      plusIcon={plusIcon}
                      max={productDetails.quantity}
                      min={0}
                      onChange={input.onChange}
                    />
                  );
                }}
              />
            </View>

            <View style={layout.productInfoBlock}>
              <View>
                {!showOrderNoteWindow && product.note === '' ? (
                  <TouchableOpacity onPress={this.openOrderNoteWindow}>
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorGray400}
                    >
                      Añade una nota
                    </CRText>
                  </TouchableOpacity>
                ) : (
                  <Field
                    name={`${item}.note`}
                    value={product.note}
                    keyboardType="default"
                    placeholder="Deja algún mensaje a la tienda."
                    placeholderTextColor={colors.colorGray300}
                    onBlur={(value) =>
                      this.updateProduct({ ...product, note: value })
                    }
                    style={layout.fieldNoteStyle}
                    component={(props) => {
                      const { input, ...inputProps } = props;
                      return (
                        <TextInput
                          {...inputProps}
                          onChangeText={input.onChange}
                          onBlur={input.onBlur}
                          onFocus={input.onFocus}
                          value={input.value}
                        />
                      );
                    }}
                  />
                )}
              </View>
              {product.product.discount > 0 ? (
                <View>
                  <CRText variant={typography.subtitle3} align="right">
                    <Text
                      style={{
                        textDecorationLine: 'line-through',
                        textDecorationStyle: 'solid',
                      }}
                    >
                      {formatNumberToPrice(product.unit_price_without_discount)}{' '}
                      MXN
                    </Text>
                  </CRText>
                  <CRText
                    variant={typography.subtitle3}
                    align="right"
                    color={'#1A1F71'}
                  >
                    {formatNumberToPrice(product.unit_price)}MXN
                  </CRText>
                </View>
              ) : (
                <CRText variant={typography.subtitle3} align="right">
                  {formatNumberToPrice(product.unit_price_without_discount)}MXN
                </CRText>
              )}
            </View>
          </View>
          {/* END: NOTE TO SELLER */}
        </View>
      );
    });
  }
}

// Pass Redux state and actions to component
function mapStateToProps(state: Object, props: Object): Object {
  return {};
}

function mapDispatchToProps(dispatch: Function): Object {
  return {
    dispatch,
    ...bindActionCreators(
      {
        removeCartProduct: shoppingCartActions.removeFromCart,
        updateCartProduct: shoppingCartActions.updateCartProduct,
      },
      dispatch,
    ),
  };
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CartProduct);
