import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity, Image, Text, StyleSheet, View } from 'react-native';
import { FieldArray } from 'redux-form';
import CartProduct from './CartProduct';
import { CRRowBoth } from '../Layout';

// DS
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';

//Will be added to the ds
import { CRIcon } from '../CRIcon';

export class CartSummary extends Component {
  render() {
    const {
      fields,
      onRemoveProduct,
      onUpdateCartProduct,
      reduxFormChange,
    } = this.props;

    // const fieldsView = fields.map((item, index) => {
    //   const order = fields.get(index);

    //   if (!order.products.length) {
    //     return null;
    //   }
    //   return (
    //     <View
    //       style={styles.containerStoreCard}
    //       key={`${order.store.slug}-${order.physical_properties.shipping_date}-${order.physical_properties.shipping_schedule.id}`}
    //     >
    //       <View style={styles.store}>
    //         <View style={styles.thumbnail}>
    //           <TouchableOpacity
    //             onPress={() => {
    //               this.props.navigation.navigate('StoreScreen', {
    //                 itemStore: order.store.slug,
    //               });
    //             }}
    //           >
    //             <Image
    //               source={{ uri: order.store.photo.small }}
    //               style={styles.photo}
    //             />
    //           </TouchableOpacity>
    //         </View>
    //         <View>
    //           <Text style={styles.storeName}>{order.store.name}</Text>
    //         </View>
    //         <View style={styles.countProductContainer}>
    //           <Text style={styles.countProduct}>
    //             Productos: {order.products.reduce((a, b) => a + b.quantity, 0)}
    //           </Text>
    //         </View>
    //       </View>
    //       {/* <FieldArray
    //           name={`${item}.products`}
    //           component={CartProductsList}
    //           removeProduct={onRemoveProduct}
    //           reduxFormChange={reduxFormChange}
    //           updateCartProduct={onUpdateCartProduct}
    //         /> */}
    //       <FieldArray
    //         name={`${item}.products`}
    //         component={CartProduct}
    //         //dispatch={dispatch}
    //         //onUpdateProduct={onUpdateProduct}
    //         //onRemoveProduct={onRemoveProduct}
    //       />
    //     </View>
    //   );
    // });

    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <CRRowBoth addedStyle={styles.warning}>
          <View style={styles.warningItem}>
            <CRIcon name="warning" size={25} color={colors.colorYellow400} />
          </View>
          <View style={styles.warningText}>
            <CRText variant={typography.paragraph} color={colors.colorDark300}>
              Aviso: Algunos productos en tu carrito no pueden ser enviados a la
              dirección seleccionada.
            </CRText>
          </View>
          <View style={styles.warningItem}>
            <CRIcon
              name="angle--right"
              size={16}
              color={colors.colorYellow400}
            />
          </View>
        </CRRowBoth>

        {/* {fieldsView} */}
      </TouchableOpacity>
    );
  }
}
export default connect()(CartSummary);

const styles = StyleSheet.create({
  containerStoreCard: {
    backgroundColor: colors.colorWhite,
    padding: 15,
  },
  store: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  storeName: {
    ...typography.txtSemiBold,
    marginLeft: 20,
  },
  thumbnail: {
    width: 50,
    height: 50,
    borderWidth: 1,
    borderColor: colors.colorGray200,
  },
  countProductContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    flex: 3,
  },
  countProduct: {
    ...typography.txtLight,
    color: colors.colorGray400,
  },

  warning: {
    backgroundColor: colors.colorYellow200,
    padding: 10,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  warningText: {
    flex: 5,
  },
  warningItem: {
    flex: 1,
    alignItems: 'center',
  },
});
