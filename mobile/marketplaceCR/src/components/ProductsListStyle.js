import { StyleSheet } from 'react-native';
import { typography } from '../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { buttons } from '../assets';
export const styles = StyleSheet.create({
  doubleProducts: {
    display: 'flex',
    flexDirection: 'row',
  },
  doubleProduct: {
    flex: 1,
  },
  done: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  doneContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: colors.colorGreen300,
  },
  message: {
    ...typography.txtLight,
    color: colors.colorGray400,
    textAlign: 'center',
  },
  productsList: {
    marginTop: 10,
  },
  productContainer: {
    width: 180,
    height: 'auto',
    marginHorizontal: 7,
    marginBottom: 7,
    backgroundColor: colors.colorWhite,
    borderRadius: 5,
    shadowColor: colors.colorDark300,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  productData: {
    padding: 10,
  },
  photo: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
    marginVertical: 5,
  },
  product: {
    ...typography.txtSemiBold,
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  price: {
    ...typography.txtRegular,
    color: colors.colorDark300,
  },
  rating: {
    ...typography.txtRegular,
    color: colors.colorDark300,
  },
  icon: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
  },
  storeName: {
    ...typography.txtSemiBold,
    fontSize: 16,
    color: colors.colorGray400,
  },
  subcategoryActive: {
    margin: 10,
    ...buttons.btnInterests,
    backgroundColor: colors.colorMain300,
  },
  subcategoryNameActive: {
    ...typography.txtLight,
    padding: 8,
    textAlign: 'center',
    color: colors.colorWhite,
  },
  gifSpinner: {
    width: 100,
    height: 100,
  },
  containerSpinner: {
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 60,
  },
});
