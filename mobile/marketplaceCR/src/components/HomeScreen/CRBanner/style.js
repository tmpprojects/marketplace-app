import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -1,
    marginHorizontal: -1,
    marginBottom: 10,
  },
  imageStyle: {
    width: '100%',
    resizeMode: 'cover',
    position: 'relative',
    flex: 2.5,
    borderRadius: 3,
  },
  bullet: {
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'transparent',
  },
  bulletActive: {
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'transparent',
  },
  wrapperStyle: {
    borderRadius: 5,
    borderColor: colors.colorGray100,
    borderWidth: 1,
  },
});
