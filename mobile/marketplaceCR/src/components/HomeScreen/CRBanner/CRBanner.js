import React, { Component } from 'react';
import Swiper from 'react-native-swiper';

import { View, Dimensions, TouchableOpacity, Linking } from 'react-native';
//import {colors, typography} from '../../assets';

import { default as style } from './style';

//DS
import { CRThumb } from '../../CRThumb';

import bannerPlaceHolder from '../../../images/assets/placeholderBanner.jpg';

export default class CRBanner extends Component {
  validateURL(url) {
    if (url && url !== '' && url !== null && url !== undefined) {
      /* predict screen */
      const stores = ['stores'];
      const products = ['products'];
      const collections = ['landing'];
      const hotsale = ['hotsale'];

      const external = ['://'];
      // this.props.navigation.navigate('Hotsale');
      // return null;

      /* if contain string  */
      if (url.includes(external)) {
        Linking.openURL(url);
      } else if (url.includes(stores) && url.includes(products)) {
        let getStore = url.split('stores/')[1].split('/')[0];
        let getProduct = url.split('products/')[1].split('/')[0];

        // this.props.navigation.navigate('ProductDetail', {
        //   itemProductSlug: getProduct,
        //   itemStoreSlug: getStore,
        // });

        globalThis.ReactApplication.ActiveScreen(
          'ProductDetail',
          {
            itemProductSlug: getProduct,
            itemStoreSlug: getStore,
          },
          this.props.navigation,
        );
      } else if (url.includes(stores)) {
        let getStore = url.split('stores/')[1].split('/')[0];

        // this.props.navigation.navigate('StoreScreen', {
        //   itemStore: getStore,
        // });

        globalThis.ReactApplication.ActiveScreen(
          'StoreScreen',
          {
            itemStore: getStore,
          },
          this.props.navigation,
        );
      } else if (url.includes(collections)) {
        let getCollection = url.split('landing/')[1].split('/')[0];

        // this.props.navigation.navigate('Collection', {
        //   slug: getCollection,
        // });

        globalThis.ReactApplication.ActiveScreen(
          'Collection',
          {
            slug: getCollection,
          },
          this.props.navigation,
        );
      } else if (url.includes(hotsale)) {
        //let getCollection = url.split('landing/')[1].split('/')[0];
        //this.props.navigation.navigate('Hotsale');

        globalThis.ReactApplication.ActiveScreen(
          'Hotsale',
          {},
          this.props.navigation,
        );
      }
    }
    return null;
  }

  render() {
    const { slides } = this.props;
    const bannerWidth = Math.round(Dimensions.get('window').width) + 2;
    const bannerHeight = bannerWidth * 0.45;
    return (
      <View style={[style.container, { width: bannerWidth }]}>
        <Swiper
          horizontal={true}
          dotStyle={style.bullet}
          activeDotStyle={style.bulletActive}
          loop={true}
          height={bannerHeight + 5}
          autoplay={true}
          autoplayTimeout={4}
        >
          {slides.map((item, i) => (
            <TouchableOpacity
              onPress={() => {
                this.validateURL(item.link);
              }}
              activeOpacity={1}
              key={i}
            >
              <View style={{ height: bannerHeight }}>
                <CRThumb
                  wrapperStyle={style.wrapperStyle}
                  source={{ uri: item.photo_app_mobile.medium }}
                  onPress={() => {}}
                  imgHeight={'100%'}
                  placeHolder={bannerPlaceHolder}
                />

                {/* <View style={style.textContainer}>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorMain300}
                  align="center">
                  {item.title}
                </CRText>
                <CRText variant={typography.paragraph} align="center">
                  {item.excerpt}
                </CRText>
                <Text style={styles.titleBanner}>{item.title}</Text>
                <Text style={styles.subtitleBanner}>{item.excerpt}</Text>
              </View>*/}
              </View>
            </TouchableOpacity>
          ))}
        </Swiper>
      </View>
    );
  }
}
