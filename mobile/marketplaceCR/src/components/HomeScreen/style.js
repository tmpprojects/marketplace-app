import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  // container: {
  //   flex: 1,
  //   flexDirection: 'column',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  // },
  bannerContainer: {
    height: 300,
  },
  imageStyle: {
    width: '100%',
    resizeMode: 'cover',
    position: 'relative',
    flex: 2.5,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginHorizontal: 10,
    marginTop: 5,
    marginBottom: 5,
  },
  /*titleBanner: {
      ...typography.txtRegular,
      fontSize: 19,
      color: colors.black,
      textAlign: 'center',
      marginTop: -40,
    },
    subtitleBanner: {
      ...typography.txtRegular,
      fontSize: 17,
      color: colors.corePink,
      textAlign: 'center',
    },
    wrapper: {},
    title: {
      ...typography.txtExtraBold,
      textAlign: 'center',
      color: colors.corePink,
      marginVertical: 10,
      marginTop: -1000,
    },*/
  cover: {
    width: '100%',
    height: 130,
    resizeMode: 'cover',
    marginVertical: 5,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  /*store: {
      ...typography.txtSemiBold,
      fontSize: 20,
      textTransform: 'uppercase',
      color: colors.white,
      textAlign: 'center',
    },
    slogan: {
      ...typography.txtLight,
      color: colors.white,
    },*/
  bullet: {
    width: 10,
    height: 10,
    backgroundColor: colors.colorGray200,
    borderRadius: 50,
    marginVertical: 5,
    marginHorizontal: 15,
  },
  bulletActive: {
    width: 10,
    height: 10,
    backgroundColor: colors.colorMain300,
    borderRadius: 50,
    marginVertical: 5,
    marginHorizontal: 15,
  },
});
