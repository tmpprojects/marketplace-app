/*        [  DEPRECATED ]   */

import React, { Component } from 'react';
import Swiper from 'react-native-swiper';

import { View, Image } from 'react-native';
//import {colors, typography} from '../../assets';

import { default as style } from './style';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

export default class MainBanner extends Component {
  render() {
    const { slides } = this.props;
    return (
      <View style={style.container}>
        <Swiper
          horizontal={true}
          style={style.wrapper}
          dotStyle={style.bullet}
          activeDotStyle={style.bulletActive}
          loop={true}
          height={370}
          autoplay
        >
          {slides.map((item, i) => (
            <View key={i} style={style.bannerContainer}>
              <Image
                style={style.imageStyle}
                source={{ uri: item.photo.medium }}
              />
              <View style={style.textContainer}>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorMain300}
                  align="center"
                >
                  {item.title}
                </CRText>
                <CRText variant={typography.paragraph} align="center">
                  {item.excerpt}
                </CRText>
                {/*<Text style={styles.titleBanner}>{item.title}</Text>
                <Text style={styles.subtitleBanner}>{item.excerpt}</Text>*/}
              </View>
            </View>
          ))}
        </Swiper>
      </View>
    );
  }
}
