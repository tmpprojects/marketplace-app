import React, { Component } from 'react';
import { TouchableOpacity, ScrollView, Text, StyleSheet } from 'react-native';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography, buttons } from '../assets';

export class Radio extends Component {
  handlePress = () => {
    this.props.onChange(this.props.value);
  };
  render() {
    const { checked, label, borderColor, color } = this.props;
    return (
      <TouchableOpacity
        onPress={this.handlePress}
        style={[
          styles.itemFilter,
          checked
            ? {
                borderColor: colors.colorMain300,
                backgroundColor: colors.colorMain300,
              }
            : { borderColor: borderColor },
        ]}
      >
        <Text
          style={[
            styles.filterName,
            checked ? { color: colors.colorWhite } : { color: color },
          ]}
        >
          {label}
        </Text>
      </TouchableOpacity>
    );
  }
}

export class RadioSet extends Component {
  render() {
    const {
      radios,
      input: { value, onChange },
      color,
      borderColor,
      scrollIndicator,
    } = this.props;
    return (
      <ScrollView horizontal={scrollIndicator}>
        {radios.map((radio, index) => (
          <Radio
            key={radio.label}
            {...radio}
            onChange={onChange}
            checked={radio.value === value}
            color={color}
            borderColor={borderColor}
          />
        ))}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  itemFilter: {
    margin: 10,
    ...buttons.btnInterests,
  },
  filterName: {
    ...typography.txtLight,
    padding: 8,
    textAlign: 'center',
    color: colors.colorWhite,
  },
});
export default RadioSet;
