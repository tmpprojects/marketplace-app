import { StyleSheet } from 'react-native';
import { typography } from '../../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const styles = StyleSheet.create({
  newOrder: {
    color: colors.colorViolet300,
  },
  productOrder: {
    color: colors.colorViolet300,
  },
  shippingOrder: {
    color: colors.colorTurquiose300,
  },
  transitingOrder: {
    color: colors.colorRed300,
  },
  orderDeliveredOrder: {
    color: colors.colorGray300,
  },
  arrowBtn: {
    marginTop: 20,
    marginHorizontal: 15,
  },
  title: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  scrollView: {
    marginBottom: 25,
  },
  ordersList: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,
  },
  ordersCount: {
    marginVertical: 30,
  },

  //ORDER CARD
  orderCard: {
    backgroundColor: colors.colorWhite,
    paddingHorizontal: 20,
  },
  indicatorBase: {
    width: 6,
    height: '100%',
    position: 'absolute',
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3,
  },
  indicator: {
    backgroundColor: colors.colorMain300,
  },

  indicatorDelivered: {
    backgroundColor: colors.colorAqua300,
  },
  indicatorInProgress: {
    backgroundColor: colors.colorViolet300,
  },
  indicatorWaitingPay: {
    backgroundColor: colors.colorMain300,
  },
  order: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  orderInfo: {
    flex: 1,
  },
  orderNumber: {
    ...typography.txtSemiBold,
  },
  storeName: {
    ...typography.txtSemiBold,
    color: colors.colorGray400,
  },
  date: {
    ...typography.txtLight,
    color: colors.colorGray400,
  },
  shippingDate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  orderDetail: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  detail: {
    marginHorizontal: 25,
  },
  price: {
    ...typography.txtBold,
    fontSize: 18,
    textAlign: 'right',
    marginBottom: 8,
  },
  btnDetails: {
    color: colors.colorGray400,
    textAlign: 'right',
  },

  //MENU

  menuContainer: {
    paddingHorizontal: 15,
    marginTop: 20,
  },
  divider: {
    borderBottomColor: colors.colorGray200,
    borderBottomWidth: 1,
    zIndex: -1,
  },
  activeDivider: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: -2,
    borderBottomColor: colors.colorTurquiose300,
    borderBottomWidth: 3,
    height: '50%',
    width: '90%',
  },
  menu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemMenu: {
    flex: 3,
    padding: 20,
  },
  name: {
    ...typography.txtLight,
    textAlign: 'center',
  },
  nameActive: {
    ...typography.txtSemiBold,
    fontSize: 16,
    color: colors.colorTurquiose300,
    textAlign: 'center',
    borderBottomWidth: 2,
    borderBottomColor: colors.colorTurquiose300,
  },
  btnReviews: {
    color: colors.colorMain300,
    ...typography.txtSemiBold,
    textAlign: 'center',
    marginTop: 15,
  },
  errorMessage: {
    color: colors.colorGray400,
    ...typography.txtLight,
    textAlign: 'center',
    marginTop: 15,
    paddingTop: 50,
    paddingHorizontal: 20,
  },
  gifSpinner: {
    width: 100,
    height: 100,
  },
  containerSpinner: {
    alignItems: 'center',
    marginTop: 60,
    marginBottom: 0,
  },
});
