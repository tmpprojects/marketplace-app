import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ordersActions } from '../../../redux/actions';

import Moment from 'dayjs';
import { Image, View, TouchableOpacity, Text, Platform } from 'react-native';
import { styles } from './OrderCardStyles';
import arrow from '../../../images/icon/icon_arrow_right.png';
import { ORDER_STATUS } from '../../../redux/constants/order.constants';
import { formatNumberToPrice } from './../../../utils/normalizePrice';
/* ⬇ Animation component */
import loader from '../../../images/gifs/Spinner.gif';
import empty from '../../../images/assets/empty.png';

//DS
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { CRRowBoth, CRRowFull } from '../../Layout';
import { ScrollView } from 'react-native-gesture-handler';
import { CRCard } from '../../CRCard/CRCard';

const displayStatus = (status, type) => {
  switch (status) {
    case ORDER_STATUS.NEW_ORDER:
      return <CRText style={styles.newOrder}>Orden Nueva</CRText>;
    case ORDER_STATUS.PREPARING_ORDER:
      return <Text style={styles.productOrder}>Preparando Orden</Text>;
    case ORDER_STATUS.ORDER_READY:
    case ORDER_STATUS.AWAITING_SHIPMENT:
      return <Text style={styles.orderNumber}>Esperando Recolección</Text>;
    case ORDER_STATUS.ORDER_IN_TRANSIT:
      return <Text style={styles.orderNumber}>Orden en Tránsito</Text>;
    case ORDER_STATUS.NEXT_PICKUP_POINT:
      return <Text style={styles.orderNumber}>Orden en Tránsito</Text>;
    case ORDER_STATUS.FAILED_PICKUP:
      return <Text style={styles.orderNumber}>Orden en Tránsito</Text>;
    case ORDER_STATUS.IN_TRANSIT_DELAYED:
      return <Text style={styles.orderNumber}>Orden en Tránsito</Text>;
    case ORDER_STATUS.NEXT_DELIVERY_POINT:
      return <Text style={styles.orderNumber}>Orden en Tránsito</Text>;
    case ORDER_STATUS.FAILED_DELIVERY:
      return <Text style={styles.orderNumber}>Orden en Tránsito</Text>;
    case ORDER_STATUS.AWAITING_PAYMENT:
      return <Text style={styles.transitingOrder}>Esperando Pago</Text>;
    case ORDER_STATUS.DELIVERED:
      return <Text style={styles.shippingOrder}>Entregada</Text>;
    default:
      return <Text style={styles.productOrder}>Orden Pendiente</Text>;
  }
};

let _setIndicador;

function ImageLading(props) {
  if (props.items.length === 0 && props.numOrders === 0) {
    return (
      <View style={styles.containerSpinner}>
        <Image source={empty} style={styles.gifSpinner} />
      </View>
    );
  } else {
    return props.numOrders === 0 ? (
      <View style={styles.containerSpinner}>
        <Image source={loader} style={styles.gifSpinner} />
      </View>
    ) : null;
  }
  return null;
}
class OrderCard extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      numOrders: 0,
      textLoading: 'Espera un momento...',
    };
  }

  calcOrders = (items) => {
    if (items.length === 0) {
      this.setState({
        ...this.state,
        textLoading: 'No hay nada por ahora.',
      });
    } else {
      this.setState({ numOrders: items.length });

      return items.length > 1
        ? this.setState({
            numOrders: items.length,
            textLoading: `Tienes ${items.length} órdenes en total`,
          })
        : this.setState({
            numOrders: items.length,
            textLoading: `Tienes ${items.length} orden en total`,
          });
    }
  };
  calcTotal(group_orders) {
    let total = 0;
    group_orders.map((order) => {
      total += Number(order.total_price);
    });
    return formatNumberToPrice(total);
  }

  openDetail(group) {
    this.props.selectOrderDetail(group);

    // this.props.navigation.navigate('OrderDetail', {
    //   initialValue: 0,
    // });

    globalThis.ReactApplication.ActiveScreen(
      'OrderDetail',
      {
        initialValue: 0,
      },
      this.props.navigation,
    );
  }

  evaluateInProgress = (group) => {
    const evaluateValue = group.orders[0].physical_properties.status.value;
    let inProgress = false;
    if (
      evaluateValue !== ORDER_STATUS.NEW_ORDER &&
      evaluateValue !== ORDER_STATUS.PREPARING_ORDER &&
      evaluateValue !== ORDER_STATUS.ORDER_READY &&
      evaluateValue !== ORDER_STATUS.AWAITING_SHIPMENT &&
      evaluateValue !== ORDER_STATUS.ORDER_IN_TRANSIT &&
      evaluateValue !== ORDER_STATUS.NEXT_PICKUP_POINT &&
      evaluateValue !== ORDER_STATUS.FAILED_PICKUP &&
      evaluateValue !== ORDER_STATUS.IN_TRANSIT_DELAYED &&
      evaluateValue !== ORDER_STATUS.NEXT_DELIVERY_POINT &&
      evaluateValue !== ORDER_STATUS.FAILED_DELIVERY &&
      evaluateValue !== ORDER_STATUS.AWAITING_PAYMENT &&
      evaluateValue !== ORDER_STATUS.DELIVERED
    ) {
      inProgress = true;
    }
    return inProgress;
  };

  evaluateIndicator = (group) => {
    const evaluateValue = group.orders[0].physical_properties.status.value;
    if (evaluateValue === ORDER_STATUS.DELIVERED) {
      _setIndicador = [styles.indicatorBase, styles.indicatorDelivered];
    }
    if (evaluateValue === ORDER_STATUS.AWAITING_PAYMENT) {
      _setIndicador = [styles.indicatorBase, styles.indicatorWaitingPay];
    } else if (this.evaluateInProgress(group)) {
      _setIndicador = [styles.indicatorBase, styles.indicatorInProgress];
    }
  };

  render() {
    let { items } = this.props.properties;
    const { orderStatus } = this.props.properties;
    _setIndicador = styles.indicator;

    let modifyFilter;

    if (orderStatus === ORDER_STATUS.DELIVERED) {
      modifyFilter = items.filter(function (group) {
        return (
          group.orders[0].physical_properties.status.value ===
          ORDER_STATUS.DELIVERED
        );
      });
      items = modifyFilter;
    }
    if (orderStatus === 'inprogress') {
      modifyFilter = items.filter(function (group) {
        const evaluateValue = group.orders[0].physical_properties.status.value;
        return (
          evaluateValue !== ORDER_STATUS.NEW_ORDER &&
          evaluateValue !== ORDER_STATUS.PREPARING_ORDER &&
          evaluateValue !== ORDER_STATUS.ORDER_READY &&
          evaluateValue !== ORDER_STATUS.AWAITING_SHIPMENT &&
          evaluateValue !== ORDER_STATUS.ORDER_IN_TRANSIT &&
          evaluateValue !== ORDER_STATUS.NEXT_PICKUP_POINT &&
          evaluateValue !== ORDER_STATUS.FAILED_PICKUP &&
          evaluateValue !== ORDER_STATUS.IN_TRANSIT_DELAYED &&
          evaluateValue !== ORDER_STATUS.NEXT_DELIVERY_POINT &&
          evaluateValue !== ORDER_STATUS.FAILED_DELIVERY &&
          evaluateValue !== ORDER_STATUS.AWAITING_PAYMENT &&
          evaluateValue !== ORDER_STATUS.DELIVERED
        );
      });
      items = modifyFilter;
    }

    this.calcOrders(items, this.state.numOrders);

    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 50;
    const SPACER_SIZE_ANDROID = 50;

    return (
      <View>
        <ImageLading items={items} numOrders={this.state.numOrders} />
        <CRRowBoth addedStyle={styles.ordersCount}>
          <CRText
            variant={typography.paragraph}
            color={colors.colorDark100}
            align="center"
          >
            {this.state.textLoading}
          </CRText>
        </CRRowBoth>
        <ScrollView>
          {items.map((group, i) => {
            this.evaluateIndicator(group);
            return (
              <CRRowBoth key={i}>
                <CRCard size="full" onPress={() => this.openDetail(group)}>
                  <View style={styles.orderCard}>
                    <View style={_setIndicador} />
                    <View style={styles.order}>
                      <View style={styles.orderInfo}>
                        <CRText
                          variant={typography.subtitle3}
                          color={colors.colorDark300}
                        >
                          {group.uid}
                        </CRText>
                        {displayStatus(
                          group.orders[0].physical_properties.status.value,
                        )}
                        <CRRowFull>
                          {group.orders.map((order, subIndex) => {
                            let numProducts = order.products.length;
                            let infonumProducts;
                            numProducts > 1
                              ? (infonumProducts = `(${numProducts} productos)`)
                              : (infonumProducts = `(${numProducts} producto)`);

                            return (
                              <View style={{ marginBottom: 8 }} key={subIndex}>
                                <CRText
                                  variant={typography.bold}
                                  color={colors.colorDark100}
                                >
                                  {order.store.name}
                                </CRText>
                                <CRText
                                  style={styles.storeName}
                                  variant={typography.caption}
                                  color={colors.colorDark100}
                                >
                                  {infonumProducts}
                                </CRText>
                                <View style={styles.shippingDate}>
                                  {order.physical_properties
                                    .shipping_method_name ==
                                  'Envíos Nacionales' ? (
                                    <CRText
                                      variant={typography.paragraph}
                                      color={colors.colorDark100}
                                    >
                                      Entrega Estimada:
                                    </CRText>
                                  ) : (
                                    <CRText
                                      variant={typography.paragraph}
                                      color={colors.colorDark100}
                                    >
                                      Entrega:
                                    </CRText>
                                  )}

                                  <CRText
                                    variant={typography.bold}
                                    color={colors.colorDark100}
                                  >
                                    &nbsp;
                                    {Moment(
                                      order.physical_properties.shipping_date,
                                    ).format('DD MMMM')}
                                  </CRText>
                                </View>
                              </View>
                            );
                          })}
                        </CRRowFull>
                        {/* <CRRowFull>
                          {group.orders.map((order, subIndex) => {
                            return (
                              <View key={subIndex}>
                                <CRText
                                  style={styles.date}
                                  variant={typography.paragraph}
                                  color={colors.colorDark100}
                                >
                                  Entrega Estimada: {Moment(order.physical_properties.shipping_date).format('DD MMMM YYYY')}
                                </CRText>
                              </View>
                            );
                          })}
                        </CRRowFull> */}
                      </View>
                      <View style={styles.orderDetail}>
                        <View style={styles.detail}>
                          <CRText
                            variant={typography.bold}
                            color={colors.colorDark300}
                            align="right"
                          >
                            {/* {this.calcTotal(group.orders)} MXN */}$
                            {group.price} MXN
                          </CRText>
                          <CRText
                            variant={typography.paragraph}
                            color={colors.colorDark100}
                            align="right"
                          >
                            Ver detalles
                          </CRText>
                        </View>
                        <TouchableOpacity>
                          <Image source={arrow} style={styles.iconItemArrow} />
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </CRCard>
              </CRRowBoth>
            );
          })}
        </ScrollView>

        {isIos && <View style={{ height: SPACER_SIZE_IOS }} />}
        {isAndroid && <View style={{ height: SPACER_SIZE_IOS }} />}
      </View>
    );
  }
}

// Redux Map Functions
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  const { selectOrderDetail } = ordersActions;
  return bindActionCreators(
    {
      selectOrderDetail,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderCard);
