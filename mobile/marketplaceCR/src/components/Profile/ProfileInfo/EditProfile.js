import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import {
  Image,
  View,
  TouchableOpacity,
  Modal,
  Text,
  TextInput,
  ScrollView,
  Alert,
} from 'react-native';
import { required, name, lastName } from '../../../utils/formValidators';

import colors from '@canastarosa/ds-theme/colors/js/variables';
import { styles } from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import pencil from '../../../images/icon/Pencil.png';
import { RadioSet } from '../../RadioSet';
import EditPassword from './EditPassword';

const renderField = ({
  keyboardType,
  meta: { touched, error, warning },
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
}) => {
  return (
    <View>
      <TextInput
        style={styles.inputForm}
        placeholder={placeholder}
        placeholderTextColor={colors.black300}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        {...restInput}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
      />
      {touched &&
        ((error && <Text style={styles.formErrors}>{error}</Text>) ||
          (warning && <Text style={styles.formErrors}>{warning}</Text>))}
    </View>
  );
};
const renderFieldTextArea = ({
  keyboardType,
  input: { onChange },
  placeholder,
}) => {
  return (
    <View style={styles.inputForm}>
      <TextInput
        style={styles.textAreaField}
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray400}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        numberOfLines={4}
        multiline={true}
        onChangeText={onChange}
        autoCapitalize="none"
      />
    </View>
  );
};
export default class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.setModalVisible(true);
          }}
        >
          <Image source={pencil} style={styles.iconEdit} />
        </TouchableOpacity>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
        >
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => {
                this.setModalVisible(false);
              }}
              style={styles.btnIcon}
              activeOpacity={0.5}
            >
              <Icon name="ios-close" style={styles.iconStyle} />
            </TouchableOpacity>
            <ScrollView style={styles.container}>
              <Text style={styles.title}>Editar información</Text>
              <View style={styles.containerForm}>
                <View style={styles.field}>
                  <Text style={styles.label}>Nombre</Text>
                  <Field
                    maxLength={40}
                    autoFocus
                    name="first_name"
                    id="first_name"
                    component={renderField}
                    type="text"
                    validate={[required, name]}
                  />
                </View>

                <View style={styles.field}>
                  <Text style={styles.label}>Apellido</Text>
                  <Field
                    maxLength={40}
                    name="last_name"
                    component={renderField}
                    id="last_name"
                    type="text"
                    validate={[required, lastName]}
                  />
                </View>

                <View style={styles.field}>
                  <Text style={styles.label}>Acerca de mí </Text>
                  <Field
                    maxLength={200}
                    name="presentation"
                    component={renderFieldTextArea}
                    placeholder="Escribe una breve descripción acerca de ti"
                    id="presentation"
                  />
                </View>

                <View style={styles.field}>
                  <Text style={styles.label}>Género </Text>
                  <Field
                    radios={[
                      {
                        label: 'Femenino',
                        value: 'M',
                        id: 'gender_female',
                      },
                      {
                        label: 'Masculino',
                        value: 'gender_male',
                        id: 'H',
                      },
                      {
                        label: 'Otro',
                        value: 'gender_other',
                        id: 'N',
                      },
                    ]}
                    component={RadioSet}
                    name="gender.value"
                    color={colors.colorGray400}
                    borderColor={colors.colorGray200}
                  />
                </View>

                <View style={styles.field}>
                  <Text style={styles.label}>Cumpleaños </Text>
                </View>
              </View>
              <EditPassword style={styles.pass} />
            </ScrollView>
            <View style={styles.containerBtn}>
              <TouchableOpacity style={styles.btn} activeOpacity={0.5}>
                <Text style={styles.btnText}> Actualizar información</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
EditProfile = reduxForm({
  form: 'editProfileForm',
})(EditProfile);
