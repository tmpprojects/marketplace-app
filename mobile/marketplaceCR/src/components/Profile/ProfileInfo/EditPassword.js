/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import {
  Image,
  View,
  TouchableOpacity,
  Modal,
  Text,
  TextInput,
  Alert,
} from 'react-native';
import {
  required,
  password,
  password_confirmation,
} from '../../../utils/formValidators';

import colors from '@canastarosa/ds-theme/colors/js/variables';
import { styles } from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import lock from '../../../images/icon/lock.png';

const renderField = ({
  keyboardType,
  meta: { touched, error, warning },
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
}) => {
  return (
    <View>
      <TextInput
        style={styles.inputForm}
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray300}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        {...restInput}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
      />
      {touched && error && <Text>{error}</Text>}
    </View>
  );
};
export default class EditPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {
    return (
      <View>
        <TouchableOpacity
          style={styles.btnPass}
          onPress={() => {
            this.setModalVisible(true);
          }}
        >
          <Image source={lock} style={styles.iconEdit} />
          <Text style={styles.btnName}>Cambiar contraseña</Text>
        </TouchableOpacity>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
        >
          <View style={styles.passModalView}>
            <View style={styles.passContainer}>
              <TouchableOpacity
                onPress={() => {
                  this.setModalVisible(false);
                }}
                style={{ alignItems: 'flex-end' }}
                activeOpacity={0.5}
              >
                <Icon
                  name="ios-close"
                  style={{
                    color: '#898989',
                    fontSize: 40,
                  }}
                />
              </TouchableOpacity>
              <View style={styles.passForm}>
                <Text style={styles.title}>Cambiar contraseña</Text>
                <View style={styles.passContainerForm}>
                  <View style={styles.field}>
                    <Text style={styles.label}>Contraseña actual</Text>
                    <Field
                      name="passwordCurrent"
                      keyboardType="default"
                      placeholder="Escribe tu contraseña actual"
                      type="password"
                      component={renderField}
                      validate={[password, required]}
                    />
                  </View>
                  <View style={styles.field}>
                    <Text style={styles.label}>Nueva contraseña</Text>
                    <Field
                      name="passwordNew"
                      keyboardType="default"
                      placeholder="Escribe tu nueva contraseña"
                      type="password"
                      component={renderField}
                      validate={[password, required]}
                    />
                  </View>
                  <View style={styles.field}>
                    <Text style={styles.label}>Confirmar contraseña</Text>
                    <Field
                      name="passwordConf"
                      keyboardType="default"
                      placeholder="Confirma tu nueva contraseña"
                      type="password"
                      component={renderField}
                      validate={[password, required, password_confirmation]}
                    />
                  </View>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    this.setModalVisible(false);
                  }}
                >
                  <Text style={styles.btnUpdate}>Actualizar</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
EditPassword = reduxForm({
  form: 'editPasswordForm',
})(EditPassword);
