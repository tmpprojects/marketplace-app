import { StyleSheet, Dimensions } from 'react-native';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography, buttons } from '../../../assets';

const { height: HEIGHT } = Dimensions.get('window');

export const styles = StyleSheet.create({
  modalView: {
    backgroundColor: colors.colorWhite,
    height: '100%',
  },
  container: {
    alignSelf: 'center',
  },
  title: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  containerForm: {
    marginTop: 30,
    // maxWidth: '85%',
  },
  field: {
    margin: 10,
  },
  label: {
    ...typography.txtRegular,
    fontSize: 13,
    color: colors.colorDark100,
  },
  textAreaField: {
    color: colors.colorDark300,
    ...typography.txtLight,
    height: 100,
  },
  inputForm: {
    height: 100,
    borderBottomColor: colors.colorGray200,
    borderTopColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderWidth: 1,
    color: colors.colorDark300,
    marginBottom: 10,
    ...typography.txtLight,
    fontSize: 18,
  },
  containerBtn: {
    width: '100%',
    height: HEIGHT,
    justifyContent: 'flex-end',
    position: 'absolute',
    bottom: 0,
    zIndex: -1,
  },
  btn: {
    ...buttons.squareCart,
    width: '100%',
    height: 80,
  },
  btnText: {
    textAlign: 'center',
    ...typography.txtRegular,
    color: colors.colorWhite,
    fontSize: 20,
  },

  passModalView: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    height: '100%',
  },
  passContainer: {
    backgroundColor: colors.colorWhite,
    width: '100%',
    height: '60%',
    position: 'absolute',
    bottom: 0,
    padding: 20,
  },
  passForm: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  passContainerForm: {
    marginTop: 30,
    width: '85%',
  },
  btnPass: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 25,
  },
  btnName: {
    ...typography.txtRegular,
    color: colors.colorMain300,
    marginLeft: 7,
  },
  btnUpdate: {
    ...typography.txtRegular,
    fontSize: 18,
    color: colors.colorMain300,
    marginTop: 30,
  },
  btnIcon: {
    alignItems: 'flex-end',
  },
  iconStyle: {
    color: '#898989',
    fontSize: 40,
    margin: 30,
  },
});
