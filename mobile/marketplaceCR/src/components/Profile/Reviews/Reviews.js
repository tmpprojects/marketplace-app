import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import VirtualListContainer from '../../../components/VirtualList/VirtualListContainer/VirtualListContainer';
import { View, Text } from 'react-native';
import { userActions } from '../../../redux/actions';

import { styles } from './ReviewsStyles';
import { CRRowFull } from '../../../components/Layout';
import CRTabWrapper from '../../../components/CRTabs/CRTabWrapper/CRTabWrapper';
import CRTab from '../../../components/CRTabs/CRTab/CRTab';
import { CRTitle } from '../../../components/CRTitle';
import * as Sentry from '@sentry/react-native';

//Google Analytics
import analytics from '@react-native-firebase/analytics';

////
let infiniteItems = [];
let currentPage = 1;
let dynamicLoading = false;
let npages = 1;
////

let infiniteItemsPending = [];
let currentPagePending = 1;
let dynamicLoadingPending = false;
let npagesPending = 1;

let templateState = {
  checked: false,
  shouldRenderPending: false,
  shouldRenderReviews: true,
  shouldModal: false,
  selectedReview: {},
  scoreProduct: 0,
  scoreLogistics: 0,
  /////////////
  showItemsCompleted: [],
  disabledCompleted: false,
  disabledInfinite: false,
  allCompleted: 0,

  /////////////
  showItemsPending: [],
  disabledPending: false,
  disabledInfinitePending: false,
  allPending: 0,

  loading: true,
};
declare var globalThis: any;
class Reviews extends Component {
  constructor() {
    super();
    this.state = templateState;
  }

  async componentDidMount() {
    this.loadReviews();
    await analytics().setCurrentScreen('ReviewsScreen');
    await analytics().logEvent('ReviewsScreen');
  }

  reloadReviews = () => {
    try {
      infiniteItems = [];
      infiniteItemsPending = [];
      currentPage = 1;
      currentPagePending = 1;
      this.setState(templateState);
      this.loadReviews();
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  async loadReviews() {
    try {
      const completed = await this.props.getUserReviews(
        '?page=1&review__isnull=false&page_size=3',
        'completed',
      );
      completed.data.results.map((element) => infiniteItems.push(element));
      this.setState({
        ...this.state,
        showItemsCompleted: infiniteItems,
        allCompleted: completed.data.count,
        loading: false,
      });
      npages = completed.data.npages;
      //////////////
      const pending = await this.props.getUserReviews(
        '?page=1&review__isnull=true&page_size=3',
        'pending',
      );
      pending.data.results.map((element) => infiniteItemsPending.push(element));
      this.setState({
        ...this.state,
        showItemsPending: infiniteItemsPending,
        allPending: pending.data.count,
        loading: false,
      });
      npagesPending = pending.data.npages;
    } catch (e) {
      Sentry.captureException(e);
    }
  }
  showReviews = () => {
    this.setState({
      shouldRenderReviews: true,
      shouldRenderPending: false,
    });
  };

  showPending = () => {
    this.setState({
      shouldRenderPending: true,
      shouldRenderReviews: false,
    });
  };

  showMore = async () => {
    try {
      if (!dynamicLoading && !this.state.disabledInfinite) {
        dynamicLoading = true;
        currentPage++;
        if (currentPage <= npages) {
          const completed = await this.props.getUserReviews(
            `?page=${currentPage}&review__isnull=false&page_size=3`,
            'completed',
          );
          completed.data.results.map((element) => infiniteItems.push(element));
          // Update state
          this.setState({
            ...this.state,
            disabledInfinite: completed.data.results.length < 4 ? true : false,
            showItemsCompleted: infiniteItems,
            loading: false,
          });
        } else {
          this.setState({
            ...this.state,
            disabledInfinite: true,
            loading: false,
          });
        }

        dynamicLoading = false;
      }
    } catch (e) {
      Sentry.captureException(e);
    }
  };
  showMorePending = async () => {
    try {
      if (!dynamicLoadingPending && !this.state.disabledInfinitePending) {
        dynamicLoadingPending = true;
        currentPagePending++;
        if (currentPagePending <= npagesPending) {
          const pending = await this.props.getUserReviews(
            `?page=${currentPagePending}&review__isnull=true&page_size=3`,
            'pending',
          );
          pending.data.results.map((element) =>
            infiniteItemsPending.push(element),
          );
          // Update state
          let evaluate = pending.data.results.length < 4 ? true : false;

          this.setState({
            ...this.state,
            disabledInfinitePending: evaluate,
            showItemsPending: infiniteItemsPending,
            loading: false,
          });
        } else {
          this.setState({
            ...this.state,
            disabledInfinitePending: true,
          });
        }

        dynamicLoadingPending = false;
      }
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  render() {
    const { shouldRenderReviews, shouldRenderPending } = this.state;

    try {
      const configComplete = {
        products: this.state.showItemsCompleted,
        screen: '',
        category: '',
        navigation: this.props.navigation,
        refreshing: true,
        type: 'reviewsCompleted',
        showMore: () => {
          this.showMore();
        },
        request: this.state.showItemsCompleted,
        disabledInfinite: this.state.disabledInfinite,
      };
      const configPending = {
        products: this.state.showItemsPending,
        screen: '',
        category: '',
        navigation: this.props.navigation,
        refreshing: true,
        type: 'reviewsPending',
        showMore: () => {
          this.showMorePending();
        },
        request: this.state.showItemsPending,
        disabledInfinite: this.state.disabledInfinitePending,
      };

      return (
        <View style={styles.containerWrapper}>
          <View style={styles.sectionReviews}>
            <CRTitle title="Mis reseñas" />
            <CRRowFull>
              <CRTabWrapper>
                <CRTab
                  title={`Pendientes (${this.state.allPending}) `}
                  isActive={shouldRenderPending}
                  onPress={() => this.showPending()}
                />
                <CRTab
                  title={`Anteriores (${this.state.allCompleted}) `}
                  isActive={shouldRenderReviews}
                  onPress={() => this.showReviews()}
                />
              </CRTabWrapper>
            </CRRowFull>

            {shouldRenderReviews && this.state.showItemsCompleted.length > 0 && (
              <View style={styles.reviewContainer}>
                <VirtualListContainer
                  config={configComplete}
                  disabled={this.state.disabledCompleted}
                  reloadReviews={this.reloadReviews}
                />
              </View>
            )}
            {shouldRenderReviews &&
              this.state.showItemsCompleted.length === 0 &&
              this.state.loading !== true && (
                <View style={styles.reviewContainerEmpty}>
                  <Text>No tienes reseñas.</Text>
                </View>
              )}

            {shouldRenderPending && this.state.showItemsPending.length > 0 && (
              <View style={styles.reviewContainer}>
                <VirtualListContainer
                  config={configPending}
                  disabled={this.state.disabledPending}
                  reloadReviews={this.reloadReviews}
                />
              </View>
            )}

            {shouldRenderPending &&
              this.state.showItemsPending.length === 0 &&
              this.state.loading !== true && (
                <View style={styles.reviewContainerEmpty}>
                  <Text>No tienes reseñas pendientes.</Text>
                </View>
              )}
          </View>
        </View>
      );
    } catch (e) {
      Sentry.captureException(e);
    }
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  const { getUserReviews } = userActions;

  return bindActionCreators(
    {
      getUserReviews,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Reviews);
