import { StyleSheet } from 'react-native';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography, buttons } from '../../../assets';
import { rgba } from '@canastarosa/ds-theme/colors';
import { color } from 'react-native-reanimated';
const isAndroid = Platform.OS === 'android';

export const styles = StyleSheet.create({
  containerWrapper: {
    backgroundColor: '#FAF7F5',
  },
  sectionReviews: {
    marginTop: 5,
  },
  title: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  reviewsList: {
    padding: 15,
    marginVertical: 30,
    height: 500,
  },

  //CARD REVIEWS
  cover: {
    width: '100%',
    height: 100,
    resizeMode: 'cover',
    // backgroundColor: rgba('000000', 0.6),
  },
  product: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  storeName: {
    ...typography.txtLight,
    color: colors.colorWhite,
  },
  productName: {
    ...typography.txtSemiBold,
    color: colors.colorWhite,
  },
  review: {
    padding: 15,
  },
  reviewContainer: {
    backgroundColor: colors.colorWhite,
    //shadowColor: colors.colorGray400,
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    //shadowOpacity: 0.23,
    //shadowRadius: 2.62,
    elevation: 4,
    //marginBottom: 15,
    width: '100%',
    //height: '100%',
  },
  reviewContainerEmpty: {
    backgroundColor: colors.colorWhite,
    //shadowColor: colors.colorGray400,
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    //shadowOpacity: 0.23,
    //shadowRadius: 2.62,
    elevation: 4,
    //marginBottom: 15,
    paddingLeft: 15,
    paddingTop: 35,
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
    display: 'flex',

    width: '100%',
    height: '100%',
  },
  headerReview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    display: 'flex',
  },
  starContainer: {
    marginLeft: 5,
    flexDirection: 'row',
  },
  mainRatingInteractionContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: 20,
    display: 'flex',
  },
  ratingInteractionContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 15,
  },
  rating: {
    ...typography.txtRegular,
  },
  icon: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
  },
  iconInteraction: {
    width: 36,
    height: 36,
    marginRight: 5,
    resizeMode: 'contain',
  },
  date: {
    ...typography.txtRegular,
    color: colors.colorGray400,
  },
  titleReview: {
    ...typography.txtSemiBold,
    color: colors.colorDark300,
  },
  txtReview: {
    ...typography.txtLight,
  },

  //MENU
  menuContainer: {
    paddingHorizontal: 15,
    marginTop: 20,
  },
  divider: {
    borderBottomColor: colors.colorGray200,
    borderBottomWidth: 1,
    zIndex: -1,
  },
  activeDivider: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: -2,
    borderBottomColor: colors.colorTurquiose300,
    borderBottomWidth: 3,
    height: '50%',
    width: '90%',
  },
  menu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemMenu: {
    flex: 3,
    padding: 20,
  },
  name: {
    ...typography.txtLight,
    textAlign: 'center',
  },
  nameActive: {
    ...typography.txtSemiBold,
    fontSize: 16,
    color: colors.colorTurquiose300,
    textAlign: 'center',
    borderBottomWidth: 2,
    borderBottomColor: colors.colorTurquiose300,
  },
  btnReviews: {
    color: colors.colorMain300,
    ...typography.txtSemiBold,
    textAlign: 'center',
    marginTop: 15,
    marginBottom: 15,
  },

  //modal
  scrollKeyboardContainer: {
    width: '100%',
    height: '100%',
    paddingLeft: 30,
    paddingRight: 30,
    //padding: 20,
  },
  scrollContainer: {
    width: '100%',
    height: '100%',
  },
  scrollContainer2: {
    width: '100%',
    height: 500,
  },
  space: {
    width: '100%',
    height: 30,
  },
  title: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  modalView: {
    flex: 1,
    backgroundColor: colors.colorWhite,
    height: '100%',
  },
  container: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    bottom: 0,
    // padding: 20,
  },

  formAddress: {
    alignItems: 'center',
  },
  btnAdd: {
    ...buttons.square,
    width: '90%',
    borderColor: colors.colorMain300,
    alignItems: 'center',
    alignSelf: 'center',
  },
  btnName: {
    ...typography.txtRegular,
    color: colors.colorMain300,
    fontSize: 18,
  },
  btnIcon: {
    alignItems: 'flex-end',
    paddingTop: isAndroid ? 5 : 20,
    paddingBottom: isAndroid ? 5 : 10,
  },
  iconStyle: {
    color: colors.colorGray400,
    fontSize: 40,
    marginTop: isAndroid ? 5 : 30,
    marginRight: 20,
  },
  scrollViewStyle: {
    flexGrow: 1,
  },
  ///input
  addressFormContainer: {
    width: '100%',
    padding: 30,
    paddingBottom: 200,
  },
  searchContainer: {
    zIndex: 99999,
    elevation: 9999,
    position: 'relative',
  },
  containerForm: {
    marginTop: 30,
    width: '90%',
    flexGrow: 1,
  },
  scrollViewStyle: {
    flexGrow: 1,
  },
  divider: {
    borderBottomColor: colors.colorGray200,
    borderBottomWidth: 1,
    zIndex: -1,
    marginVertical: 20,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  middle: {
    flex: 2,
  },
  middleMargin: {
    marginRight: 10,
  },
  label: {
    ...typography.txtRegular,
    fontSize: 13,
    color: colors.colorDark100,
  },
  field: {
    marginBottom: 10,
    marginTop: 10,
  },
  inputForm: {
    height: 50,
    borderColor: colors.colorGray200,
    borderRadius: 5,
    borderWidth: 1,
    color: colors.colorDark300,
    ...typography.txtLight,
    fontSize: 16,
    paddingHorizontal: 10,
  },
  inputAreaForm: {
    height: 150,
    borderColor: colors.colorGray200,
    borderRadius: 5,
    borderWidth: 1,
    color: colors.colorDark300,
    ...typography.txtLight,
    fontSize: 16,
    paddingHorizontal: 10,
  },
  btnSave: {
    ...typography.txtRegular,
    fontSize: 18,
    color: colors.colorMain300,
    marginTop: 15,
    textAlign: 'center',
  },
  btnSaveDisable: {
    ...typography.txtRegular,
    fontSize: 18,
    color: colors.colorGray300,
    marginTop: 15,
    textAlign: 'center',
  },
});
