import React, { Component } from 'react';
import * as Sentry from '@sentry/react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { formatDate, formatDateToUTC } from '../../../utils/date';
import { formatNumberToPrice } from '../../../utils/normalizePrice';
import { Field, reduxForm } from 'redux-form';
import { required } from '../../../utils/formValidators';
import {
  Image,
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  Alert,
  Modal,
  KeyboardAvoidingView,
  TextInput,
} from 'react-native';
import { userActions } from '../../../redux/actions';

import Icon from 'react-native-vector-icons/Ionicons';

import { styles } from './ReviewsStyles';
import { formatTitle } from '../../../utils/formatTitle';

import star from '../../../images/icon/rating.png';
import disabledStar from '../../../images/icon/disabledRating.png';
import { CRIcon } from '../../CRIcon';
import CRCategoryCard from '../../../components/CRCard/CRCategoryCard/CRCategoryCard';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import style from '../../CRCard/CRCard/style';
//Google Analytics
import analytics from '@react-native-firebase/analytics';

/**--------------------------------------
 * InputField Class Component
 --------------------------------------*/
const InputField = ({
  keyboardType,
  meta: { touched, error, warning },
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
}) => {
  return (
    <View>
      <TextInput
        style={styles.inputForm}
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray300}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
      />
      {touched && error && (
        <CRText variant={typography.caption} color={colors.colorRed300}>
          {error}
        </CRText>
      )}
    </View>
  );
};

/**--------------------------------------
 * InputField Class Component
 --------------------------------------*/
const InputAreaField = ({
  keyboardType,
  meta: { touched, error, warning },
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
}) => {
  return (
    <View>
      <TextInput
        style={styles.inputAreaForm}
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray300}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
        multiline={true}
        numberOfLines={6}
      />
      {touched && error && (
        <CRText variant={typography.caption} color={colors.colorRed300}>
          {error}
        </CRText>
      )}
    </View>
  );
};

class CardReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      shouldRenderPending: false,
      shouldRenderReviews: true,
      shouldModal: false,
      selectedReview: {},
      scoreProduct: 0,
      scoreLogistics: 0,
    };
  }

  closeModal = () => {
    this.setState({
      ...this.state,
      shouldModal: false,
      selectedReview: {},
      scoreLogistics: 0,
      scoreProduct: 0,
    });
  };

  createReview = async (review) => {
    await this.setState({
      ...this.state,
      selectedReview: review,
      shouldModal: true,
    });
  };

  setInteractionScore = (num, type = 'product', max = 5) => {
    try {
      let score = [];
      let currentScore = 1;
      let i = 0;
      if (num > 5) {
        num = 5;
      }
      for (let i = 0; i < num; i++) {
        let sendValue = currentScore;
        score.push(
          <TouchableOpacity onPress={() => this.setNewScore(sendValue, type)}>
            {/* <CRIcon name="star" size={16} color={colors.colorYellow300} />, */}
            <Image source={star} style={styles.iconInteraction} />
          </TouchableOpacity>,
        );
        currentScore++;
      }
      let remaining = max - Number(num);
      if (remaining) {
        for (let i = 0; i < remaining; i++) {
          let sendValue = currentScore;
          score.push(
            <TouchableOpacity onPress={() => this.setNewScore(sendValue, type)}>
              {/* <CRIcon name="star--hollow" size={16} color={colors.colorYellow300}/> */}
              <Image source={disabledStar} style={styles.iconInteraction} />
            </TouchableOpacity>,
          );
          currentScore++;
        }
      }

      return score;
    } catch (e) {
      Sentry.captureException(e);
    }
  };
  setNewScore(num, type) {
    try {
      if (type === 'product') {
        this.setState({
          ...this.state,
          scoreProduct: num,
        });
      } else {
        this.setState({
          ...this.state,
          scoreLogistics: num,
        });
      }
    } catch (e) {
      Sentry.captureException(e);
    }
  }

  submit = async (e) => {
    try {
      const sendReview = {
        ...this.state.selectedReview,
        title: e.titleReview,
        comment: e.bodyReview,
        product_score: this.state.scoreProduct,
        logistics_score: this.state.scoreLogistics,
        order_product: this.state.selectedReview.id,
      };

      this.props.submitProductReview(sendReview).then(() => {
        this.closeModal();
        this.props.reloadReviews();
      });
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  setScore = (num, max = 5) => {
    try {
      let score = [];
      if (num > 5) {
        num = 5;
      }
      for (let i = 0; i < num; i++) {
        score.push(
          <CRIcon name="star" size={16} color={colors.colorYellow300} />,
        );
      }
      let remaining = max - Number(num);
      if (remaining) {
        for (let i = 0; i < remaining; i++) {
          score.push(
            <CRIcon
              name="star--hollow"
              size={16}
              color={colors.colorYellow300}
            />,
          );
        }
      }

      return score;
    } catch (e) {
      Sentry.captureException(e);
    }
  };
  render() {
    let review = this.props.review;
    let key = review.id;
    let type = this.props.type;

    const {
      handleSubmit,
      submit,
      initialValues,
      btnAction,
      pristine,
      submitting,
      valid,
    } = this.props;

    try {
      if (review !== undefined) {
        return (
          <View style={styles.reviewContainer} key={key}>
            <CRCategoryCard
              category={formatTitle(review.product.store.name)}
              thumbImg={review.product.photo.medium}
              size="full"
            />
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.shouldModal}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}
            >
              <View style={styles.modalView}>
                <View style={styles.container}>
                  <TouchableOpacity
                    onPress={this.closeModal}
                    style={styles.btnIcon}
                    activeOpacity={0.5}
                  >
                    <Icon name="ios-close" style={styles.iconStyle} />
                  </TouchableOpacity>
                  <KeyboardAvoidingView
                    behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
                    enabled
                    // contentContainerStyle={styles.scrollViewStyle}
                    // resetScrollToCoords={{ x: 0, y: 0 }}
                    // scrollEnabled
                    style={styles.scrollKeyboardContainer}
                  >
                    <ScrollView
                      keyboardShouldPersistTaps="always"
                      style={styles.scrollContainer}
                    >
                      <CRText
                        variant={typography.subtitle2}
                        color={colors.colorDark200}
                        align="center"
                      >
                        ¿Qué opinas de{' '}
                        {this.state.shouldModal &&
                          this.state.selectedReview.product.name}
                        ?
                      </CRText>
                      <View style={styles.space}></View>
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark300}
                      >
                        Tus comentarios ayudan a otros compradores a encontrar
                        los mejores artículos y a las tiendas a mejorar sus
                        productos.
                      </CRText>
                      <View style={styles.space}></View>

                      <View style={styles.mainRatingInteractionContainer}>
                        <CRText
                          variant={typography.bold}
                          color={colors.colorDark300}
                        >
                          ¿Cómo calificas este producto?
                        </CRText>
                        <View style={styles.ratingInteractionContainer}>
                          {this.setInteractionScore(
                            this.state.scoreProduct,
                            'product',
                          )}
                        </View>
                      </View>
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark300}
                      >
                        Cuéntanos más de tu experiencia:
                      </CRText>
                      <View style={styles.space}></View>
                      <Field
                        name="titleReview"
                        component={InputField}
                        placeholder="Título de tu reseña"
                        validate={[required]}
                      />
                      <View style={styles.space}></View>
                      <Field
                        name="bodyReview"
                        component={InputAreaField}
                        placeholder="¿Qué te pareció este producto? ¿Lo disfrutaste? ¿Qué podría mejorar?"
                        validate={[required]}
                      />
                      <View style={styles.space}></View>

                      <View style={styles.mainRatingInteractionContainer}>
                        <CRText
                          variant={typography.bold}
                          color={colors.colorDark300}
                        >
                          ¿Cómo calificas la logística de Canasta Rosa?
                        </CRText>
                        <View style={styles.ratingInteractionContainer}>
                          {this.setInteractionScore(
                            this.state.scoreLogistics,
                            'logistics',
                          )}
                        </View>
                      </View>

                      <TouchableOpacity
                        onPress={handleSubmit(this.submit)}
                        disabled={pristine || submitting || !valid}
                      >
                        <Text
                          style={
                            pristine || submitting || !valid
                              ? styles.btnSaveDisable
                              : styles.btnSave
                          }
                        >
                          Enviar reseña
                        </Text>
                      </TouchableOpacity>
                    </ScrollView>
                  </KeyboardAvoidingView>
                </View>
              </View>
            </Modal>

            <View style={styles.review}>
              <View style={styles.headerReview}>
                {type === 'completed' && (
                  <View style={styles.ratingContainer}>
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorGray400}
                    >
                      {review.review.product_score}
                    </CRText>
                    <View style={styles.starContainer}>
                      {this.setScore(review.review.product_score)}
                    </View>
                  </View>
                )}

                <View>
                  <CRText
                    variant={typography.paragraph}
                    color={colors.colorDark200}
                  >
                    Realizada:
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorDark200}
                    >
                      &nbsp;
                      {formatDate(
                        formatDateToUTC(new Date(review.order.created)),
                      )}
                    </CRText>
                  </CRText>
                </View>
              </View>
              <View>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorDark200}
                >
                  Orden:
                  <CRText
                    variant={typography.paragraph}
                    color={colors.colorDark200}
                  >
                    &nbsp;{review.order.uid}
                  </CRText>
                </CRText>
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorDark200}
                >
                  Precio:
                  <CRText
                    variant={typography.paragraph}
                    color={colors.colorDark200}
                  >
                    &nbsp;{formatNumberToPrice(review.product.price)} MXN
                  </CRText>
                </CRText>
                {type === 'completed' && (
                  <React.Fragment>
                    <CRText
                      variant={typography.subtitle3}
                      color={colors.colorDark200}
                    >
                      Status:
                      <CRText
                        variant={typography.paragraph}
                        color={colors.colorDark200}
                      >
                        &nbsp;{review.review.is_approved && 'Aprobada'}
                        &nbsp;
                        {!review.review.is_approved && 'Pendiente de revisión'}
                      </CRText>
                    </CRText>
                    <CRText
                      variant={typography.subtitle3}
                      color={colors.colorDark200}
                    >
                      Comentario:
                    </CRText>
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorDark200}
                    >
                      {review.review.comment}
                    </CRText>
                  </React.Fragment>
                )}
              </View>
              {type === 'pending' && (
                <React.Fragment>
                  <TouchableOpacity
                    onPress={() => {
                      this.createReview(review);
                    }}
                    activeOpacity={0.5}
                  >
                    <Text style={styles.btnReviews}>Escribe una reseña</Text>
                  </TouchableOpacity>
                </React.Fragment>
              )}
            </View>
          </View>
        );
      } else {
        return null;
      }
    } catch (e) {
      Sentry.captureException(e);
      return null;
    }
  }
}

CardReview.defaultProps = {
  review: undefined,
  key: 0,
  type: 'completed',
};
// Wrap component within redux-form
const formName = 'createReviewForm';
CardReview = reduxForm({
  form: formName,
})(CardReview);

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { user } = state;
  return {
    completed: user.reviews.completed.results,
    pending: user.reviews.pending.results,
  };
}
function mapDispatchToProps(dispatch) {
  const { getUserReviews, addReview } = userActions;

  return bindActionCreators(
    {
      getUserReviews,
      submitProductReview: addReview,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(CardReview);
