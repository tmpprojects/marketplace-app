import React, { Component } from 'react';
import { View } from 'react-native';
import * as Sentry from '@sentry/react-native';
import { styles } from './styles';
import NewPaymentMethod from './NewPaymentMethod';
import { ScrollView } from 'react-native-gesture-handler';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { userActions } from '../../../redux/actions';
import { getCardsList } from '../../../redux/selectors/users.selectors';
import { Field, reduxForm } from 'redux-form';
import { FadeInView } from '../../../utils/animations/FadeInView';
import PaymentCard from './PaymentCard';

import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography } from '@canastarosa/ds-theme/typography';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import Loader from '../../../components/Loader';
import { CRTitle } from '../../CRTitle';
//Google Analytics
import analytics from '@react-native-firebase/analytics';
import AsyncStorage from '@react-native-community/async-storage';

declare var globalThis: any;
export class PaymentMethod extends Component {
  constructor() {
    super();
    this.state = {
      checked: false,
    };
    this.onDeleteCard = this.onDeleteCard.bind(this);
    this.makeDefaultCard = this.makeDefaultCard.bind(this);
  }

  async componentDidMount() {
    await analytics().setCurrentScreen('PaymentMethodsScreen');
    await analytics().logEvent('PaymentMethodsScreen');
  }

  async onDeleteCard(cardId) {
    await this.props
      .deleteUserCard(cardId)
      .then((response) => {
        this.props.getUserCards();
        AsyncStorage.removeItem('paymentMethodSaved');
        //this.props.getCurrentUser();
      })
      .catch((e) => {
        Sentry.captureException(e);
        throw e;
      });
  }

  makeDefaultCard(cardId) {
    this.props
      .setDefaultUserCard(cardId)
      .then((response) => {
        this.props.getUserCards();
        //this.props.getCurrentUser();
      })
      .catch((error) => {
        console.log(error);
        throw error;
      });
  }

  render() {
    const { cards, cardsLoad, defaultCard, userLoad } = this.props;
    return (
      <View style={styles.paymentMethodsWrapper}>
        <React.Fragment>
          <CRTitle title="Mis métodos de pago" />
          <NewPaymentMethod />
          <ScrollView>
            {cards.length === 0 ? (
              <View style={styles.message}>
                <CRText
                  variant={typography.paragraph}
                  color={colors.colorGray400}
                  align="center"
                >
                  Aún no tienes tarjetas guardadas
                </CRText>
              </View>
            ) : !cardsLoad.loading ? (
              !defaultCard.loading ? (
                <React.Fragment>
                  <FadeInView>
                    {cards.map((card) => (
                      <View key={card.id}>
                        <Field
                          id={`card_${card.id}`}
                          name="default_card"
                          card={card}
                          //defaultCard={defaultCard.stripe.default_card_id}
                          value={card.id}
                          deleteUserCard={this.onDeleteCard}
                          setDefaultUserCard={this.makeDefaultCard}
                          component={PaymentCard}
                        />
                      </View>
                    ))}
                  </FadeInView>
                </React.Fragment>
              ) : (
                <Loader />
              )
            ) : (
              <Loader />
            )}
          </ScrollView>
        </React.Fragment>
      </View>
    );
  }
}

PaymentMethod = reduxForm({
  form: 'CardsForm',
  enableReinitialize: true,
  destroyOnUnmount: false,
})(PaymentMethod);

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { user } = state;
  return {
    cards: getCardsList(state),
    cardsLoad: user.cards,
    //userLoad: user.profile.loading,
    defaultCard: user.profile,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getUserCards: userActions.getUserCards,
      deleteUserCard: userActions.deleteUserCard,
      setDefaultUserCard: userActions.setDefaultUserCard,
      getCurrentUser: userActions.getCurrentUser,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(PaymentMethod);
