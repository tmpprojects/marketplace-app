/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Alert, View, TouchableOpacity, Modal, Text } from 'react-native';
import { PaymentCardTextField } from 'tipsi-stripe';
import stripe from 'tipsi-stripe';
import { styles } from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { config } from './../../../../env';

import { userActions } from '../../../redux/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getCardsList } from '../../../redux/selectors/users.selectors';
import * as Sentry from '@sentry/react-native';

stripe.setOptions({
  publishableKey: config.STRIPE_PK,
});

export class NewPaymentMethod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      valid: false,
      loading: false,
      error: null,
      params: {
        number: '',
        expMonth: 0,
        expYear: 0,
        cvc: '',
      },
    };
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  handleFieldParamsChange = (valid, params) => {
    this.setState({
      valid,
      params,
    });
  };

  handleCustomPayPress = async () => {
    this.openCardForm()
      .then((response) => {
        {
          this.props
            .addUserCard({ stripe_card_token: response.tokenId })
            .then(() => {
              this.props.getUserCards();
              this.props.getCurrentUser();
            })
            .catch((e) => {
              Sentry.captureException(e);

              Alert.alert(
                'No se pudo agregar la tarjeta,',
                'Lo sentimos, pero por un problema ajeno a nosotros, no pudimos agregar tu tarjeta.',
                [
                  {
                    text: 'Aceptar',
                    //onPress: () => this.props.navigation.goBack(null),
                  },
                ],
                { cancelable: false },
              );
              throw e;
            });
        }
      })
      .catch((e) => {
        Sentry.captureException(e);
        throw e;
      });
    return;
  };

  openCardForm = () => {
    return stripe.paymentRequestWithCardForm();
  };

  render() {
    return (
      <View>
        <TouchableOpacity
          style={styles.btnAdd}
          onPress={() => {
            this.handleCustomPayPress();
          }}
        >
          <Text style={styles.btnName}>Agregar método de pago</Text>
        </TouchableOpacity>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
        >
          <View style={styles.modalView}>
            <View style={styles.container}>
              <TouchableOpacity
                onPress={() => {
                  this.setModalVisible(false);
                }}
                style={{ alignItems: 'flex-end' }}
                activeOpacity={0.5}
              >
                <Icon
                  name="ios-close"
                  style={{
                    color: '#898989',
                    fontSize: 40,
                  }}
                />
              </TouchableOpacity>
              <View style={styles.formPayment}>
                <Text style={styles.title}>Nuevo método de pago</Text>
                <View style={styles.containerForm}>
                  <View style={styles.row}>
                    <View style={styles.field}>
                      <Text style={styles.label}>Número de Tarjeta</Text>
                      <PaymentCardTextField
                        accessible={false}
                        style={styles.field}
                        onParamsChange={this.handleFieldParamsChange}
                        numberPlaceholder="XXXX XXXX XXXX XXXX"
                        expirationPlaceholder="MM/YY"
                        cvcPlaceholder="CVC"
                      />
                    </View>
                  </View>
                  <TouchableOpacity onPress={this.handleCustomPayPress}>
                    <Text style={styles.btnSave}>Guardar método de pago</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    cards: getCardsList(state),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addUserCard: userActions.addUserCard,
      getUserCards: userActions.getUserCards,
      getCurrentUser: userActions.getCurrentUser,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(NewPaymentMethod);
