import { StyleSheet } from 'react-native';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography, buttons } from '../../../assets';

export const styles = StyleSheet.create({
  paymentMethodsWrapper: {
    flex: 1,
    backgroundColor: '#FAF7F5',
  },
  title: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  cardsList: {
    padding: 15,
    // marginVertical: 30,
  },
  message: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  //CARD
  card: {
    backgroundColor: colors.colorWhite,
    borderColor: colors.colorGray100,
    borderWidth: 1,
    borderRadius: 10,
    padding: 15,
  },
  headerCard: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  cardContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    marginLeft: 24,
  },
  cardNumber: {
    ...typography.txtRegular,
    marginLeft: 15,
  },
  validation: {
    ...typography.txtLight,
  },
  owner: {
    ...typography.txtSemiRegular,
    marginTop: 15,
    marginLeft: 32,
    fontSize: 16,
  },
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: -20,
  },
  checkedIcon: {
    color: colors.colorMain300,
    fontSize: 25,
  },
  uncheckedIcon: {
    color: colors.colorGray400,
    fontSize: 25,
  },
  default: {
    ...typography.txtLight,
    paddingBottom: 2,
  },

  //WINDOW FORM
  modalView: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    height: '100%',
  },
  container: {
    backgroundColor: colors.colorWhite,
    width: '100%',
    height: '60%',
    position: 'absolute',
    bottom: 0,
    padding: 20,
  },
  formPayment: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerForm: {
    marginTop: 30,
    width: '85%',
  },
  field: {
    margin: 10,
  },
  label: {
    ...typography.txtRegular,
    fontSize: 13,
    color: colors.colorDark100,
  },
  inputForm: {
    height: 50,
    borderBottomColor: colors.colorGray200,
    borderTopColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderWidth: 1,
    color: colors.colorDark300,
    marginBottom: 10,
    ...typography.txtLight,
    fontSize: 18,
  },
  btnAdd: {
    ...buttons.square,
    width: '90%',
    borderColor: colors.colorMain300,
    backgroundColor: colors.colorWhite,
    alignItems: 'center',
    alignSelf: 'center',
  },
  btnName: {
    ...typography.txtRegular,
    color: colors.colorMain300,
    fontSize: 18,
  },
  btnSave: {
    ...typography.txtRegular,
    fontSize: 18,
    color: colors.colorMain300,
    marginTop: 30,
    textAlign: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  middle: {
    flex: 2,
  },
});
