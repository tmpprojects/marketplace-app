import React, { Component } from 'react';
import { Image, View, TouchableOpacity, Text, Alert } from 'react-native';

import { styles } from './styles';
import trash from '../../../images/icon/trashcan.png';
import cardMC from '../../../images/icon/icon_mastercard.png';
import cardV from '../../../images/icon/visaCard.png';
import { CheckBox } from 'react-native-elements';
import { CRIcon } from '../../CRIcon';
import { colors } from '@canastarosa/ds-theme/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { _getPaymentMethodSaved } from './../../../utils/asyncStorage';

export default class PaymentCard extends Component {
  constructor() {
    super();
    this.state = {
      checked: false,
    };
  }

  async componentDidMount() {
    this.setState({
      paymentMethodSaved: (await _getPaymentMethodSaved()).paymentMethodSaved,
    });
  }

  render() {
    const {
      card,
      defaultCard,
      deleteUserCard,
      setDefaultUserCard = () => false,
    } = this.props;
    const onRemoveCard = function () {
      Alert.alert(
        '¿Estás seguro de querer eliminar esta tarjeta?',
        'Esta acción no podrá deshacerse.',
        [
          {
            text: 'Cancelar',
            onPress: () => null,
            style: 'cancel',
          },
          { text: 'OK', onPress: () => deleteUserCard(card.id) },
        ],
        { cancelable: false },
      );
    };
    return (
      <View style={styles.cardsList}>
        <View style={styles.card}>
          {/* {defaultCard === card.id ?
              <CRText
              variant={typography.paragraph}
              color={colors.colorMain300}
              align="center"
            >
              Tarjeta principal{"\n"}
            </CRText> :
              <Text/>
            } */}
          <View style={styles.headerCard}>
            <View style={styles.cardContainer}>
              {card.card.brand === 'visa' ? (
                <Image source={cardV} />
              ) : (
                <Image source={cardMC} />
              )}
              <Text style={styles.cardNumber}>
                •••• •••• •••• {card.card.last4}
              </Text>
            </View>
            <View>
              <Text style={styles.validation}>
                {card.card.exp_month}/{card.card.exp_year}
              </Text>
            </View>
            {defaultCard === card.id ? (
              <Text />
            ) : (
              <View style={styles.btnsContainer}>
                <TouchableOpacity style={styles.btn} onPress={onRemoveCard}>
                  <Image source={trash} />
                </TouchableOpacity>
              </View>
            )}
          </View>
          {/* {defaultCard === card.id ?
              <Text/> :
              <View style={styles.checkboxContainer}>
              <CheckBox
                checkedIcon={
                  <Icon name={'md-checkbox'} style={styles.checkedIcon} />
                }
                uncheckedIcon={
                  <Icon
                    name={'md-square-outline'}
                    style={styles.uncheckedIcon}
                  />
                }
                checked={this.state.checked}
                onPress={() =>
                  setDefaultUserCard(card.id)
                }
              />
              <Text
                style={[
                  styles.default,
                  this.state.checked
                    ? { color: colors.colorMain300 }
                    : { color: colors.colorGray400 },
                ]}
              >
                Establecer como principal
              </Text>
            </View>
            } */}
        </View>
      </View>
    );
  }
}
