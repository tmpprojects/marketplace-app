import React, { Component } from 'react';
import type { Node } from 'react';
import { View, TouchableOpacity, Text, Alert } from 'react-native';
import type { AddressType } from '../../../../redux/types/AddressesTypes';
import AddressModal from '../AddressModal/AddressModal';
import { colors } from '@canastarosa/ds-theme/colors';
import { styles } from './AddressCardStyles';
import { CRIcon } from '../../../CRIcon';

type AddressCardProps = {
  active: boolean,
  address: AddressType,
  changeAddress: Function,
  deleteAddress: Function,
  updateAddress: Function,
  invalidAddress: Function,
  input: { onChange: Function, value: AddressType },
};
type AddressCardState = {
  loading: boolean,
  modalVisible: boolean,
};
class AddressCard extends Component<AddressCardProps, AddressCardState> {
  state = {
    loading: false,
    modalVisible: false,
  };
  constructor(props) {
    super(props);
  }

  toggleModal = (visible: boolean): void => {
    visible
      ? this.setState({ modalVisible: true })
      : this.setState({ modalVisible: false });
  };

  closeModalWindow = async (address: AddressType): void => {
    //FIX: Telephone
    if (Boolean(address.phone)) {
      await this.props.changeAddress(address);
      //await this.props.input.onChange(address);
      //this.props.closeModalWindow();
    }
  };

  onChangeAddress = async (address: AddressType): void => {
    if (Boolean(address.phone)) {
      this.props.changeAddress(address);
      this.props.input.onChange(address);
    } else {
      //FIX: telephone
      //The address doesn't have a valid phone
      this.setState({ modalVisible: true });
    }
  };

  makeDefault = (): Node => {
    const { address, active } = this.props;
    //
    return (
      <TouchableOpacity
        style={styles.addressContainer}
        onPress={(e) => {
          this.onChangeAddress(address);
        }}
      >
        <View>
          <View style={styles.wrapperAddress}>
            <Text style={styles.owner}>{address.client_name}</Text>
            <Text style={styles.phone}>{address.phone}</Text>
            <Text style={styles.address}>
              {address.street_address} {address.num_ext},&nbsp;
              {address.num_int ? `${address.num_int},` : ''}
              {address.neighborhood}, {address.city}, {address.zip_code}.
            </Text>
          </View>
          <View style={styles.wrapperCheck}>
            <View style={styles.checkboxContainer}>
              {!active ? (
                <React.Fragment>
                  <View style={styles.unselectedCheck}></View>
                  <Text style={styles.txtCheck}>
                    Establecer dirección de entrega
                  </Text>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <View style={styles.selectedCheck}>
                    <CRIcon
                      name={'check'}
                      size={12}
                      color={colors.colorWhite}
                    />
                  </View>
                  <Text style={styles.default}>Dirección de entrega</Text>
                </React.Fragment>
              )}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render(): Node {
    const {
      address,
      deleteAddress,
      updateAddress,
      invalidAddress,
      isGuest,
    } = this.props;
    const onRemoveAddress = function () {
      Alert.alert(
        '¿Estás seguro de querer eliminar esta dirección?',
        'Esta acción no podrá deshacerse.',
        [
          {
            text: 'Cancel',
            onPress: () => null,
            style: 'cancel',
          },
          { text: 'OK', onPress: () => deleteAddress(address.uuid) },
        ],
        { cancelable: false },
      );
    };

    return (
      <View style={styles.addressCard}>
        <View style={styles.headerCard}>
          <Text style={styles.addressName}>{address.address_name}</Text>
          <View style={styles.btnsContainer}>
            <TouchableOpacity style={styles.btn} onPress={onRemoveAddress}>
              <CRIcon name={'trash'} size={20} color={colors.colorDark100} />
            </TouchableOpacity>
            {!isGuest && (
              <TouchableOpacity onPress={this.toggleModal}>
                <CRIcon name={'pencil'} size={20} color={colors.colorDark100} />
              </TouchableOpacity>
            )}

            <AddressModal
              onSubmit={updateAddress}
              title="Editar dirección de entrega"
              initialValues={address}
              btnAction="Actualizar"
              modalVisible={this.state.modalVisible}
              toggleModal={this.toggleModal}
              closeModalWindow={this.closeModalWindow}
            />
          </View>
        </View>
        {this.makeDefault()}
      </View>
    );
  }
}
AddressCard.defaultProps = {
  active: false,
  changeAddress: (address: AddressType) => false,
  deleteAddress: (addressId: string) => false,
  updateAddress: (address: AddressType) => false,
  invalidAddress: (address: AddressType) => false,
};

export default AddressCard;
