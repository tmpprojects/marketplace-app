import { StyleSheet } from 'react-native';
import { typography } from '../../../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const styles = StyleSheet.create({
  addressCard: {
    backgroundColor: colors.colorWhite,
    borderColor: colors.colorGray100,
    borderWidth: 1,
    padding: 15,
    marginTop: 25,
    flexGrow: 1,
    alignContent: 'stretch',
  },
  addressContainer: {
    flexGrow: 1,
    justifyContent: 'space-between',
  },
  headerCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    marginRight: 24,
  },
  addressName: {
    ...typography.txtSemiBold,
    marginBottom: 8,
  },
  owner: {
    ...typography.txtSemiBold,
    fontSize: 16,
  },
  phone: {
    ...typography.txtLight,
    marginBottom: 8,
  },
  address: {
    ...typography.txtLight,
  },
  wrapperCheck: {
    flexGrow: 1,
  },
  checkboxContainer: {
    marginTop: 22,
    flexDirection: 'row',
  },
  checkedIcon: {
    color: colors.colorMain300,
    fontSize: 25,
  },
  uncheckedIcon: {
    color: colors.colorGray400,
    fontSize: 25,
  },
  unselectedCheck: {
    width: 20,
    height: 20,
    borderWidth: 2,
    borderColor: colors.colorGray400,
    borderRadius: 2,
  },
  selectedCheck: {
    width: 20,
    height: 20,
    borderWidth: 2,
    borderColor: colors.colorMain300,
    backgroundColor: colors.colorMain300,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  default: {
    ...typography.txtRegular,
    color: colors.colorMain300,
    paddingBottom: 2,
    marginLeft: 10,
  },
  txtCheck: {
    ...typography.txtRegular,
    color: colors.colorGray400,
    paddingBottom: 2,
    marginLeft: 10,
  },
  scrollViewStyle: {
    flexGrow: 1,
  },
});
