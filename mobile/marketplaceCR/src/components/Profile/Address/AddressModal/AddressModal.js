import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Modal,
  Alert,
  ScrollView,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';

import { styles } from './AddressModalStyles';
import AddressForm from '../AddressForm/AddressForm';

//DS
import { colors } from '@canastarosa/ds-theme/colors';
import { typography } from '@canastarosa/ds-theme/typography';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { CRIcon } from '../../../CRIcon';

type AddressModalProps = {
  title: string,
  initialValues: Object,
  modalVisible: boolean,
  onSubmit: Function,
  btnAction: Function,
  toggleModal: Function,
  closeModalWindow: Function,
};
export default class AddressModal extends Component<AddressModalProps> {
  toggleModal = () => {
    Alert.alert(
      'Dirección.',
      '¿Estás segura(o) de cancelar la edición?',
      [
        {
          text: 'No',
          onPress: () => null,
          style: 'cancel',
        },
        { text: 'Sí', onPress: () => this.props.toggleModal(false) },
      ],
      { cancelable: false },
    );
  };

  render() {
    const {
      title,
      btnAction,
      onSubmit,
      initialValues,
      closeModalWindow,
    } = this.props;
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.modalVisible}
        >
          <View style={styles.modalView}>
            <View style={styles.container}>
              <TouchableOpacity
                onPress={this.toggleModal}
                style={styles.btnIcon}
                activeOpacity={0.5}
              >
                <View style={styles.iconStyle}>
                  <CRIcon name="close" size={18} color={colors.colorGray400} />
                </View>
              </TouchableOpacity>
              <KeyboardAvoidingView
                behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
                enabled
                // contentContainerStyle={styles.scrollViewStyle}
                // resetScrollToCoords={{ x: 0, y: 0 }}
                // scrollEnabled
                style={styles.scrollKeyboardContainer}
              >
                <ScrollView
                  keyboardShouldPersistTaps="always"
                  style={styles.scrollContainer}
                >
                  <CRText
                    variant={typography.subtitle2}
                    color={colors.colorDark200}
                    align="center"
                  >
                    {title}
                  </CRText>
                  <View style={styles.formAddress}>
                    <AddressForm
                      onSubmit={async (e) => {
                        await onSubmit(e);

                        //FIX: Telephone
                        if (btnAction === 'Actualizar') {
                          await this.props.closeModalWindow(e);
                        }

                        this.props.toggleModal(false);
                      }}
                      btnAction={btnAction}
                      initialValues={initialValues}
                    />
                  </View>
                </ScrollView>
              </KeyboardAvoidingView>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
