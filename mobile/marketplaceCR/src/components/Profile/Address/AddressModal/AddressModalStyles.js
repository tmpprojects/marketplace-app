import { StyleSheet, Platform } from 'react-native';
import { typography, buttons } from '../../../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

const isAndroid = Platform.OS === 'android';

export const styles = StyleSheet.create({
  scrollKeyboardContainer: {
    width: '100%',
    height: '100%',
    //padding: 20,
  },
  scrollContainer: {
    width: '100%',
    height: '100%',
  },
  title: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  modalView: {
    flex: 1,
    backgroundColor: colors.colorWhite,
    height: '100%',
  },
  container: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    bottom: 0,
    // padding: 20,
  },

  formAddress: {
    alignItems: 'center',
  },
  btnAdd: {
    ...buttons.square,
    width: '90%',
    borderColor: colors.colorMain300,
    alignItems: 'center',
    alignSelf: 'center',
  },
  btnName: {
    ...typography.txtRegular,
    color: colors.colorMain300,
    fontSize: 18,
  },
  btnIcon: {
    alignItems: 'flex-end',
    paddingTop: isAndroid ? 5 : 20,
    paddingBottom: isAndroid ? 5 : 10,
  },
  iconStyle: {
    color: colors.colorGray400,
    fontSize: 40,
    marginTop: isAndroid ? 5 : 30,
    marginRight: 20,
  },
  scrollViewStyle: {
    flexGrow: 1,
  },
});
