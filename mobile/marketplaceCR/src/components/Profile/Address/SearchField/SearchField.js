import React, { Component } from 'react';
import { View } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { styles, searchInputStyle } from './SearchFieldStyles';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { CRIcon } from '../../../CRIcon';
export default class SearchField extends Component {
  handlePress = (details) => {
    const response = details;
    this.props.searchAddress(response);
  };
  render() {
    return (
      <View style={styles.searchBox}>
        {/* <Text style={styles.label}>Buscar tú dirección de entrega</Text> */}
        <GooglePlacesAutocomplete
          placeholder="Busca tu dirección por calle y colonia"
          placeholderTextColor={colors.colorMain300}
          minLength={2}
          autoFocus={false}
          returnKeyType={'search'}
          listViewDisplayed={false}
          fetchDetails={true}
          renderDescription={(row) => row.description}
          onPress={(data, details = null) => {
            this.handlePress(details);
          }}
          getDefaultValue={() => ''}
          query={{
            key: 'AIzaSyCFeV9zPq0UFEFZ_BHwMHyNBGrwte8MMF8',
            language: 'es', // language of the results
          }}
          styles={searchInputStyle}
          nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
        />
        <CRIcon
          name="search"
          size={20}
          color={colors.colorMain300}
          style={styles.iconSearch}
        />
      </View>
    );
  }
}
