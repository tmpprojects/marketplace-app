import { StyleSheet } from 'react-native';
import { typography } from '../../../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';
export const searchInputStyle = {
  container: {
    backgroundColor: colors.colorWhite,
  },
  description: {
    fontWeight: 'bold',
    color: colors.colorDark300,
  },
  predefinedPlacesDescription: {
    color: colors.colorGray300,
  },
  textInputContainer: {
    height: 50,
    width: '100%',
    backgroundColor: colors.color,
    borderTopColor: 'transparent',
    borderBottomColor: 'transparent',
  },
  textInput: {
    height: 50,
    width: '100%',
    ...typography.txtRegular,
    color: colors.colorDark300,
    borderWidth: 1,
    borderColor: colors.colorMain300,
  },
  // listView: {
  //   color: colors.colorDark300,
  //   position: 'absolute',
  //   ...typography.txtRegular,
  //   backgroundColor: colors.colorGray100,
  //   borderColor: colors.colorGray100,
  //   borderWidth: 1,
  // },

  listView: {
    color: colors.colorDark300,
    ...typography.txtRegular,
    //backgroundColor: colors.colorGray100,
    // position: 'absolute',
    // backgroundColor: 'red',
    borderColor: colors.colorGray300,
    borderWidth: 1,
    top: 10,
    marginTop: 50, // This right here - remove the margin top and click on the first result, that will work.
    backgroundColor: 'white',
    position: 'absolute', // and the absolute position.
    borderRadius: 8,
  },
};
export const styles = StyleSheet.create({
  searchBox: {
    justifyContent: 'center',
    width: '100%',
    marginBottom: 20,
    position: 'relative',
    zIndex: 999,
    elevation: 999,
  },
  iconSearch: {
    position: 'absolute',
    top: 20,
    right: 18,
    backgroundColor: colors.colorWhite,
  },
});
