import { StyleSheet } from 'react-native';
import { typography } from '../../../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { getDisplay } from 'react-native-device-info';

export const styles = StyleSheet.create({
  alertTelephone: {
    width: '100%',
    padding: 15,
    backgroundColor: colors.colorYellow200,
  },
  alertTelephoneBold: {
    fontWeight: 'bold',
  },
  alertTelephoneNormal: {
    fontWeight: 'normal',
  },
  addressFormContainer: {
    width: '100%',
    padding: 30,
    paddingBottom: 200,
  },
  searchContainer: {
    zIndex: 99999,
    elevation: 9999,
    position: 'relative',
  },
  dropDownWrapper: {
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    width: 21,
    height: 21,
    borderColor: colors.colorGray200,
    paddingLeft: 1,
    paddingTop: 2,
  },
  containerForm: {
    marginTop: 30,
    width: '90%',
    flexGrow: 1,
  },
  scrollViewStyle: {
    flexGrow: 1,
  },
  divider: {
    borderBottomColor: colors.colorGray200,
    borderBottomWidth: 1,
    zIndex: -1,
    marginVertical: 20,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  middle: {
    flex: 2,
  },
  middleMargin: {
    marginRight: 10,
  },
  label: {
    ...typography.txtRegular,
    fontSize: 13,
    color: colors.colorDark100,
  },
  invalidTelephone: {
    backgroundColor: colors.colorYellow200,
    height: 50,
    borderColor: colors.colorYellow400,
    borderRadius: 5,
    borderWidth: 0,
    color: colors.colorDark400,
    ...typography.txtLight,
    fontSize: 18,
    paddingHorizontal: 10,
  },
  field: {
    marginBottom: 10,
    marginTop: 10,
  },
  inputForm: {
    height: 50,
    borderColor: colors.colorGray200,
    borderRadius: 5,
    borderWidth: 1,
    color: colors.colorDark300,
    ...typography.txtLight,
    fontSize: 18,
    paddingHorizontal: 10,
  },
  btnSave: {
    ...typography.txtRegular,
    fontSize: 18,
    color: colors.colorMain300,
    marginTop: 15,
    textAlign: 'center',
  },
  btnSaveDisable: {
    ...typography.txtRegular,
    fontSize: 18,
    color: colors.colorGray300,
    marginTop: 15,
    textAlign: 'center',
  },
});

export const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: colors.colorGray200,
    borderRadius: 4,
    color: colors.colorDark300,
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: colors.colorGray200,
    borderRadius: 8,
    color: colors.colorDark300,
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
