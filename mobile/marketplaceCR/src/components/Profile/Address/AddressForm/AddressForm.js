import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RNPickerSelect from 'react-native-picker-select';
import * as Sentry from '@sentry/react-native';

import SearchField from '../SearchField/SearchField';
import { userActions } from '../../../../redux/actions';
import {
  required,
  name,
  phoneNumberValid,
  number,
  minLength,
  maxLength,
  numIntExt,
  password,
} from '../../../../utils/formValidators';
import { STATES_MEXICO } from '../../../../redux/constants/app.constants';
import { styles, pickerSelectStyles } from './AddressFormStyles';

//DS
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { CRIcon } from '../../../CRIcon';

/**--------------------------------------
 * Validation helper functions
 --------------------------------------*/
const streetValidation = (value) =>
  value && value.length < 3
    ? 'Escribe una dirección con mínimo 3 caracteres.'
    : undefined;
const stateValidation = (value) =>
  !value ? 'Selecciona tu estado.' : undefined;
const neighborhoodValidation = (value) =>
  !value ? 'Selecciona tu colonia.' : undefined;
const cityValidation = (value) =>
  !value ? 'Selecciona tu alcaldía o municipio.' : undefined;
const postalValidation = (value) =>
  value && value.length !== 5 ? 'Escribe tu código postal.' : undefined;
const phoneMinLength = (min) => (value) =>
  value && value.length < min
    ? `El número debe contener al menos 8 digitos.`
    : undefined;
const phoneNumber = phoneMinLength(8);
const extNumberMin = minLength(1);
const extNumberMax = maxLength(5);
const intNumberMin = minLength(1);
const intNumberMax = maxLength(5);
/**--------------------------------------
 * InputField Class Component
 --------------------------------------*/
const InputField = ({
  keyboardType,
  meta: { touched, error, warning },
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
  style,
}) => {
  return (
    <View>
      <TextInput
        style={
          style === 'invalidTelephone'
            ? styles.invalidTelephone
            : styles.inputForm
        }
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray300}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        {...restInput}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
        placeholderTextColor={
          style === 'invalidTelephone' ? 'gray' : colors.colorGray300
        }
      />
      {touched && error && (
        <CRText variant={typography.caption} color={colors.colorRed300}>
          {error}
        </CRText>
      )}
    </View>
  );
};

/**--------------------------------------
 * PickerField Class Component
 --------------------------------------*/
type PickerProps = {
  input: { value: any, items: Array<any>, onChange: Function },
  meta: { error: any, warning: any },
  items: {},
  placeholder: {},
  showErrorsOnTouch: boolean,
};
class PickerField extends Component<PickerProps, { selectedValue: any }> {
  state = {
    selectedValue: null,
  };

  constructor(props: PickerProps) {
    super(props);
    this.state = {
      selectedValue: this.props.input.value,
      loadList: false,
    };
  }

  componentWillReceiveProps = (nextProps: PickerProps) => {
    if (this.props.input.value !== nextProps.input.value) {
      this.setState({
        selectedValue: nextProps.input.value,
      });
    }
  };

  changeValue = (value) => {
    this.props.input.onChange(value);
    this.setState({
      selectedValue: value,
    });
  };

  //
  renderErrors = () => (
    <View>
      {(this.props.meta.error && <Text>{this.props.meta.error}</Text>) ||
        (this.props.meta.warning && <Text>{this.props.meta.warning}</Text>)}
    </View>
  );

  //
  render() {
    const {
      items,
      placeholder,
      showErrorsOnTouch = true,
      meta,
      input,
    } = this.props;
    const { selectedValue } = this.state;

    //
    return (
      <React.Fragment>
        <RNPickerSelect
          placeholder={placeholder}
          items={items}
          onValueChange={this.changeValue}
          style={{
            ...pickerSelectStyles,
            iconContainer: {
              top: 10,
              right: 12,
            },
          }}
          value={selectedValue}
          Icon={() => {
            return (
              <View style={styles.dropDownWrapper}>
                <CRIcon
                  name="angle--down"
                  size={8}
                  color={colors.colorGray300}
                />
              </View>
            );
          }}
        />
        {(!showErrorsOnTouch && meta.error && this.renderErrors()) ||
          (meta.touched && meta.error && this.renderErrors())}
      </React.Fragment>
    );
  }
}

/**--------------------------------------
 * AddressForm Class Component
 --------------------------------------*/
type AddressFormFieldsType = {
  client_name: ?string,
  phone: ?string,
  address_name: ?string,
  street_address: ?string,
  num_ext: ?string,
  num_int: ?string,
  zip_code: ?string,
  neighborhood: ?string,
  city: ?string,
  state: ?string,
  default_address: boolean,
  pickup_address: boolean,
  latitude: string,
  longitude: string,
};
type AddressFormProps = {
  initialValues: Object,
  change: Function,
  getAddressDetailsByZipCode: Function,
  handleSubmit: Function,
  submit: Function,
  btnAction: Function,
  pristine: boolean,
  submitting: boolean,
  valid: boolean,
};
type AddressFormState = {
  neighborhoodList: Array<Object>,
  municipalityList: Array<Object>,
  stateList: Array<Object>,
};
class AddressForm extends Component<AddressFormProps, AddressFormState> {
  state = {
    neighborhoodList: [],
    stateList: [],
    municipalityList: [],
  };
  componentDidMount() {
    if (this.props.initialValues) {
      this.updateLocalityFormFields(this.props.initialValues.zip_code);
    }
  }
  /**
   @name validatePostalCode()
   @desc Validates zip code format and then request for location details.
   @param zipCode: string | number
   */
  validatePostalCode = (zipCode): void => {
    /******  temporary fix  *******/
    // if (zipCode && zipCode.length >= 5) {
    //   return this.updateLocalityFormFields(zipCode);
    // }
    if (zipCode) {
      return this.updateLocalityFormFields(zipCode);
    }
  };

  /**
   * @name updateLocalityFormFields
   * @desc Takes a zip code and request the API for location details.
   * Then, update form values based on search results.
   * @param zipCode: string | Zip code to lookup.
   */
  updateLocalityFormFields = (zipCode: string): void => {
    // Request API for zip code details
    this.props
      .getAddressDetailsByZipCode(zipCode)

      // When request has finished, fill up address form with zip code details.
      .then((response) => {
        try {
          const locationDetails: {
            state: string,
            zip_code: string,
            municipality: string,
            settlements: Array<{
              id: number,
              name: string,
            }>,
          } = response.data;

          // Neighborhood

          const neighborhoodList = locationDetails.settlements.map(
            (item, i) => ({
              key: item.id,
              label: item.name,
              value: item.name,
            }),
          );
          this.props.change('neighborhood', neighborhoodList[0].value);

          // Municipality
          const municipality = [
            {
              key: 0,
              value: locationDetails.municipality,
              label: locationDetails.municipality,
            },
          ];
          this.props.change('city', municipality[0].value);

          // State
          let stateKey = Object.keys(STATES_MEXICO).find((key) => {
            return locationDetails.state === STATES_MEXICO[key];
          });
          let state = {
            key: 0,
            value: stateKey,
            label: stateKey ? STATES_MEXICO[stateKey] : null,
          };
          this.props.change('state', stateKey);

          // Update State.
          this.setState({
            neighborhoodList,
            municipalityList: municipality,
            stateList: [state],
          });
        } catch (e) {
          Sentry.captureException(e);
        }
      })

      // Notify users
      // if something went wrong with API request,
      .catch((e) => {
        Sentry.captureException(e);
      });
  };

  /**
   @name SearchAddress()
    @desc Takes a google maps place object
   and update redux form fields based on this information.
   @params place: GoogleMapsPlace
   */
  searchAddress = async (place): void => {
    await this.setState({
      ...this.state,
      loadList: true,
    });
    const { initialValues } = this.props;

    // Update lat/lng fields
    const latitude = place.geometry.location.lat.toFixed(8);
    await this.props.change('latitude', latitude);
    const longitude = place.geometry.location.lng.toFixed(8);
    await this.props.change('longitude', longitude);

    // Update 'default address' field
    await this.props.change(
      'pickup_address',
      initialValues ? initialValues.pickup_address : false,
    );
    await this.props.change(
      'default_address',
      initialValues ? initialValues.default_address : false,
    );

    // Define an undefined zip_code
    let zipCode;
    // Get each address component from 'place' object
    // and fill the corresponding field on the form.
    for (let i = 0; i < place.address_components.length; i++) {
      const addressType = place.address_components[i].types[0];
      const value = place.address_components[i].long_name;

      // Update redux form values
      switch (addressType) {
        // Exterior Number
        case 'street_number':
          this.props.change('num_ext', value);
          break;
        // Street
        case 'route':
          this.props.change('street_address', value);
          break;
        // State
        case 'administrative_area_level_1': {
          const localityList = Object.keys(STATES_MEXICO);
          const locality = localityList.find((s) => STATES_MEXICO[s] === value);
          //this.props.change('state', locality || 'DF');
          break;
        }
        // Country
        case 'country':
          this.props.change('country', { value: 'MX', display_name: 'México' });
          break;
        // Postal code
        case 'postal_code':
          zipCode = value;
          break;
        default:
      }
    }

    // Verify if 'place' return by Google, does not includes a zip_code
    // use current address zip_code as default.
    // if (!zipCode && initialValues) {
    //   zipCode = initialValues.zip_code;
    // }
    await this.props.change('zip_code', zipCode);
    // Based on the zip_code,
    // get location details (state, settlement, city, etc...)
    if (zipCode) {
      this.updateLocalityFormFields(zipCode);
    }
    await this.setState({
      ...this.state,
      loadList: false,
    });
  };

  /**
   * Render()
   */
  render() {
    const {
      handleSubmit,
      submit,
      initialValues,
      btnAction,
      pristine,
      submitting,
      valid,
    } = this.props;
    const { neighborhoodList, municipalityList, stateList } = this.state;
    const isIos = Platform.OS === 'ios';
    const isAndroid = Platform.OS === 'android';
    const SPACER_SIZE_IOS = 320;
    const SPACER_SIZE_ANDROID = 320;

    return (
      <View style={styles.addressFormContainer} onSubmit={handleSubmit}>
        {this.props.btnAction === 'Actualizar' && !Boolean(this.props.phone) && (
          <View style={styles.alertTelephone}>
            <Text style={styles.alertTelephoneBold}>
              Actualización requerida:
              <Text style={styles.alertTelephoneNormal}>
                {' '}
                Ingresa tu número de teléfono y haz clic en Actualizar.
              </Text>
            </Text>
          </View>
        )}

        <View style={styles.searchContainer}>
          <SearchField searchAddress={this.searchAddress} />
        </View>

        {!this.state.loadList && (
          <React.Fragment>
            <View style={styles.field}>
              <Field
                component={InputField}
                id="address1"
                name="street_address"
                validate={[required, streetValidation]}
                showErrorsOnTouch={!initialValues}
                placeholder="Calle*"
              />
            </View>

            <View style={styles.row}>
              <View style={[styles.field, styles.middle, styles.middleMargin]}>
                <Field
                  component={InputField}
                  id="exterior_number"
                  name="num_ext"
                  validate={[required, extNumberMin, extNumberMax, numIntExt]}
                  showErrorsOnTouch={!initialValues}
                  placeholder="Número Ext"
                />
              </View>
              <View style={[styles.field, styles.middle]}>
                <Field
                  component={InputField}
                  id="interior_number"
                  validate={[intNumberMin, intNumberMax, numIntExt]}
                  name="num_int"
                  placeholder="Número Int"
                />
              </View>
            </View>

            <View style={styles.field}>
              <Field
                component={InputField}
                id="zip_code"
                name="zip_code"
                validate={[required, postalValidation, number]}
                type="number"
                maxLength="5"
                onChange={(e, value) => this.validatePostalCode(value)}
                placeholder="Código Postal*"
              />
            </View>

            <View style={styles.field}>
              <Field
                id="neighborhood"
                name="neighborhood"
                iosHeader="Selecciona una Opción:"
                placeholder={{ label: 'Colonia*', value: null }}
                items={neighborhoodList}
                validate={[required, neighborhoodValidation]}
                component={PickerField}
                showErrorsOnTouch={!initialValues}
              />
            </View>

            <View style={styles.field}>
              <Field
                id="city"
                name="city"
                iosHeader="Selecciona una Opción:"
                placeholder={{ label: 'Alcaldía / Municipio*', value: null }}
                items={municipalityList}
                validate={[required, cityValidation]}
                component={PickerField}
                showErrorsOnTouch={!initialValues}
              />
            </View>

            <View style={styles.field}>
              <Field
                id="state"
                name="state"
                iosHeader="Selecciona una Opción:"
                placeholder={{ label: 'Estado*', value: null }}
                items={stateList}
                validate={[required, stateValidation]}
                component={PickerField}
                showErrorsOnTouch={!initialValues}
              />
            </View>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <View style={styles.field}>
                <Field
                  component={InputField}
                  id="client_name"
                  name="client_name"
                  validate={[required, name]}
                  placeholder="¿Quién va a recibir el pedido?*"
                  validate={[required]}
                  returnKeyType="done"
                />
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <View style={styles.field}>
                <Field
                  component={InputField}
                  id="phone"
                  name="phone"
                  validate={[required, phoneNumber]}
                  //keyboardType="number-pad"
                  //returnKeyType='done'
                  placeholder="Tu teléfono*"
                  style={
                    this.props.btnAction === 'Actualizar' &&
                    !Boolean(this.props.phone)
                      ? 'invalidTelephone'
                      : ''
                  }
                />
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <View style={styles.field}>
                <Field
                  component={InputField}
                  id="addressName"
                  S
                  name="address_name"
                  placeholder="Casa, Trabajo, Novi@, Sucursal... *"
                  validate={[required]}
                />
              </View>
            </TouchableWithoutFeedback>
            <TouchableOpacity
              onPress={submit}
              disabled={pristine || submitting || !valid}
            >
              <Text
                style={
                  pristine || submitting || !valid
                    ? styles.btnSaveDisable
                    : styles.btnSave
                }
              >
                {btnAction}
              </Text>
            </TouchableOpacity>
          </React.Fragment>
        )}
        {this.state.loadList && <Text>Cargando</Text>}

        {/* isIos && <View style={{ height: SPACER_SIZE_IOS }} /> */}
        {/* isAndroid && <View style={{ height: SPACER_SIZE_ANDROID }} /> */}
      </View>
    );
  }
}

// Wrap component within redux-form
const formName = 'newAddressForm';
AddressForm = reduxForm({
  form: formName,
})(AddressForm);

// Pass Redux state and actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  const { getAddressDetailsByZipCode } = userActions;
  return bindActionCreators(
    {
      getAddressDetailsByZipCode,
    },
    dispatch,
  );
}
// Export Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(AddressForm);
