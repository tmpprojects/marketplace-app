import React from 'react';
import { View, TouchableOpacity } from 'react-native';

import { style } from './style';

//Theme
import { colors } from '@canastarosa/ds-theme/colors';

const CRBottomButton = ({
  onPress,
  children,
  backgroundColor = 'colorMain300',
  attitude,
  customContainerBtn = false,
}) => {
  return (
    <View
      style={customContainerBtn ? style.customContainerBtn : style.containerBtn}
    >
      <TouchableOpacity
        style={[
          style.base,
          attitude === 'disabled'
            ? { backgroundColor: colors.colorGray200 }
            : { backgroundColor: colors.colorMain300 },
        ]}
        onPress={onPress}
        disabled={attitude === 'disabled' ? true : false}
      >
        {children}
      </TouchableOpacity>
    </View>
  );
};

export default CRBottomButton;
