import { StyleSheet, Platform, Dimensions } from 'react-native';
// DS
import { colors } from '@canastarosa/ds-theme/colors';

const alto = Dimensions.get('window').height;

export const style = StyleSheet.create({
  containerBtn: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: Platform.OS === 'android' ? 60 : alto < 737 ? 50 : 70,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    borderColor: colors.colorGray200,
    borderTopWidth: 1,
    paddingVertical: 15,
    backgroundColor: colors.colorWhite,
  },
  base: {
    alignSelf: 'stretch',
    paddingVertical: 15,
    marginHorizontal: 10,
    paddingHorizontal: 10,
    borderRadius: 3,
  },
  customContainerBtn: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,

    borderColor: colors.colorGray200,
    backgroundColor: colors.colorWhite,
    borderTopWidth: 1,
    paddingVertical: 15,
    height: 100,
    paddingTop: 10,
    //
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 8.3,
  },
});
