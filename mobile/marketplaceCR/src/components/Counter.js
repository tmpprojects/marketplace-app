import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';

//DS
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';

export default class Counter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      number: this.props.start,
    };

    // bind functions..
    this.onPressMinus = this.onPressMinus.bind(this);
    this.onPressPlus = this.onPressPlus.bind(this);
  }

  onPressMinus() {
    const { number } = this.state;
    const minusNumber = number - 1;

    if (number === this.props.min) {
      return;
    }

    return this.setState({ number: minusNumber }, () =>
      this.props.onChange(minusNumber, '-'),
    );
  }

  onPressPlus() {
    const { number } = this.state;
    const plusNumber = number + 1;

    if (number === this.props.max) {
      return;
    }

    return this.setState({ number: plusNumber }, () =>
      this.props.onChange(plusNumber, '+'),
    );
  }

  renderMinusButton() {
    const { min, touchableDisabledColor, touchableColor } = this.props;
    const isMinusDisabled = min === this.state.number;
    const buttonStyle = {
      borderColor: isMinusDisabled ? touchableDisabledColor : touchableColor,
    };

    return (
      <TouchableOpacity
        onPress={this.onPressMinus}
        activeOpacity={isMinusDisabled ? 0.9 : 0.2}
      >
        {this.props.minusIcon ? (
          this.props.minusIcon(
            isMinusDisabled,
            touchableDisabledColor,
            touchableColor,
          )
        ) : (
          <Text
            style={[
              Styles.iconText,
              {
                color: isMinusDisabled
                  ? touchableDisabledColor
                  : touchableColor,
              },
            ]}
          >
            -
          </Text>
        )}
      </TouchableOpacity>
    );
  }

  renderMinusAndroidButton() {
    const { min, touchableDisabledColor, touchableColor } = this.props;
    const isMinusDisabled = min === this.state.number;
    const buttonStyle = {
      borderColor: isMinusDisabled ? touchableDisabledColor : touchableColor,
    };

    return (
      <TouchableOpacity
        onPress={this.onPressMinus}
        activeOpacity={isMinusDisabled ? 0.9 : 0.2}
      >
        {!this.props.minusIcon ? (
          this.props.minusIcon(
            isMinusDisabled,
            touchableDisabledColor,
            touchableColor,
          )
        ) : (
          <Text
            style={[
              Styles.iconText,
              {
                color: isMinusDisabled
                  ? touchableDisabledColor
                  : touchableColor,
              },
            ]}
          >
            -
          </Text>
        )}
      </TouchableOpacity>
    );
  }

  renderPlusButton() {
    const { max, touchableDisabledColor, touchableColor } = this.props;
    const isPlusDisabled = max === this.state.number;
    const buttonStyle = {
      borderColor: isPlusDisabled ? touchableDisabledColor : touchableColor,
    };

    return (
      <TouchableOpacity
        onPress={this.onPressPlus}
        activeOpacity={isPlusDisabled ? 0.9 : 0.2}
      >
        {this.props.plusIcon ? (
          this.props.plusIcon(
            isPlusDisabled,
            touchableDisabledColor,
            touchableColor,
          )
        ) : (
          <Text
            style={[
              Styles.iconText,
              {
                color: isPlusDisabled ? touchableDisabledColor : touchableColor,
              },
            ]}
          >
            +
          </Text>
        )}
      </TouchableOpacity>
    );
  }

  renderPlusAndroidButton() {
    const { max, touchableDisabledColor, touchableColor } = this.props;
    const isPlusDisabled = max === this.state.number;
    const buttonStyle = {
      borderColor: isPlusDisabled ? touchableDisabledColor : touchableColor,
    };

    return (
      <TouchableOpacity
        onPress={this.onPressPlus}
        activeOpacity={isPlusDisabled ? 0.9 : 0.2}
      >
        {!this.props.plusIcon ? (
          this.props.plusIcon(
            isPlusDisabled,
            touchableDisabledColor,
            touchableColor,
          )
        ) : (
          <Text
            style={[
              Styles.iconText,
              {
                color: isPlusDisabled ? touchableDisabledColor : touchableColor,
              },
            ]}
          >
            +
          </Text>
        )}
      </TouchableOpacity>
    );
  }

  render() {
    const { number } = this.state;
    const isAndroid = Platform.OS === 'android';
    const isIOS = Platform.OS === 'ios';
    return (
      <View style={Styles.container}>
        {isIOS && <View>{this.renderMinusButton()}</View>}
        {isAndroid && <View>{this.renderMinusAndroidButton()}</View>}
        <View style={Styles.number}>
          <CRText variant={typography.subtitle}>{number}</CRText>
        </View>
        {isIOS && <View>{this.renderPlusButton()}</View>}
        {isAndroid && <View>{this.renderPlusAndroidButton()}</View>}
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },

  text: {
    ...typography.bold,
    fontSize: 16,
    paddingLeft: 30,
    paddingRight: 30,
  },

  iconText: {
    ...typography.subtitle3,
    fontSize: 22,
    marginTop: -3,
  },

  number: {
    width: 85,
    alignItems: 'center',
    justifyContent: 'center',
  },

  touchable: {
    width: 51,
    height: 51,
    borderWidth: 1,
    borderRadius: 300,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

Counter.propTypes = {
  start: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  onChange: PropTypes.func,

  textColor: PropTypes.string,
  touchableColor: PropTypes.string,
  touchableDisabledColor: PropTypes.string,

  minusIcon: PropTypes.func,
  plusIcon: PropTypes.func,
};

Counter.defaultProps = {
  start: 1,
  min: 1,
  max: 10,
  onChange(number, type) {
    // Number, - or +
  },

  textColor: '#43434C',
  touchableColor: colors.colorDark100,
  touchableDisabledColor: colors.colorGray100,

  minusIcon: null,
  plusIcon: null,
};
