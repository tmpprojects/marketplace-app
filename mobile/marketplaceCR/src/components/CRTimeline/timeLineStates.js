import React from 'react';
import { View } from 'react-native';
import { default as style } from './style';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRIcon } from '../CRIcon';

export const timeLineStates = {
  state0: {
    firstStepBullet: <View style={[style.circleIndicatorBase]} />,
    firstStepText: colors.colorGray400,
    secondStepBullet: <View style={[style.circleIndicatorBase]} />,
    secondStepText: colors.colorGray400,
    thirdStepBullet: <View style={[style.circleIndicatorBase]} />,
    thirdStepText: colors.colorGray400,
    timeLineProgress: '0%',
  },
  state1: {
    firstStepBullet: (
      <View style={[style.circleIndicatorBase, style.circleIndicatorCurrent]} />
    ),
    firstStepText: colors.colorMain300,
    secondStepBullet: <View style={[style.circleIndicatorBase]} />,
    secondStepText: colors.colorGray400,
    thirdStepBullet: <View style={[style.circleIndicatorBase]} />,
    thirdStepText: colors.colorGray400,
    timeLineProgress: '0%',
  },
  state2: {
    firstStepBullet: (
      <View style={[style.circleIndicatorBase, style.circleIndicatorActive]}>
        <CRIcon name="check" color={colors.colorMain300} size={10} />
      </View>
    ),
    firstStepText: colors.colorMain300,
    secondStepBullet: (
      <View style={[style.circleIndicatorBase, style.circleIndicatorCurrent]} />
    ),
    secondStepText: colors.colorMain300,
    thirdStepBullet: <View style={[style.circleIndicatorBase]} />,
    thirdStepText: colors.colorGray400,
    timeLineProgress: '47%',
  },
  state3: {
    firstStepBullet: (
      <View style={[style.circleIndicatorBase, style.circleIndicatorFinished]}>
        <CRIcon name="check" color={colors.colorTurquiose400} size={10} />
      </View>
    ),
    firstStepText: colors.colorTurquiose400,
    secondStepBullet: (
      <View style={[style.circleIndicatorBase, style.circleIndicatorFinished]}>
        <CRIcon name="check" color={colors.colorTurquiose400} size={10} />
      </View>
    ),
    secondStepText: colors.colorTurquiose400,
    thirdStepBullet: (
      <View style={[style.circleIndicatorBase, style.circleIndicatorFinished]}>
        <CRIcon name="check" color={colors.colorTurquiose400} size={10} />
      </View>
    ),
    thirdStepText: colors.colorTurquiose400,
    timeLineProgress: '100%',
  },
};
