import React from 'react';
import { View, Animated, Easing } from 'react-native';

import { default as style } from './style';
import { timeLineStates } from './timeLineStates';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';

//will be added to the ds

const CRTimeLine = ({ status, progress }) => {
  let timeLineConfig = {
    ...timeLineStates.state0,
  };

  switch (status) {
    case 'awaiting_payment':
      timeLineConfig = { ...timeLineStates.state0 };
      break;

    case 'awaiting_vendor_response':
      timeLineConfig = { ...timeLineStates.state1 };
      break;
    case 'preparing_order':
      timeLineConfig = { ...timeLineStates.state1 };
      break;
    case 'order_ready':
      timeLineConfig = { ...timeLineStates.state1 };
      break;
    case 'vendor_awaiting_shipment':
      timeLineConfig = { ...timeLineStates.state1 };
      break;
    case 'in_transit':
      timeLineConfig = { ...timeLineStates.state2 };
      break;

    case 'delivered':
      timeLineConfig = { ...timeLineStates.state3 };
      break;
    case 'other':
      timeLineConfig = { ...timeLineStates.state0 };
      break;
    case 'cancelled':
      timeLineConfig = { ...timeLineStates.state0 };
      break;

    default:
      timeLineConfig = { ...timeLineStates.state0 };
      break;
  }

  let progressValue = new Animated.Value(0);
  progressValue.setValue(0);
  Animated.timing(progressValue, {
    toValue: 100,
    duration: 450,
    delay: 350,
    easing: Easing.ease,
    useNativeDriver: true,
  }).start();

  const animated = {
    line: {
      width: timeLineConfig.timeLineProgress,
      backgroundColor: timeLineConfig.firstStepText,
      height: '100%',
    },
  };

  return (
    <View style={style.orderIndicator}>
      <View style={style.trackWrapper}>
        <View style={style.orderTrack}>
          <Animated.View style={animated.line}></Animated.View>
        </View>
      </View>
      <View style={style.orderIndicatorStep}>
        {timeLineConfig.firstStepBullet}
        <CRText
          variant={typography.paragraph}
          color={timeLineConfig.firstStepText}
        >
          Pagada
        </CRText>
      </View>
      <View style={style.orderIndicatorStep}>
        {timeLineConfig.secondStepBullet}
        <CRText
          variant={typography.paragraph}
          color={timeLineConfig.secondStepText}
        >
          En tránsito
        </CRText>
      </View>
      <View>
        <View style={style.orderIndicatorStep}>
          {timeLineConfig.thirdStepBullet}
          <CRText
            variant={typography.paragraph}
            color={timeLineConfig.thirdStepText}
          >
            Entregado
          </CRText>
        </View>
      </View>
    </View>
  );
};

export default CRTimeLine;
