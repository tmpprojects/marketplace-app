import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  orderIndicator: {
    backgroundColor: colors.colorWhite,
    borderBottomWidth: 1,
    borderBottomColor: colors.colorGray200,
    padding: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  orderIndicatorStep: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleIndicatorBase: {
    backgroundColor: colors.colorWhite,
    borderRadius: 50,
    width: 26,
    height: 26,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.colorGray200,
    marginBottom: 8,
  },
  circleIndicatorActive: {
    borderColor: colors.colorMain300,
  },
  circleIndicatorCurrent: {
    borderColor: colors.colorMain300,
    borderWidth: 8,
  },
  circleIndicatorFinished: {
    borderColor: colors.colorTurquiose400,
  },

  trackWrapper: {
    position: 'absolute',
    width: '115%',
    height: 87,
    paddingHorizontal: '15%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  orderTrack: {
    width: '100%',
    height: 2,
    backgroundColor: colors.colorGray200,
  },
});
