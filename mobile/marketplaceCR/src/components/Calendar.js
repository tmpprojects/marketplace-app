import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { colors } from '../assets';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import { CRIcon } from './CRIcon';

LocaleConfig.locales.es = {
  monthNames: [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ],
  monthNamesShort: [
    'Enr.',
    'Feb.',
    'Mar.',
    'Abr.',
    'May.',
    'Jun.',
    'Jul.',
    'Ago.',
    'Sept.',
    'Oct.',
    'Nov.',
    'Dic.',
  ],
  dayNames: [
    'Domingo',
    'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado',
  ],
  dayNamesShort: ['Dom.', 'Lun.', 'Mar.', 'Mier.', 'Jue.', 'Vie.', 'Sab.'],
  today: 'Hoy',
};
LocaleConfig.defaultLocale = 'es';

export default class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: undefined,
    };
  }
  render() {
    return (
      <View style={styles.calendarContainer}>
        <CRIcon name={'calendar'} size={26} color={colors.coreDarkGray} />
        <Text> {this.state.selected}</Text>
        <Calendar
          style={styles.calendar}
          hideExtraDays
          onDayPress={this.onDayPress}
          markedDates={{
            [this.state.selected]: {
              selected: true,
              disableTouchEvent: true,
              selectedDotColor: 'orange',
            },
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  calendarContainer: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  calendar: {
    marginBottom: 10,
    width: '90%',
  },
});
