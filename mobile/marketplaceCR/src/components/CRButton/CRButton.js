/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { TouchableOpacity } from 'react-native';
import { default as style } from './style';
import { typography } from '@canastarosa/ds-theme/typography';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { colors } from '@canastarosa/ds-theme/colors';

const CRButton = ({ children, variant, onPress, textColor, attitude }) => {
  var variantStyle = '';
  var variantDisabled = '';
  var finalStyle = '';
  var finalTextStyle = '';
  var disabled = '';
  switch (variant) {
    case 'whiteGhost':
      variantStyle = style.whiteGhost;
      variantDisabled = style.disabledGhost;
      break;

    case 'white':
      variantStyle = style.white;
      variantDisabled = style.disabled;
      break;

    case 'pink':
      variantStyle = style.pink;
      variantDisabled = style.disabled;
      break;

    case 'pinkGhost':
      variantStyle = style.pinkGhost;
      variantDisabled = style.disabledGhost;
      break;

    default:
      variantStyle = style.whiteGhost;
      variantDisabled = style.disabledGhost;
      break;
  }

  if (attitude === 'disabled') {
    finalStyle = variantDisabled;
    finalTextStyle = colors.colorGray400;
    disabled = true;
  } else {
    disabled = false;
    if (attitude === 'selected') {
    } else {
      finalStyle = variantStyle;
      finalTextStyle = textColor;
    }
  }

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[finalStyle, style.base]}
      disabled={disabled}
    >
      <CRText variant={typography.paragraph} color={finalTextStyle}>
        {children}
      </CRText>
    </TouchableOpacity>
  );
};

export default CRButton;
