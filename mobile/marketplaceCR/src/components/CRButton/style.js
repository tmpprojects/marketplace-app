/*

This file was created by Canasta Rosa
under the MIT license.

*/

import { StyleSheet } from 'react-native';
import { colors, rgba } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  base: {
    paddingVertical: 10,
    paddingHorizontal: 5,
    shadowRadius: 20,
    shadowColor: colors.colorDark400,
    shadowOpacity: 0,
    width: '100%',
    borderRadius: 3,
    borderWidth: 0.65,
    marginBottom: 20,
  },

  white: {
    borderColor: colors.colorWhite,
    backgroundColor: colors.colorWhite,
  },

  whiteGhost: {
    borderColor: colors.colorWhite,
  },

  pink: {
    borderColor: colors.colorMain300,
    backgroundColor: colors.colorMain300,
  },

  pinkGhost: {
    borderColor: colors.colorMain300,
  },

  /* disabled variants */
  disabled: {
    borderColor: colors.colorGray200,
    backgroundColor: colors.colorGray200,
  },

  disabledGhost: {
    borderColor: colors.colorGray400,
    backgroundColor: rgba(colors.colorDark400, 0.5),
  },

  /* Other variants */
  facebook: {
    borderColor: colors.colorBlue400,
    backgroundColor: colors.colorBlue400,
    paddingVertical: 10,
    paddingHorizontal: 5,
    shadowRadius: 20,
    shadowColor: colors.colorDark400,
    shadowOpacity: 1,
    minWidth: 200,
    borderRadius: 3,
    borderWidth: 0.65,
    height: 45,
  },
});
