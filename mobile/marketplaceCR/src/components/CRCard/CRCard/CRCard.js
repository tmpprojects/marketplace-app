/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { TouchableOpacity } from 'react-native';
import { CRThumb } from '@canastarosa/ds-reactnative/components/CRThumb';
import { default as style } from './style';

const CRCard = ({
  children,
  onPress,
  thumbImg,
  padding,
  size,
  marginBottom,
  attitude,
}) => {
  let thumbnail = null;
  let sizeType = style.card;
  if (thumbImg) {
    thumbnail = <CRThumb source={thumbImg} />;
  }

  if (padding) {
    sizeType = style.cardPadding;
  }

  switch (size) {
    case 'horizontal':
      sizeType = style.cardHorizontal;
      break;
    case 'order':
      sizeType = style.cardOrder;
      break;
    case 'product':
      sizeType = style.card;
      break;
    case 'half':
      sizeType = style.cardHalf;
      break;
    case 'full':
      sizeType = style.cardFull;
      break;
    case 'mini':
      sizeType = style.cardMini;
      break;
    case 'tag':
      sizeType = style.cardTag;
      break;
    default:
      break;
  }

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        style.base,
        sizeType,
        marginBottom ? { marginBottom: marginBottom } : false,
      ]}
      disabled={attitude === 'disabled' ? true : false}
    >
      {thumbnail}
      {children}
    </TouchableOpacity>
  );
};

export default CRCard;
