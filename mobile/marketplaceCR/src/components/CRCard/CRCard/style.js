/*

This file was created by Canasta Rosa
under the MIT license.

*/

import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  base: {
    backgroundColor: colors.colorWhite,
    borderRadius: 5,
    marginRight: 10,
    marginBottom: 0,
    borderColor: colors.colorGray100,
    borderWidth: 1,
  },

  card: {
    width: 170,
  },

  cardHalf: {
    flex: 1,
  },
  cardFull: {
    width: '100%',
  },
  cardFullNoMarg: {
    width: '100%',
    marginBottom: 0,
  },
  cardHorizontal: {
    width: 254,
  },
  cardMini: {
    width: 110,
  },
  cardPadding: {
    paddingHorizontal: 5,
    paddingVertical: 20,
    width: '50%',
  },
  details: {
    paddingTop: 14,
    paddingHorizontal: 7,
  },

  half: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imageRating: {
    height: 13,
    width: 13,
    resizeMode: 'contain',
  },
  add: {
    marginTop: 13,
    paddingBottom: 13,
  },
  cardTag: {
    width: 130,
  },

  //Order
  cardOrder: {
    backgroundColor: colors.colorWhite,
    width: '90%',
  },

  orderDetails: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  orderHalf: {
    paddingVertical: 14,
    width: '42%',
  },
  orderPinkLine: {
    backgroundColor: colors.colorMain,
    width: 6,
    borderTopStartRadius: 3,
    borderBottomLeftRadius: 3,
    marginRight: 14,
    height: '100%',
  },
  imageArrowRight: {
    height: 15,
    width: 12,
    resizeMode: 'contain',
    marginLeft: 13,
  },
});
