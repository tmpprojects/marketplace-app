/*

This file was created by Canasta Rosa
under the MIT license.

*/

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  baseWrapper: { borderRadius: 5 },
});
