/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { Dimensions } from 'react-native';
import { default as style } from './style';

//Components
import { CRThumb } from '../../CRThumb';
import { default as CRCard } from '../CRCard/CRCard';

const CRPromoCard = ({ onPress, thumbImg }) => {
  const bannerWidth = Math.round(Dimensions.get('window').width) - 20;
  const bannerHeight = bannerWidth * 0.15;

  return (
    <CRCard onPress={onPress} size={'full'}>
      <CRThumb
        source={{ uri: thumbImg }}
        imgHeight={bannerHeight}
        wrapperStyle={style.baseWrapper}
      />
    </CRCard>
  );
};

export default CRPromoCard;
