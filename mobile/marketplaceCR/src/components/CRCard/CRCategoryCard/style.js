import { StyleSheet } from 'react-native';
import { rgba } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  //Store
  categoryContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: rgba('000000', 0.6),
    height: 120,
    borderRadius: 5,
  },
  cover: {
    width: '100%',
    borderRadius: 5,
    position: 'absolute',
  },
});
