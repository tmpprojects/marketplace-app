/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { View } from 'react-native';

//Components
import { CRThumb } from '../../CRThumb';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { default as CRCard } from '../CRCard/CRCard';
import { default as style } from './style';

//Theme
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

const CRCategoryCard = ({
  onPress,
  thumbImg,
  category,
  size,
  marginBottom,
}) => {
  return (
    <CRCard
      onPress={onPress}
      size={size ? size : 'full'}
      marginBottom={marginBottom ? marginBottom : null}
    >
      <CRThumb
        source={{ uri: thumbImg }}
        imgHeight={size === 'tag' ? 52 : 120}
        wrapperStyle={style.cover}
      />
      <View style={style.categoryContainer}>
        <CRText
          variant={size === 'tag' ? typography.bold : typography.subtitle3}
          color={colors.colorWhite}
          align="center"
        >
          {category}
        </CRText>
      </View>
    </CRCard>
  );
};

export default CRCategoryCard;
