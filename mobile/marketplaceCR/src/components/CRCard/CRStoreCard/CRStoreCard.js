import React, { Component } from 'react';
import { View } from 'react-native';

//Components
import { CRThumb } from '../../CRThumb';
import { CRIcon } from '../../CRIcon';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { default as CRCard } from '../CRCard/CRCard';
import { default as style } from './style';

//Theme
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import placeHolder from '../../../images/assets/placeholderCover.jpg';
import placeHolderStore from '../../../images/assets/placeholderStores.jpg';

class CRStoreCard extends Component {
  //Default state
  state = {
    onPress: null,
    thumbImg: '',
    store: false,
    slogan: '',
    storeLogo: '',
    marginBottom: 0,
    size: 'horizontal',
    cardWidth: 0,
    rating: 10,
  };

  constructor(props) {
    super(props);

    this.state = {
      onPress: this.props.onPress,
      thumbImg: this.props.thumbImg,
      store: this.props.store,
      slogan: this.props.slogan,
      storeLogo: this.props.storeLogo,
      size: this.props.size ? this.props.size : 'horizontal',
      marginBottom: this.props.marginBottom ? this.props.marginBottom : 0,
      productTitle: this.props.productTitle,
      rating: this.props.rating,
    };
  }
  elipsis(text, width) {
    text = text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
    // ******* FIX: Correction of the problem when there are icons, use Array.from(string).length instead of string.length;
    const ratio = width / Array.from(text).length;
    const elipsis = width / 10;
    if (ratio < 10) {
      return text.substring(0, elipsis) + '...';
    } else {
      return text;
    }
  }

  getCardWidth(layout) {
    this.setState({ cardWidth: layout.width });
  }

  render() {
    return (
      <CRCard
        onPress={this.state.onPress}
        size={this.state.size ? this.state.size : 'horizontal'}
        marginBottom={this.state.marginBottom ? this.state.marginBottom : null}
      >
        <CRThumb
          source={{ uri: this.state.thumbImg }}
          wrapperStyle={[style.cover, style.coverPlaceHolder]}
          placeHolder={placeHolder}
          imgHeight={100}
        />
        <View style={style.storeLogo}>
          <CRThumb
            source={{ uri: this.state.storeLogo }}
            wrapperStyle={style.baseWrapper}
            imgHeight={50}
            placeHolder={placeHolderStore}
          />
        </View>
        <View
          style={style.storeContainer}
          onLayout={(event) => {
            this.getCardWidth(event.nativeEvent.layout);
          }}
        >
          <View style={style.storeDesc}>
            <CRText variant={typography.subtitle3} color={colors.colorDark300}>
              {this.elipsis(this.state.store, this.state.cardWidth)}
            </CRText>
            <CRText variant={typography.paragraph} color={colors.colorDark100}>
              {this.elipsis(this.state.slogan, this.state.cardWidth * 2)}
            </CRText>
          </View>
          {this.state.rating > 0 && (
            <View style={style.rating}>
              <CRText variant={typography.bold} color={colors.colorGray300}>
                {this.state.rating}
                <CRIcon name="star" size={13} color={colors.colorYellow300} />
              </CRText>
            </View>
          )}
        </View>
      </CRCard>
    );
  }
}

export default CRStoreCard;
