import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  //Store
  storeContainer: {
    paddingHorizontal: 10,
    height: 95,
    borderTopWidth: 1,
    borderTopColor: colors.colorGray100,
    width: '100%',
  },
  cover: {
    width: '100%',
    height: 100,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  coverPlaceHolder: {
    position: 'absolute',
  },
  storeLogo: {
    width: 50,
    height: 52,
    marginVertical: 24,
    marginHorizontal: 10,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: colors.colorGray200,
    backgroundColor: colors.colorWhite,
  },
  baseWrapper: {
    borderRadius: 3,
  },
  storeDesc: {
    height: 65,
  },
  rating: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
});
