/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { View } from 'react-native';

//Components
import { CRThumb } from '../../CRThumb';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { default as CRCard } from '../CRCard/CRCard';
import { default as style } from './style';
import placeHolder from '../../../images/assets/placeholderCollection.jpg';

//Theme
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

const CRHomeSectionCard = ({ onPress, thumbImg, name, size }) => {
  return (
    <CRCard onPress={onPress} size={size}>
      <CRThumb
        source={{ uri: thumbImg }}
        imgHeight={106}
        placeHolder={placeHolder}
      />
      <View style={style.categoryDetails}>
        <CRText
          variant={typography.paragraph}
          color={colors.colorDark300}
          align="center"
        >
          {name}
        </CRText>
      </View>
    </CRCard>
  );
};

export default CRHomeSectionCard;
