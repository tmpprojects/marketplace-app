/*

This file was created by Canasta Rosa
under the MIT license.

*/

import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  details: {
    marginTop: 10,
    paddingHorizontal: 7,
    marginBottom: 20,
    width: '100%',
  },

  half: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },

  discount: {
    backgroundColor: colors.colorTurquiose400,
    paddingVertical: 3,
    paddingHorizontal: 6,
    borderRadius: 5,
    position: 'absolute',
    top: '60%',
    left: '60%',
  },
  ratingContainer: {
    backgroundColor: colors.colorWhite,
    paddingVertical: 3,
    paddingHorizontal: 6,
    borderRadius: 5,
    position: 'absolute',
    top: '40%',
    right: 8,
  },
  visa: {
    height: 10,
    width: 33,
    resizeMode: 'contain',
  },
  shipping: {
    marginTop: 10,
  },
  priceMargin: {
    marginBottom: 10,
  },
  storeMargin: {
    marginTop: 5,
  },
  priceWithoutDiscount: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    fontSize: 15,
  },

  shippingText: { fontSize: 18 },
  discountText: { fontSize: 14 },
  productText: { fontSize: 18 },
  storeText: { fontSize: 16 },
});
