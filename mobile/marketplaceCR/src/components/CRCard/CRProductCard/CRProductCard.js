/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

//Components
import { CRThumb } from '../../CRThumb';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { CRCard } from '../CRCard';
import { default as style } from './style';

//Theme
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRIcon } from '../../CRIcon';
import { BottomNavigation } from 'react-native-paper';
import visaIMG from '../../../images/assets/VISALogo--blue.png';

class CRProductCard extends Component {
  //Default state
  state = {
    onPress: null,
    thumbImg: '',
    productTitle: false,
    store: false,
    price: '',
    priceDiscount: '',
    rating: false,
    size: 'card',
    marginBottom: 0,
    cardWidth: 0,
    discount: '',
    shippingMethod: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      onPress: this.props.onPress,
      thumbImg: this.props.thumbImg,
      store: this.props.store,
      price: this.props.price,
      shippingMethod: this.props.shippingMethod,
      priceWithoutDiscount: this.props.priceWithoutDiscount,
      discount: this.props.discount,
      rating: this.props.rating,
      size: this.props.size ? this.props.size : 'card',
      marginBottom: this.props.marginBottom ? this.props.marginBottom : 0,
      productTitle: this.props.productTitle,
    };
  }

  elipsis(text, width) {
    text = text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
    // ******* FIX: Correction of the problem when there are icons, use Array.from(string).length instead of string.length;
    const ratio = width / Array.from(text).length;
    const elipsis = width / 10;
    if (ratio < 10) {
      return text.substring(0, elipsis) + '...';
    } else {
      return text;
    }
  }

  showStore(store) {
    if (store) {
      return (
        <View style={style.storeMargin}>
          <CRText variant={typography.caption} color={colors.colorDark100}>
            <Text style={style.storeText}>
              {this.elipsis(store, this.state.cardWidth)}
            </Text>
          </CRText>
        </View>
      );
    } else {
      return null;
    }
  }

  getCardWidth(layout) {
    this.setState({ cardWidth: layout.width });
  }

  render() {
    return (
      <CRCard
        onPress={this.state.onPress}
        size={this.state.size}
        marginBottom={this.state.marginBottom}
      >
        <CRThumb source={this.state.thumbImg} imgHeight={171} />

        {this.state.rating && (
          <View style={style.ratingContainer}>
            <CRText variant={typography.paragraph} color={colors.colorDark300}>
              {this.state.rating}{' '}
              <CRIcon name="star" size={16} color={colors.colorYellow300} />
            </CRText>
          </View>
        )}

        <View
          style={style.details}
          onLayout={(event) => {
            this.getCardWidth(event.nativeEvent.layout);
          }}
        >
          {this.state.productTitle && (
            <CRText variant={typography.paragraph} color={colors.colorDark300}>
              <Text style={style.productText}>
                {this.elipsis(this.state.productTitle, this.state.cardWidth)}
              </Text>
            </CRText>
          )}
          {this.state.store && this.showStore(this.state.store)}

          {this.state.shippingMethod.find(
            (i) => i === 'express-moto' || i === 'express-car',
          ) && (
            <CRText
              variant={typography.paragraph}
              color={colors.colorViolet300}
            >
              Envío express
            </CRText>
          )}
          {!this.state.shippingMethod.find(
            (i) => i === 'express-moto' || i === 'express-car',
          ) &&
            this.state.shippingMethod.find((i) => i === 'standard') && (
              <CRText
                variant={typography.paragraph}
                color={colors.colorViolet300}
              >
                Envío nacional
              </CRText>
            )}

          {this.state.discount > 0 ? (
            <View style={style.half}>
              {this.state.priceWithoutDiscount && (
                <View>
                  <View style={style.priceMargin}>
                    <CRText
                      variant={typography.subtitle3}
                      color={colors.colorDark300}
                    >
                      {`$${this.state.price} MXN`}
                    </CRText>
                  </View>
                  <CRText
                    variant={typography.paragraph}
                    color={colors.colorGray300}
                  >
                    <Text style={style.priceWithoutDiscount}>
                      {`$${this.state.priceWithoutDiscount}`}
                    </Text>
                  </CRText>
                  {this.state.discount > 0 && this.state.discount && (
                    <View style={style.discount}>
                      <CRText
                        variant={typography.bold}
                        color={colors.colorWhite}
                      >
                        <Text style={style.discountText}>
                          -{parseInt(this.state.discount)}%
                        </Text>
                      </CRText>
                    </View>
                  )}
                </View>
              )}
            </View>
          ) : (
            <View style={style.half}>
              {this.state.price && (
                <CRText
                  variant={typography.subtitle3}
                  color={colors.colorDark300}
                >
                  {`$${this.state.priceWithoutDiscount} MXN`}
                </CRText>
              )}
            </View>
          )}

          <View style={style.shipping}>
            {this.state.shippingMethod.find(
              (i) => i === 'express-moto' || i === 'express-car',
            ) && (
              <CRText
                variant={typography.paragraph}
                color={colors.colorViolet400}
              >
                <Text style={style.shippingText}>Envío express</Text>
              </CRText>
            )}
            {!this.state.shippingMethod.find(
              (i) => i === 'express-moto' || i === 'express-car',
            ) &&
              this.state.shippingMethod.find((i) => i === 'standard') && (
                <CRText
                  variant={typography.paragraph}
                  color={colors.colorViolet400}
                >
                  <Text style={style.shippingText}>Envío nacional</Text>
                </CRText>
              )}
          </View>
        </View>
      </CRCard>
    );
  }
}

export default CRProductCard;
