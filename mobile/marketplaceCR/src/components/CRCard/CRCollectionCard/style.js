import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';
import { color } from 'react-native-reanimated';

export default StyleSheet.create({
  //Store
  storeContainer: {
    paddingHorizontal: 5,
    height: 95,
    marginTop: 120,
    backgroundColor: colors.colorPeach100,
    width: '100%',
    borderRadius: 10,
  },
  cover: {
    width: '100%',
    height: 160,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: colors.colorPeach100,
    borderRadius: 3,
  },
  coverPlaceHolder: {
    position: 'absolute',
  },
  baseWrapper: {
    borderRadius: 10,
  },
  base: {
    backgroundColor: colors.colorPeach100,
    borderRadius: 5,
    marginRight: 25,
    marginLeft: -10,
    width: 254,
  },
  storeDesc: {
    marginLeft: 5,
  },
});
