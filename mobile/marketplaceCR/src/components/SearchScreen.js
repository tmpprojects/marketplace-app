import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Image } from 'react-native';
import searchIC from '../images/icon/search_gray.png';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography } from '../assets';
import { CRButton } from '@canastarosa/ds-reactnative/components/CRButton';

export default class SearchScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { text: '' };
    this.searchElement = this.searchElement.bind(this);
  }

  async searchElement() {
    // await this.props.navigation.navigate('SearchResult', {
    //   itemSearch: this.state.text,
    // });

    this.searchInput.clear();
    this.setState({ text: '' });

    globalThis.ReactApplication.ActiveScreen(
      'SearchResult',
      {
        itemSearch: this.state.text,
      },
      this.props.navigation,
    );
  }
  onSubmitEditing() {
    if (this.state.text && this.state.text !== null && this.state.text !== '') {
      this.searchElement();
    }
  }

  render() {
    return (
      <View>
        <View style={styles.inputForm}>
          <Image source={searchIC} style={styles.iconSearch} />
          <TextInput
            autoFocus={true}
            ref={(input) => {
              this.searchInput = input;
            }}
            style={{
              color: colors.colorDark300,
              ...typography.txtLight,
            }}
            placeholder="Ingrese su busqueda"
            placeholderTextColor={colors.colorGray400}
            underlineColorAndroid="transparent"
            onChangeText={(text) => this.setState({ text })}
            autoCapitalize="none"
            onSubmitEditing={({ nativeEvent }) =>
              this.onSubmitEditing(nativeEvent)
            }
          />
        </View>
        {!this.state.text ? (
          <React.Fragment></React.Fragment>
        ) : (
          <CRButton
            variant="whiteGhost"
            textColor={colors.colorMain300}
            onPress={this.searchElement}
          >
            Buscar
          </CRButton>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputForm: {
    marginTop: 25,
    borderRadius: 20,
    backgroundColor: colors.colorGray100,
    padding: 10,
    marginBottom: 10,
    flexDirection: 'row',
    marginLeft: 20,
    marginRight: 20,
  },
  iconSearch: {
    width: 20,
    height: 20,
    marginRight: 8,
  },
});
