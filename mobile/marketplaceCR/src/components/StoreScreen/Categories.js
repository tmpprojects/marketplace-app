import React from 'react';

import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { typography, buttons } from '../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { ScrollView } from 'react-native-gesture-handler';

export default (props) => {
  const { categories } = props;
  const searchProducts = function (filter) {
    searchProducts(filter);
  };
  const formSubmit = function (e) {
    e.preventDefault();
    const term = e.target.querySelector('[name="s"]').value;

    searchProducts(`&search=${term}`);
  };
  return (
    <View style={styles.containerSection}>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <TouchableOpacity
          onSubmit={formSubmit}
          style={styles.section}
          onPress={() => {
            searchProducts('');
          }}
        >
          <Text style={styles.sectionName}>Todos</Text>
        </TouchableOpacity>
        {categories.map((item, i) => (
          <TouchableOpacity
            key={i}
            style={styles.section}
            onPress={() => {
              searchProducts(`&section__slug=${item.slug}`);
            }}
          >
            <Text style={styles.sectionName}>{item.name}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  containerSection: {
    paddingHorizontal: 15,
    marginVertical: 10,
  },
  section: {
    margin: 10,
    ...buttons.btnInterests,
    borderColor: colors.colorGray300,
  },
  sectionName: {
    ...typography.txtLight,
    padding: 8,
    textAlign: 'center',
  },
});
