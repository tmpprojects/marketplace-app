import React, { Component } from 'react';
import Swiper from 'react-native-swiper';

import {
  View,
  FlatList,
  Text,
  Image,
  TouchableOpacity,
  Linking,
} from 'react-native';

import { addHourPeriodSuffix } from '../../../utils/date';
import { socialNetworksURL } from '../../../redux/constants/app.constants';
import { styles } from './AboutStoreStyles';
import fbIcon from '../../../images/icon/social_media_fb.png';
import instaIcon from '../../../images/icon/social_media_insta.png';
import pinterestIcon from '../../../images/icon/social_media_pin.png';

const Schedules = ({ name, open, close, index }) => (
  <View key={index} style={styles.itemSchedules}>
    <Text style={styles.schedulesName}>{name}</Text>
    <Text style={styles.schedules}>
      {`${addHourPeriodSuffix(open)} - ${addHourPeriodSuffix(close)}`}
    </Text>
  </View>
);

export default class AboutStore extends Component {
  render() {
    const {
      description,
      slogan,
      user,
      gallery,
      work_schedules,
      social_links,
    } = this.props.store;

    const socialMediaList = social_links.map((item, index) => {
      const networkPrefix = socialNetworksURL[item.name];
      return !networkPrefix ? null : (
        <TouchableOpacity
          style={styles.buttonSocialMedia}
          key={index}
          onPress={() => Linking.openURL(`${networkPrefix}${item.link}`)}
        >
          <Image
            source={
              item.name === 'facebook'
                ? fbIcon
                : item.name === 'instagram'
                ? instaIcon
                : pinterestIcon
            }
            style={styles.iconSocialMedia}
          />
        </TouchableOpacity>
      );
    });

    return (
      <View style={styles.about}>
        <Text style={styles.slogan}>{slogan}</Text>
        <Text style={styles.description}>{description}</Text>
        {gallery.length > 0 && (
          <View style={styles.containerSlide}>
            <Swiper
              horizontal={true}
              style={styles.wrapper}
              dotStyle={styles.bullet}
              activeDotStyle={styles.bulletActive}
              loop={true}
              height={350}
              autoplay
            >
              {gallery.map((item, i) => (
                <View key={i} style={styles.slide}>
                  <Image
                    style={styles.imageSlide}
                    source={{ uri: item.photo.medium }}
                  />
                </View>
              ))}
            </Swiper>
          </View>
        )}
        {work_schedules > 0 && (
          <View style={styles.containerSchedules}>
            <Text style={styles.title}>Horarios de la Tienda</Text>
            <FlatList
              data={work_schedules}
              renderItem={({ item, index }) => (
                <Schedules
                  key={index}
                  name={item.name}
                  open={item.open.value}
                  close={item.close.value}
                />
              )}
              keyExtractor={(item, index) => String(index)}
            />
          </View>
        )}
        <View style={styles.containerOwner}>
          <View style={styles.owner}>
            <View style={styles.containerPhoto}>
              <Image
                source={{ uri: user.profile_photo.small }}
                style={styles.photoProfile}
              />
            </View>
            <View>
              <Text style={styles.subtitle}>Creado por:</Text>
              <Text style={styles.ownerName}>
                {user.first_name} {user.last_name}
              </Text>
            </View>
          </View>
        </View>

        {socialMediaList.length > 0 && (
          <View style={styles.containerSocialMedia}>
            <View style={styles.socialMedia}>{socialMediaList}</View>
          </View>
        )}
      </View>
    );
  }
}
