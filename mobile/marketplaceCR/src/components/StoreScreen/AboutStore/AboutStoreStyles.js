import { StyleSheet } from 'react-native';
import { typography } from '../../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const styles = StyleSheet.create({
  slogan: {
    ...typography.txtSemiBold,
    textAlign: 'center',
    marginBottom: 8,
    paddingHorizontal: 15,
  },
  description: {
    ...typography.txtLight,
    textAlign: 'center',
    paddingHorizontal: 15,
    marginBottom: 12,
  },
  title: {
    ...typography.txtLight,
    textAlign: 'center',
    marginBottom: 17,
  },
  containerSocialMedia: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FAF7F5',
    marginTop: 20,
  },
  socialMedia: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    maxWidth: 200,
    paddingVertical: 15,
  },
  buttonSocialMedia: {
    flex: 3,
    alignItems: 'center',
  },
  iconSocialMedia: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
  // pinterest: {
  //   backgroundImage: ,
  // },
  // facebook: {

  // },
  // instagram: {

  // },
  containerOwner: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FAF7F5',
  },
  owner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    maxWidth: 250,
  },
  subtitle: {
    ...typography.txtLight,
  },
  ownerName: {
    ...typography.txtSemiBold,
  },
  containerPhoto: {
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    borderWidth: 1,
    borderColor: colors.colorGray400,
    marginRight: 20,
  },
  photoProfile: {
    width: '100%',
    height: '100%',
    borderRadius: 60 / 2,
    resizeMode: 'contain',
  },
  containerSchedules: {
    backgroundColor: colors.colorWhite,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  itemSchedules: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 250,
  },
  schedulesName: {
    ...typography.txtLight,
    marginBottom: 8,
  },
  schedules: {
    ...typography.txtLight,
  },
  containerSlide: {
    paddingHorizontal: 15,
  },
  slide: {
    width: '100%',
    height: 290,
  },
  imageSlide: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  bullet: {
    width: 10,
    height: 10,
    backgroundColor: colors.colorGray400,
    borderRadius: 50,
    margin: 5,
  },
  bulletActive: {
    width: 10,
    height: 10,
    backgroundColor: colors.colorMain300,
    borderRadius: 50,
    margin: 5,
  },
});
