import { StyleSheet } from 'react-native';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography } from '../../../assets';

export const styles = StyleSheet.create({
  message: {
    ...typography.txtLight,
    color: colors.colorGray400,
    textAlign: 'center',
  },
  containerList: {},
  reviewsCount: {
    ...typography.txtLight,
    color: colors.colorGray400,
    textAlign: 'center',
    marginVertical: 20,
  },
  reviewBtn: {
    alignItems: 'center',
  },
  btn: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
  },
});
