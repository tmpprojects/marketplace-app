import { StyleSheet } from 'react-native';
import { typography } from '../../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const styles = StyleSheet.create({
  review: {
    backgroundColor: colors.colorWhite,
    paddingBottom: 30,
    marginBottom: 30,
    borderBottomWidth: 1,
    borderBottomColor: colors.colorGray200,
    paddingHorizontal: 10,
  },
  containerData: {
    flex: 3,
    marginBottom: 18,
  },
  containerUser: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  creationDay: {
    ...typography.txtLight,
    fontSize: 14,
    color: colors.colorDark400,
  },
  name: {
    ...typography.txtSemiBold,
  },
  rating: {
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  count: {
    ...typography.txtLight,
  },
  icon: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
  },
  containerPhoto: {
    width: 60,
    height: 60,
    borderRadius: 300,
    borderWidth: 1,
    borderColor: colors.colorGray200,
    marginRight: 20,
  },
  photoProfile: {
    borderTopStartRadius: 60 / 2,
    borderTopEndRadius: 60 / 2,
    borderRadius: 60 / 2,
    resizeMode: 'contain',
  },
  productName: {
    ...typography.txtSemiBold,
  },
  comment: {
    ...typography.txtLight,
  },
  containerPhotoProduct: {
    width: 88,
    height: 88,
    borderColor: colors.colorGray400,
    marginTop: 20,
    marginRight: 20,
  },
  photoProduct: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
});
