import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { View, Text, Platform } from 'react-native';

import { styles } from './ReviewsListStyles';
import Reviews from './Review';
import * as Sentry from '@sentry/react-native';

class ReviewsList extends React.Component {
  constructor(props) {
    super(props);
    this.currentPage = 1;
  }
  render() {
    const isIos = Platform.OS === 'ios';
    const SPACER_SIZE = 80;
    const {
      reviews,
      reviewsCount,
      showProductDetails,
      pendingReviews,
      navigation,
    } = this.props;

    try {
      return (
        <View>
          {(reviews === [] || pendingReviews === []) && (
            <Text style={styles.message}>
              Aún no hay reseñas de esta tienda
            </Text>
          )}
          <Text style={styles.reviewsCount}>{reviewsCount} reseñas</Text>
          <View style={styles.containerList}>
            <Reviews
              // profile={profile}
              reviews={pendingReviews}
              showProductDetails={showProductDetails}
              navigation={navigation}
            />
            <Reviews
              reviews={reviews}
              showProductDetails={showProductDetails}
              navigation={navigation}
            />
          </View>

          {/* {reviews.next !== null && (
          <View style={styles.reviewBtn}>
            <TouchableOpacity
              style={styles.buttonMore}
              onClick={() => this.getReviews(store, currentPage + 1)}>
              <Text style={styles.btn}>Ver más</Text>
            </TouchableOpacity>
          </View>
        )} */}
          {isIos && <View style={{ height: SPACER_SIZE }} />}
        </View>
      );
    } catch (e) {
      Sentry.captureException(e);
      return null;
    }
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    // profile: state.users.profile,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Connect Component to Redux
export default connect(mapStateToProps, mapDispatchToProps)(ReviewsList);
