import React from 'react';
import Moment from 'moment';

import { View, Image, Text } from 'react-native';

import star from '@canastarosa/ds-theme/assets/icons/repo/star.png';
import { styles } from './ReviewStyles';
import { CRThumb } from '../../CRThumb';
import placeholder from '../../../images/assets/palceholderUser.jpg';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';
import { CRIcon } from '../../CRIcon';
import { CRRowBoth } from '../../Layout';

const Stars = ({ rating }) => {
  let roundedRating = Math.round(rating);
  let halfStar = !Number.isInteger(rating);
  let starsArray = [];
  let hollowStars = 5 - roundedRating;
  if (halfStar) {
    roundedRating = roundedRating - 1;
    for (let i = 0; i < roundedRating; i++) {
      starsArray.push(
        <CRIcon name="star" key={i} size={16} color={colors.colorYellow300} />,
      );
    }
    starsArray.push(
      <CRIcon name="star--half" size={16} color={colors.colorYellow300} />,
    );
    for (let i = 0; i < hollowStars; i++) {
      starsArray.push(
        <CRIcon
          name="star--hollow"
          key={i}
          size={16}
          color={colors.colorYellow300}
        />,
      );
    }
  } else {
    for (let i = 0; i < roundedRating; i++) {
      starsArray.push(
        <CRIcon name="star" key={i} size={16} color={colors.colorYellow300} />,
      );
    }
    for (let i = 0; i < hollowStars; i++) {
      starsArray.push(
        <CRIcon
          name="star--hollow"
          key={i}
          size={16}
          color={colors.colorYellow300}
        />,
      );
    }
  }

  return starsArray;
};

const Review = (props) => {
  return props.reviews.map((review) => (
    <View style={styles.review} key={review.id}>
      <CRRowBoth>
        <View style={styles.containerUser}>
          {review.is_approved === null ? (
            <CRText variant={typography.caption} color={colors.colorYellow400}>
              Bajo revisión
            </CRText>
          ) : (
            <View>
              <View style={styles.containerPhoto}>
                <CRThumb
                  source={{ uri: review.user.profile_photo.small }}
                  wrapperStyle={styles.photoProfile}
                  imgHeight={58}
                  placeHolder={placeholder}
                />
              </View>
              <View style={styles.containerData}>
                <View>
                  <CRText
                    variant={typography.subtitle3}
                  >{`${review.user.first_name}`}</CRText>
                  <CRText
                    variant={typography.caption}
                    color={colors.colorDark100}
                  >
                    {' '}
                    {Moment(review.created).format('DD MMMM YYYY')}
                  </CRText>
                </View>
                <View style={styles.rating}>
                  {/*<CRText variant={typography.paragraph}>
              {review.product_score}
            </CRText>*/}
                  <Stars rating={review.product_score} />
                </View>
              </View>
            </View>
          )}
        </View>
        <View style={styles.containerProduct}>
          {review.product === undefined ? null : (
            <CRText variant={typography.subtitle3}>
              {review.product.name}
            </CRText>
          )}
          {/* {review.product.name !== "" && } */}
          {review.comment !== '' && (
            <CRText color={colors.colorDark100} variant={typography.paragraph}>
              {review.comment}
            </CRText>
          )}
          {props.showProductDetails && (
            <View style={styles.containerPhotoProduct}>
              <Image
                source={{ uri: review.product.photo.small }}
                style={styles.photoProduct}
              />
            </View>
          )}
        </View>
      </CRRowBoth>
    </View>
  ));
};

export default Review;
