import { StyleSheet } from 'react-native';
import { typography } from '../../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const styles = StyleSheet.create({
  containerData: {
    paddingHorizontal: 14,
    alignItems: 'center',
  },
  containerExtra: {
    maxWidth: 300,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 15,
  },
  storeName: {
    ...typography.txtExtraBold,
    color: colors.colorMain300,
    textAlign: 'center',
    paddingBottom: 8,
  },
  storeExcerpt: {
    ...typography.txtLight,
    textAlign: 'center',
    alignSelf: 'center',
    maxWidth: 300,
  },
  txtExtra: {
    ...typography.txtLight,
    textAlign: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
    // position: 'absolute',
    // bottom: -40,
    // right: 15,
  },
  icon: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    marginRight: 5,
  },
  iconShare: {
    position: 'absolute',
    bottom: -40,
    right: 15,
  },
  iconPremium: {
    position: 'absolute',
    bottom: '-40%',
    right: '30%',
    width: 75,
    height: 18,
  },
  containerImage: {
    marginBottom: 80,
  },
  containerLogo: {
    width: 120,
    height: 120,
    position: 'absolute',
    left: '35%',
    bottom: '-35%',
    shadowColor: colors.colorDark200,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  logo: {
    backgroundColor: colors.colorWhite,
  },
  cover: {
    width: '100%',
    height: 155,
    resizeMode: 'cover',
    marginVertical: 5,
    marginTop: 0,
    borderBottomColor: colors.colorGray200,
    borderBottomWidth: 1,
  },
  holidays: {
    backgroundColor: colors.colorYellow300,
    alignItems: 'center',
    padding: 8,
    marginTop: 20,
  },
  txtHolidays: {
    ...typography.txtLight,
    color: colors.colorDark300,
  },
  day: {
    ...typography.txtSemiBold,
    color: colors.colorDark300,
  },
});
