import React, { Component } from 'react';

import {
  View,
  ImageBackground,
  Text,
  Image,
  TouchableOpacity,
  Share,
} from 'react-native';

import { formatDate, formatDateToUTC } from '../../../utils/date';
import { styles } from './StoreProfileStyles';
import premium from '../../../images/icon/premium.png';
import storePlaceholder from '../../../images/assets/placeholderStores.jpg';
import coverPlaceholder from '../../../images/assets/placeholderCover.jpg';

//DS
import { colors } from '@canastarosa/ds-theme/colors';
import { typography } from '@canastarosa/ds-theme/typography';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { CRIcon } from '../../CRIcon';
import { CRThumb } from '../../CRThumb';

const storePremium = false;
export default class StoreProfile extends Component {
  onShare() {
    Share.share(
      {
        title: 'Compartir',
        url: 'www.canastarosa.com',
        message: ' Cool',
      },
      {
        //android
        dialogTitle: 'Compartir este contenido',
        //ios
        excludedActivityTypes: ['com.apple.UIKit.activity.PostToTwitter'],
      },
    );
  }
  render() {
    const {
      name,
      excerpt,
      photo,
      cover,
      reviews,
      total_sales,
      is_on_vacation,
      vacation_range,
      reviewsCount = reviews.count,
      mean_store_score,
    } = this.props.store;

    let location = this.props.store.location;
    if (
      location === 'DF' ||
      location === 'MX' ||
      location === 'Ciudad de México'
    ) {
      location = 'CDMX';
    }
    const ratingStore = mean_store_score.toFixed(1);
    return (
      <React.Fragment>
        <View style={styles.containerImage}>
          <CRThumb
            source={{ uri: cover.medium }}
            imgHeight={155}
            wrapperStyle={styles.cover}
            placeHolder={coverPlaceholder}
          />
          {/*<ImageBackground
            source={{ uri: cover.medium }}
            style={styles.cover}
          />*/}
          <View style={styles.containerLogo}>
            <CRThumb
              source={{ uri: photo.small }}
              imgHeight={128}
              placeHolder={storePlaceholder}
              resizeMode={'contain'}
              wrapperStyle={styles.logo}
            />
          </View>
          {storePremium && (
            <Image source={premium} style={styles.iconPremium} />
          )}
          {/* <TouchableOpacity onPress={this.onShare}>
            <Image source={share} style={styles.iconShare} />
          </TouchableOpacity> */}
        </View>
        <View style={styles.containerData}>
          <CRText
            variant={typography.subtitle}
            color={colors.colorDark200}
            align="center"
          >
            {name}
          </CRText>
          <CRText
            variant={typography.paragraph}
            color={colors.colorDark100}
            align="center"
          >
            {excerpt}
          </CRText>
          <View style={styles.containerExtra}>
            {/* {total_sales < 50 ? (
              <Text style={styles.txtExtra}>Menos de 50 ventas</Text>
            ) : (
              <Text style={styles.txtExtra}>
                {total_sales} venta{total_sales > 1 ? 's' : ''}
              </Text>
            )} */}
            {ratingStore > 0 && (
              <Text style={styles.txtExtra}>
                {ratingStore}{' '}
                <CRIcon name="star" size={15} color={colors.colorYellow300} />
              </Text>
            )}
            <Text style={styles.txtExtra}>
              <CRIcon name="map" size={15} color={colors.colorGray300} />{' '}
              {location}
            </Text>
          </View>
        </View>
        {is_on_vacation ? (
          <View style={styles.holidays}>
            <Text style={styles.txtHolidays}>Estamos de Vacaciones</Text>
            <Text style={styles.day}>
              Regresamos el{' '}
              {formatDate(formatDateToUTC(new Date(vacation_range.end)))}
            </Text>
          </View>
        ) : null}
      </React.Fragment>
    );
  }
}
