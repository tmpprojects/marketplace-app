/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export const style = StyleSheet.create({
  base: {
    position: 'absolute',
    top: 0,
    left: 0,
    flex: 1,
    zIndex: 100,
    elevation: 4,
    width: '100%',
    backgroundColor: colors.colorWhite,
  },
});
