import React from 'react';
import { Animated } from 'react-native';

//style
import { style } from './style';

export const CRDinamicMenu = ({ children, position }) => {
  return (
    <Animated.View
      style={[style.base, { transform: [{ translateY: position }] }]}
    >
      {children}
    </Animated.View>
  );
};
