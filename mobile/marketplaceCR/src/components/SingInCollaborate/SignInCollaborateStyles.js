import { StyleSheet } from 'react-native';
import { typography, buttons } from '../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.colorWhite,
  },
  image: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
  },
  message: {
    marginTop: 20,
    ...typography.txtRegular,
  },
  btnLogin: {
    ...buttons.square,
    borderColor: colors.colorMain300,
  },
  txtLogin: {
    textAlign: 'center',
    ...typography.txtRegular,
    color: colors.colorMain300,
    fontSize: 20,
  },
  txtSingUp: {
    ...typography.txtRegular,
    color: colors.colorDark300,
    marginTop: 25,
  },
  txtBold: {
    ...typography.txtBold,
    color: colors.colorMain300,
  },
});
