import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';

import alarm from '../../images/illustration/alarm.png';
import { styles } from './SignInCollaborateStyles';

const SignInCollaborate = (props) => {
  return (
    <View style={styles.container}>
      <Image source={alarm} style={styles.image} />
      <Text style={styles.message}>
        Necesitas iniciar sesión para continuar.
      </Text>
      <TouchableOpacity
        style={styles.btnLogin}
        activeOpacity={0.5}
        onPress={() => {
          //props.navigation.navigate('LoginScreen');

          globalThis.ReactApplication.ActiveScreen(
            'LoginScreen',
            {
              initialValue: 0,
            },
            props.navigation,
          );
        }}
      >
        <Text style={styles.txtLogin}>Iniciar sesión</Text>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => {
          //props.navigation.navigate('SignUpScreen');

          globalThis.ReactApplication.ActiveScreen(
            'LoginScreen',
            {
              initialValue: 0,
            },
            props.navigation,
          );
        }}
      >
        <Text style={styles.txtSingUp}>
          ¿No tienes cuenta?
          <Text style={styles.txtBold}> Regístrate</Text>
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default SignInCollaborate;
