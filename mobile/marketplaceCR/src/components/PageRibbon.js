import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { typography } from '../assets';

type Props = {};
export default class PageRibbon extends Component<Props> {
  render() {
    return (
      <View>
        <View style={styles.paggeRibbon}>
          <Text style={styles.txtStyle}>Best Seller</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  paggeRibbon: {
    width: 120,
    padding: 10,
    paddingLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e63976',
    height: 40,
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
  },
  txtStyle: {
    color: '#FFF',
    ...typography.txtBold,
  },
});
