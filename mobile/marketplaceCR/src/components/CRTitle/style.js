import { StyleSheet } from 'react-native';

const isAndroid = Platform.OS === 'android';

export default StyleSheet.create({
  headerWrapper: {
    marginVertical: isAndroid ? 12 : 20,
  },
  subtitleWrapper: {
    marginTop: 7,
  },
});
