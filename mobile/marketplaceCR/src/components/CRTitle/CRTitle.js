/*

This file was created by Canasta Rosa
under the MIT license.

*/

import React from 'react';
import { View } from 'react-native';
import { default as style } from './style';
import { CRRowBoth } from '../Layout';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

const CRTitle = ({ title, align, subtitle }) => {
  return (
    <CRRowBoth addedStyle={style.headerWrapper}>
      <CRText
        variant={typography.subtitle}
        color={colors.colorDark200}
        align={align ? align : 'center'}
      >
        {title}
      </CRText>
      {subtitle ? (
        <View style={style.subtitleWrapper}>
          <CRText
            variant={typography.paragraph}
            color={colors.colorDark100}
            align={align ? align : 'center'}
          >
            {subtitle}
          </CRText>
        </View>
      ) : null}
    </CRRowBoth>
  );
};

export default CRTitle;
