/**
 * CRAPP icon set component.
 * Usage: <CRIcon name="icon-name" size={20} color="#4F8EF7" />
 */
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import glyphMap from './glyphMap.json';
const CRIcon = createIconSetFromIcoMoon(glyphMap);
export default CRIcon;
