import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import star from '@canastarosa/ds-theme/assets/icons/repo/star.png';
import { colors, typography } from '../assets';

export default class BadgeRating extends Component {
  render() {
    const { reviews } = this.props.product;
    return (
      <>
        {reviews.rating === 0 ? (
          <View />
        ) : (
          <View style={styles.productContainer}>
            <View style={styles.priceContainer}>
              <Text style={styles.rating}>
                {reviews.rating.toFixed(1)}
                <Image source={star} style={styles.icon} />
              </Text>
            </View>
          </View>
        )}
      </>
    );
  }
}

const styles = StyleSheet.create({
  productContainer: {
    width: 60,
    height: 'auto',
    marginHorizontal: 7,
    marginBottom: 7,
    backgroundColor: colors.white,
    borderRadius: 5,
    shadowColor: colors.black300,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  rating: {
    marginLeft: 10,
    ...typography.txtRegular,
    color: colors.black300,
  },
  icon: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    marginLeft: 10,
  },
});
