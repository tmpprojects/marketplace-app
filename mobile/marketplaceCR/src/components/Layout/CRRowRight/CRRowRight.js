import React from 'react';
import { View } from 'react-native';

import { default as style } from './style';

const CRRowRight = ({ children, addedStyle }) => {
  return <View style={[style.row, addedStyle]}>{children}</View>;
};

export default CRRowRight;
