import React from 'react';
import { View } from 'react-native';

import { default as style } from './style';

const CRRowFull = ({ children, addedStyle }) => {
  return <View style={[style.row, addedStyle]}>{children}</View>;
};

export default CRRowFull;
