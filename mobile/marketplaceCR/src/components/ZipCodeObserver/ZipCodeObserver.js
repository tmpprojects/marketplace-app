import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text } from 'react-native';

class ZipCodeObserver extends Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {
    //**** Mock - refresh this screen by changing the zipCode
    if (nextProps.zipCode !== this.props.zipCode) {
      // *** Reload Collection
      if (this.props.observe === 'Collection' && this.props.observe) {
        // this.props.navigation.navigate('ChangeZipCode', {
        //   reloadTo: 'Collection',
        //   Collection: this.props.Collection,
        // });

        globalThis.ReactApplication.ActiveScreen(
          'ChangeZipCode',
          {
            reloadTo: 'Collection',
            Collection: this.props.Collection,
          },
          this.props.navigation,
        );
      }
      // *** Reload HomeScreen
      // if (this.props.observe === 'HomeScreen' && this.props.observe) {
      //   this.props.navigation.navigate('ChangeZipCode', {
      //     reloadTo: 'HomeScreen',
      //   });
      // }

      // *** Reload Collection
      if (this.props.observe === 'NewStores' && this.props.observe) {
        // this.props.navigation.navigate('ChangeZipCode', {
        //   reloadTo: 'NewStores',
        // });

        globalThis.ReactApplication.ActiveScreen(
          'ChangeZipCode',
          {
            reloadTo: 'NewStores',
          },
          this.props.navigation,
        );
      }
      // *** Reload CatalogScreen
      if (this.props.observe === 'CatalogScreen' && this.props.observe) {
        // this.props.navigation.navigate('ChangeZipCode', {
        //   reloadTo: 'CatalogScreen',
        //   categorySlug: this.props.categorySlug,
        //   categoryName: this.props.categoryName,
        // });

        globalThis.ReactApplication.ActiveScreen(
          'ChangeZipCode',
          {
            reloadTo: 'CatalogScreen',
            categorySlug: this.props.categorySlug,
            categoryName: this.props.categoryName,
          },
          this.props.navigation,
        );
      }
      // *** Reload SearchResult
      if (this.props.observe === 'SearchResult' && this.props.observe) {
        // this.props.navigation.navigate('ChangeZipCode', {
        //   reloadTo: 'SearchResult',
        //   itemSearch: this.props.search,
        // });

        globalThis.ReactApplication.ActiveScreen(
          'ChangeZipCode',
          {
            reloadTo: 'SearchResult',
            itemSearch: this.props.search,
          },
          this.props.navigation,
        );
      }
    }
    //**** Mock - refresh this screen by changing the zipCode
  }

  render() {
    return <React.Fragment></React.Fragment>;
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    zipCode: state.app.zipCode,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ZipCodeObserver);
