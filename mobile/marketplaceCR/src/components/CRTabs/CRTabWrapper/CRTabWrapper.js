import React from 'react';
import { View } from 'react-native';
import { default as style } from './style';

const CRTabWrapper = ({ children }) => {
  return <View style={style.container}>{children}</View>;
};

export default CRTabWrapper;
