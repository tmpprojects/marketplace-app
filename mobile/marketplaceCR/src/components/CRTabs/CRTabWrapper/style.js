import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopColor: colors.colorGray100,
    borderTopWidth: 1,
    borderBottomColor: colors.colorGray100,
    borderBottomWidth: 1,
    backgroundColor: colors.colorWhite,
  },
});
