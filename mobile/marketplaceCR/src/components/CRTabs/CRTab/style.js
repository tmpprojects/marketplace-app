import { StyleSheet } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';

export default StyleSheet.create({
  base: {
    flex: 1,
    paddingVertical: 15,
    paddingBottom: 18,
  },

  tab: {
    borderBottomColor: colors.colorWhite,
    borderBottomWidth: 4,
  },
  tabActive: {
    flex: 1,
    borderBottomColor: colors.colorTurquiose300,
    borderBottomWidth: 3,
  },
});
