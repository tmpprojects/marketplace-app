import React from 'react';
import { TouchableOpacity } from 'react-native';
import { default as style } from './style';

import { colors } from '@canastarosa/ds-theme/colors';
import { typography } from '@canastarosa/ds-theme/typography';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';

const CRTab = ({ title, isActive, onPress }) => {
  return (
    <TouchableOpacity
      style={[style.base, !isActive ? style.tab : style.tabActive]}
      onPress={onPress}
    >
      <CRText
        variant={typography.subtitle3}
        color={[!isActive ? colors.colorGray300 : colors.colorTurquiose300]}
        align="center"
      >
        {title}
      </CRText>
    </TouchableOpacity>
  );
};

export default CRTab;
