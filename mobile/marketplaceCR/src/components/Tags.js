import React, { Component } from 'react';
import {
  TouchableOpacity,
  ScrollView,
  Text,
  StyleSheet,
  Image,
} from 'react-native';

import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography } from '../assets';
import { formatTitle } from '../utils/formatTitle';

//will be added to the ds
import { CRCategoryCard } from './CRCard';

type TagProps = {
  onChange: Function,
  slug: string,
  name: string,
  image: string,
  checked: boolean,
};
export class Tag extends Component<TagProps> {
  handlePress = () => {
    this.props.onChange(this.props.slug, this.props.name);
  };

  //
  render() {
    const { name, image } = this.props;
    return (
      <CRCategoryCard
        onPress={this.handlePress}
        thumbImg={image}
        category={name}
        size="tag"
      />
    );
  }
}

type TagsProperties = {
  tags: Array<{ name: string, slug: string, photo: { medium: string } }>,
  onChange: Function,
  activeSlug: string,
};
export class Tags extends Component<TagsProperties> {
  render() {
    const { tags, activeSlug, onChange } = this.props;
    return (
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {tags &&
          tags.map((tag, index) => (
            <Tag
              key={index}
              slug={tag.slug}
              name={formatTitle(tag.name.toLowerCase())}
              onChange={onChange}
              image={tag.photo.medium}
              checked={tag.slug === activeSlug}
            />
          ))}
      </ScrollView>
    );
  }
}

//
const styles = StyleSheet.create({
  itemFilter: {
    minWidth: 150,
    margin: 10,
    borderColor: colors.colorGray200,
    borderWidth: 1,
  },
  subcategoryImage: {
    height: 60,
    resizeMode: 'cover',
  },
  filterName: {
    ...typography.txtLight,
    padding: 8,
    textAlign: 'center',
    color: colors.colorDark300,
  },
});

export default Tags;
