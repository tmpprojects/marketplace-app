import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '@canastarosa/ds-theme/colors';
import { typography } from '@canastarosa/ds-theme/typography';

const alto = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  //IOS
  inputForm: {
    marginVertical: 10,
    borderRadius: 10,
    backgroundColor: colors.colorGray100,
    flexDirection: 'row',
    marginBottom: alto < 737 ? 10 : 20,
    height: alto < 737 ? 35 : 40,
  },
  iconSearch: {
    width: 20,
    height: 20,
    marginTop: alto < 737 ? 8 : 10,
    marginRight: 10,
  },
  searchContainer: {
    width: '100%',
    alignContent: 'center',
    display: 'flex',
  },
  textInput: {
    color: colors.colorDark300,
    ...typography.paragraph,
    width: '88%',
    marginLeft: 10,
  },
  //ANDROID
  textInputAnd: {
    color: colors.colorDark300,
    ...typography.paragraph,
    width: '85%',
    marginLeft: 10,
    paddingTop: 0,
    paddingBottom: 0,
  },
  inputFormAnd: {
    marginVertical: 10,
    borderRadius: 10,
    backgroundColor: colors.colorGray100,
    flexDirection: 'row',
    marginBottom: 10,
    height: 40,
  },
  iconSearchAnd: {
    width: 20,
    height: 20,
    marginTop: 10,
  },
});
