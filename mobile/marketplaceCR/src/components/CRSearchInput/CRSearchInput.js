import React, { Component } from 'react';
import { View, TextInput, Platform, TouchableOpacity } from 'react-native';
import { styles } from './style';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
//DS
import { colors } from '@canastarosa/ds-theme/colors';
import { CRRowBoth } from '../Layout';
import { CRIcon } from '../CRIcon';

import { appActions } from '../../../src/redux/actions';

// import { resetAction } from '../../utils/resetActions';

declare var globalThis: any;

class CRSearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = { text: '' };
    this.searchElement = this.searchElement.bind(this);
  }

  searchElement() {
    // this.props.navigation.dispatch(resetAction);
    // this.props.navigation.navigate('SearchResult', {
    //   itemSearch: this.state.text,
    // });
    globalThis.ReactApplication.ActiveScreen(
      'SearchResult',
      {
        itemSearch: this.state.text,
      },
      this.props.navigation,
    );

    this.searchInput.clear();
    this.setState({ text: '' });
    this.props.changeSearch(false);
  }

  render() {
    const isAndroid: Boolean = Platform.OS === 'android';
    return (
      <View style={styles.searchContainer}>
        <View style={isAndroid ? styles.inputFormAnd : styles.inputForm}>
          <TextInput
            returnKeyType="search"
            ref={(input) => {
              this.searchInput = input;
            }}
            style={isAndroid ? styles.textInputAnd : styles.textInput}
            placeholder="¡Encuentra algo único!"
            placeholderTextColor={colors.colorGray400}
            underlineColorAndroid="transparent"
            onChangeText={(text) => this.setState({ text })}
            autoCapitalize="none"
            onSubmitEditing={this.searchElement}
          />
          <CRIcon
            name="search"
            size={20}
            color={colors.colorDark100}
            style={isAndroid ? styles.iconSearchAnd : styles.iconSearch}
          />
        </View>
      </View>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    openSearch: state.app.openSearch,
  };
}
function mapDispatchToProps(dispatch) {
  const { changeSearch } = appActions;
  return bindActionCreators({ changeSearch }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CRSearchInput);
