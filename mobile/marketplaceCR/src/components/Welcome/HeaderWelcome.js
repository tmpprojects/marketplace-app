import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import logo from '../../images/logo/horizontal-core.png';

const HeaderWelcome = () => (
  <View style={styles.logoContainer}>
    <Image style={styles.logo} source={logo} />
  </View>
);

export default HeaderWelcome;

const styles = StyleSheet.create({
  logoContainer: {
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  logo: {
    transform: [{ scaleX: 0.12 }, { scaleY: 0.12 }],
  },
});
