import React from 'react';
import { StyleSheet, Text, FlatList, TouchableHighlight } from 'react-native';

import { colors, typography, buttons } from '../../assets';

const InterestBtn = ({ id, title, selected, onSelect }) => (
  <TouchableHighlight
    onPress={() => onSelect(id)}
    style={[
      styles.interestBtn,
      { borderColor: selected ? colors.corePink : colors.white },
    ]}
  >
    <Text
      style={[
        styles.interestTitle,
        { backgroundColor: selected ? colors.corePink : 'transparent' },
      ]}
    >
      {title}
    </Text>
  </TouchableHighlight>
);

export default function Interestbtn({ navigation }) {
  const [selected, setSelected] = React.useState(new Map());

  const onSelect = React.useCallback(
    (id) => {
      const newSelected = new Map(selected);
      newSelected.set(id, !selected.get(id));

      setSelected(newSelected);
    },
    [selected],
  );
  return (
    <FlatList
      data={this.props.interests}
      numColumns={3}
      columnWrapperStyle={styles.interestsList}
      renderItem={({ item }) => (
        <InterestBtn
          id={item.id}
          title={item.title}
          selected={!!selected.get(item.id)}
          onSelect={onSelect}
        />
      )}
    >
      keyExtractor={(item, index) => String(index)}
      extraData={selected}
    </FlatList>
  );
}

const styles = StyleSheet.create({
  backgorund: {
    width: '100%',
    height: '100%',
  },

  headerContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 40,
  },
  header: {
    ...typography.mainTitle,
    color: colors.white,
    marginBottom: 20,
  },
  headerInfo: {
    ...typography.specialText,
    width: 300,
    color: colors.white,
    textAlign: 'center',
  },

  interestsList: {
    flex: 1,
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  interestBtn: {
    margin: 10,
    ...buttons.btnInterests,
  },
  interestTitle: {
    ...typography.txtLight,
    fontSize: 16,
    padding: 10,
    textAlign: 'center',
    color: colors.white,
  },

  linksContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  link: {
    ...typography.btnWelcome,
    marginVertical: 30,
  },
  linkGray: {
    color: colors.black300,
  },
});
