import * as React from 'react';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import Swiper from 'react-native-swiper/src';

import { colors, typography } from '../../assets';

const slides = [
  {
    id: '0',
    title: 'DESCUBRE',
    text: 'Miles de productos originales creados por micro emprendedoras.',
  },
  {
    id: '1',
    title: 'INSPIRATE',
    text: 'Miles de productos originales creados por micro emprendedoras.',
  },
  {
    id: '2',
    title: 'CREA',
    text: 'Miles de productos originales creados por micro emprendedoras.',
  },
];

const WIDTH = Dimensions.get('window').width;

class Carousel extends React.Component {
  scrollRef = React.createRef();
  constructor(props) {
    super(props);
    this.state = {
      selectedSlide: 0,
    };
  }

  render() {
    return (
      <View style={styles.slideContainer}>
        <Swiper
          horizontal={true}
          height={1000}
          style={styles.wrapper}
          loop={true}
          dotStyle={styles.bullet}
          activeDotStyle={styles.bulletActive}
          autoplay
        >
          {slides.map((slide) => (
            <View key={slide.id} style={styles.slide}>
              <Text style={styles.slideTitle}>{slide.title}</Text>
              <Text style={styles.slideInfo}>{slide.text}</Text>
            </View>
          ))}
        </Swiper>
      </View>
    );
  }
}
export { Carousel };

const styles = StyleSheet.create({
  slideContainer: {
    flex: 3,
    justifyContent: 'flex-start',
  },
  container: {
    flexGrow: 0,
  },
  slide: {
    width: WIDTH,
  },
  slideTitle: {
    ...typography.txtExtraBold,
    color: colors.corePink,
    fontSize: 26,
    textAlign: 'center',
  },
  slideInfo: {
    ...typography.txtLight,
    color: colors.white,
    fontSize: 18,
    marginHorizontal: 25,
    marginVertical: 20,
    textAlign: 'center',
  },
  slidesBullets: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 30,
  },
  bullet: {
    width: 10,
    height: 10,
    backgroundColor: colors.black300,
    borderRadius: 50,
    margin: 5,
  },
  bulletActive: {
    width: 10,
    height: 10,
    backgroundColor: colors.corePink,
    borderRadius: 50,
    margin: 5,
  },
  wrapper: {},
});
