import { StyleSheet, Platform } from 'react-native';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography, buttons } from '../../assets';

const isAndroid = Platform.OS === 'android';

export const styles = StyleSheet.create({
  modalView: {
    backgroundColor: colors.colorWhite,
    height: '100%',
  },
  btnClose: { alignItems: 'flex-end' },
  iconClose: {
    color: '#898989',
    fontSize: 40,
    marginTop: isAndroid ? 5 : 45,
    marginRight: 20,
  },
  container: {
    marginVertical: 10,
    marginHorizontal: 35,
  },
  containerStore: {
    height: 200,
  },
  containerFilter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    ...typography.txtSemiBold,
    fontSize: 20,
    color: colors.colorDark300,
    textAlign: 'center',
  },
  date: {
    ...typography.txtSemiBold,
    color: colors.colorDark300,
    textAlign: 'center',
  },
  filterTitle: {
    ...typography.txtRegular,
    color: colors.colorDark300,
    marginVertical: 20,
  },
  // filterBtn: {
  //   width: 30,
  //   height: 30,
  //   borderRadius: 45 / 2,
  //   backgroundColor: colors.colorMain300,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  // },
  disabledFilterBtn: {
    width: 30,
    height: 30,
    borderRadius: 45 / 2,
    backgroundColor: colors.colorGray300,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconFilter: {
    width: 20,
    height: 20,
  },
  itemFilter: {
    margin: 10,
    ...buttons.btnInterests,
  },
  filterName: {
    ...typography.txtLight,
    padding: 8,
    textAlign: 'center',
    color: colors.colorDark300,
  },
  inputForm: {
    ...typography.txtRegular,
    fontSize: 18,
    borderBottomWidth: 1,
    borderBottomColor: colors.colorGray300,
    width: 100,
    margin: 10,
    padding: 10,
  },
  containerBtn: {
    alignItems: 'center',
  },
  resetBtn: {
    ...typography.txtLight,
    fontSize: 16,
    color: colors.colorGray400,
    marginVertical: 15,
  },
  submitBtn: {
    ...typography.txtRegular,
    color: colors.colorMain300,
    fontSize: 16,
  },
});
