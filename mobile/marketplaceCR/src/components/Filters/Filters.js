import React, { Component } from 'react';
import type { Node } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, getFormValues, SubmissionError } from 'redux-form';
import Moment from 'moment';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import * as Sentry from '@sentry/react-native';

import {
  Alert,
  View,
  Text,
  Image,
  TouchableOpacity,
  Modal,
  ScrollView,
  TextInput,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { getDaysBetweenDates, isValidWorkingDay } from '../../utils/date';
import { styles } from './FiltersStyles';
import filter from '../../images/icon/filter.png';
import { RadioSet } from '../RadioSet';
import { CRIcon } from '../CRIcon';

//DS
import { colors } from '@canastarosa/ds-theme/colors';
import { typography } from '@canastarosa/ds-theme/typography';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';

const renderInput = ({
  keyboardType,
  input: { onChange, ...restInput },
  placeholder,
  secureTextEntry,
}) => {
  return (
    <View>
      <TextInput
        style={[styles.inputForm, { color: colors.colorDark300 }]}
        placeholder={placeholder}
        placeholderTextColor={colors.colorGray400}
        underlineColorAndroid="transparent"
        keyboardType={keyboardType}
        onChangeText={onChange}
        {...restInput}
        secureTextEntry={secureTextEntry}
      />
    </View>
  );
};

// Calendar configuration
LocaleConfig.locales.es = {
  monthNames: [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ],
  monthNamesShort: [
    'Enr.',
    'Feb.',
    'Mar.',
    'Abr.',
    'May.',
    'Jun.',
    'Jul.',
    'Ago.',
    'Sept.',
    'Oct.',
    'Nov.',
    'Dic.',
  ],
  dayNames: [
    'Domingo',
    'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado',
  ],
  dayNamesShort: ['Dom.', 'Lun.', 'Mar.', 'Mier.', 'Jue.', 'Vie.', 'Sab.'],
  today: 'Hoy',
};
LocaleConfig.defaultLocale = 'es';

/**
 * Filters Component
 * Renders a component with UI parameters to construct a filter query string.
 */
const DEFAULT_FILTERS = {
  delivery_day: '',
  delivery_day_picked: Moment().format('YYYY-MM-DD'),
  shipping: '',
  price: '',
  sort: '',
  min_price: '',
  max_price: '',
};

type MarkedDatesType = {
  [date: string]: {
    selected: boolean,
    disabled: boolean,
    disableTouchEvent: boolean,
  },
};
type FiltersType = {
  delivery_day: string,
  delivery_day_picked: string,
  shipping: string,
  price: string,
  sort: string,
  min_price: string,
  max_price: string,
};
type FiltersProps = {
  isenabled: boolean,
  categorySlug: string,
  formValues: FiltersType,
  change: Function,
  initialize: Function,
  handleSubmit: Function,
  onFilterSubmit: Function,
};
type FiltersState = {
  shouldRenderCalendar: boolean,
  modalVisible: boolean,
};

export class Filters extends Component<FiltersProps, FiltersState> {
  state = {
    shouldRenderCalendar: false,
    modalVisible: false,
  };

  /**
   * Filter Submit Handler
   */
  onSubmit = (): void => {
    const { formValues } = this.props;
    if (formValues !== undefined) {
      if (
        formValues.delivery_day === 'picked' &&
        !formValues.delivery_day_picked
      ) {
        throw new SubmissionError({
          _error: '*Selecciona una fecha en el calendario*',
        });
      } else {
        let filtersQuery = `${this.onSortChange(formValues)}`;
        filtersQuery += `${this.filterPrice(formValues)}`;
        filtersQuery += `${this.filterShipping(formValues)}`;
        filtersQuery += `${this.filterDelivery(formValues)}`;

        this.setState({ modalVisible: false });
        this.props.onFilterSubmit(this.props.categorySlug, filtersQuery);
      }
    }
  };

  /**
   * resetForm() Update form 'initialValues' with default values.
   */
  resetForm = (): void => {
    this.props.initialize({
      ...DEFAULT_FILTERS,
    });
    this.props.onFilterSubmit(this.props.categorySlug, '');
    this.setState({ modalVisible: false });
  };

  /**
   * onSortChange()
   * @param {string} sortBy | Sorting criteria.
   */
  onSortChange = (formValues: FiltersType): string => {
    return `${formValues.sort}`;
  };

  /**
   * onDeliveryDateOptionChange()
   */
  onDeliveryDateOptionChange = (option: string): void => {
    const { formValues } = this.props;

    // Update delivery date depending on selected option on UI
    switch (option) {
      case 'tomorrow': {
        this.toggleCalendar(false);
        this.props.change(
          'delivery_day_picked',
          Moment().add(1, 'days').format('YYYY-MM-DD'),
        );
        break;
      }
      case 'picked': {
        try {
          this.toggleCalendar(true);
          const date =
            formValues.delivery_day_picked || Moment().format('YYYY-MM-DD');
          this.props.change('delivery_day_picked', date);
        } catch (e) {
          Sentry.captureException(e);
        }
        break;
      }
      case 'today': {
        this.toggleCalendar(false);
        this.props.change('delivery_day_picked', Moment().format('YYYY-MM-DD'));
        break;
      }
      default: {
        this.toggleCalendar(false);
        this.props.change('delivery_day_picked', null);
        break;
      }
    }
  };

  /**
   * filterDelivery()
   * Constructs a queryString portion based on delivery date availability.
   * @param {object} formValues || Form values
   * @returns {string} Delivery date queryString
   */
  filterDelivery = (formValues: FiltersType): string => {
    const deliveryDay = formValues.delivery_day;
    const pickedDay = formValues.delivery_day_picked;

    // Determine delivery date
    switch (deliveryDay) {
      case 'today':
        return `&delivery_date=${Moment().format('YYYY-MM-DD')}`;
      case 'tomorrow':
        return `&delivery_date=${Moment().add(1, 'days').format('YYYY-MM-DD')}`;
      case 'picked':
        if (pickedDay) {
          return `&delivery_date=${pickedDay}`;
        }
        return '';
      default:
        return '';
    }
  };

  /**
   * filterShipping()
   * Constructs a queryString portion based on shipping method.
   * @param {object} formValues || Form values
   * @returns {string} Shipping Method queryString
   */
  filterShipping = (formValues: FiltersType): string => {
    switch (formValues.shipping) {
      case 'cdmx':
        return '&zone=area-metropolitana';
      case 'monterrey':
        return '&zone=monterrey';
      case 'guadalajara':
        return '&zone=guadalajara';
      case 'national':
        return '&zone=national';
      default:
        return '';
    }
  };

  /**
   * filterPrice()
   * Constructs a queryString portion based on price range.
   * @param {FiltersType} formValues || Form values
   * @returns {string} Price range queryString
   */
  filterPrice = (formValues: FiltersType): string => {
    let concat = '';
    let minPrice = formValues.min_price;
    let maxPrice = formValues.max_price;

    if (minPrice === undefined) {
      minPrice = '';
    } else {
      concat += `&min_price=${minPrice}`;
    }
    if (maxPrice === undefined) {
      maxPrice = '';
    } else {
      concat += `&max_price=${maxPrice}`;
    }
    return concat;
  };

  setModalVisible(visible: boolean): void {
    this.setState({ modalVisible: visible });
  }

  /**
   * toggleCalendar()
   * Toogle calendar component visibility (use 'flag' param to enforce view state)
   * @param {bool} showCalendar | Optional view state
   */
  toggleCalendar = (showCalendar: boolean = false): void => {
    const shouldRenderCalendar =
      showCalendar !== undefined
        ? showCalendar
        : !this.state.shouldRenderCalendar;

    this.setState({
      shouldRenderCalendar,
    });
  };

  /**
   * getCalendarInstance
   * @returns  Calendar Compontent
   */
  getCalendarInstance = ({
    input,
    meta,
    markedDates,
    minDate,
    maxDate,
    selectedDate,
  }: {
    input: { onChange: Function },
    meta: {},
    markedDates: MarkedDatesType,
    minDate: string,
    maxDate: string,
    selectedDate: string,
  }) => {
    // Define default value
    if (!minDate) {
      minDate = Moment().format('YYYY-MM-DD');
    }
    if (!maxDate) {
      maxDate = Moment(minDate).add(1, 'M').format('YYYY-MM-DD');
    }
    // Get a list of days between a date interval.
    const datesRange: Array<string> = getDaysBetweenDates(minDate, maxDate);

    // Loop through a range of dates and evaluate if current date is a valid shipping date.
    // This is used to mark and control dates interaction on calendar.
    markedDates = datesRange.reduce(
      (
        datesAccumulator: MarkedDatesType,
        currentDate: string,
      ): MarkedDatesType => {
        // If selected date is equal to current iteration date,
        // mark this date as selected on the calendar UI.
        if (selectedDate === currentDate) {
          return {
            ...datesAccumulator,
            [currentDate]: {
              selected: true,
              disableTouchEvent: true,
            },
          };
        }

        // Validate if this is a valid working/shipping date.
        const enableDate: boolean = isValidWorkingDay(currentDate);
        return {
          ...datesAccumulator,
          [currentDate]: {
            disabled: !enableDate,
            disableTouchEvent: !enableDate,
          },
        };
      },
      markedDates,
    );

    // Return functional calendar component
    return (
      <Calendar
        style={styles.calendar}
        locale="es"
        view="month"
        minDate={minDate}
        maxDate={maxDate}
        onDayPress={(date) => {
          const selectedDateUTC = date.dateString;
          input.onChange(selectedDateUTC);
        }}
        hideExtraDays={true}
        markedDates={markedDates}
        theme={{
          calendarBackground: 'transparent',
          textSectionTitleColor: colors.colorWhite,
          selectedDayBackgroundColor: colors.colorMain300,
          selectedDayTextColor: colors.colorWhite,
          todayTextColor: colors.colorMain300,
          dayTextColor: colors.colorDark300,
          textDisabledColor: colors.colorGray400,
          dotColor: colors.colorMain300,
          selectedDotColor: colors.colorsMain300,
          arrowColor: colors.colorMain300,
          disabledArrowColor: colors.colorGray400,
          monthTextColor: colors.colorGray400,
          indicatorColor: 'blue',
          textDayFontFamily: 'Sarabun-Light',
          textMonthFontFamily: 'Sarabun-SemiBold',
          textDayHeaderFontFamily: 'Sarabun-Light',
          textDayFontWeight: '300',
          textMonthFontWeight: 'bold',
          textDayHeaderFontWeight: '300',
          textDayFontSize: 16,
          textMonthFontSize: 16,
          textDayHeaderFontSize: 16,
        }}
      />
    );
  };

  /**
   * render
   */
  render(): Node {
    const { handleSubmit, formValues } = this.props;
    const { shouldRenderCalendar } = this.state;

    // Get calendar component
    const selectedDate = formValues ? formValues.delivery_day_picked : null;
    const minimumShippingDate = Moment().format('YYYY-MM-DD');
    const maximumShippingDate = Moment(minimumShippingDate)
      .add(1, 'M')
      .format('YYYY-MM-DD');

    // Return JSX
    return (
      <View>
        {this.props.isenabled && (
          <TouchableOpacity
            style={styles.filterBtn}
            onPress={() => {
              this.setModalVisible(true);
            }}
          >
            <CRIcon name="filter" size={18} color={colors.colorGray300} />
          </TouchableOpacity>
        )}
        {!this.props.isenabled && (
          <View style={styles.disabledFilterBtn}>
            <Image source={filter} style={styles.iconFilter} />
          </View>
        )}

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
        >
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => {
                this.setModalVisible(false);
              }}
              style={styles.btnClose}
              activeOpacity={0.5}
            >
              <View style={styles.iconClose}>
                <CRIcon name="close" size={18} color={colors.colorGray400} />
              </View>
            </TouchableOpacity>
            <KeyboardAwareScrollView
              contentContainerStyle={styles.scrollViewStyle}
              resetScrollToCoords={{ x: 0, y: 0 }}
              scrollEnabled
            >
              <CRText
                variant={typography.subtitle2}
                color={colors.colorDark200}
                align="center"
              >
                Filtros
              </CRText>

              <View style={styles.container}>
                {/*<View>
                  <Text style={styles.filterTitle}>Ordenar por</Text>
                  <View style={styles.containerFilter}>
                    <Field
                      radios={[
                        {
                          label: 'Creación',
                          value: 'created',
                        },
                        {
                          label: 'Precio',
                          value: 'price',
                        },
                        {
                          label: 'Nombre',
                          value: 'name',
                        },
                      ]}
                      component={RadioSet}
                      name="sort"
                      color={colors.colorWhite}
                      borderColor={colors.colorWhite}
                    />
                  </View>
                    </View> */}
                <View>
                  <Text style={styles.filterTitle}>Día de entrega</Text>
                  <View>
                    <Field
                      radios={[
                        {
                          label: 'Recíbelo hoy',
                          value: 'today',
                          id: 'today',
                        },
                        {
                          label: 'Recíbelo mañana',
                          value: 'tomorrow',
                          id: 'tomorrow',
                        },
                      ]}
                      component={RadioSet}
                      name="delivery_day"
                      color={colors.colorGray400}
                      borderColor={colors.colorGray300}
                      onChange={this.onDeliveryDateOptionChange}
                    />
                    <Field
                      radios={[
                        {
                          label: 'Elige una fecha',
                          value: 'picked',
                          id: 'picked',
                        },
                      ]}
                      component={RadioSet}
                      name="delivery_day"
                      color={colors.colorGray400}
                      borderColor={colors.colorGray300}
                      onChange={this.onDeliveryDateOptionChange}
                    />

                    {shouldRenderCalendar && (
                      <Field
                        name="delivery_day_picked"
                        component={this.getCalendarInstance}
                        selectedDate={selectedDate}
                        minDate={minimumShippingDate}
                        maxDate={maximumShippingDate}
                        markedDates={{}}
                      />
                    )}
                  </View>
                </View>
                {/* <View>
                  <Text style={styles.filterTitle}>Lugar de entrega</Text>
                  <Field
                    radios={[
                      {
                        label: 'Envío Nacional',
                        value: 'national',
                        id: 'national',
                      },
                      {
                        label: 'CDMX',
                        value: 'area-metropolitana',
                        id: 'cdmx',
                      },
                      {
                        label: 'Monterrey',
                        value: 'monterrey',
                        id: 'monterrey',
                      },
                      {
                        label: 'Guadalajara',
                        value: 'guadalajara',
                        id: 'guadalajara',
                      },
                    ]}
                    component={RadioSet}
                    name="shipping"
                    color={colors.colorGray400}
                    borderColor={colors.colorGray300}
                  />
                </View> */}
                {/*
                <View style={styles.containerFilter}>
                  <Text style={styles.filterTitle}>Ubicación</Text>
                  <Field
                    name="location"
                    id="location"
                    keyboardType="number-pad"
                    placeholder="C.P."
                    component={renderInput}
                  />
                </View>
                */}
                <View style={styles.containerFilter}>
                  <Text style={styles.filterTitle}>Precio</Text>
                  <Field
                    name="min_price"
                    id="min_price"
                    keyboardType="number-pad"
                    placeholder="min"
                    component={renderInput}
                  />
                  <Text style={styles.filterTitle}>-</Text>
                  <Field
                    name="max_price"
                    id="max_price"
                    keyboardType="number-pad"
                    placeholder="max"
                    component={renderInput}
                  />
                </View>
              </View>
              <View style={styles.containerBtn}>
                <TouchableOpacity onPress={handleSubmit(this.onSubmit)}>
                  <Text style={styles.submitBtn}>Aplicar Filtros</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.resetForm}>
                  <Text style={styles.resetBtn}>Borrar</Text>
                </TouchableOpacity>
              </View>
            </KeyboardAwareScrollView>
          </View>
        </Modal>
      </View>
    );
  }
}

Filters = reduxForm({
  form: 'filtersForm',
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
})(Filters);

// Map Redux Props and Actions to component
function mapStateToProps(state, ownProps) {
  let initialValues = {
    ...DEFAULT_FILTERS,
    stores: state.app.storesList.results,
  };
  // const definedValues = Object.keys(ownProps.urlValues).reduce((b, a) => {
  //   if (initialValues[a] !== ownProps.urlValues[a]) {
  //     return {...b, [a]: ownProps.urlValues[a]};
  //   }
  //   return b;
  // }, {});
  initialValues = {
    ...initialValues,
    // ...definedValues,
  };
  return {
    formValues: getFormValues('filtersForm')(state),
    initialValues,
  };
}

function mapDispatchToProps(dispatch) {
  // const {getShippingZones} = appActions;
  return bindActionCreators(
    {
      // getShippingZones,
    },
    dispatch,
  );
}

// Export Component
Filters = connect(mapStateToProps, mapDispatchToProps)(Filters);
