import { StyleSheet } from 'react-native';

export const style = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  text: {
    fontSize: 16,
    paddingLeft: 5,
    paddingRight: 5,
  },

  number: {
    minWidth: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },

  touchable: {
    width: 31,
    height: 31,
    borderRadius: 300,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
