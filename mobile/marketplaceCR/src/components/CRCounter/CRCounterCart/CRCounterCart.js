import React, { Component } from 'react';
import { View, TouchableOpacity, Platform } from 'react-native';
import PropTypes from 'prop-types';

import { style } from './style';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '../../../assets';
import { colors } from '@canastarosa/ds-theme/colors';

export default class Counter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      number: this.props.start,
    };

    // bind functions..
    this.onPressMinus = this.onPressMinus.bind(this);
    this.onPressPlus = this.onPressPlus.bind(this);
  }

  onPressMinus() {
    const { number } = this.state;
    const minusNumber = number - 1;

    if (number === this.props.min) {
      return;
    }

    return this.setState({ number: minusNumber }, () =>
      this.props.onChange(minusNumber, '-'),
    );
  }

  onPressPlus() {
    const { number } = this.state;
    const plusNumber = number + 1;

    if (number === this.props.max) {
      return;
    }

    return this.setState({ number: plusNumber }, () =>
      this.props.onChange(plusNumber, '+'),
    );
  }

  renderMinusButton() {
    const { min, touchableDisabledColor, touchableColor } = this.props;
    const isMinusDisabled = min === this.state.number;
    const buttonStyle = {
      borderColor: isMinusDisabled ? touchableDisabledColor : touchableColor,
    };
    return (
      <TouchableOpacity
        style={[style.touchable, buttonStyle]}
        onPress={this.onPressMinus}
        activeOpacity={isMinusDisabled ? 0.9 : 0.2}
      >
        {this.props.minusIcon ? (
          this.props.minusIcon(
            isMinusDisabled,
            touchableDisabledColor,
            touchableColor,
          )
        ) : (
          <CRText
            variant={typography.paragraph}
            color={isMinusDisabled ? touchableDisabledColor : touchableColor}
          >
            -
          </CRText>
        )}
      </TouchableOpacity>
    );
  }

  renderMinusAndroidButton() {
    const { min, touchableDisabledColor, touchableColor } = this.props;
    const isMinusDisabled = min === this.state.number;
    const buttonStyle = {
      borderColor: isMinusDisabled ? touchableDisabledColor : touchableColor,
    };
    return (
      <TouchableOpacity
        style={[style.touchable, buttonStyle]}
        onPress={this.onPressMinus}
        activeOpacity={isMinusDisabled ? 0.9 : 0.2}
      >
        {!this.props.minusIcon ? (
          this.props.minusIcon(
            isMinusDisabled,
            touchableDisabledColor,
            touchableColor,
          )
        ) : (
          <CRText
            variant={typography.txtExtraBold}
            color={isMinusDisabled ? touchableDisabledColor : touchableColor}
          >
            -
          </CRText>
        )}
      </TouchableOpacity>
    );
  }

  renderPlusButton() {
    const { max, touchableDisabledColor, touchableColor } = this.props;
    const isPlusDisabled = max === this.state.number;
    const buttonStyle = {
      borderColor: isPlusDisabled ? touchableDisabledColor : touchableColor,
    };

    return (
      <TouchableOpacity
        style={[style.touchable, buttonStyle]}
        onPress={this.onPressPlus}
        activeOpacity={isPlusDisabled ? 0.9 : 0.2}
      >
        {this.props.plusIcon ? (
          this.props.plusIcon(
            isPlusDisabled,
            touchableDisabledColor,
            touchableColor,
          )
        ) : (
          <CRText
            variant={typography.txtExtraBold}
            color={isPlusDisabled ? touchableDisabledColor : touchableColor}
            align="center"
          >
            +
          </CRText>
        )}
      </TouchableOpacity>
    );
  }

  renderPlusAndroidButton() {
    const { max, touchableDisabledColor, touchableColor } = this.props;
    const isPlusDisabled = max === this.state.number;
    const buttonStyle = {
      borderColor: isPlusDisabled ? touchableDisabledColor : touchableColor,
    };

    return (
      <TouchableOpacity
        style={[style.touchable, buttonStyle]}
        onPress={this.onPressPlus}
        activeOpacity={isPlusDisabled ? 0.9 : 0.2}
      >
        {!this.props.plusIcon ? (
          this.props.plusIcon(
            isPlusDisabled,
            touchableDisabledColor,
            touchableColor,
          )
        ) : (
          <CRText
            variant={typography.paragraph}
            color={isPlusDisabled ? touchableDisabledColor : touchableColor}
            align="center"
          >
            +
          </CRText>
        )}
      </TouchableOpacity>
    );
  }

  renderPlusAndroidButton() {
    const { max, touchableDisabledColor, touchableColor } = this.props;
    const isPlusDisabled = max === this.state.number;
    const buttonStyle = {
      borderColor: isPlusDisabled ? touchableDisabledColor : touchableColor,
    };

    return (
      <TouchableOpacity
        style={[style.touchable, buttonStyle]}
        onPress={this.onPressPlus}
        activeOpacity={isPlusDisabled ? 0.9 : 0.2}
      >
        {!this.props.plusIcon ? (
          this.props.plusIcon(
            isPlusDisabled,
            touchableDisabledColor,
            touchableColor,
          )
        ) : (
          <CRText
            variant={typography.txtExtraBold}
            color={isPlusDisabled ? touchableDisabledColor : touchableColor}
            align="center"
          >
            +
          </CRText>
        )}
      </TouchableOpacity>
    );
  }

  render() {
    const { number } = this.state;
    const isAndroid = Platform.OS === 'android';
    const isIOS = Platform.OS === 'ios';

    return (
      <View style={style.container}>
        {isIOS && <View>{this.renderMinusButton()}</View>}
        {isAndroid && <View>{this.renderMinusAndroidButton()}</View>}
        <View style={style.number}>
          <CRText color={this.props.textColor}>{number}</CRText>
        </View>
        {isIOS && <View>{this.renderPlusButton()}</View>}
        {isAndroid && <View>{this.renderPlusAndroidButton()}</View>}
      </View>
    );
  }
}

Counter.propTypes = {
  start: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  onChange: PropTypes.func,

  textColor: PropTypes.string,
  touchableColor: PropTypes.string,
  touchableDisabledColor: PropTypes.string,

  minusIcon: PropTypes.func,
  plusIcon: PropTypes.func,
};

Counter.defaultProps = {
  start: 1,
  min: 1,
  max: 10,
  onChange(number, type) {
    // Number, - or +
  },

  textColor: '#43434C',
  touchableColor: colors.colorDark100,
  touchableDisabledColor: colors.colorGray100,

  minusIcon: null,
  plusIcon: null,
};
