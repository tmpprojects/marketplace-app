import React from 'react';
import { StyleSheet, Image, View } from 'react-native';

import mayordomo from '@canastarosa/ds-theme/assets/images/illustrations/mayordomo.png';
import arrow from '../images/icon/icon_arrow_right.png';

//DS
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
import { typography } from '@canastarosa/ds-theme/typography';
import { colors } from '@canastarosa/ds-theme/colors';

export default function BannerMayordomo() {
  return (
    <View style={styles.container}>
      <Image source={mayordomo} style={styles.image} />
      <CRText variant={typography.paragraph} color={colors.colorDark300}>
        ¿Buscas recomendaciones? Contacta al
      </CRText>
      <CRText variant={typography.paragraph} color={colors.colorMain300}>
        Mayordomo Rosa
      </CRText>
      <Image source={arrow} style={styles.icon} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.white,
  },
  text: {
    //...typography.txtRegular,
    flex: 4,
    padding: 20,
    textAlign: 'center',
  },
  bold: {
    //color: colors.corePink,
  },
  icon: {
    flex: 1,
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  image: {
    flex: 1,
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
});
