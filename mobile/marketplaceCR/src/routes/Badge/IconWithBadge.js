import React from 'react';
import { View, Text, Animated } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { CRIcon } from '../../components/CRIcon';
import { styles } from './IconWithBadgeStyles';

declare var globalThis: any;

export class Badge extends React.Component {
  state = {
    badgeScale: new Animated.Value(0),
  };

  animatedBadge() {
    Animated.timing(this.state.badgeScale, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
    });
  }

  render() {
    const { name, productsCount, color } = this.props;
    return (
      <View style={styles.container}>
        <CRIcon name={name} size={25} color={color} />
        {productsCount > 0 ? (
          <View style={styles.badge}>
            <Text style={styles.badgeCount}>{productsCount}</Text>
          </View>
        ) : null}
      </View>
    );
  }
}

const IconWithBadge = (props) => {
  return <Badge {...props} />;
};

function mapStateToProps(state) {
  return {
    productsCount: globalThis.ShoppingCartManager.getCartProductsCountTotal(),
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({});
}

export default connect(mapStateToProps, mapDispatchToProps)(IconWithBadge);
