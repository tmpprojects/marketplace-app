import { StyleSheet } from 'react-native';
import { typography } from '../../assets';
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const styles = StyleSheet.create({
  container: { width: 29, height: 29, margin: 15 },
  iconMenu: { width: 20, height: 20 },
  icon: { width: 23, height: 20 },
  badge: {
    position: 'absolute',
    right: -10,
    top: -8,
    backgroundColor: colors.colorMain300,
    borderRadius: 18 / 2,
    width: 18,
    height: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgeCount: {
    ...typography.txtBold,
    fontSize: 11,
    color: colors.colorWhite,
  },
});
