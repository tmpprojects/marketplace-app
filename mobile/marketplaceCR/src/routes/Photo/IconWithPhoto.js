import React from 'react';
import { Image, View, Platform } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import defaultProfile from '../../images/icon/profileDefault.png';
import { styles } from './IconWithPhotoStyles';
import { getUserProfile } from '../../redux/selectors/users.selectors';
import { CRIcon } from '../../components/CRIcon';

export class Photo extends React.Component {
  // If it's a SVG then use a mock PNG
  checkImageProfile = (user) => {
    if (user.profile_photo) {
      if (user.profile_photo.small) {
        const currenImage = String(user.profile_photo.small);
        const extension = currenImage.split('.').pop().toLowerCase();
        if (
          extension !== 'svg' &&
          extension !== '' &&
          extension !== null &&
          extension !== undefined
        ) {
          return { uri: user.profile_photo.small };
        } else {
          return defaultProfile;
        }
      }
    }
    return defaultProfile;
  };

  render() {
    const { user, name, isLogged, color } = this.props;
    const isAndroid: Boolean = Platform.OS === 'android';

    return (
      <React.Fragment>
        {!isLogged || user.profile_photo === undefined ? (
          <CRIcon name={name} size={20} color={color} />
        ) : (
          <View style={styles.photoProfile}>
            <Image source={this.checkImageProfile(user)} style={styles.photo} />
          </View>
        )}
      </React.Fragment>
    );
  }
}

const IconWithPhoto = (props) => {
  return <Photo {...props} />;
};

function mapStateToProps(state) {
  const { user } = state;
  return {
    isLogged: user.isLogged,
    user: getUserProfile(state),
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({});
}

export default connect(mapStateToProps, mapDispatchToProps)(IconWithPhoto);
