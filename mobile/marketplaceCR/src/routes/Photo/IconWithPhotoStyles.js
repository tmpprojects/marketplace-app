import { StyleSheet, Platform, Dimensions } from 'react-native';
import colors from '@canastarosa/ds-theme/colors/js/variables';
const alto = Dimensions.get('window').height;
console.log(alto);

export const styles = StyleSheet.create({
  photoProfile: {
    width: Platform.OS === 'android' ? 28 : alto < 737 ? 30 : 28,
    height: Platform.OS === 'android' ? 28 : alto < 737 ? 30 : 28,
    borderRadius: 30 / 2,
    backgroundColor: colors.colorWhite,
  },
  photo: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 96 / 2,
  },
  iconProfile: { width: 20, height: 20 },
});
