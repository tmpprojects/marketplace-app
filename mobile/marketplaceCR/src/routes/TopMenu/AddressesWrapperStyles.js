import { StyleSheet } from 'react-native';
import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography } from '../../assets';

export const styles = StyleSheet.create({
  modalView: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    height: '100%',
  },
  modalViewTO: {
    height: '100%',
  },
  loginGuest: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
    paddingTop: 20,
  },
  container: {
    backgroundColor: colors.colorWhite,
    width: '100%',
    height: '60%',
    position: 'absolute',
    bottom: 0,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  addresesListWrapper: {},
  messageAddress: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: '60%',
  },
  iconArrow: {
    marginTop: 3,
    marginLeft: 8,
    alignSelf: 'center',
  },
  iconLocation: {
    marginTop: 3,
    marginLeft: 8,
    marginRight: 8,
    alignSelf: 'center',
  },
  addressWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 5,
  },
  title: {
    ...typography.txtSemiBold,
    color: colors.colorMain300,
    textAlign: 'center',
  },
  btnClose: { alignItems: 'flex-end' },
  addressesList: {
    paddingBottom: 10,
    paddingHorizontal: 5,
  },
  iconClose: {
    color: colors.colorGray400,
    fontSize: 20,
    marginVertical: 15,
  },
  addressCard: {
    width: 280,
    marginRight: 20,
    alignContent: 'stretch',
  },
});
