import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TouchableOpacity, SafeAreaView, View } from 'react-native';

import { layout } from './TopMenuStyles';
import { CRSearchInput } from '../../components/CRSearchInput';

// DS
import { colors } from '@canastarosa/ds-theme/colors';
import { CRIcon } from '../../components/CRIcon';
import AddressesWrapper from './AddressesWrapper';

declare var globalThis: any;
export class TopMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isModalVisible: false,
    };
  }

  toggleModal = (visible) => {
    visible
      ? this.setState({ isModalVisible: true })
      : this.setState({ isModalVisible: false });
  };

  openSearch = () => {
    if (
      this.props.navigation.state.key === 'Cart' &&
      this.props.cart.length === 0
    ) {
      return false;
    }
    this.setState({ isOpen: true });
  };
  render() {
    const { navigation, addressList = [] } = this.props;
    const { isOpen } = this.state;
    const currentScreen = navigation.state;

    return (
      <SafeAreaView style={layout.container}>
        <View style={layout.barContainer}>
          {currentScreen.key !== 'Home' && (
            <TouchableOpacity
              onPress={() => navigation.goBack(null)}
              title="Back"
              style={layout.arrowWrapper}
            >
              <CRIcon
                name="arrow--left"
                size={20}
                color={colors.colorDark300}
              />
            </TouchableOpacity>
          )}
          {currentScreen.routeName !== 'CheckoutScreens' && (
            <AddressesWrapper
              addresses={addressList}
              style={layout.addressWrapper}
              navigation={this.props.navigation}
              route={this.props.route}
            />
          )}
          {currentScreen.key !== 'Home' &&
            currentScreen.routeName !== 'CheckoutScreens' && (
              <TouchableOpacity
                onPress={this.openSearch}
                style={layout.searchWrapper}
              >
                <CRIcon name="search" size={20} color={colors.colorDark300} />
              </TouchableOpacity>
            )}
        </View>
        {(currentScreen.key === 'Home' ||
          (currentScreen.key === 'Cart' && this.props.cart.length === 0)) && (
          <CRSearchInput navigation={navigation} />
        )}
        {isOpen && <CRSearchInput navigation={navigation} />}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    addressList: globalThis.ShoppingCartManager.getAddresses(),
    cart: state.cart.products,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({});
}

export default connect(mapStateToProps, mapDispatchToProps)(TopMenu);
