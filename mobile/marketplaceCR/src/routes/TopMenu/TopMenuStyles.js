import { StyleSheet } from 'react-native';

//DS
import { colors } from '@canastarosa/ds-theme/colors';

export const layout = StyleSheet.create({
  //IOS STYLES
  container: {
    backgroundColor: colors.colorWhite,
  },
  barContainer: {
    flex: 1,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colors.colorGray200,
    paddingHorizontal: 15,
    backgroundColor: colors.colorWhite,
  },
  addressWrapper: {
    flex: 1,
  },
  arrowWrapper: {
    flex: 1,
  },
  searchWrapper: {
    // flex: 1,
    // alignItems: 'flex-end',
    marginRight: 10,
  },
  //ANDROID STYLES
  containerAndroid: {
    height: 50,
    flexDirection: 'row',
    marginBottom: -10,
  },
  containerBackArrowAndroid: {
    width: '20%',
    alignSelf: 'center',
    alignItems: 'center',
  },
  containerAddressAndroid: {
    width: '20%',
    alignSelf: 'center',
    alignItems: 'center',
  },
  containerSearchAndroid: { width: '60%', alignSelf: 'center' },
});
