/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, formValueSelector } from 'redux-form';
import { Alert, View, TouchableOpacity, ScrollView, Modal } from 'react-native';
import * as Sentry from '@sentry/react-native';

import { useNavigationState } from '@react-navigation/native';
import { _customSetData, _customGetData } from '../../utils/asyncStorage';

import withShoppingCartForm from '../../utils/hocs/withShoppingCartForm';
import { AddressType } from '../../redux/types/AddressesTypes';
import { getAddressesList } from '../../redux/selectors/users.selectors';
import { appActions, userActions } from '../../redux/actions';
import AddressCard from '../../components/Profile/Address/AddressCard/AddressCard';
import AddressModal from '../../components/Profile/Address/AddressModal/AddressModal';

import colors from '@canastarosa/ds-theme/colors/js/variables';
import { typography } from '@canastarosa/ds-theme/typography';
import { styles } from './AddressesWrapperStyles';
import { CRIcon } from '../../components/CRIcon';
import { CRText } from '@canastarosa/ds-reactnative/components/CRText';
declare var globalThis: any;

type AddresType = {
  uuid: string | number,
  zip_code: string | number,
};
type AddressesWrapperProps = {
  addAddress: Function,
  deleteAddress: Function,
  updateAddress: Function,
  listAddresses: Function,
  onUpdateAddress: Function,
  addressList: Array<AddresType>,
  shippingAddress: AddresType,
};
type AddressesWrapperState = {
  shouldRenderAddressesListModal: boolean,
  shouldRenderAddressFormModal: boolean,
};

export class AddressesWrapper extends Component<
  AddressesWrapperProps,
  AddressesWrapperState,
> {
  state = {
    shouldRenderAddressesListModal: false,
    shouldRenderAddressFormModal: false,
  };

  constructor(props) {
    super(props);
  }

  getAndUpdateZipCode(newZipCode) {
    this.props.updateZipCode(newZipCode);
  }

  componentDidMount = () => {
    this.toggleAddressFormModal(false);
  };

  toggleAddressFormModal = (visible: boolean = false): void => {
    visible === undefined
      ? this.setState({ shouldRenderAddressFormModal: false })
      : this.setState({ shouldRenderAddressFormModal: visible });
  };

  toggleAddressListModal = (visible: boolean = false): void => {
    visible === undefined
      ? this.setState({ shouldRenderAddressesListModal: false })
      : this.setState({ shouldRenderAddressesListModal: visible });
  };

  onAddAddress = async (addressId: string): void => {
    try {
      // Close address form and list modals.
      this.toggleAddressFormModal(false);
      this.toggleAddressListModal(false);

      // Add address
      await this.props.addAddress(addressId).catch((e) => {
        Sentry.captureException(e);
      });

      let getAddresses = await this.props.addresses;

      if (this.props.isGuest && getAddresses) {
        if (getAddresses.length) {
          _customSetData('addresses', getAddresses);
        }
      }
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  onDeleteAddress = (addressId: string): void => {
    try {
      // Close address form and list modals.
      this.toggleAddressFormModal(false);

      // Delete address
      this.props.deleteAddress(addressId).catch((e) => {
        Sentry.captureException(e);
      });
    } catch (e) {
      Sentry.captureException(e);
    }
  };
  componentDidUpdate(prevProps) {
    try {
      if (this.props.shippingAddress) {
        this.getAndUpdateZipCode(this.props.shippingAddress.zip_code);
      }
    } catch (e) {}
  }

  onUpdateAddress = async (targetAddress: AddressType): void => {
    try {
      //verify that address exists
      const validatedAddress = this.props.addressList.find(
        (a) => a.uuid === targetAddress.uuid,
      );
      // TODO: Return an UI error to notify user about something went wrong.
      if (!validatedAddress) {
        // Handle state here...
      }

      // Close address form and list modals.
      await this.toggleAddressListModal(false);

      // Update address
      await this.props
        .updateAddress({ ...validatedAddress, ...targetAddress })
        .catch((error) => {
          Sentry.captureException(error);
        });

      let getAddresses = await this.props.addresses;
      // _customSetData('addresses',getAddresses);

      if (this.props.isGuest && getAddresses) {
        if (getAddresses.length) {
          _customSetData('addresses', getAddresses);
        }
      }
    } catch (e) {
      Sentry.captureException(e);
    }
  };

  onAddressChange = (targetAddress: AddressType): void => {
    // Update Default Pickup Address
    // FIX: Telephone
    this.onUpdateAddress({
      ...targetAddress,
      default_address: true,
    });
  };

  render() {
    const { addressList, shippingAddress, navigation } = this.props;

    // Return JSX
    return (
      <View>
        <TouchableOpacity
          style={styles.addressWrapper}
          onPress={() => {
            this.toggleAddressListModal(true);
          }}
        >
          <CRIcon
            name="map"
            size={18}
            color={colors.colorWhite}
            style={styles.iconLocation}
          />
          {!shippingAddress || addressList.length === 0 ? (
            <CRText
              variant={typography.paragraph}
              color={colors.colorWhite}
              align="center"
            >
              Añadir dirección
            </CRText>
          ) : (
            <CRText
              variant={typography.paragraph}
              color={colors.colorWhite}
              align="center"
            >
              Enviar a C.P. {shippingAddress.zip_code}
            </CRText>
          )}
          {/* <CRIcon
            name="angle--down"
            size={8}
            color={colors.colorGray400}
            style={styles.iconArrow}
          /> */}
        </TouchableOpacity>

        {/* ADDRESSES LIST MODAL BOX */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.shouldRenderAddressesListModal}
          onRequestClose={() => {
            this.toggleAddressListModal(false);
          }}
        >
          <View style={styles.modalView}>
            <TouchableOpacity
              style={styles.modalViewTO}
              onPress={() => {
                this.toggleAddressListModal(false);
              }}
            />
            <View style={styles.container}>
              <TouchableOpacity
                onPress={() => {
                  this.toggleAddressListModal(false);
                }}
                style={styles.btnClose}
                activeOpacity={0.5}
              >
                <CRIcon name="close" style={styles.iconClose} />
              </TouchableOpacity>

              {/* ADDRESSES LIST */}
              <View style={styles.addresesListWrapper}>
                {/* USER HAS NO STORED ADDRESSES */}
                {addressList.length === 0 ? (
                  <View style={styles.messageAddress}>
                    <CRIcon
                      name="home"
                      size={20}
                      color={colors.colorGray300}
                      style={styles.iconItem}
                    />
                    <CRText
                      variant={typography.paragraph}
                      color={colors.colorGray400}
                      align="center"
                    >
                      Aún no tienes direcciones
                    </CRText>
                  </View>
                ) : (
                  <React.Fragment>
                    {/* USER HAS ADDRESSES */}
                    <CRText
                      variant={typography.subtitle2}
                      color={colors.colorDark200}
                    >
                      Selecciona una dirección
                    </CRText>

                    {/* LIST */}
                    <ScrollView
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                      style={styles.addressesList}
                    >
                      {addressList.map((address, index) => (
                        <View key={index} style={styles.addressCard}>
                          <Field
                            name="shippingAddress"
                            address={address}
                            component={AddressCard}
                            id={`address_${address.uuid}`}
                            toggleModal={this.toggleAddressFormModal}
                            changeAddress={this.onAddressChange}
                            //updateAddress={this.onUpdateAddress}
                            deleteAddress={this.onDeleteAddress}
                            isGuest={this.props.isGuest}
                            modalVisible={false}
                            // active={
                            //   shippingAddress &&
                            //   address.uuid === shippingAddress.uuid
                            // }
                            active={
                              shippingAddress &&
                              address.latitude === shippingAddress.latitude &&
                              address.longitude === shippingAddress.longitude
                            }
                          />
                        </View>
                      ))}
                    </ScrollView>
                    {/* END: LIST */}
                  </React.Fragment>
                )}

                <TouchableOpacity
                  style={styles.btnAdd}
                  onPress={() => this.toggleAddressFormModal(true)}
                  activeOpacity={0.5}
                >
                  <CRText
                    variant={typography.paragraph}
                    color={colors.colorMain300}
                    align="center"
                  >
                    Añadir nueva dirección de entrega
                  </CRText>
                </TouchableOpacity>
                <AddressModal
                  title="Nueva dirección de entrega"
                  btnAction="Guardar"
                  onSubmit={this.onAddAddress}
                  modalVisible={this.state.shouldRenderAddressFormModal}
                  toggleModal={this.toggleAddressFormModal}
                  initialValues={{}}
                />
              </View>
              {/* END: ADDRESSES LIST */}
            </View>
          </View>
        </Modal>
        {/* END: ADDRESSES LIST MODAL BOX */}
      </View>
    );
  }
}

AddressesWrapper = withShoppingCartForm(AddressesWrapper, {});

function mapStateToProps(state) {
  return {
    addresses: getAddressesList(state),
    isGuest: state.user.isGuest,
    zipCode: state.app.zipCode,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      listAddresses: userActions.listAddresses,
      addAddress: userActions.addAddress,
      deleteAddress: userActions.deleteAddress,
      updateAddress: userActions.updateAddress,
      updateZipCode: appActions.updateZipCde,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(AddressesWrapper);
