import { userActionsType } from '../constants';
import { _clearData } from '../../utils/asyncStorage';

const loadingStatusIdle = {
  error: false,
  loading: false,
};
const loadingStatus = {
  idle: {
    error: false,
    loading: false,
  },
  loading: {
    error: false,
    loading: true,
  },
  error: {
    error: true,
    loading: false,
  },
};
const defaultState = {
  isLogged: false,
  isGuest: false,
  message: 'Welcome.',
  verify: false,
  verifyAutoLogin: false,
  verifyStatus: 'pending',
  validate: false,
  cookies: null,
  profile: {
    has_store: false,
    uploadphoto: '',
    ...loadingStatus.loading,
    profile: [],
  },
  addresses: {
    ...loadingStatus.loading,
    addresses: [],
  },
  reviews: {
    pending: {
      ...loadingStatus.loading,
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
    completed: {
      ...loadingStatus.loading,
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
  },
  cards: {
    ...loadingStatus.loading,
    cards: [],
  },
};

export function user(state = defaultState, action) {
  switch (action.type) {
    // UPLOAD PHOTO
    case userActionsType.UPLOAD_PHOTO_REQUEST:
      return {
        ...state,
        profile: {
          ...loadingStatus.loading,
          ...state.profile,
          uploadphoto: 'pending',
        },
      };
    case userActionsType.UPLOAD_PHOTO_SUCCESS:
      return {
        ...state,
        profile: {
          ...loadingStatus.idle,
          ...action.payload,
          uploadphoto: 'done',
        },
      };
    case userActionsType.UPLOAD_PHOTO_FAILURE:
      return {
        ...state,

        profile: {
          ...state.profile,
          ...loadingStatus.error,
          error: action.error,
          uploadphoto: 'error',
        },
      };

    case userActionsType.AUTOLOGIN_SUCCESS:
      return {
        ...state,
      };

    case userActionsType.VERIFY_AUTOLOGIN_SUCCESS:
      return {
        ...state,
        verifyAutoLogin: action.verifyAutoLogin,
      };

    //LOGIN ACCESS
    case userActionsType.USER_SET_COOKIES:
      return {
        ...state,
        isLogged: true,
        isGuest: false,
        message: 'Using a previous session.',
        cookies: action.headers,
      };

    case userActionsType.USERS_LOGIN_SUCCESS:
      //Set info cookie
      return {
        ...state,
        isLogged: true,
        isGuest: false,
        cookies: action.headers,
        message: 'Se inicio sesión.',
      };
    case userActionsType.USERS_LOGIN_FAILURE:
      return {
        ...state,
        isLogged: false,
        cookies: undefined,
        message: 'Ocurrio un error.',
      };
    case userActionsType.LOGOUT_SUCCESS:
      //Clear AsyncStorage
      _clearData();
      return {
        ...state,
        isLogged: false,
        cookies: undefined,
        message: 'Logout.',
        addresses: {
          addresses: [],
        },
      };

    // SET USER AS GUEST
    case userActionsType.SET_USER_AS_GUEST_REQUEST:
      return {
        ...state,
      };
    case userActionsType.SET_USER_AS_GUEST_SUCCESS:
      return {
        ...state,
        isGuest: true,
        isLogged: false,
      };
    case userActionsType.SET_USER_AS_GUEST_FAILURE:
      return {
        ...state,
        isGuest: false,
      };

    //REGISTER
    case userActionsType.USERS_REGISTER_REQUEST:
      return {
        ...state,
        status: 'waiting',
        payload: {},
      };
    case userActionsType.USERS_REGISTER_SUCCESS:
      return {
        ...state,
        status: 'success',
        verifyStatus: 'waiting',
        payload: action.payload,
      };
    case userActionsType.USERS_REGISTER_FAILURE:
      return {
        ...state,
        status: 'error',
        payload: action.payload.data,
      };

    //  VERIFY EMAIL ACCOUNT
    case userActionsType.VERIFY_EMAIL_ACCOUNT_REQUEST:
      return {
        ...state,
        verifyStatus: 'waiting',
      };
    case userActionsType.VERIFY_EMAIL_ACCOUNT_SUCCESS:
      return {
        ...state,
        isLogged: true,
        isGuest: false,
        verifyStatus: 'done',
        cookies: action.headers,
        //payload: action.payload.data,
      };
    case userActionsType.VERIFY_EMAIL_ACCOUNT_FAILURE:
      return {
        ...state,
      };

    //  VERIFY ACCOUNT
    case userActionsType.VERIFY_ACCOUNT_REQUEST:
      return {
        ...state,
        verify: false,
        payload: {},
      };
    case userActionsType.VERIFY_ACCOUNT_SUCCESS:
      return {
        ...state,
        verify: true,
        payload: action.payload.data,
      };
    case userActionsType.VERIFY_ACCOUNT_FAILURE:
      return {
        ...state,
        verify: false,
        payload: action.payload.data,
      };

    // VALIDATE CODE
    case userActionsType.VALIDATE_CODE_REQUEST:
      return {
        ...state,
        validate: false,
        payload: {},
      };
    case userActionsType.VALIDATE_CODE_SUCCESS:
      return {
        ...state,
        validate: true,
        payload: action.payload.data,
      };
    case userActionsType.VALIDATE_CODE_FAILURE:
      return {
        ...state,
        validate: false,
        payload: action.payload.data,
      };

    // PASSWORD RECOVER
    case userActionsType.PASSWORD_RECOVER_REQUEST:
      return {
        ...state,
      };
    case userActionsType.PASSWORD_RECOVER_SUCCESS:
      return {
        ...state,
        message: action.payload,
      };
    case userActionsType.PASSWORD_RECOVER_FAILURE:
      return {
        ...state,
        message: action.payload,
      };

    // CURRENT USER
    case userActionsType.GET_CURRENT_USER_REQUEST:
      return {
        ...state,
        profile: {
          // ...loadingStatus.loading
        },
      };
    case userActionsType.GET_CURRENT_USER_SUCCESS:
      return {
        ...state,
        isLogged: true,
        isGuest: false,
        profile: {
          ...loadingStatus.idle,
          ...action.payload,
        },
      };
    case userActionsType.GET_CURRENT_USER_FAILURE:
      //_clearData();
      return {
        ...state,
        isLogged: false,
        cookies: undefined,
        message: 'Logout.',
      };

      return {
        ...state,
        profile: {
          // ...loadingStatus.error,
          error: action.error,
        },
      };

    // UPDATE USER PROFILE
    case userActionsType.UPDATE_INFO_REQUEST:
      return {
        ...state,
        profile: {
          ...loadingStatus.loading,
          ...state.profile,
        },
      };
    case userActionsType.UPDATE_INFO_SUCCESS:
      return {
        ...state,
        profile: {
          ...state.profile,
          ...loadingStatus.idle,
          ...action.payload,
        },
      };
    case userActionsType.UPDATE_INFO_ERROR:
      return {
        ...state,
        profile: {
          ...state.profile,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // USER ADDRESS
    case userActionsType.ADDRESS_ADD_REQUEST:
      return {
        ...state,
        addresses: {
          ...state.addresses,
          loading: true,
        },
      };
    case userActionsType.ADDRESS_ADD_SUCCESS:
      return {
        ...state,
        addresses: {
          ...state.addresses,
          ...loadingStatusIdle,
          addresses: [action.payload, ...state.addresses.addresses],
        },
      };
    case userActionsType.ADDRESS_ADD_FAILURE:
      return {
        ...state,
        addresses: {
          ...state.addresses,
          loading: false,
          error: action.payload,
        },
      };
    case userActionsType.ADDRESS_LIST_SUCCESS:
      return {
        ...state,
        addresses: {
          ...loadingStatusIdle,
          addresses: action.payload,
        },
      };
    case userActionsType.ADDRESS_LIST_FAILURE:
      return {
        ...state,
        addresses: {
          loading: false,
          error: action.payload,
          addresses: [],
        },
      };
    case userActionsType.ADDRESS_REMOVE_SUCCESS:
      return {
        ...state,
        addresses: {
          ...loadingStatusIdle,
          addresses: state.addresses.addresses.filter(
            (address) => address.uuid !== action.payload,
          ),
        },
      };
    case userActionsType.ADDRESS_REMOVE_FAILURE:
      return {
        ...state,
        addresses: {
          ...state.addresses,
          loading: false,
          error: action.payload,
        },
      };
    case userActionsType.ADDRESS_UPDATE_SUCCESS:
      return {
        ...state,
        addresses: {
          ...state.addresses,
          addresses: state.addresses.addresses.map((address) =>
            address.uuid === action.payload.uuid ? action.payload : address,
          ),
        },
      };

    // GET USER REVIEWS
    case userActionsType.GET_USER_REVIEWS_REQUEST:
      return {
        ...state,
        reviews: {
          ...state.reviews,
          ...loadingStatus.loading,
        },
      };
    case userActionsType.GET_USER_REVIEWS_SUCCESS:
      if (action.reviewType === 'pending') {
        let results = [...action.payload.results];
        //In case results is not empty
        if (state.reviews.pending.results !== []) {
          //Add more reviews (not repeated) to the current reviews array
          const newProductReviews = results.filter(
            (r) =>
              state.reviews.pending.results.findIndex(
                (pr) => pr.id === r.id,
              ) === -1,
          );
          results = [...state.reviews.pending.results, ...newProductReviews];
        }
        return {
          ...state,
          reviews: {
            pending: {
              ...loadingStatus.idle,
              ...action.payload,
              results: results.filter((r) => r.product),
            },
            completed: {
              ...state.reviews.completed,
            },
          },
        };
      }
      //In case of 'completed' reviews
      return {
        ...state,
        reviews: {
          ...loadingStatus.loading,
          pending: {
            ...state.reviews.pending,
          },
          completed: {
            ...loadingStatus.idle,
            ...action.payload,
            results: action.payload.results.filter((r) => r.product),
          },
        },
      };
    case userActionsType.GET_USER_REVIEWS_FAILURE:
      return {
        ...state,
        reviews: {
          ...state.reviews,
          ...loadingStatus.error,
          error: action.error,
        },
      };
    case userActionsType.REVIEWS_ADD_REQUEST:
      return {
        ...state,
        reviews: {
          ...state.reviews,
          ...loadingStatus.loading,
        },
      };
    case userActionsType.REVIEWS_ADD_SUCCESS:
      const foundProductReview = state.reviews.pending.results.findIndex(
        (pr) => pr.id === action.currentReview.id,
      );
      return {
        ...state,
        reviews: {
          ...state.reviews,
          ...loadingStatus.idle,
          pending: {
            ...state.reviews.pending,
            results: [
              ...state.reviews.pending.results.slice(0, foundProductReview),
              ...state.reviews.pending.results.slice(foundProductReview + 1),
            ],
          },
          completed: {
            ...state.reviews.completed,
          },
        },
      };
    case userActionsType.REVIEWS_ADD_FAILURE:
      return {
        ...state,
        reviews: {
          ...state.reviews,
          ...loadingStatus.error,
          error: action.error,
        },
      };
    case userActionsType.GET_USER_CARDS_REQUEST:
      return {
        ...state,
        isLogged: true,
        isGuest: false,
        cards: {
          ...state.cards,
          ...loadingStatus.loading,
          ...action.payload,
        },
      };
    case userActionsType.GET_USER_CARDS_SUCCESS:
      return {
        ...state,
        isLogged: true,
        isGuest: false,
        cards: {
          ...loadingStatus.idle,
          cards: action.payload,
        },
      };
    case userActionsType.GET_USER_CARDS_FAILURE:
      return {
        ...state.cards,
        isLogged: true,
        isGuest: false,
        cards: {
          ...loadingStatus.error,
          error: action.error,
        },
      };
    default:
      return state;
  }
}

//
export const getAddress = (address = {}) => {
  // Here we can make any validations,
  // data transformation, formatting, etc...
  return {
    ...address,
    //default_address: address.default_address || false,
    //pickup_address: address.pickup_address || false
  };
};
