import { orderActionTypes, userTypes } from '../constants';
//import { SHIPPING_METHODS } from '../Constants/config.constants';
//import { ORDER_STATUS } from '../Constants/orders.constants';

// Initial State
const loadingStatus = {
  idle: {
    error: false,
    loading: false,
  },
  loading: {
    error: false,
    loading: true,
  },

  error: {
    error: true,
    loading: false,
  },
};
const defautlState = {
  detailorder: null,
  customer: {
    ...loadingStatus.loading,
    count: '',
    npages: '',
    pages: [],
    results: [],
    stats: {
      ...loadingStatus.loading,
    },
  },
  vendor: {
    ...loadingStatus.loading,
    count: '',
    npages: '',
    pages: [],
    results: [],
    stats: {
      ...loadingStatus.loading,
    },
  },
};

// Export Reducers
export function orders(state = defautlState, action) {
  switch (action.type) {
    // GET ORDERS
    case orderActionTypes.SELECT_DETAIL_ORDER_REQUEST: {
      return {
        ...state,
        detailorder: action.setItem,
      };
    }

    // GET ORDERS
    case orderActionTypes.GET_ORDERS_REQUEST: {
      let userType = action.userType || userTypes.CUSTOMER;
      userType = userType.toLowerCase();
      return {
        ...state,
        [userType]: {
          ...loadingStatus.loading,
        },
      };
    }
    case orderActionTypes.GET_ORDERS_SUCCESS: {
      const userType = action.userType.toLowerCase();
      return {
        ...state,
        [userType]: {
          ...state[userType],
          ...action.payload,
          ...loadingStatus.idle,
        },
      };
    }
    case orderActionTypes.GET_ORDERS_FAILURE: {
      let userType = action.userType || userTypes.CUSTOMER;
      userType = userType.toLowerCase();
      return {
        ...state,
        [userType]: {
          ...loadingStatus.error,
        },
      };
    }

    // // UPDATE STATUS ORDER
    // case orderActionTypes.UPDATE_STATUS_ORDER_REQUEST:
    //     return {
    //         ...state
    //     };
    // case orderActionTypes.UPDATE_STATUS_ORDER_SUCCESS:
    //     return {
    //         ...state,
    //         vendor: {
    //             ...state.vendor,
    //             results: state.vendor.results.map(item => {
    //                 if (item.id === action.payload.id) {
    //                     return action.payload;
    //                 }
    //                 return item;
    //             })
    //         }
    //     };
    // case orderActionTypes.UPDATE_STATUS_ORDER_FAILURE:
    //     return {
    //         ...state
    //     };

    // // REQUEST DHL GUIDES
    // case orderActionTypes.MAKE_DHL_GUIDES_REQUEST:
    //     return {
    //         ...state
    //     };
    // case orderActionTypes.MAKE_DHL_GUIDES_SUCCESS:
    //     return {
    //         ...state,
    //         vendor: {
    //             ...state.vendor,
    //             results: state.vendor.results.map(item => {
    //                 if (item.uid === action.uid) {
    //                     return {
    //                         ...item,
    //                         physical_properties: {
    //                             ...item.physical_properties,
    //                             shipment_dhl: action.payload
    //                         }
    //                     };
    //                 }
    //                 return item;
    //             })
    //         }
    //     };
    // case orderActionTypes.MAKE_DHL_GUIDES_FAILURE:
    //     return {
    //         ...state
    //     };

    // // SCHEDULE DHL PICKUP
    // case orderActionTypes.SCHEDULE_DHL_PICKUP_REQUEST:
    //     return {
    //         ...state
    //     };
    // case orderActionTypes.SCHEDULE_DHL_PICKUP_SUCCESS:
    //     return {
    //         ...state,
    //         vendor: {
    //             ...state.vendor,
    //             results: state.vendor.results.map(item => {
    //                 if (item.uid === action.uid) {
    //                     return {
    //                         ...item,
    //                         physical_properties: {
    //                             ...item.physical_properties,
    //                             can_store_change_status_to: null,
    //                             status: {
    //                                 value: 'vendor_awaiting_shipment',
    //                                 display_name: 'Esperando a ser enviada'
    //                             },
    //                             pickup_dhl: action.payload
    //                         }
    //                     };
    //                 }
    //                 return item;
    //             })
    //         }
    //     };
    // case orderActionTypes.SCHEDULE_DHL_PICKUP_FAILURE:
    //     return {
    //         ...state
    //     };

    //     // GET ORDER STATISTICS
    // case orderActionTypes.GET_ORDER_STATS_REQUEST:
    //     return {
    //         ...state,
    //         vendor: {
    //             ...state.vendor,
    //             ...loadingStatus.loading,
    //         }
    //     };
    // case orderActionTypes.GET_ORDER_STATS_SUCCESS: {
    //     return {
    //         ...state,
    //         vendor: {
    //             ...state.vendor,
    //             stats: action.payload,
    //             ...loadingStatus.idle,
    //         }
    //     };
    // }
    // case orderActionTypes.GET_ORDER_STATS_FAILURE:
    //     return {
    //         ...state,
    //         vendor: {
    //             ...state.vendor,
    //             ...loadingStatus.error,
    //         }
    //     };

    // DEFAULT
    default:
      return state;
  }
}

// export const getVendorOrdersList = state => {
//     const { orders } = state;
//     let vendorOrders = orders[userTypes.VENDOR.toLowerCase()];
//     vendorOrders = vendorOrders.results ? vendorOrders.results : [];

//     return vendorOrders.map(order => {
//         let shippingDate = Moment(order.physical_properties.shipping_date);
//         if (order.physical_properties.shipping_method_slug
//             === SHIPPING_METHODS.STANDARD.slug
//         ) {
//             shippingDate = Moment(shippingDate).subtract(1, 'days');
//         }
//         return {
//             ...order,
//             physical_properties: {
//                 ...order.physical_properties,
//                 shipping_date: shippingDate
//             },
//             created: Moment(order.created)
//         };
//     });
// };

export const getCustomerOrdersList = (state) => {
  const { orders: orderGroups } = state;
  let customerOrders = orderGroups[userTypes.CUSTOMER.toLowerCase()];
  let orders = customerOrders.results ? customerOrders.results : [];

  return (customerOrders = {
    ...customerOrders,
    results: orders.reduce((acc, group) => {
      // Filter all orders without store information & or without valid products.
      // This affects just a few orders. Most of them are old orders
      // created by the CR staff for testing purposes.
      let validOrders = group.orders.filter((order) => order.store !== null);
      validOrders = validOrders.map((order) => ({
        ...order,
        products: order.products.filter((p) => p.product || false),
      }));
      validOrders = validOrders.filter((order) => order.products.length);

      // Check if this grouo has valid orders
      if (!validOrders.length) {
        return acc;
      }

      // Add valid orders to array
      return [
        ...acc,
        {
          ...group,
          orders: validOrders,
        },
      ];
    }, []),
  });
};

// Setup Order Groups list.
// return customerOrders.reduce((a, b) => {
//     let ordersArray = a;

//     // Filter all orders without store information.
//     // This affects just a few orders. Most of them are old orders
//     // created by the CR staff for testing purposes.
//     let validOrders = b.orders.filter(order => order.store !== null);
//     validOrders = validOrders.filter(order => order.products.length);
//     return validOrders;

//     validOrders.forEach(order => {
//         const uidIndex = ordersArray.findIndex(item => item.uid === b.uid);
//         const storeIndex = ordersArray.findIndex(item => (
//             order.store !== null ? item.store.slug === order.store.slug : false
//         ));

//         if ((uidIndex && storeIndex !== -1) && uidIndex === storeIndex) {
//             const newOrder = {
//                 ...ordersArray[uidIndex],
//                 products: [
//                     ...ordersArray[uidIndex].products.concat(order.products),
//                 ]
//             };
//             ordersArray = [
//               ...ordersArray.slice(0, uidIndex),
//               newOrder,
//               ...ordersArray.slice(uidIndex + 1)
//             ];
//         } else {
//             ordersArray = [...ordersArray, {
//                 ...order,
//                 uid: b.uid
//             }];
//         }
//     });

//     return ordersArray;
// }, []);
// };

// const getCategory = (status, paid) => {
//     if (status === ORDER_STATUS.NEW_ORDER) {
//         return 'new';
//     } else if (
//         status === ORDER_STATUS.PREPARING_ORDER ||
//         status === ORDER_STATUS.ORDER_READY ||
//         status === ORDER_STATUS.AWAITING_SHIPMENT ||
//         status === ORDER_STATUS.ORDER_IN_TRANSIT
//     ) {
//         return 'in-process';
//     } else if (status === ORDER_STATUS.DELIVERED && !paid) {
//         return 'delivered';
//     } else if (status === ORDER_STATUS.DELIVERED && paid) {
//         return 'paid';
//     } else if (
//         status === ORDER_STATUS.CANCELLED ||
//         status === ORDER_STATUS.OTHER ||
//         status === ORDER_STATUS.FRAUD
//     ) {
//         return 'cancelled';
//     } else {
//         return 'none';
//     }
// };

// export const categorizedOrdersList = (orders, userType) => {
//     const categorizedOrders = {
//         new: [],
//         'in-process': [],
//         delivered: [],
//         paid: [],
//         cancelled: [],
//         none: []
//     };

//     orders.forEach(order => {
//         const status = order.physical_properties.status.value;
//         const paid = order.payout_sent;
//         const category = getCategory(status, paid);
//         categorizedOrders[`${category}`] = [...categorizedOrders[`${category}`], order];
//     });
//     return categorizedOrders;
// };
