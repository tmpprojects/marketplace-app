import { shoppingCartActionTypes } from '../constants';

const loadingStatus = {
  idle: {
    error: false,
    loading: false,
  },
  loading: {
    error: false,
    loading: true,
  },
  error: {
    error: true,
    loading: false,
  },
};
const initialState = {
  products: [],
  order_confirmed: false,
  order_confirmation: {
    ...loadingStatus.loading,
  },
};

//VISA Promotion
let referenceProducts = [];
function getProductListWithDiscount(discount) {
  let productsList = referenceProducts;
  if (discount) {
    productsList = productsList.map((a) => ({
      ...a,
      unit_price: a.unit_price,
    }));
  } else {
    productsList = productsList.map((a) => ({
      ...a,
      unit_price: a.unit_price_without_discount,
    }));
  }

  return productsList;
}

export default function shoppingCart(state = initialState, action) {
  let indexToUpdate;

  // SHOPPING CART ACTIONS
  switch (action.type) {
    // UPDATE CART PRODUCT
    case shoppingCartActionTypes.UPDATE_CART_PRODUCT: {
      indexToUpdate = state.products.findIndex(
        (item) => item.product.slug === action.product.product.slug,
      );

      // If we´ve found the product on the cart, update existing item.
      if (indexToUpdate !== -1) {
        const productsUpdate = [
          ...state.products.slice(0, indexToUpdate),
          action.product,
          ...state.products.slice(indexToUpdate + 1),
        ];
        return {
          ...state,
          products: productsUpdate,
        };
      }
      // ...else, create a new entry on the list
      return {
        ...state,
        products: [action.product, ...state.products],
      };
    }

    // REMOVE PRODUCT FROM CART
    case shoppingCartActionTypes.REMOVE_FROM_CART_SUCCESS: {
      // Filter products
      const modifiedCartProducts = state.products.filter(
        (item) => item.product.slug !== action.product.slug,
      );

      return {
        ...state,
        products: modifiedCartProducts,
      };
    }

    // GET ALL PRODUCTS FROM CART
    case shoppingCartActionTypes.GET_CART_PRODUCTS:
      referenceProducts = action.products;
      let productsList = [];

      //VISA Promotion
      if (
        action?._store?.form?.shoppingOrder_form?.values?.paymentMethod
          ?.payment_method_id === 'Visa' ||
        action?._store?.form?.shoppingOrder_form?.values?.paymentMethod
          ?.payment_method_id === 'visa'
      ) {
        // console.log(
        //   '******************** PRIMER COMPROBACIÓN EN VISA.',
        //   referenceProducts,
        // );
        productsList = getProductListWithDiscount(true);
      } else {
        productsList = getProductListWithDiscount(false);
      }

      return {
        ...state,
        products: productsList,
        ...loadingStatus.idle,
        previouslyLoaded: true,
      };

    //VISA Promotion
    case '@@redux-form/INITIALIZE':
      if (
        action.payload?.paymentMethod?.payment_method_id === 'Visa' ||
        action.payload?.paymentMethod?.payment_method_id === 'visa'
      ) {
        // console.log(
        //   '******************** SEGUNDA COMPROBACIÓN EN VISA.',
        //   referenceProducts,
        // );
        return {
          ...state,
          products: getProductListWithDiscount(true),
        };
      }

    // UPDATE CART STATUS
    case shoppingCartActionTypes.SET_CART_UPDATE_STATUS_REQUEST:
      return {
        ...state,
        isShoppingFormUpdated: action.status,
      };

    // EMPTY SHOPPING CART
    case shoppingCartActionTypes.EMPTY_SHOPPING_CART:
      return {
        ...initialState,
      };

    // Default Case
    default:
      return {
        ...state,
      };
  }
}
