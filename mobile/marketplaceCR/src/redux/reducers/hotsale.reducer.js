import { hotsaleActionTypes } from '../constants';

const loadingStatus = {
  idle: {
    error: false,
    loading: false,
  },
  loading: {
    error: false,
    loading: true,
  },
  error: {
    error: true,
    loading: false,
  },
};
const initialState = {
  hotsale: [],
  ...loadingStatus.idle,
};

export function hotsale(state = initialState, action) {
  switch (action.type) {
    case hotsaleActionTypes.GET_HOTSALE_REQUEST: {
      return {
        ...state,
        ...loadingStatus.loading,
      };
    }

    case hotsaleActionTypes.GET_HOTSALE_SUCCESS: {
      return {
        ...state,
        hotsale: action.payload,
        ...loadingStatus.idle,
      };
    }

    // GET ALL PRODUCTS FROM CART
    case hotsaleActionTypes.GET_HOTSALE_FAILURE:
      return {
        ...state,
        ...loadingStatus.error,
      };
  }
}
