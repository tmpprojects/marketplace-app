import { appActionsType } from '../constants';
import { formatShippingSchedules } from '../selectors/store.selectors';
import { SHIPPING_METHODS } from '../constants/app.constants';
import * as Sentry from '@sentry/react-native';

// Default States.
const loadingStatus = {
  idle: {
    error: false,
    loading: false,
  },
  loading: {
    error: false,
    loading: true,
  },
  error: {
    error: true,
    loading: false,
  },
};
// Initial State
const initialState = {
  interests: [
    {
      id: '0',
      title: 'Celebraciones',
    },
    {
      id: '1',
      title: 'Regalos',
    },
    {
      id: '2',
      title: 'Comida',
    },
    {
      id: '3',
      title: 'Fiesta',
    },
    {
      id: '4',
      title: 'Diseño',
    },
    {
      id: '5',
      title: 'Flores',
    },
    {
      id: '6',
      title: 'Eco',
    },
    {
      id: '7',
      title: 'Infantil',
    },
    {
      id: '8',
      title: 'Belleza',
    },
    {
      id: '9',
      title: 'Mascotas',
    },
    {
      id: '10',
      title: 'Ropa y calzado',
    },
    {
      id: '11',
      title: 'Accesorios y joyería',
    },
  ],
  pickupSchedules: [],
  shippingMethods: [],
  paymentMethods: [],
  marketCategories: {
    categories: [],
    ...loadingStatus.loading,
  },
  categories: {
    categories: [],
    ...loadingStatus.loading,
  },
  storesList: {
    results: [],
    ...loadingStatus.loading,
  },
  featuredBanners: {
    banners: [],
    ...loadingStatus.loading,
  },
  featuredProducts: {
    products: [],
    ...loadingStatus.loading,
  },
  featuredStores: {
    stores: [],
    ...loadingStatus.loading,
  },
  searchResults: {
    stores: {},
    products: {},
    ...loadingStatus.loading,
  },
  categoryResults: {
    results: {},
    category: '',
    ...loadingStatus.loading,
  },
  openSearch: false,
  zipCode: '',
  prevZipCode: '',
};

export function app(state = initialState, action) {
  switch (action.type) {
    // FETCH INTERSTS
    case appActionsType.FETCH_INTERESTS_REQUEST:
      return {
        ...state,
      };
    case appActionsType.FETCH_INTERESTS_SUCCESS:
      return {
        ...state,
        interests: action.payload,
      };
    case appActionsType.FETCH_INTERESTS_FAILURE:
      return {
        ...state,
        error: action.payload,
      };

    //UPDATE ZIP CODE
    case appActionsType.UPDATE_ZIPCODE_SUCCESS:
      if (
        action.zipCode !== '' &&
        action.zipCode !== undefined &&
        action.zipCode !== null
      ) {
        return {
          ...state,
          prevZipCode: state.zipCode,
          zipCode: String(action.zipCode),
        };
      } else {
        return {
          ...state,
          zipCode: null,
        };
      }

    // CHANGE_SEARCH_
    case appActionsType.CHANGE_SEARCH_REQUEST:
      return {
        ...state,
      };
    case appActionsType.CHANGE_SEARCH_SUCCESS:
      return {
        ...state,
        openSearch: action.payload,
      };
    case appActionsType.CHANGE_SEARCH_FAILURE:
      return {
        ...state,
      };

    // MARKET CATEGORIES
    case appActionsType.GET_MARKET_CATEGORIES_REQUEST:
      return {
        ...state,
        marketCategories: {
          ...state.marketCategories,
          //...loadingStatus.loading,
        },
      };
    case appActionsType.GET_MARKET_CATEGORIES_SUCCESS:
      return {
        ...state,
        marketCategories: {
          ...state.marketCategories,
          categories: [...action.categories],
          //...loadingStatus.idle,
        },
      };
    case appActionsType.GET_MARKET_CATEGORIES_FAILURE:
      return {
        ...state,
        marketCategories: {
          ...state.marketCategories,
          //...loadingStatus.error,
          error: action.error,
        },
      };

    // CATEGORIES
    case appActionsType.GET_CATEGORIES_REQUEST:
      return {
        ...state,
        categories: {
          ...state.categories,
        },
      };
    case appActionsType.GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: {
          ...state.categories,
          categories: action.categories,
        },
      };
    case appActionsType.GET_CATEGORIES_FAILURE:
      return {
        ...state,
        categories: {
          ...state.categories,
          error: action.error,
        },
      };

    // STORES LIST
    case appActionsType.STORES_LIST_REQUEST:
      return {
        ...state,
        storesList: {
          ...state.storesList,
          ...loadingStatus.loading,
        },
      };
    case appActionsType.STORES_LIST_SUCCESS:
      return {
        ...state,
        storesList: {
          ...state.storesList,
          ...action.stores,
          ...loadingStatus.idle,
        },
      };
    case appActionsType.STORES_LIST_FAILURE:
      return {
        ...state,
        storesList: {
          ...state.storesList,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // PICKUP SCHEDULES
    case appActionsType.GET_PICKUP_SCHEDULES_REQUEST:
      return {
        ...state,
        pickupSchedules: state.pickupSchedules,
      };
    case appActionsType.GET_PICKUP_SCHEDULES_SUCCESS:
      return {
        ...state,
        pickupSchedules: formatShippingSchedules(action.payload),
      };
    case appActionsType.GET_PICKUP_SCHEDULES_FAILURE:
      return {
        ...state,
        pickupSchedules: state.pickupSchedules,
      };

    // SHIPPING METHODS
    case appActionsType.GET_SHIPPING_METHODS_REQUEST:
      return {
        ...state,
      };
    case appActionsType.GET_SHIPPING_METHODS_SUCCESS:
      return {
        ...state,
        shippingMethods: action.shippingMethods.sort((a, b) => {
          // Map results against config settings
          const methods = [
            Object.keys(SHIPPING_METHODS).filter(
              (method) => SHIPPING_METHODS[method].slug === a.slug,
            ),
            Object.keys(SHIPPING_METHODS).filter(
              (method) => SHIPPING_METHODS[method].slug === b.slug,
            ),
          ];
          // Sort items
          try {
            return SHIPPING_METHODS[methods[0]].order >
              SHIPPING_METHODS[methods[1]].order
              ? 1
              : SHIPPING_METHODS[methods[0]].order <
                SHIPPING_METHODS[methods[1]].order
              ? -1
              : 0;
          } catch (e) {
            Sentry.captureException(e);
          }
        }),
      };
    case appActionsType.GET_SHIPPING_METHODS_FAILURE:
      return {
        ...state,
        error: action.error,
      };

    // PAYMENT METHODS
    case appActionsType.GET_PAYMENT_METHODS_REQUEST:
      return {
        ...state,
      };
    case appActionsType.GET_PAYMENT_METHODS_SUCCESS:
      return {
        ...state,
        paymentMethods: action.paymentMethods,
      };
    case appActionsType.GET_PAYMENT_METHODS_FAILURE:
      return {
        ...state,
        error: action.error,
      };

    // HOMEPAGE
    case appActionsType.FEATURED_BANNERS_REQUEST:
      return {
        ...state,
        featuredBanners: {
          ...state.featuredBanners,
        },
      };
    case appActionsType.FEATURED_BANNERS_FAILURE:
      return {
        ...state,
        featuredBanners: {
          ...state.featuredBanners,
          error: action.error,
        },
      };
    case appActionsType.FEATURED_BANNERS_SUCCESS:
      return {
        ...state,
        featuredBanners: {
          ...state.featuredBanners,
          banners: action.banners,
        },
      };

    // FEATURED PROUCTS
    case appActionsType.FEATURED_PRODUCTS_REQUEST:
      return {
        ...state,
        featuredProducts: {
          ...state.featuredProducts,
        },
      };
    case appActionsType.FEATURED_PRODUCTS_SUCCESS:
      return {
        ...state,
        featuredProducts: {
          ...state.featuredProducts,
          products: action.featuredProducts,
        },
      };
    case appActionsType.FEATURED_PRODUCTS_FAILURE:
      return {
        ...state,
        featuredProducts: {
          ...state.featuredProducts,
          error: action.error,
        },
      };

    // FEATURED STORES
    case appActionsType.FEATURED_STORES_REQUEST:
      return {
        ...state,
        featuredStores: {
          ...state.featuredStores,
        },
      };
    case appActionsType.FEATURED_STORES_SUCCESS:
      return {
        ...state,
        featuredStores: {
          ...state.featuredStores,
          stores: action.featuredStores,
        },
      };
    case appActionsType.FEATURED_STORES_FAILURE:
      return {
        ...state,
        featuredStores: {
          ...state.featuredStores,
          error: action.error,
        },
      };

    // SEARCH STORES
    case appActionsType.GET_SEARCH_STORES_REQUEST:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          ...loadingStatus.loading,
        },
      };
    case appActionsType.GET_SEARCH_STORES_SUCCESS:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          stores: action.results,
        },
      };
    case appActionsType.GET_SEARCH_STORES_FAILURE:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    case appActionsType.GET_SEARCH_PRODUCTS_REQUEST:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          ...loadingStatus.loading,
        },
      };
    case appActionsType.GET_SEARCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          products: action.results,
        },
      };
    case appActionsType.GET_SEARCH_PRODUCTS_FAILURE:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // CATEGORY RESULTS
    case appActionsType.GET_CATEGORY_RESULTS_REQUEST:
      return {
        ...state,
        categoryResults: {
          ...state.categoryResults,
          ...loadingStatus.loading,
        },
      };
    case appActionsType.GET_CATEGORY_RESULTS_SUCCESS:
      return {
        ...state,
        categoryResults: {
          ...state.categoryResults,
          results: action.payload,
          ...loadingStatus.idle,
        },
      };
    case appActionsType.GET_CATEGORY_RESULTS_FAILURE:
      return {
        ...state,
        categoryResults: {
          ...state.categoryResults,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // DEFAULT
    default:
      return state;
  }
}
