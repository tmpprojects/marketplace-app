import { storeActionsType } from '../constants';
import { getProductDetail, getStoreDetail } from '../selectors/store.selectors';

// Initial State
const loadingStatus = {
  idle: {
    error: false,
    loading: false,
    loaded: false,
  },
  loading: {
    error: false,
    loading: true,
    loaded: false,
  },
  loaded: {
    error: false,
    loading: false,
    loaded: true,
  },
  error: {
    error: true,
    loading: false,
    loaded: false,
  },
};
const defautlState = {
  data: {},
  productsList: [],
  productDetail: {
    ...loadingStatus.loading,
  },
  sections: {
    ...loadingStatus.idle,
    sections: [],
  },
  reviews: {
    store: {
      ...loadingStatus.idle,
      current_store: '',
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
    product: {
      ...loadingStatus.idle,
      current_product: '',
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
  },
  pending_reviews: {
    store: {
      ...loadingStatus.idle,
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
    product: {
      ...loadingStatus.idle,
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
  },
  new_stores: {
    stores: {
      ...loadingStatus.idle,
      results: [],
    },
  },
};

// Export Reducers
export function store(state = defautlState, action) {
  switch (action.type) {
    // GET STORE
    case storeActionsType.GET_STORE_REQUEST: {
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.loading,
        },
      };
    }
    case storeActionsType.GET_STORE_SUCCESS: {
      const raw = action.payload;
      const socialLinks = [];
      const storeData = Object.keys(raw)
        .filter((key) => {
          // Exclude object members with keys begining with 'link_'.
          // (Social Networks Links)
          if (/^link_\w+$/i.test(key)) {
            // Fill Array with valid social links
            if (raw[key] !== '') {
              socialLinks.push({
                name: key.split(/link_/)[1],
                link: raw[key],
              });
            }
            return false;
          }
          return true;

          // Create new Object from filtered array
        })
        .reduce((obj, key) => {
          obj[key] = raw[key];
          return obj;
        }, {});

      // // Add social links array to store data object
      storeData.social_links = socialLinks;
      return {
        ...state,
        data: {
          ...getStoreDetail(storeData),
          ...loadingStatus.loaded,
        },
      };
    }
    case storeActionsType.GET_STORE_FAILURE: {
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.error,
        },
      };
    }

    // FETCH PRODUCTS
    case storeActionsType.FETCH_PRODUCTS_REQUEST:
      return {
        ...state,
      };
    case storeActionsType.FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        productsList: action.payload,
      };
    case storeActionsType.FETCH_PRODUCTS_FAILURE:
      return {
        ...state,
      };

    // FETCH STORE SECTIONS
    case storeActionsType.FETCH_STORE_SECTIONS_REQUEST:
      return {
        ...state,
        sections: {
          sections: [],
          ...loadingStatus.loading,
        },
      };
    case storeActionsType.FETCH_STORE_SECTIONS_SUCCESS:
      return {
        ...state,
        sections: {
          sections: action.payload,
          ...loadingStatus.idle,
        },
      };
    case storeActionsType.FETCH_STORE_SECTIONS_FAILURE:
      return {
        ...state,
        sections: {
          ...loadingStatus.error,
        },
      };

    // FETCH PRODUCTS DETAIL
    case storeActionsType.FETCH_PRODUCT_DETAIL_REQUEST:
      return {
        ...state,
        productDetail: {
          ...loadingStatus.loading,
        },
      };
    case storeActionsType.FETCH_PRODUCT_DETAIL_SUCCESS:
      return {
        ...state,
        productDetail: {
          ...getProductDetail(action.payload.data),
          ...loadingStatus.loaded,
        },
      };
    case storeActionsType.FETCH_PRODUCT_DETAIL_FAILURE:
      return {
        ...state,
        productDetail: {
          ...loadingStatus.error,
          error: action.payload,
        },
      };

    // GET ALL REVIEWS (APPROVED) FROM STORE
    case storeActionsType.GET_STORE_REVIEWS_REQUEST:
      return {
        ...state,
      };
    case storeActionsType.GET_STORE_REVIEWS_SUCCESS:
      let storeReviews = [...action.payload.results];
      //In case are more reviews from the same store
      //Add more reviews (not repeated) to the current reviews array
      if (state.reviews.store.current_store === action.store) {
        const newStoreReviews = storeReviews.filter(
          (r) =>
            state.reviews.store.results.findIndex((sr) => sr.id === r.id) ===
            -1,
        );
        storeReviews = [...state.reviews.store.results, ...newStoreReviews];
      }
      return {
        ...state,
        reviews: {
          store: {
            ...action.payload,
            current_store: action.store,
            results: storeReviews,
          },
          product: {
            ...state.reviews.product,
          },
        },
      };
    case storeActionsType.GET_STORE_REVIEWS_FAILURE:
      return {
        ...state,
      };
    //GET PENDING REVIEWS OF USER FROM STORE
    case storeActionsType.GET_PENDING_STORE_REVIEWS_REQUEST:
      return {
        ...state,
      };
    case storeActionsType.GET_PENDING_STORE_REVIEWS_SUCCESS:
      return {
        ...state,
        pending_reviews: {
          ...state.pending_reviews,
          store: {
            ...state.pending_reviews.store,
            ...action.payload,
          },
        },
      };
    case storeActionsType.GET_PENDING_STORE_REVIEWS_FAILURE:
      return {
        ...state,
      };
    // GET ALL REVIEWS (APPROVED) FROM PRODUCT
    case storeActionsType.GET_PRODUCT_REVIEWS_REQUEST:
      return {
        ...state,
      };

    case storeActionsType.GET_PRODUCT_REVIEWS_SUCCESS:
      let productReviews = [...action.payload.results];

      //In case are more reviews from the same product
      if (state.reviews.product.current_product === action.product) {
        //Add more reviews (not repeated) to the current reviews array
        const newProductReviews = productReviews.filter(
          (r) =>
            state.reviews.product.results.findIndex((pr) => pr.id === r.id) ===
            -1,
        );
        productReviews = [
          ...state.reviews.product.results,
          ...newProductReviews,
        ];
      }

      return {
        ...state,
        reviews: {
          product: {
            ...action.payload,
            current_product: action.product,
            results: productReviews,
          },
          store: {
            ...state.reviews.store,
          },
        },
      };

    case storeActionsType.GET_PRODUCT_REVIEWS_FAILURE:
      return {
        ...state,
      };

    //GET PENDING REVIEWS OF USER FROM STORE
    case storeActionsType.GET_PENDING_PRODUCT_REVIEWS_REQUEST:
      return {
        ...state,
      };
    case storeActionsType.GET_PENDING_PRODUCT_REVIEWS_SUCCESS:
      return {
        ...state,
        pending_reviews: {
          ...state.pending_reviews,
          product: {
            ...state.pending_reviews.product,
            ...action.payload,
          },
        },
      };
    case storeActionsType.GET_PENDING_PRODUCT_REVIEWS_FAILURE:
      return {
        ...state,
      };

    //GET NEW STORES
    case storeActionsType.GET_NEW_STORES_REQUEST:
      return {
        ...state,
      };
    case storeActionsType.GET_NEW_STORES_SUCCESS:
      return {
        ...state,
        new_stores: {
          ...state.new_stores,
          stores: {
            ...state.new_stores.stores,
            ...action.payload,
            ...loadingStatus.loaded,
          },
        },
      };
    case storeActionsType.GET_NEW_STORES_FAILURE:
      return {
        ...state,
      };

    // DEFAULT
    default:
      return state;
  }
}
