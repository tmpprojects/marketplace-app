import { collectionActionTypes } from '../constants';

const loadingStatus = {
  idle: {
    error: false,
    loading: false,
  },
  loading: {
    error: false,
    loading: true,
  },
  error: {
    error: true,
    loading: false,
  },
};
const initialState = {
  content: [],
  ...loadingStatus.idle,
};

export function collection(state = initialState, action) {
  switch (action.type) {
    case collectionActionTypes.GET_COLLECTION_RESULTS_REQUEST: {
      return {
        ...state,
        ...loadingStatus.loading,
      };
    }

    case collectionActionTypes.GET_COLLECTION_RESULTS_SUCCESS: {
      return {
        ...state,
        content: action.payload,
        ...loadingStatus.idle,
      };
    }

    // GET ALL PRODUCTS FROM CART
    case collectionActionTypes.GET_COLLECTION_RESULTS_FAILURE:
      return {
        ...state,
        ...loadingStatus.error,
      };
    //////////////////GET ALL COLLECTIONS
    case collectionActionTypes.GET_ALL_COLLECTIONS_RESULTS_REQUEST: {
      return {
        ...state,
        ...loadingStatus.loading,
      };
    }

    case collectionActionTypes.GET_ALL_COLLECTIONS_RESULTS_SUCCESS: {
      const setCollections = action.payload;
      setCollections.length = 3;
      return {
        ...state,
        allcollections: setCollections,
        ...loadingStatus.idle,
      };
    }

    // GET ALL PRODUCTS FROM CART
    case collectionActionTypes.GET_ALL_COLLECTIONS_RESULTS_FAILURE:
      return {
        ...state,
        ...loadingStatus.error,
      };
    // Default Case
    default:
      return {
        ...state,
      };
  }
}
