import { PRODUCT_TYPES } from '../constants';
import Moment from 'moment';

import {
  formatDateToUTC,
  WEEK_OBJECT,
  formatHour24To12,
  addHourPeriodSuffix,
} from '../../utils/date';

// INFORMATION FORMAT AND TRSNFORMATION UTILITIES
export const formatWorkSchedules = (schedules) => {
  const orderedSchedules = schedules.map((schedule) => ({
    value: schedule.week_day.value,
    name: WEEK_OBJECT.find((a) => a.value === schedule.week_day.value).name,
    open: {
      name: formatHour24To12(schedule.start.split(':').slice(0, -1).join(':')),
      value: schedule.start.split(':').slice(0, -1).join(':'),
    },
    close: {
      name: formatHour24To12(schedule.end.split(':').slice(0, -1).join(':')),
      value: schedule.end.split(':').slice(0, -1).join(':'),
    },
  }));

  return orderedSchedules.sort((a, b) => {
    if (a.value > b.value) {
      return 1;
    } else if (a.value < b.value) {
      return -1;
    }
    return 0;
  });
};

export const formatShippingSchedules = (schedules) => {
  const classNames = ['morning', 'evening', 'afternoon'];

  // Order schedules by hour
  const orderedSchedules = schedules.sort((a, b) => {
    if (a > b) {
      return 1;
    } else if (a < b) {
      return -1;
    }
    return 0;
  });

  // return formatted schedules
  return orderedSchedules.map((schedule, index) => {
    // Format time display.
    const pickupStart = addHourPeriodSuffix(
      schedule.collection_start.split(':').slice(0, -1).join(':'),
    );
    const pickupEnd = addHourPeriodSuffix(
      schedule.collection_end.split(':').slice(0, -1).join(':'),
    );
    const deliveryStart = addHourPeriodSuffix(
      schedule.delivery_start.split(':').slice(0, -1).join(':'),
    );
    const deliveryEnd = addHourPeriodSuffix(
      schedule.delivery_end.split(':').slice(0, -1).join(':'),
    );
    // Return schedules list.
    return {
      id: schedule.id,
      value: schedule.id,
      className: classNames[index],
      limit_to_order: schedule.limit_to_order,
      schedules: {
        pickupStartTime: schedule.collection_start,
        pickupEndTime: schedule.collection_end,
        pickupStart,
        pickupEnd,
        pickup: `${schedule.name} ${pickupStart} - ${pickupEnd}`,
        deliveryStartTime: schedule.delivery_start,
        deliveryEndTime: schedule.delivery_end,
        deliveryStart,
        deliveryEnd,
        delivery: `${schedule.name} ${deliveryStart} - ${deliveryEnd}`,
      },
    };
  });
};

export const getProductDetail = (productDetail) => {
  let customProps = {};
  if (productDetail.product_type.value === PRODUCT_TYPES.PHYSICAL) {
    const minimumShippingDate = Moment(
      productDetail.physical_properties.minimum_shipping_date,
    ).format('YYYY-MM-DD');
    const maximumShippingDate = Moment(
      productDetail.physical_properties.maximum_shipping_date,
    ).format('YYYY-MM-DD');

    customProps = {
      physical_properties: {
        ...productDetail.physical_properties,
        maximum_shipping_date: maximumShippingDate,
        minimum_shipping_date: minimumShippingDate,
        shipping_schedules: formatShippingSchedules(
          productDetail.physical_properties.shipping_schedules,
        ),
      },
      productMadeOnDemand:
        productDetail.physical_properties.is_available.value === 2,
    };
  }

  return {
    ...productDetail,
    price: parseFloat(productDetail.price),
    work_schedules: formatWorkSchedules(productDetail.work_schedules),
    ...customProps,
  };
};
export const getStoreDetail = (storeDetail) => {
  //const {data: storeDetail} = store;

  return {
    loading: storeDetail.loaded || true,
    ...storeDetail,
    work_schedules: !storeDetail.loaded
      ? []
      : formatWorkSchedules(storeDetail.work_schedules),
  };
};
