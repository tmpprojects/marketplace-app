import { formValueSelector } from 'redux-form';
import { SHIPPING_METHODS, COUPON_TYPES } from '../constants/app.constants';
import { formatWorkSchedules } from '../../utils/schedules';
import { SHOPPING_CART_FORM_CONFIG } from '../../utils/shoppingCartUtils/shoppingCartFormConfig';
import Moment from 'moment';

export const getCartProductsTotal = (state) => {
  //const { products } = state.cart;
  const orders = getAvailableProducts(state); //formValueSelector(SHOPPING_CART_FORM_CONFIG.formName)(state, 'orders');
  const products = orders
    ? orders.reduce((p, o) => [...p, ...o.products], [])
    : [];
  const productsTotal = products
    .map(
      (item) => parseFloat(item.unit_price, 10) * parseFloat(item.quantity, 10),
    )
    .reduce((a, b) => a + b, 0);
  return productsTotal ? parseFloat(productsTotal.toFixed(2), 10) : 0;
};

export const getCartProductsCount = (state) =>
  state.cart.products.reduce((a, item) => a + item.quantity, 0);

export const getCartGrandTotal = (state) => {
  const { couponDetails } = state.cart;
  let grandTotal =
    parseFloat(getCartProductsTotal(state), 10) +
    parseFloat(getCartShippingPrice(state), 10);

  if (state.cart.couponDetails.valid) {
    const discountAmount = parseFloat(couponDetails.discount, 10);
    switch (state.cart.couponDetails.type) {
      case COUPON_TYPES.PERCENTUAL:
      case COUPON_TYPES.FREE_SHIPPING_AND_PERCENTUAL:
        grandTotal -= grandTotal * discountAmount;
        break;
      case COUPON_TYPES.QUANTITY:
        grandTotal -= discountAmount;
        if (grandTotal < 0) {
          return 0;
        }
        break;
      default:
    }
  }

  return parseFloat(grandTotal.toFixed(2));
};

export const getCartNumberOfShippings = (state) =>
  getCartProductsByStoreAndShipping(state).length;

export const getCartShippingPrice = (state) => {
  // getCartProductsByStoreAndShipping(state).reduce((a, item) => (
  const orders = getAvailableProducts(state); //formValueSelector(SHOPPING_CART_FORM_CONFIG.formName)(state, 'orders') || [];
  const ordersShippingPrice = orders.reduce(
    (a, item) => a + parseFloat(item.physical_properties.shipping_price, 10),
    0,
  );
  let freeShippingCoupon = false;
  if (
    state.cart.couponDetails.valid &&
    (state.cart.couponDetails.type === COUPON_TYPES.FREE_SHIPPING ||
      state.cart.couponDetails.type === COUPON_TYPES.FREE_SHIPPING_ALL ||
      state.cart.couponDetails.type === COUPON_TYPES.FREE_SHIPPING_UNIQUE ||
      state.cart.couponDetails.type === COUPON_TYPES.FREE_SHIPPING_STORE ||
      state.cart.couponDetails.type ===
        COUPON_TYPES.FREE_SHIPPING_GENERAL_STORE ||
      state.cart.couponDetails.type ===
        COUPON_TYPES.FREE_SHIPPING_AND_PERCENTUAL)
  ) {
    freeShippingCoupon = true;
  }

  return (
    parseFloat(ordersShippingPrice.toFixed(2), 10) *
    (freeShippingCoupon ? 0 : 1)
  );
};

export const getCartProducts = (state) =>
  //formValueSelector(SHOPPING_CART_FORM_CONFIG.formName)(state, 'orders'
  state.cart.products.map((product) => {
    const { product: productDetails, ...productGeneral } = product;

    const productMadeOnDemand =
      productDetails.physical_properties.is_available.value === 2;
    const remainingStock =
      productGeneral.attribute && !productMadeOnDemand
        ? productGeneral.attribute.stock
          ? productGeneral.attribute.stock
          : productDetails.quantity
        : productDetails.quantity;
    const minimumShippingDate = Moment(
      productDetails.physical_properties.minimum_shipping_date,
      'YYYY-MM-DD',
      true,
    ).toDate();
    const maximumShippingDate = Moment(minimumShippingDate)
      .add(2, 'M')
      .toDate();

    // Return product details
    return {
      product: productDetails,
      ...productGeneral,
      remainingStock,
      productMadeOnDemand,
      slug: productDetails.slug,
      attribute: productGeneral.attribute,
      quantity: productGeneral.quantity || 1,
      note: productGeneral.note || '',
      price: productGeneral.unit_price,
      maximum_shipping_date: maximumShippingDate,
      minimum_shipping_date: minimumShippingDate,
      physical_properties: {
        ...productGeneral.physical_properties,
        shipping_date: Moment(
          productGeneral.physical_properties.shipping_date,
        ).toDate(),
      },
      work_schedules: formatWorkSchedules(productDetails.store.work_schedules),
    };
  });

export const getCartProductsByStore = (state) => {
  const products = getCartProducts(state);
  return products.reduce((a, b) => {
    let productArray = a;
    // Create a new empty cart product model
    const newProduct = {
      ...b,
      note: b.note,
      quantity: b.quantity,
    };

    // Try to find current product´s store (newProduct) on stores list (a)
    const storeIndex = productArray.findIndex(
      (item) => item.store.slug === b.product.store.slug,
    );
    // If store is not found, add it to the list
    if (storeIndex === -1) {
      productArray = [
        ...productArray,
        {
          //shipping_method: shippingMethod,
          store: b.product.store,
          products: [newProduct],
        },
      ];

      // if store is found, add current product (newProduct) to product list
    } else {
      const product = {
        ...productArray[storeIndex],
        products: [...productArray[storeIndex].products, newProduct],
      };
      productArray = [
        ...productArray.slice(0, storeIndex),
        product,
        ...productArray.slice(storeIndex + 1),
      ];
    }

    return productArray;
  }, []);
};

export const getFormattedOrders = (state) => {
  const cartStores = getCartProductsByStore(state);
  const platformPickupSchedules = state.app.pickupSchedules;
  const platformShippingMethods = state.app.shippingMethods;

  return cartStores.reduce((orders, store, storeIndex) => {
    // Loop through each store products.
    // This will return an array of stores (with products)
    // based on distinct shipping dates/schedules.
    let ordersByShipping = store.products.reduce(
      (
        a,
        product = {
          product: {
            physical_properties: {
              shipping: {},
              shipping_date: {},
              shipping_schedule: {
                id: {},
              },
            },
          },
        },
      ) => {
        let returnArray = a;

        // Store/Order Model.
        const basicStoreModel = {
          ...store,
          physical_properties: {
            shipping_methods: product.product.physical_properties.shipping.map(
              (s) => ({
                ...s,
                price: parseInt(s.price, 10),
              }),
            ),
            //shipping_date:
            shipping_date: Moment(
              product.physical_properties.shipping_date,
              'YYYY-MM-DD',
              true,
            ).format('YYYY-MM-DD'),
            shipping_schedule: platformPickupSchedules.find(
              (schedule) =>
                schedule.id ===
                product.physical_properties.shipping_schedule.id,
            ),
          },
        };

        // Try to find distinct shipping dates
        const foundDateIndex = returnArray.findIndex(
          (p) =>
            p.physical_properties.shipping_date ===
            Moment(product.physical_properties.shipping_date).format(
              'YYYY-MM-DD',
            ),
        );

        // If we found a distinct date, add product to list
        if (foundDateIndex === -1) {
          returnArray = [
            ...returnArray,
            {
              ...basicStoreModel,
              products: [product],
            },
          ];

          // if shipping date already exists...
        } else {
          // Try to find distinct schedules
          const foundScheduleIndex = returnArray.findIndex(
            (d) =>
              d.physical_properties.shipping_schedule.id ===
              product.physical_properties.shipping_schedule.id,
          );

          // If the schedule is not found, add item to list
          if (foundScheduleIndex === -1) {
            returnArray = [
              ...returnArray,
              {
                ...basicStoreModel,
                products: [product],
              },
            ];

            // If this products doesn´t have a distinct shipping schedule,
            // only add product to list
          } else {
            returnArray = [
              ...returnArray.slice(0, foundDateIndex),
              {
                ...returnArray[foundDateIndex],
                products: [...returnArray[foundDateIndex].products, product],
              },
              ...returnArray.slice(foundDateIndex + 1),
            ];
          }
        }

        // Return list of distinct shippings
        return returnArray;
      },
      [],
    );

    // Make some adjustments to the order
    let ordersFromForm = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName)(
      state,
      'orders',
    );
    ordersFromForm =
      ordersFromForm && ordersFromForm.length ? ordersFromForm : undefined;

    ordersByShipping = ordersByShipping.map((o, index) => {
      // Slice products list based on shipping availability (per product).
      let availableProducts = o.products.filter(
        (p) =>
          p.product.physical_properties.shipping.filter((s) => s.is_available)
            .length,
      );
      let unavailableProducts = o.products.filter(
        (p) => availableProducts.findIndex((a) => a.slug === p.slug) === -1,
      );

      // Get unique shipping methods for the whole order
      let availableShippingMethods = availableProducts.reduce((a, p) => {
        const shippingMethod = p.product.physical_properties.shipping.filter(
          (s) => !a.find((x) => s.slug === x.slug && s.is_available),
        );
        return [...a, ...shippingMethod];
      }, []);

      // Filter Shipping Methods.
      // If the order has at least one product that needs to be sent
      // by car (and car shipping is available), the whole order should be sent by car.
      const hasCarShipping = availableShippingMethods.find(
        (s) => s.slug === SHIPPING_METHODS.EXPRESS_CAR.slug && s.is_available,
      );

      // If NOT all products in this order have 'bike shipping',
      // disable this shipping method
      if (hasCarShipping && availableProducts.length > 1) {
        const allProductsHaveBikeShipping = availableProducts.reduce((a, p) => {
          return !a
            ? false
            : p.product.physical_properties.shipping.find(
                (sm) => sm.slug === SHIPPING_METHODS.EXPRESS_BIKE.slug,
              );
        }, true);

        if (!allProductsHaveBikeShipping) {
          availableShippingMethods = availableShippingMethods.map((a) =>
            a.slug === SHIPPING_METHODS.EXPRESS_BIKE.slug
              ? {
                  ...a,
                  is_available: false,
                }
              : a,
          );
        }
      }

      /*------------------------------------------------------------------------*/
      // PATCH!!! Mercado Rosa pickup point
      // TODO: Improve this functionality. This must be managed on the database,
      /*------------------------------------------------------------------------*/
      const tempStores = []; // <-- Insert here store slugs to enable 'pickup point'.

      /////////// Add tents of mercado rosa dynamically
      const availablePickupPoint = availableShippingMethods.map(
        (a) => a.slug === 'pickup-point',
      );
      if (availablePickupPoint) {
        if (!tempStores.includes(o.store.slug)) {
          tempStores.push(o.store.slug);
        }
      }
      ///////////

      const storeHasPickupPoint = tempStores.find((s) => s === o.store.slug);
      if (storeHasPickupPoint) {
        availableShippingMethods = availableShippingMethods.map((a) =>
          a.slug === SHIPPING_METHODS.PICKUP_POINT.slug
            ? {
                ...a,
                is_available: true,
                delivery_date: availableProducts[0]
                  ? Moment(
                      availableProducts[0].physical_properties.shipping_date,
                    ).toDate()
                  : Moment().toDate(),
              }
            : a,
        );
      } else {
        // Remove 'Pickup Point' from shipping methods list
        availableShippingMethods = availableShippingMethods.filter(
          (sm) => sm.slug !== SHIPPING_METHODS.PICKUP_POINT.slug,
        );
      }
      /*------------------------------------------------------------------------*/
      // END: PATCH!!!
      /*------------------------------------------------------------------------*/

      // Verify Product Availablility Again
      // Slice products list based on shipping availability (per product).
      availableProducts = availableProducts.filter((p) => {
        return p.product.physical_properties.shipping.filter(
          (s) =>
            availableShippingMethods.filter((sm) => sm.slug === s.slug).length,
        ).length;
      });
      unavailableProducts = o.products.filter(
        (p) => availableProducts.findIndex((a) => a.slug === p.slug) === -1,
      );

      // Get Selected shipping method.
      let selectedShippingMethod;
      let userSelectedShippingMethod;
      // (try to retrieve selected shipping methos from redux form)

      if (
        ordersFromForm &&
        ordersFromForm[storeIndex] &&
        ordersFromForm[storeIndex].physical_properties
          .selected_shipping_method &&
        availableShippingMethods.length
      ) {
        userSelectedShippingMethod =
          ordersFromForm[storeIndex].physical_properties
            .selected_shipping_method;
        selectedShippingMethod = availableShippingMethods.find((sm) =>
          ordersFromForm[storeIndex]
            ? sm.slug === userSelectedShippingMethod.slug &&
              userSelectedShippingMethod.is_available
            : false,
        );
      }
      selectedShippingMethod =
        selectedShippingMethod ||
        availableShippingMethods.find((sm) => sm.is_available);
      /* if (!selectedShippingMethod || !selectedShippingMethod.is_available) {
          selectedShippingMethod = availableShippingMethods.find(sm => sm.is_available);
        }*/

      // Define default order shipping price
      // First get the highest shipping price as default.
      let shippingPrice = 0;
      //shippingPrice = platformShippingMethods.reduce((a, b) => Math.max(a, b.price), 0);

      // If the order has products, get the price of the first element
      // * All products on the same order have the same shipping price.
      if (selectedShippingMethod && availableProducts.length) {
        shippingPrice = availableProducts.reduce((sm, p) => {
          if (!sm) {
            return availableShippingMethods.find(
              (s) => s.slug === selectedShippingMethod.slug,
            );
          }
          return sm;
        }, null);
        shippingPrice = shippingPrice ? shippingPrice.price : 0;
      }

      // Get shipping date depending on the selected shipping method.
      let shippingDate = Moment().format('YYYY-MM-DD'); //selectedShippingMethod.delivery_date;
      if (selectedShippingMethod && availableProducts.length) {
        shippingDate = availableProducts.reduce((sd, p) => {
          if (!sd) {
            return availableShippingMethods.find(
              (s) => s.slug === selectedShippingMethod.slug,
            );
          }
          return sd;
        }, null).delivery_date;
        //shippingDate = shippingDate ? shippingDate.delivery_date : Moment().format('YYYY-MM-DD');
      }

      // let minimumShippingDate = availableProducts.length
      //   ? availableProducts.find(p =>
      //       p.product.physical_properties.shipping.find(sm =>
      //         sm.slug === selectedShippingMethod.slug
      //       ) !== -1
      //     ).minimum_shipping_date
      //   : shippingDate;
      const minimumShippingDate = availableProducts.length
        ? Moment(availableProducts[0].minimum_shipping_date).toDate()
        : Moment(shippingDate).add(1, 'days').toDate();
      const maximumShippingDate = Moment(minimumShippingDate)
        .add(2, 'M')
        .toDate();
      // availableProducts.length
      // ? availableProducts.find(p =>
      //   p.product.physical_properties.shipping.find(sm =>
      //     sm.slug === selectedShippingMethod.slug) !== -1
      // ).maximum_shipping_date : shippingDate;
      //
      // if (selectedShippingMethod
      //   && (selectedShippingMethod.slug === SHIPPING_METHODS.STANDARD.slug)
      // ) {
      //   availableProducts = availableProducts.map(p => {
      //     const sm = p.product.physical_properties.shipping.find(s =>
      //       s.slug === selectedShippingMethod.slug
      //     );
      //     return {
      //       ...p,
      //       minimum_shipping_date: Moment(
      //         sm.delivery_date,
      //         'YYYY-MM-DD',
      //         true
      //       ).toDate()
      //     };
      //   });
      //   minimumShippingDate = availableProducts.reduce((acc, p) => {
      //     if (!acc) {
      //       const sm = p.product.physical_properties.shipping.find(s =>
      //         s.slug === selectedShippingMethod.slug
      //       );
      //       return Moment(sm.delivery_date, 'YYYY-MM-DD', true).toDate();
      //     }
      //     return acc;
      //   }, null);
      // }

      // Enable/Disable shipping methods based on the availability for each product
      availableShippingMethods = platformShippingMethods.map((a) => {
        const sm = availableShippingMethods.find((s) => s.slug === a.slug);
        if (sm) {
          return sm;
        }
        return { ...a, is_available: false };
      });
      availableShippingMethods = availableShippingMethods.filter((sm) => {
        if (sm.slug === SHIPPING_METHODS.PICKUP_POINT.slug) {
          return sm.is_available;
        }
        return true;
      });

      // Sort remaining available shipping methods by priority.
      const shippingMethodsOrderSequence = [
        SHIPPING_METHODS.EXPRESS_CAR,
        SHIPPING_METHODS.EXPRESS_BIKE,
        SHIPPING_METHODS.STANDARD,
        SHIPPING_METHODS.PICKUP_POINT,
      ];
      availableShippingMethods = availableShippingMethods.sort((a, b) => {
        const aKey = shippingMethodsOrderSequence.findIndex(
          (sm) => sm.slug === a.slug,
        );
        const bKey = shippingMethodsOrderSequence.findIndex(
          (sm) => sm.slug === b.slug,
        );
        return aKey - bKey;
      });

      // Compose and return Order.
      return {
        ...o,
        products: availableProducts,
        unavailableProducts,
        physical_properties: {
          ...o.physical_properties,
          shipping_date: Moment(shippingDate).format('YYYY-MM-DD'),
          shipping_methods: availableShippingMethods,
          selected_shipping_method: selectedShippingMethod,
          shipping_price: shippingPrice,
          minimum_shipping_date: minimumShippingDate,
          maximum_shipping_date: maximumShippingDate,
        },
      };
    });

    // Return list of orders by store and shipping
    return [...orders, ...ordersByShipping];
  }, []);
};

export const getCartProductsByStoreAndShipping = (state) =>
  getAvailableProducts(state);

export const getAvailableProducts = (state) => {
  const ordersByShipping = getFormattedOrders(state);
  //ordersByShipping = ordersByShipping.filter(o => o.physical_properties.shipping_methods.length);
  //ordersByShipping = ordersByShipping.filter(o => o.products.length);

  return ordersByShipping;
};

export const getUnavailableProducts = (state) => {
  let ordersByShipping = getFormattedOrders(state);
  //ordersByShipping = ordersByShipping.filter(o => !o.physical_properties.shipping_methods.length);
  //ordersByShipping = ordersByShipping.filter(o => !o.products.length);

  ordersByShipping = ordersByShipping.reduce(
    (list, p) => [...list, ...p.unavailableProducts],
    [],
  );
  return ordersByShipping;
};
