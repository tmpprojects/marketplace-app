import { addHourPeriodSuffix } from '../../utils/date';

export const formatShippingSchedules = (schedules) => {
  const classNames = ['morning', 'evening', 'afternoon'];

  // Order schedules by hour
  const orderedSchedules = schedules.sort((a, b) => {
    if (a > b) {
      return 1;
    } else if (a < b) {
      return -1;
    }
    return 0;
  });

  // return formatted schedules
  return orderedSchedules.map((schedule, index) => {
    // Format time display.
    const pickupStart = addHourPeriodSuffix(
      schedule.collection_start.split(':').slice(0, -1).join(':'),
    );
    const pickupEnd = addHourPeriodSuffix(
      schedule.collection_end.split(':').slice(0, -1).join(':'),
    );
    const deliveryStart = addHourPeriodSuffix(
      schedule.delivery_start.split(':').slice(0, -1).join(':'),
    );
    const deliveryEnd = addHourPeriodSuffix(
      schedule.delivery_end.split(':').slice(0, -1).join(':'),
    );

    // Return schedules list.
    return {
      id: schedule.id,
      value: schedule.id,
      className: classNames[index],
      schedules: {
        limit_to_order: schedule.limit_to_order,
        pickupStart,
        pickupEnd,
        pickup: `${schedule.name} ${pickupStart} - ${pickupEnd}`,
        deliveryStart,
        deliveryEnd,
        delivery: `${schedule.name} ${deliveryStart} - ${deliveryEnd}`,
      },
    };
  });
};
