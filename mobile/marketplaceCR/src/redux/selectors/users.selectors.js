export const getAddress = (address = {}) => {
  // Here we can make any validations,
  // data transformation, formatting, etc...
  return {
    ...address,
    //default_address: address.default_address || false,
    //pickup_address: address.pickup_address || false
  };
};

export const getCards = (cards = {}) => {
  // Here we can make any validations,
  // data transformation, formatting, etc...
  return {
    ...cards,
    //default_address: address.default_address || false,
    //pickup_address: address.pickup_address || false
  };
};

export const getAddressesList = (state) =>
  state.user.addresses.addresses.map((a) => getAddress(a));

export const getCardsList = (state) =>
  state.user.cards.cards.map((a) => getCards(a));

export const getUserProfile = (state) => {
  const { profile } = state.user;

  // Format / Default Values
  let birthday;
  if (profile.birthday) {
    birthday = profile.birthday.split('-');
  } else {
    birthday = ['', '', ''];
  }

  return {
    loading: profile.loading,
    ...profile,
    bd_year: birthday[0],
    bd_month: birthday[1],
    bd_day: birthday[2],
  };
};
