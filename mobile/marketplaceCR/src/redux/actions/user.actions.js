//import config from 'react-native-config';
import { config } from '../../../env';
import { userActionsType } from '../constants';
import { shoppingCartActions } from './shoppingCart.actions';
import {
  middlewareSetCookie,
  middlewareClearCookie,
} from '../../utils/middlewareSetCookie';
import {
  _clearData,
  _customSetData,
  _customGetData,
} from '../../utils/asyncStorage';
import * as Sentry from '@sentry/react-native';
declare var globalThis: any;

// SET COOKIES USER
const setCookiesUser = (cookies) => async (dispatch, getState, api) => {
  if (cookies) {
    //Set cookies in the Redux
    dispatch({
      type: userActionsType.USER_SET_COOKIES,
      headers: cookies,
    });
  }
};

// GUEST USER USER
const loginAsGuest = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionsType.SET_USER_AS_GUEST_REQUEST,
  });

  // Dispatch Login Success
  dispatch({
    type: userActionsType.SET_USER_AS_GUEST_SUCCESS,
    payload: true,
  });
  return Promise.resolve();
};

// LOGIN USER
const loginUser = (email, password) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionsType.USERS_LOGIN_REQUEST,
  });

  // API call configuration
  let response;

  // Make/Handle API call
  try {
    response = await api.post(
      config.EXTERNAL_AUTH,
      JSON.stringify({ email: email, password: password }),
    );
  } catch (error) {
    Sentry.captureException(error);

    //_clearData();
    dispatch({
      type: userActionsType.USERS_LOGIN_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: userActionsType.USERS_LOGIN_SUCCESS,
    headers: response.data,
  });

  //
  middlewareSetCookie(api, getState);
  return response;
};

// LOGOUT USER
const autoLogin = () => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  await middlewareSetCookie(api, getState);
  dispatch({
    type: userActionsType.AUTOLOGIN_SUCCESS,
  });
};

// LOGOUT USER
const verifyAutoLogin = () => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // API call configuration
  let response;
  // Make/Handle API call
  try {
    response = await api.get('api/v1/auth/user-is-authenticated/');
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }

  dispatch({
    type: userActionsType.VERIFY_AUTOLOGIN_SUCCESS,
    verifyAutoLogin: response.data['is-authenticated'],
  });
};

// LOGOUT USER
const logout = () => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  dispatch({
    type: userActionsType.LOGOUT_REQUEST,
  });

  let response;
  // Make/Handle API call
  try {
    response = await api.get('/api/v1/auth/logout/');
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: userActionsType.LOGOUT_FAILURE,
      payload: error.response,
    });
    throw error;
  }
  middlewareClearCookie(api);

  globalThis.ShoppingCartManager.setAddresses([]);
  dispatch({
    type: userActionsType.LOGOUT_SUCCESS,
    payload: response,
  });
};

// REGISTER USER
const registerUser = (user) => async (dispatch, getState, api) => {
  middlewareSetCookie(api, getState);

  // Login Request
  dispatch({
    type: userActionsType.USERS_REGISTER_REQUEST,
  });

  // API call configuration
  let response;
  const data = {
    first_name: user.first_name,
    last_name: user.last_name,
    email: user.email,
    password: user.password,
    confirmPassword: user.confirmPassword,
  };

  // Make/Handle API call
  try {
    response = await api.post('/api/v1/user/', JSON.stringify(data));
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: userActionsType.USERS_REGISTER_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }
  // Dispatch Login Success
  dispatch({
    type: userActionsType.USERS_REGISTER_SUCCESS,
    payload: response.data,
  });
  return response;
};

// VERIFY EMAIL ACCOUNT
const verifyEmailAccount = (user) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionsType.VERIFY_EMAIL_ACCOUNT_REQUEST,
  });
  // API call configuration
  let response;
  const data = {
    email: user.email,
    password: user.password,
  };
  // Make/Handle API call
  try {
    response = await api.post(config.EXTERNAL_AUTH, JSON.stringify(data));
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: userActionsType.VERIFY_EMAIL_ACCOUNT_FAILURE,
      error: true,
      verify: false,
    });
    throw error;
  }
  // Dispatch Login Success
  dispatch({
    type: userActionsType.VERIFY_EMAIL_ACCOUNT_SUCCESS,
    headers: response.data,
    payload: response.data,
  });
  return response;
};

// VERIFY ACCOUNT
const verifyAccount = (queryString = '') => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionsType.VERIFY_ACCOUNT_REQUEST,
  });

  // API call configuration
  let query = '';
  if (queryString !== '') {
    query = queryString;
  }
  let response;
  // This functionality is not released ***********+
  response = await axios.get(`http://3.130.75.81:1337/${query}`).catch((e) => {
    Sentry.captureException(e);
    dispatch({
      type: userActionsType.VERIFY_ACCOUNT_FAILURE,
      e,
    });
  });

  // Dispatch Response
  dispatch({
    type: userActionsType.VERIFY_ACCOUNT_SUCCESS,
    payload: response.data,
  });
  return response;
};

// VERIFY ACCOUNT
const validateCode = (queryString = '') => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionsType.VALIDATE_CODE_REQUEST,
  });

  // API call configuration
  let query = '';
  if (queryString !== '') {
    query = queryString;
  }
  let response;
  // This functionality is not released ***********+
  response = await axios.get(`http://3.130.75.81:1337/${query}`).catch((e) => {
    Sentry.captureException(e);
    dispatch({
      type: userActionsType.VALIDATE_CODE_FAILURE,
      e,
    });
  });
  // Dispatch Response
  dispatch({
    type: userActionsType.VALIDATE_CODE_SUCCESS,
    payload: response.data,
  });
  return response;
};

// PASSWORD RECOVER
const passwordRecover = (userData) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionsType.PASSWORD_RECOVER_REQUEST,
  });

  // API call configuration
  let response;
  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/user/forgot-password/',
      JSON.stringify(userData),
    );
  } catch (error) {
    Sentry.captureException(error);

    dispatch({
      type: userActionsType.PASSWORD_RECOVER_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: userActionsType.PASSWORD_RECOVER_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET USER STATUS
const getCurrentUser = () => async (dispatch, getState, api) => {
  middlewareSetCookie(api, getState);

  // Login Request
  dispatch({
    type: userActionsType.GET_CURRENT_USER_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get('/api/v1/user/profile/');
  } catch (error) {
    Sentry.captureException(error);

    dispatch({
      type: userActionsType.GET_CURRENT_USER_FAILURE,
      error: error,
    });

    throw error;
  }

  // Update ShoppingCartManager
  try {
    globalThis.ShoppingCartManager.setClientData(response.data);
  } catch (error) {
    Sentry.captureException(error);
  }

  // Dispatch Login Success
  dispatch({
    type: userActionsType.GET_CURRENT_USER_SUCCESS,
    payload: response.data,
  });

  // dispatch(getUserCreditCards());

  return response;
};

// UPDATE USER PROFILE
const updateUserInfo = (formData) => async (dispatch, getState, api) => {
  // Dipatch Request
  dispatch({
    type: userActionsType.UPDATE_INFO_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.patch('/api/v1/user/profile/', formData);
  } catch (error) {
    Sentry.captureException(error);

    dispatch({
      type: userActionsType.UPDATE_INFO_FAILURE,
      error,
    });
    throw error;
  }
  // Dispatch Login Success
  dispatch({
    type: userActionsType.UPDATE_INFO_SUCCESS,
    payload: response.data,
  });
  return response;
};

// ADDRESS ACTIONS
const addAddress = (addressData) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionsType.ADDRESS_ADD_REQUEST,
  });

  // Get current user
  const currentUser = getState().user;
  //const currentAddresses = currentUser.addresses.addresses;
  if (currentUser.isLogged && !currentUser.isGuest) {
    // Make/Handle API call
    let response;
    try {
      response = await api.post(
        '/api/v1/user/address/',
        JSON.stringify(addressData),
      );
    } catch (error) {
      Sentry.captureException(error);

      dispatch({
        type: userActionsType.ADDRESS_ADD_FAILURE,
        payload: error,
      });
      throw error;
    }

    // Add user addresses to ShoppingCartManager.
    try {
      globalThis.ShoppingCartManager.setAddresses([
        addressData,
        ...globalThis.ShoppingCartManager.getAddresses(),
      ]);
    } catch (error) {
      Sentry.captureException(error);
    }

    // Dispatch Results
    dispatch({
      type: userActionsType.ADDRESS_ADD_SUCCESS,
      payload: addressData,
    });

    // Return Promise
    return response;
  } else {
    // Add user addresses to ShoppingCartManager.
    try {
      globalThis.ShoppingCartManager.setAddresses([
        addressData,
        //...currentAddresses,
        ...globalThis.ShoppingCartManager.getAddresses(),
      ]);
    } catch (error) {
      Sentry.captureException(error);
    }

    // Dispatch Results
    dispatch({
      type: userActionsType.ADDRESS_ADD_SUCCESS,
      payload: addressData,
    });

    // Return Promise
    return Promise.resolve();
  }
};
const updateAddress = ({ uuid, ...addressData }) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: userActionsType.ADDRESS_UPDATE_REQUEST,
  });

  // Get current user
  const currentUser = getState().user;
  //const currentAddresses = currentUser.addresses.addresses;
  let response = {
    data: null,
  };
  if (currentUser.isLogged && !currentUser.isGuest) {
    // Make/Handle API call

    //console.log('globalThis.ShoppingCartManager ->',globalThis.ShoppingCartManager);

    try {
      response = await api.patch(
        `/api/v1/user/address/${uuid}/`,
        JSON.stringify(addressData),
      );
    } catch (error) {
      Sentry.captureException(error);

      dispatch({
        type: userActionsType.ADDRESS_UPDATE_FAILURE,
        payload: error,
      });
      throw error;
    }
    // Add user addresses to ShoppingCartManager.
    // try {
    //   let taddress = currentAddresses.indexOf((address) =>
    //     address.uuid === response.data.uuid ? response.data : address,
    //   );
    //   taddress = currentAddresses.slice(taddress, 1);
    //   globalThis.ShoppingCartManager.setAddresses([
    //     taddress,
    //     ...currentAddresses,
    //   ]);
    // } catch (error) {

    // }

    // Dispatch Response
    dispatch({
      type: userActionsType.ADDRESS_UPDATE_SUCCESS,
      payload: response.data,
    });
    dispatch(listAddresses());

    return response;
  } else {
    // response.data = [addressData];
    // dispatch({
    //   type: userActionsType.ADDRESS_UPDATE_SUCCESS,
    //   payload: response.data,
    // });
    // dispatch(listAddresses());
    // return response;
  }

  // Add user addresses to ShoppingCartManager.
  // try {
  //   globalThis.ShoppingCartManager.setAddresses(
  //     currentAddresses.map((address) =>
  //       address.uuid === addressData.uuid ? addressData : address,
  //     ),
  //   );
  // } catch (error) {

  // }

  // Dispatch Response
  dispatch({
    type: userActionsType.ADDRESS_UPDATE_SUCCESS,
    payload: addressData,
  });
  dispatch(listAddresses());

  // Return Promise
  return Promise.resolve(addressData);
};
const deleteAddress = (addressId) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionsType.ADDRESS_REMOVE_REQUEST,
  });

  // Get current user
  const currentUser = getState().user;
  if (currentUser.isLogged && !currentUser.isGuest) {
    // API call configuration
    let response;
    try {
      response = await api.delete(
        `/api/v1/user/address/${addressId}/`,
        JSON.stringify({ address_uid: addressId }),
      );
    } catch (error) {
      Sentry.captureException(error);

      dispatch({
        type: userActionsType.ADDRESS_REMOVE_FAILURE,
        payload: error,
      });
      throw error;
    }

    // Dispatch Response
    dispatch({
      type: userActionsType.ADDRESS_REMOVE_SUCCESS,
      payload: addressId,
    });
    dispatch(listAddresses());

    return response;
  } else {
    // Delete guest
    globalThis.ShoppingCartManager.setAddresses([]);
    _customSetData('addresses', []);
  }
  // Dispatch Response
  dispatch({
    type: userActionsType.ADDRESS_REMOVE_SUCCESS,
    payload: currentUser.addresses.addresses.filter(
      (address) => address.uuid === addressId,
    ),
  });
  dispatch(listAddresses());

  return Promise.resolve();
};
const listAddresses = () => async (dispatch, getState, api) => {
  dispatch({
    type: userActionsType.ADDRESS_LIST_REQUEST,
  });

  // Get current user
  const currentUser = getState().user;

  ///////////////////////////////////////////////////////////////////////////////********
  if (currentUser.isLogged && !currentUser.isGuest) {
    // Make/Handle API call
    let response;
    try {
      response = await api.get('/api/v1/user/address/');
      // if (response.data.length === 0) {
      //   throw error;
      // }
    } catch (error) {
      Sentry.captureException(error);

      dispatch({
        type: userActionsType.ADDRESS_LIST_FAILURE,
        payload: error,
      });
      throw error;
    }

    // Add user addresses to ShoppingCartManager.
    const formattedAddresses = response.data.map((address) => ({
      ...address,
      client_name: address.client_name
        ? address.client_name
        : `${currentUser.profile.first_name} ${currentUser.profile.last_name}`,
      phone: address.phone ? address.phone : currentUser.profile.mobile,
    }));
    try {
      globalThis.ShoppingCartManager.setAddresses(formattedAddresses);
    } catch (error) {
      Sentry.captureException(error);
    }

    // Dispatch Response
    dispatch({
      type: userActionsType.ADDRESS_LIST_SUCCESS,
      payload: formattedAddresses,
    });

    dispatch(shoppingCartActions.fetchShoppingCart());

    return response;
  } else {
    //async
    let AsyncAddressess = await _customGetData('addresses');
    let setAddresses = [];
    if (AsyncAddressess) {
      await Object.entries(AsyncAddressess).forEach(([key, value]) => {
        if (value) {
          setAddresses.push(value);
        }
      });

      try {
        globalThis.ShoppingCartManager.setAddresses(setAddresses);
      } catch (error) {
        Sentry.captureException(error);

        return Promise.reject(error);
      }
      return AsyncAddressess;
    }
  }
  ///////////////////////////////////////////////////////////////////////////////********
  // Add user addresses to ShoppingCartManager.
  try {
    globalThis.ShoppingCartManager.setAddresses(
      currentUser.addresses.addresses,
    );
  } catch (error) {
    Sentry.captureException(error);
    return Promise.reject(error);
  }

  // Dispatch Response
  dispatch({
    type: userActionsType.ADDRESS_LIST_SUCCESS,
    payload: currentUser.addresses.addresses,
  });

  dispatch(shoppingCartActions.fetchShoppingCart());

  return Promise.resolve(currentUser.addresses.addresses);
};

const getAddressDetailsByZipCode = (zipCode) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: userActionsType.GET_ADDRESS_DETAILS_BY_ZIPCODE_REQUEST,
  });

  // Make/Handle API call
  const response = await api
    .get(`/api/v1/shipping/locations/${zipCode}`)
    .catch((e) => {
      Sentry.captureException(e);

      dispatch({
        type: userActionsType.GET_ADDRESS_DETAILS_BY_ZIPCODE_FAILURE,
        payload: e,
      });
      return Promise.reject(e);
    });

  // Dispatch Response
  try {
    dispatch({
      type: userActionsType.GET_ADDRESS_DETAILS_BY_ZIPCODE_SUCCESS,
      payload: response.data,
    });
  } catch (e) {
    Sentry.captureException(e);
    return Promise.reject(e);
  }

  return response;
};

// REVIEWS ACTIONS
const addReview = (productPurchase) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionsType.REVIEWS_ADD_REQUEST,
  });

  // API call configuration
  let response;

  // Make/Handle API call
  try {
    response = await api.post(
      `/api/v1/reviews/my-reviews/order_product/${productPurchase.order_product}/`,
      JSON.stringify(productPurchase),
    );
  } catch (error) {
    Sentry.captureException(error);

    dispatch({
      type: userActionsType.REVIEWS_ADD_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Sent event notification via Slack.
  // axios
  //   .post('/tools/integrations/slack/approve-review-request/', {
  //     id: productPurchase.order.uid,
  //     name: productPurchase.product.name,
  //     store: productPurchase.product.store.name,
  //     comment: productPurchase.comment,
  //     rating: productPurchase.rating,
  //     image: productPurchase.product.photo.small || '',
  //   })

  // Dispatch Results
  dispatch({
    type: userActionsType.REVIEWS_ADD_SUCCESS,
    payload: response.data,
    currentReview: productPurchase,
  });

  // Return Promise
  return response;
};
const updateReview = ({ uuid, ...addressData }) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: userActionsType.ADDRESS_UPDATE_REQUEST,
  });

  // API call configuration
  let response;

  // Make/Handle API call
  try {
    response = await api.patch(
      `/api/v1/user/address/${uuid}/`,
      JSON.stringify(addressData),
    );
  } catch (error) {
    Sentry.captureException(error);

    dispatch({
      type: userActionsType.ADDRESS_UPDATE_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Dispatch Response
  dispatch({
    type: userActionsType.ADDRESS_UPDATE_SUCCESS,
    payload: response.data,
  });

  return response;
};
const deleteReview = (addressId) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionsType.ADDRESS_REMOVE_REQUEST,
  });

  // API call configuration

  let response;
  // Make/Handle API call
  try {
    response = await api.delete(
      `/api/v1/user/address/${addressId}/`,
      JSON.stringify({ address_uid: addressId }),
    );
  } catch (error) {
    Sentry.captureException(error);

    dispatch({
      type: userActionsType.ADDRESS_REMOVE_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Dispatch Response
  dispatch({
    type: userActionsType.ADDRESS_REMOVE_SUCCESS,
    payload: addressId,
  });
};
const getUserReviews = (queryString = '', reviewType = 'pending') => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: userActionsType.GET_USER_REVIEWS_REQUEST,
  });

  let query = '';
  if (queryString !== '') {
    query = queryString;
  }
  // Make/Handle API call
  let response;
  response = await api
    .get(`/api/v1/reviews/my-purchased-products/${query}`)
    .catch((e) => {
      Sentry.captureException(e);

      dispatch({
        type: userActionsType.GET_USER_REVIEWS_FAILURE,
        e,
      });
    });

  // Dispatch Response
  dispatch({
    type: userActionsType.GET_USER_REVIEWS_SUCCESS,
    payload: response.data,
    reviewType,
  });
  return response;
};

// UPDATE USER PROFILE
const uploadUserPhoto = (formData) => async (dispatch, getState, api) => {
  // Dipatch Request
  dispatch({
    type: userActionsType.UPLOAD_PHOTO_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.patch('/api/v1/user/profile/', formData);
  } catch (error) {
    Sentry.captureException(error);

    dispatch({
      type: userActionsType.UPLOAD_PHOTO_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: userActionsType.UPLOAD_PHOTO_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET USER STATUS
const getUserCards = () => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Login Request
  dispatch({
    type: userActionsType.GET_USER_CARDS_REQUEST,
  });
  // Make/Handle API call
  let response: Object;
  try {
    response = await api.get('/api/v1/payment/stripe/my-cards/');
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: userActionsType.GET_CURRENT_USER_FAILURE,
      error: error,
    });
    throw error;
  }
  // Dispatch Success
  dispatch({
    type: userActionsType.GET_USER_CARDS_SUCCESS,
    payload: response.data,
  });
  // dispatch(getUserCreditCards());
  // Return handler
  return response;
};

// DELETE USER CARD
const deleteUserCard = (cardId: string) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  dispatch({
    type: userActionsType.USER_CARD_REMOVE_REQUEST,
  });
  // Make/Handle API call
  let response: Object;
  try {
    response = await api.delete(`/api/v1/payment/stripe/my-card/${cardId}`);
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: userActionsType.USER_CARD_REMOVE_FAILURE,
      payload: error,
    });
    throw error;
  }
  // Dispatch Response
  dispatch({
    type: userActionsType.USER_CARD_REMOVE_SUCCESS,
    payload: cardId,
  });
  return response;
};

// ADD USER CARD
const addUserCard = (tokenData: Object) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Login Request
  dispatch({
    type: userActionsType.USER_CARD_ADD_REQUEST,
  });

  // Make/Handle API call
  let response: Object;
  try {
    response = await api.post(
      '/api/v1/payment/stripe/save-card/',
      JSON.stringify(tokenData),
    );
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: userActionsType.USER_CARD_ADD_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Card Success
  dispatch({
    type: userActionsType.USER_CARD_ADD_SUCCESS,
    payload: response.data,
  });

  // Return handler
  return response;
};

// SET DEFAULT USER CARD
const setDefaultUserCard = (cardID: String) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  dispatch({
    type: userActionsType.USER_CARD_SET_DEFAULT_REQUEST,
  });
  // Make/Handle API call
  let response: Object;
  try {
    response = await api.post('/api/v1/payment/stripe/set-default-card/', {
      card_id: cardID,
    });
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: userActionsType.USER_CARD_SET_DEFAULT_FAILURE,
      payload: error,
    });
    throw error;
  }
  // Dispatch Response
  dispatch({
    type: userActionsType.USER_CARD_SET_DEFAULT_SUCCESS,
    payload: cardID,
  });
  return response;
};

/**---------------------------------------
            EXPORT ACTIONS
-----------------------------------------*/
export const userActions: Object = {
  // Login
  setCookiesUser,
  loginAsGuest,
  loginUser,
  logout,
  passwordRecover,
  autoLogin,

  // Register and Account
  registerUser,
  verifyAutoLogin,
  verifyEmailAccount,
  verifyAccount,
  validateCode,
  getCurrentUser,
  updateUserInfo,
  uploadUserPhoto,

  // Addresses
  addAddress,
  updateAddress,
  deleteAddress,
  listAddresses,
  getAddressDetailsByZipCode,

  // Reviews
  getUserReviews,
  addReview,
  updateReview,
  deleteReview,

  // Cards
  getUserCards,
  deleteUserCard,
  addUserCard,
  setDefaultUserCard,
};
