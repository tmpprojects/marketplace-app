import { shoppingCartActionTypes } from '../constants';
import * as Sentry from '@sentry/react-native';
// Declar global scope reference
declare var globalThis: any;

//
const updateCartProduct = (
  product: Object,
  paymentMethod: void = undefined,
) => async (dispatch: Function, getState: Function, api: Object) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.UPDATE_CART_PRODUCT_REQUEST,
  });

  // API call configuration
  let response: Object;

  try {
    // Construct query params
    const address = globalThis.ShoppingCartManager.getShippingAddress();
    let queryParams: string = '';
    queryParams = address
      ? `?${queryParams}shipping_postal_code=${address.zip_code}&`
      : queryParams;
    queryParams = paymentMethod
      ? `${queryParams}payment_method=${paymentMethod}&`
      : queryParams;

    response = await api.post(
      `/api/v1/cart/?${queryParams}`,
      JSON.stringify(product),
      {
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: shoppingCartActionTypes.UPDATE_CART_PRODUCT_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: shoppingCartActionTypes.UPDATE_CART_PRODUCT,
    product: response.data,
  });

  // Update Cart
  dispatch(fetchShoppingCart());

  // Return promise
  return response;
};

const fetchShoppingCart = (paymentMethod: void = undefined) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.GET_CART_PRODUCTS_REQUEST,
  });

  // API call configuration
  let response: Object;
  try {
    // Construct query params
    const address = globalThis.ShoppingCartManager.getShippingAddress();
    let queryParams: string = '';
    queryParams = address
      ? `?${queryParams}shipping_postal_code=${address.zip_code}&`
      : queryParams;
    queryParams = paymentMethod
      ? `${queryParams}payment_method=${paymentMethod}&`
      : queryParams;
    response = await api.get(`/api/v1/cart/${queryParams}`);
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: shoppingCartActionTypes.GET_CART_PRODUCTS_FAILURE,
      error: true,
      payload: error,
    });

    // ATENTION!
    // We´re not throwing an error because this action is called in several places,
    // and Backend API sends an 500 error if something goes wrong.
    // This should be refactored.
    //throw error // <---- Uncomment this line when ready.
    return response;
  }

  //Update ShoopingCartManager
  globalThis.ShoppingCartManager.updateCart(response.data);

  // Dispatch Success
  dispatch({
    type: shoppingCartActionTypes.GET_CART_PRODUCTS,
    products: response.data,
    _store: getState(),
  });

  //
  return response;
};

const removeFromCart = (product: Object) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.REMOVE_FROM_CART_REQUEST,
  });

  // API call configuration
  let response: Object;
  try {
    const productAttributeID = product.attribute ? `${product.attribute}/` : '';
    response = await api.delete(
      `/api/v1/cart/${product.slug}/${productAttributeID}`,
    );
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: shoppingCartActionTypes.REMOVE_FROM_CART_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Update Cart Products
  dispatch(fetchShoppingCart());

  // Dispatch Success
  // dispatch({
  //     type: shoppingCartActionTypes.REMOVE_FROM_CART_SUCCESS,
  //     product
  // });

  // Return axios promise
  return response;
};

const emptyShoppingCart = () => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.EMPTY_SHOPPING_CART,
  });
};

//VERIFY PROMO COUPON
const validateCoupon = (coupon: Object) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.VERIFY_PROMO_CODE_REQUEST,
  });

  // API call configuration
  let response: Object;
  const requestOptions: Object = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    response = await api.post(
      '/api/v1/cart/check-code/',
      { code: coupon },
      requestOptions,
    );
  } catch (error) {
    Sentry.captureException(error);
    // Update ShoppingCartManager
    globalThis.ShoppingCartManager.setCoupon({
      code: coupon,
      statusMessage: error.response.data.code,
    });

    // Dispatch Action
    dispatch({
      type: shoppingCartActionTypes.VERIFY_PROMO_CODE_FAILURE,
      error,
    });
    throw error;
  }

  // Update ShoppingCartManager
  globalThis.ShoppingCartManager.setCoupon({
    code: coupon,
    ...response.data,
  });

  // Dispatch Success
  dispatch({
    type: shoppingCartActionTypes.VERIFY_PROMO_CODE_SUCCESS,
    coupon,
    couponDetails: response.data,
  });

  //dispatch(fetchShoppingCart())
  return response;
};

const sendOrder = (order: Object) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.SEND_ORDER_REQUEST,
  });

  // API call configuration
  const requestOptions: Object = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  let response: Object;
  try {
    response = await api.post(
      '/api/v1/order/group/',
      JSON.stringify(order),
      requestOptions,
    );
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: shoppingCartActionTypes.SEND_ORDER_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: shoppingCartActionTypes.SEND_ORDER_SUCCESS,
    payload: response.data,
  });

  // Update Cart
  dispatch(fetchShoppingCart());

  // Return Promise
  return response;
};

const sendPayPalOrder = (order: Object) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.SEND_ORDER_REQUEST,
  });

  // API call configuration
  const requestOptions: Object = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Send Order to backend server
  return api
    .post('/api/v1/order/group/', order, requestOptions)
    .then((response) => response)
    .catch((error) => {
      Sentry.captureException(error);
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_FAILURE,
        error: true,
        payload: error,
      });
      throw error;
    });
};

//
const payPalAuthorize = (paymentData: Object) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.PAYPAL_AUTHORIZE_REQUEST,
  });

  // Send Order to backend server
  return api
    .get('/api/v1/payment/paypal/execute-payment/', paymentData)
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_SUCCESS,
        payload: response.data,
      });

      // // Update Cart
      // dispatch(fetchShoppingCart());

      return response;
    })
    .catch((error) => {
      Sentry.captureException(error);
      dispatch({
        type: shoppingCartActionTypes.PAYPAL_AUTHORIZE_FAILURE,
        error: true,
        payload: error,
      });
      return error;
    });
};

//
const paymentSuccess = (order: Object) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  // dispatch({
  //     type: shoppingCartActionTypes.SEND_ORDER_REQUEST,
  // });

  // API call configuration
  const requestOptions: Object = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Send Order to backend server
  return api
    .post('/api/v1/order/group/', order, requestOptions)
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_SUCCESS,
        payload: response.data,
      });

      // Update Cart
      dispatch(fetchShoppingCart());

      return response;
    })
    .catch((error) => {
      Sentry.captureException(error);
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_FAILURE,
        error: true,
        payload: error,
      });
    });
};

//
const paymentError = (order: Object) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  // dispatch({
  //     type: shoppingCartActionTypes.SEND_ORDER_REQUEST,
  // });

  // API call configuration
  const requestOptions: Object = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Send Order to backend server
  return api
    .post('/api/v1/payment/paypal/cancel-payment/', order, requestOptions)
    .then((response) => response)
    .catch((error) => {
      Sentry.captureException(error);
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_FAILURE,
        error: true,
        payload: error,
      });
    });
};

//
const paymentCancel = (order: Object) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  // dispatch({
  //     type: shoppingCartActionTypes.SEND_ORDER_REQUEST,
  // });

  // API call configuration
  const requestOptions: Object = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Send Order to backend server
  // Encviar paymentID
  return api
    .post('/api/v1/payment/paypal/cancel-payment/', order, requestOptions)
    .then((response) => response)
    .catch((error) => {
      Sentry.captureException(error);
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_FAILURE,
        error: true,
        payload: error,
      });
    });
};

export const shoppingCartActions = {
  // Cart modifiers
  emptyShoppingCart,
  removeFromCart,
  fetchShoppingCart,
  updateCartProduct,

  // Card, Oxxo and Bank Transfer
  sendOrder,

  // Paypal
  sendPayPalOrder,
  payPalAuthorize,
  paymentSuccess,
  paymentError,
  paymentCancel,

  //Promo Coupon
  validateCoupon,
};
