import { appActionsType } from '../constants';

import { config } from '../../../env';

// Declar global scope reference
declare var globalThis: any;

/**---------------------------------------
 GENERAL APP DATA
-----------------------------------------*/
// INTERESTS
const fetchInterests = () => async (dispatch, getState, api) => {
  dispatch({
    type: appActionsType.FETCH_INTERESTS_REQUEST,
  });
  const interestsList = [
    {
      id: '0',
      title: 'Celebraciones',
    },
    {
      id: '1',
      title: 'Regalos',
    },
    {
      id: '2',
      title: 'Comida',
    },
    {
      id: '3',
      title: 'Fiesta',
    },
    {
      id: '4',
      title: 'Diseño',
    },
    {
      id: '5',
      title: 'Flores',
    },
    {
      id: '6',
      title: 'Eco',
    },
    {
      id: '7',
      title: 'Infantil',
    },
    {
      id: '8',
      title: 'Belleza',
    },
    {
      id: '9',
      title: 'Mascotas',
    },
    {
      id: '10',
      title: 'Ropa y calzado',
    },
    {
      id: '11',
      title: 'Accesorios y joyería',
    },
  ];
  let response;
  try {
    response = await interestsList;
  } catch (error) {
    dispatch({
      type: appActionsType.FETCH_INTERESTS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  dispatch({
    type: appActionsType.FETCH_INTERESTS_SUCCESS,
    payload: interestsList,
  });
  return response;
};
// CATEGORIES
const getCategories = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.GET_CATEGORIES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await api.get(
      `/api/v1/market/categories/?${config.ZIPCODEQUERY}=${
        getState().app.zipCode
      }`,
    );
  } catch (error) {
    dispatch({
      type: appActionsType.GET_CATEGORIES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionsType.GET_CATEGORIES_SUCCESS,
    categories: response.data,
  });
  return response;
};

// UPDATE ZIP CODE
const updateZipCde = (newZipCode = null) => async (dispatch, getState, api) => {
  // Dispatch Success
  dispatch({
    type: appActionsType.UPDATE_ZIPCODE_SUCCESS,
    zipCode: newZipCode,
  });
  //return response;
};

// MARKETPLACE CATEGORIES
const getMarketCategories = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.GET_MARKET_CATEGORIES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await api.get(
      `/api/v1/market/categories/?${config.ZIPCODEQUERY}=${
        getState().app.zipCode
      }`,
    );
    // response = await api.get('/api/v1/market/categories/');
    // response = await LocalRedis(
    //   api,
    //   '/api/v1/market/categories/',
    //   'GET_MARKET_CATEGORIES_REQUEST'
    // );
  } catch (error) {
    dispatch({
      type: appActionsType.GET_MARKET_CATEGORIES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionsType.GET_MARKET_CATEGORIES_SUCCESS,
    categories: response.data,
  });
  return response;
};

//
const getStoresList = (queryString: string = '', slug: string) => async (
  dispatch: Function,
  getState: Function,
  api: Object,
) => {
  // Request
  dispatch({
    type: appActionsType.STORES_LIST_REQUEST,
  });
  // API call configuration
  let response;
  let query = `?ordering=-created&page=1&page_size=3&has_cover=true&has_photo=true&${
    config.ZIPCODEQUERY
  }=${getState().app.zipCode}`;
  if (queryString !== '') {
    query = queryString;
  }
  let apiEndpoint;
  if (slug === 'envio-nacional') {
    apiEndpoint = `/api/v1/market/stores/${query}&zone=national&${
      config.ZIPCODEQUERY
    }=${getState().app.zipCode}`;
  } else {
    apiEndpoint = `/api/v1/market/stores/${query}&${config.ZIPCODEQUERY}=${
      getState().app.zipCode
    }`;
  }
  try {
    response = await api.get(apiEndpoint);
  } catch (error) {
    dispatch({
      type: appActionsType.STORES_LIST_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionsType.STORES_LIST_SUCCESS,
    stores: response.data,
  });
  return response;
};

//SHIPPING SCHEDULES
const getPickupSchedules = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.GET_PICKUP_SCHEDULES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await api.get('/api/v1/shipping/general-shipping-schedules/');
  } catch (error) {
    dispatch({
      type: appActionsType.GET_PICKUP_SCHEDULES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Update Shopping Cart Delivery Schedules
  globalThis.ShoppingCartManager.setDeliverySchedules(response.data);

  // Dispatch Success
  dispatch({
    type: appActionsType.GET_PICKUP_SCHEDULES_SUCCESS,
    payload: response.data,
  });
  return response;
};

//SHIPPING METHODS
const getShippingMethods = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.GET_SHIPPING_METHODS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await api.get('/api/v1/shipping/methods/');
  } catch (error) {
    dispatch({
      error,
      type: appActionsType.GET_SHIPPING_METHODS_FAILURE,
    });
    throw error;
  }

  // Update Shopping Cart Shipping Methods
  try {
    globalThis.ShoppingCartManager.setShippingMethods(response.data);
  } catch (error) {}

  // Dispatch Success
  dispatch({
    type: appActionsType.GET_SHIPPING_METHODS_SUCCESS,
    shippingMethods: response.data,
  });
  return response;
};
//PAYMENT METHODS
const getPaymentMethods = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.GET_PAYMENT_METHODS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await api.get('/api/v1/payment/methods/');
  } catch (error) {
    dispatch({
      error,
      type: appActionsType.GET_PAYMENT_METHODS_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionsType.GET_PAYMENT_METHODS_SUCCESS,
    paymentMethods: response.data,
  });
  return response;
};

/**---------------------------------------
HOMEPAGE DATA
-----------------------------------------*/
// FEATURED BANNER
const changeSearch = (actionButton) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.CHANGE_SEARCH_REQUEST,
  });

  // API call configuration
  // let response;
  // try {
  //   response = await api.get('/api/v1/featured/market/banner/');
  // } catch (error) {
  //   dispatch({
  //     type: appActionsType.CHANGE_SEARCH_FAILURE,
  //     error: true,
  //     payload: error,
  //   });
  //   throw error;
  // }

  // Dispatch Success
  dispatch({
    type: appActionsType.CHANGE_SEARCH_SUCCESS,
    payload: actionButton,
  });
  // return response;
};

// FEATURED BANNER
const getFeaturedBanners = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.FEATURED_BANNERS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await api.get(
      `/api/v1/featured/market/banner/?${config.ZIPCODEQUERY}=${
        getState().app.zipCode
      }`,
    );
  } catch (error) {
    dispatch({
      type: appActionsType.FEATURED_BANNERS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionsType.FEATURED_BANNERS_SUCCESS,
    banners: response.data,
  });
  return response;
};

// FEATURED PRODUCTS
const getFeaturedProducts = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.FEATURED_PRODUCTS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await api.get(
      `/api/v1/featured/market/products/?page_size=10&${config.ZIPCODEQUERY}=${
        getState().app.zipCode
      }`,
    );
  } catch (error) {
    dispatch({
      type: appActionsType.FEATURED_PRODUCTS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionsType.FEATURED_PRODUCTS_SUCCESS,
    featuredProducts: response.data,
  });
  return response;
};

// FEATURES STORES
const getFeaturedStores = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.FEATURED_STORES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await api.get(
      `/api/v1/featured/market/stores/?page_size=10&${config.ZIPCODEQUERY}=${
        getState().app.zipCode
      }`,
    );
  } catch (error) {
    dispatch({
      type: appActionsType.FEATURED_STORES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }
  // Dispatch Success
  dispatch({
    type: appActionsType.FEATURED_STORES_SUCCESS,
    featuredStores: response.data,
  });
  return response;
};

/**---------------------------------------
SEARCH
-----------------------------------------*/
const getSearchStores = (queryString) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.GET_SEARCH_STORES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await api.get(
      `/api/v1/search/?q=${queryString}&type=stores&${config.ZIPCODEQUERY}=${
        getState().app.zipCode
      }`,
    );
  } catch (error) {
    dispatch({
      error,
      type: appActionsType.GET_SEARCH_STORES_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionsType.GET_SEARCH_STORES_SUCCESS,
    results: response.data,
  });
  return response;
};
const getSearchProducts = (queryString) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionsType.GET_SEARCH_PRODUCTS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    //response = await api.get(`/api/v1/search/?q=${queryString}&type=products`);
    response = await api.get(
      `/api/v1/search/${queryString}&${config.ZIPCODEQUERY}=${
        getState().app.zipCode
      }`,
    );
  } catch (error) {
    dispatch({
      error,
      type: appActionsType.GET_SEARCH_PRODUCTS_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionsType.GET_SEARCH_PRODUCTS_SUCCESS,
    results: response.data,
  });
  return response;
};
const getCategoryResults = (queryString, slug) => async (
  dispatch,
  getState,
  api,
) => {
  // Request
  dispatch({
    type: appActionsType.GET_CATEGORY_RESULTS_REQUEST,
  });
  let apiEndpoint;
  if (slug === 'envio-nacional') {
    apiEndpoint = `/api/v1/market/products/${queryString}&national_shipping=true&${
      config.ZIPCODEQUERY
    }=${getState().app.zipCode}`;
    // localRedisKey = `${queryString}_national_shipping`;
  } else {
    apiEndpoint = `/api/v1/market/products/${queryString}&${
      config.ZIPCODEQUERY
    }=${getState().app.zipCode}`;
    // localRedisKey = `${queryString}`;
  }
  // API call configuration
  let response;
  try {
    response = await api.get(apiEndpoint);
  } catch (error) {
    dispatch({
      error,
      type: appActionsType.GET_CATEGORY_RESULTS_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionsType.GET_CATEGORY_RESULTS_SUCCESS,
    payload: response.data,
  });
  return response;
};

/**---------------------------------------
 EXPORT ACTIONS
-----------------------------------------*/
export const appActions = {
  updateZipCde,
  changeSearch,
  fetchInterests,
  getCategories,
  getStoresList,
  getPickupSchedules,
  getShippingMethods,
  getPaymentMethods,
  getFeaturedBanners,
  getFeaturedProducts,
  getFeaturedStores,
  getCategoryResults,
  getSearchProducts,
  getSearchStores,
  getMarketCategories,
};
