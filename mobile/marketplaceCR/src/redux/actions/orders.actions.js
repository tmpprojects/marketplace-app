//import config from 'react-native-config';
import { config } from '../../../env';
import { userTypes, orderActionTypes } from '../constants';

// GET ORDERºS
const getOrders = (userType = userTypes.CUSTOMER, searchQuery) => async (
  dispatch,
  getState,
  api,
) => {
  // Login Request
  dispatch({
    type: orderActionTypes.GET_ORDERS_REQUEST,
    userType,
  });

  // Make/Handle API call
  let response;

  //let query = `page=1&page_size=${config.DEFAULT_ORDERS}&ordering=-created_date`;
  let query = `page=1&page_size=100&ordering=-created_date`;
  if (searchQuery) {
    query = searchQuery;
  }

  let apiEndpoint;
  if (userType === userTypes.VENDOR) {
    apiEndpoint = `/api/v1/order/my-store/?${query}`;
  } else {
    apiEndpoint = `/api/v1/order/group/?${query}`;
  }

  try {
    response = await api.get(apiEndpoint);
  } catch (error) {
    dispatch({
      type: orderActionTypes.GET_ORDERS_FAILURE,
      error,
      userType,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: orderActionTypes.GET_ORDERS_SUCCESS,
    payload: response.data,
    userType,
  });
  return response;
};

const selectOrderDetail = (item) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: orderActionTypes.SELECT_DETAIL_ORDER_REQUEST,
    setItem: item,
  });
};
// // UPDATE STATUS ORDER
// const updateStatusOrder = (uid, status) => async (dispatch, getState, api) => {
//     // Dispatch Request
//     dispatch({
//         type: orderActionTypes.UPDATE_STATUS_ORDER_REQUEST,
//     });

//     // Make/Handle API call
//     let response;
//     //debugger;
//     try {
//         response = await api.patch(
//             `/api/v1/order/my-store/${uid}/`,
//             { physical_properties: { status } }
//         );
//     //
//     } catch (error) {
//         dispatch({
//             type: orderActionTypes.UPDATE_STATUS_ORDER_FAILURE,
//             error,
//         });
//         throw error;
//     }

//     // Dispatch Success
//     dispatch({
//         type: orderActionTypes.UPDATE_STATUS_ORDER_SUCCESS,
//         payload: response.data
//     });
//     return response;
// };

// // REQUEST DHL GUIDES
// const requestDhlGuides = (uid, packages) => async (dispatch, getState, api) => {
//     // Dispatch Request
//     dispatch({
//         type: orderActionTypes.MAKE_DHL_GUIDES_REQUEST,
//     });

//     // Make/Handle API call
//     let response;

//     try {
//         response = await api.post(
//             `/api/v1/order/my-store/${uid}/generate-labels/`,
//             packages
//         );
//     //
//     } catch (error) {
//         dispatch({
//             type: orderActionTypes.MAKE_DHL_GUIDES_FAILURE,
//             error,
//         });
//         throw error;
//     }

//     // Dispatch Success
//     dispatch({
//         type: orderActionTypes.MAKE_DHL_GUIDES_SUCCESS,
//         payload: response.data,
//         uid
//     });
//     return response;
// };

// // SCHEDULE PICKUP DHL
// const scheduleDHLPickup = uid => async (dispatch, getState, api) => {
//     // Dispatch Request
//     dispatch({
//         type: orderActionTypes.SCHEDULE_DHL_PICKUP_REQUEST,
//     });

//     // Make/Handle API call
//     let response;

//     try {
//         response = await api.post(
//             `/api/v1/order/my-store/${uid}/schedule-pickup/`
//         );
//     //
//     } catch (error) {
//         dispatch({
//             type: orderActionTypes.SCHEDULE_DHL_PICKUP_FAILURE,
//             error,
//         });
//         throw error;
//     }

//     // Dispatch Success
//     dispatch({
//         type: orderActionTypes.SCHEDULE_DHL_PICKUP_SUCCESS,
//         payload: response.data,
//         uid
//     });
//     return response;
// };

// // GET ORDER STATISTICS
// const getOrderStats = () => async (dispatch, getState, api) => {
//     // Request
//     dispatch({
//         type: orderActionTypes.GET_ORDER_STATS_REQUEST,
//     });
//     // API call configuration
//     let response;
//     const apiEndpoint = '/api/v1/order/my-store/order-stats/';

//     try {
//         response = await api.get(apiEndpoint);
//     } catch (error) {
//         dispatch({
//             error,
//             type: orderActionTypes.GET_ORDER_STATS_FAILURE
//         });
//         throw error;
//     }
//     // Dispatch Success
//     dispatch({
//         type: orderActionTypes.GET_ORDER_STATS_SUCCESS,
//         payload: response.data
//     });
//     return response;
// };

export const ordersActions = {
  getOrders,
  selectOrderDetail,
  // updateStatusOrder,
  // requestDhlGuides,
  // scheduleDHLPickup,
  // getOrderStats,
};
