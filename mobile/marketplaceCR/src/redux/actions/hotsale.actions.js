//import config from 'react-native-config';
import { config } from '../../../env';
import { collectionActionTypes } from '../constants';

// GET HOTSALE
const getHotsale = (slug = '') => async (dispatch, getState, api) => {
  dispatch({
    type: collectionActionTypes.GET_HOTSALE_REQUEST,
  });

  let response;
  try {
    response = await api.get(
      `https://canastarosa.com/api/v1/marketing/landing-page/${slug}/`,
    );
  } catch (error) {
    dispatch({
      type: collectionActionTypes.GET_HOTSALE_FAILURE,
      error,
    });
    throw error;
  }

  dispatch({
    type: collectionActionTypes.GET_HOTSALE_SUCCESS,
    payload: response.data,
  });
  return response;
};

export const collectionActions = {
  getHotsale,
};
