//import config from 'react-native-config';
import { config } from '../../../env';
import { collectionActionTypes } from '../constants';

// GET COLLECTION
const getCollection = (slug = '') => async (dispatch, getState, api) => {
  dispatch({
    type: collectionActionTypes.GET_COLLECTION_RESULTS_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get(`/api/v1/marketing/landing-page/${slug}/`);
  } catch (error) {
    dispatch({
      type: collectionActionTypes.GET_COLLECTION_RESULTS_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: collectionActionTypes.GET_COLLECTION_RESULTS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET ALL COLLECTIONS
const getAllCollections = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: collectionActionTypes.GET_ALL_COLLECTIONS_RESULTS_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get(`/api/v1/marketing/landing-pages/`);
  } catch (error) {
    dispatch({
      type: collectionActionTypes.GET_ALL_COLLECTIONS_RESULTS_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: collectionActionTypes.GET_ALL_COLLECTIONS_RESULTS_SUCCESS,
    payload: response.data,
  });
  return response;
};

export const collectionActions = {
  getCollection,
  getAllCollections,
};
