import { storeActionsType } from '../constants';

import { config } from '../../../env';
import * as Sentry from '@sentry/react-native';
// STORE
const fetchStore = (slug = '') => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: storeActionsType.GET_STORE_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get(
      `/api/v1/market/stores/${slug}/?${config.ZIPCODEQUERY}=${
        getState().app.zipCode
      }`,
    );
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: storeActionsType.GET_STORE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Success
  dispatch({
    type: storeActionsType.GET_STORE_SUCCESS,
    payload: response.data,
  });
  return response;
};

// PRODUCTS LIST
const fetchProducts = (store_slug, filter = '') => async (
  dispatch,
  getState,
  api,
) => {
  // Dispatch Action
  dispatch({
    type: storeActionsType.FETCH_PRODUCTS_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get(
      `/api/v1/market/stores/${store_slug}/products/?ordering=-created_date&page_size=50${filter}&${
        config.ZIPCODEQUERY
      }=${getState().app.zipCode}`,
    );
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: storeActionsType.FETCH_PRODUCTS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: storeActionsType.FETCH_PRODUCTS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// STORE SECTIONS
const fetchStoreSections = (store_slug) => async (dispatch, getState, api) => {
  // Dispatch Action
  dispatch({
    type: storeActionsType.FETCH_STORE_SECTIONS_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get(`/api/v1/market/stores/${store_slug}/sections/`);
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: storeActionsType.FETCH_STORE_SECTIONS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: storeActionsType.FETCH_STORE_SECTIONS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// PRODUCT DETAIL
const fetchProductDetail = (storeSlug, productSlug) => async (
  dispatch,
  getState,
  api,
) => {
  // Dispatch Action
  dispatch({
    type: storeActionsType.FETCH_PRODUCT_DETAIL_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get(
      `/api/v1/market/stores/${storeSlug}/products/${productSlug}/`,
    );
  } catch (error) {
    Sentry.captureException(error);
    dispatch({
      type: storeActionsType.FETCH_PRODUCT_DETAIL_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: storeActionsType.FETCH_PRODUCT_DETAIL_SUCCESS,
    payload: response,
  });
  return response;
};

//REVIEWS FROM STORE
const getStoreReviews = (store_slug, queryString) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: storeActionsType.GET_STORE_REVIEWS_REQUEST,
  });

  // Make/Handle API call
  let response;
  let query = '';
  if (queryString) {
    query = queryString;
  }

  response = await api
    .get(`/api/v1/reviews/reviews/stores/${store_slug}/${query}`)
    .catch((e) => {
      Sentry.captureException(e);
      dispatch({
        type: storeActionsType.GET_STORE_REVIEWS_FAILURE,
        e,
      });
      throw e;
    });

  // Dispatch Response
  dispatch({
    type: storeActionsType.GET_STORE_REVIEWS_SUCCESS,
    payload: response.data,
    store: store_slug,
  });
};

//PENDING REVIEWS OF USER FROM STORE
const getPendingStoreReviews = (store_slug) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: storeActionsType.GET_PENDING_STORE_REVIEWS_REQUEST,
  });

  // Make/Handle API call
  let response;
  response = await api
    .get(
      `/api/v1/reviews/my-reviews/stores/${store_slug}/?is_approved=null&page_size=100`,
    )
    .catch((e) => {
      Sentry.captureException(e);
      dispatch({
        type: storeActionsType.GET_PENDING_STORE_REVIEWS_FAILURE,
        e,
      });
    });

  // Dispatch Response
  try {
    dispatch({
      type: storeActionsType.GET_PENDING_STORE_REVIEWS_SUCCESS,
      payload: response.data,
    });
  } catch (e) {
    Sentry.captureException(e);
  }
};

//GET ALL REVIEWS FROM A PRODUCT
const getProductReviews = (product, queryString) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: storeActionsType.GET_PRODUCT_REVIEWS_REQUEST,
  });

  // Make/Handle API call
  let response;
  let query = '';
  if (queryString) {
    query = queryString;
  }

  response = await api
    .get(
      `/api/v1/reviews/reviews/stores/${product.store_slug}/products/${product.product_slug}/${query}`,
    )
    .catch((e) => {
      Sentry.captureException(e);
      dispatch({
        type: storeActionsType.GET_PRODUCT_REVIEWS_FAILURE,
        e,
      });
    });

  // Dispatch Response
  dispatch({
    type: storeActionsType.GET_PRODUCT_REVIEWS_SUCCESS,
    payload: response.data,
    product: product.product_slug,
  });
};

//GET PENDING REVIEWS OF USER FROM PRODUCT
const getPendingProductReviews = (store_slug, product_slug) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: storeActionsType.GET_PENDING_PRODUCT_REVIEWS_REQUEST,
  });

  // Make/Handle API call
  let response;
  response = await api
    .get(
      `/api/v1/reviews/my-reviews/stores/${store_slug}/products/${product_slug}/?is_approved=null&page_size=100`,
    )
    .catch((e) => {
      Sentry.captureException(e);
      dispatch({
        type: storeActionsType.GET_PENDING_PRODUCT_REVIEWS_FAILURE,
        e,
      });
    });

  // Dispatch Response
  dispatch({
    type: storeActionsType.GET_PENDING_PRODUCT_REVIEWS_SUCCESS,
    payload: response.data,
  });
};

//NEW STORES
const getNewsStores = () => async (dispatch, getState, api, zipCode) => {
  dispatch({
    type: storeActionsType.GET_NEW_STORES_REQUEST,
  });

  // Make/Handle API call
  let response;
  response = await api
    //.get(`/api/v1/market/stores/?page=1&page_size=10&zipcode=${zipCode}`,)
    .get(
      `/api/v1/market/stores/?page=1&page_size=50&${config.ZIPCODEQUERY}=${
        getState().app.zipCode
      }`,
    )
    .catch((e) => {
      Sentry.captureException(e);
      dispatch({
        type: storeActionsType.GET_NEW_STORES_FAILURE,
        e,
      });
    });

  // Dispatch Response
  dispatch({
    type: storeActionsType.GET_NEW_STORES_SUCCESS,
    payload: response.data,
  });
};

// EXPORT INDEX
export const storeActions = {
  fetchStore,
  fetchProducts,
  fetchStoreSections,
  fetchProductDetail,
  getStoreReviews,
  getPendingStoreReviews,
  getProductReviews,
  getPendingProductReviews,
  getNewsStores,
};
