export const appActionsType = {
  UPDATE_ZIPCODE_SUCCESS: 'UPDATE_ZIPCODE_SUCCESS',

  CHANGE_SEARCH_SUCCESS: 'CHANGE_SEARCH_SUCCESS',
  CHANGE_SEARCH_REQUEST: 'CHANGE_SEARCH_REQUEST',
  CHANGE_SEARCH_FAILURE: 'CHANGE_SEARCH_FAILURE',

  FETCH_INTERESTS_SUCCESS: 'FETCH_INTERESTS_SUCCESS',
  FETCH_INTERESTS_REQUEST: 'FETCH_INTERESTS_REQUEST',
  FETCH_INTERESTS_FAILURE: 'FETCH_INTERESTS_FAILURE',

  GET_CATEGORIES_SUCCESS: 'GET_CATEGORIES_SUCCESS',
  GET_CATEGORIES_REQUEST: 'GET_CATEGORIES_REQUEST',
  GET_CATEGORIES_FAILURE: 'GET_CATEGORIES_FAILURE',

  GET_MARKET_CATEGORIES_SUCCESS: 'GET_MARKET_CATEGORIES_SUCCESS',
  GET_MARKET_CATEGORIES_REQUEST: 'GET_MARKET_CATEGORIES_REQUEST',
  GET_MARKET_CATEGORIES_FAILURE: 'GET_MARKET_CATEGORIES_FAILURE',

  STORES_LIST_SUCCESS: 'STORES_LIST_SUCCESS',
  STORES_LIST_REQUEST: 'STORES_LIST_REQUEST',
  STORES_LIST_FAILURE: 'STORES_LIST_FAILURE',

  GET_PICKUP_SCHEDULES_REQUEST: 'GET_PICKUP_SCHEDULES_REQUEST',
  GET_PICKUP_SCHEDULES_SUCCESS: 'GET_PICKUP_SCHEDULES_SUCCESS',
  GET_PICKUP_SCHEDULES_FAILURE: 'GET_PICKUP_SCHEDULES_FAILURE',

  GET_SHIPPING_METHODS_REQUEST: 'GET_SHIPPING_METHODS_REQUEST',
  GET_SHIPPING_METHODS_SUCCESS: 'GET_SHIPPING_METHODS_SUCCESS',
  GET_SHIPPING_METHODS_FAILURE: 'GET_SHIPPING_METHODS_FAILURE',

  GET_PAYMENT_METHODS_REQUEST: 'GET_PAYMENT_METHODS_REQUEST',
  GET_PAYMENT_METHODS_SUCCESS: 'GET_PAYMENT_METHODS_SUCCESS',
  GET_PAYMENT_METHODS_FAILURE: 'GET_PAYMENT_METHODS_FAILURE',

  FEATURED_BANNERS_SUCCESS: 'FEATURED_BANNERS_SUCCESS',
  FEATURED_BANNERS_REQUEST: 'FEATURED_BANNERS_REQUEST',
  FEATURED_BANNERS_FAILURE: 'FEATURED_BANNERS_FAILURE',

  FEATURED_PRODUCTS_SUCCESS: 'FEATURED_PRODUCTS_SUCCESS',
  FEATURED_PRODUCTS_REQUEST: 'FEATURED_PRODUCTS_REQUEST',
  FEATURED_PRODUCTS_FAILURE: 'FEATURED_PRODUCTS_FAILURE',

  FEATURED_STORES_SUCCESS: 'FEATURED_STORES_SUCCESS',
  FEATURED_STORES_REQUEST: 'FEATURED_STORES_REQUEST',
  FEATURED_STORES_FAILURE: 'FEATURED_STORES_FAILURE',

  GET_SEARCH_STORES_SUCCESS: 'GET_SEARCH_STORES_SUCCESS',
  GET_SEARCH_STORES_REQUEST: 'GET_SEARCH_STORES_REQUEST',
  GET_SEARCH_STORES_FAILURE: 'GET_SEARCH_STORES_FAILURE',

  GET_SEARCH_PRODUCTS_SUCCESS: 'GET_SEARCH_PRODUCTS_SUCCESS',
  GET_SEARCH_PRODUCTS_REQUEST: 'GET_SEARCH_PRODUCTS_REQUEST',
  GET_SEARCH_PRODUCTS_FAILURE: 'GET_SEARCH_PRODUCTS_FAILURE',

  GET_CATEGORY_RESULTS_SUCCESS: 'GET_CATEGORY_RESULTS_SUCCESS',
  GET_CATEGORY_RESULTS_REQUEST: 'GET_CATEGORY_RESULTS_REQUEST',
  GET_CATEGORY_RESULTS_FAILURE: 'GET_CATEGORY_RESULTS_FAILURE',
};

export const ORDERS_PHONE = '55-7583-8134';
export const BUTLER_PHONE = '55-7939-3795';
export const ORDERS_PHONE_PLAIN_FORMAT = '5215575838134';
export const BUTLER_PHONE_PLAIN_FORMAT = '5215579393795';
export const VENDORS_PHONE = '55-7583-9041';
export const VENDORS_WHATSAPP_PLAIN_FORMAT = '525579393799';
export const ORDERS_EMAIL = 'pedidos@canastarosa.com';
export const CANASTAROSA_PRO_SLUG = 'canastarosa-pro';
export const SHIPPING_FEE = 70.0;
export const MARKETPLACE_FEE = 0.05;
export const FIXED_FEE = 70.0;
export const PAYMENT_FEE = 0.04;
export const LIMIT_CACHE = 900; // 900 seconds = 15 mins
export const userTypes = {
  VENDOR: 'VENDOR',
  CUSTOMER: 'CUSTOMER',
  ADMIN: 'ADMIN',
};
export const APP_DOMAIN = 'https://canastarosa.com';
export const socialNetworksURL = {
  facebook: 'https://www.facebook.com/',
  pinterest: 'https://www.pinterest.com/',
  instagram: 'https://www.instagram.com/',
};
export const socialNetworksShareMessages = {
  TWITTER_INSPIRE: 'Mira este artículo para crear cosas increíbles.',
  TWITTER_MARKET: 'Mira este producto único e increible',
};
export const APPLICATION_ENVIRONMENTS = {
  DEVELOPMENT: 'development',
  STAGING: 'staging',
  PRODUCTION: 'production',
};
export const APP_SECTIONS = {
  INSPIRE: 'inspire',
  MARKET: 'market',
  SHOPPING_CART: 'checkout',
  CART: 'cart',
  STORE_APP: 'my-store',
  PRO: 'pro',
  AWARDS: 'awards',
  SEARCH: 'search',
  LANDING: 'landing',
};
export const SEARCH_SECTIONS = {
  STORES: 'stores',
  ARTICLES: 'articles',
  PRODUCTS: 'products',
};
export const PRODUCT_TYPES = {
  PHYSICAL: 'physical',
  EVENT: 'event',
  SERVICE: 'service',
  COURSE: 'course',
  DIGITAL: 'digital',
};
export const ABOUTUS_SECTIONS = {
  ABOUTUS: 'about-us',
  TEAM: 'team',
  FAQS: 'faqs',
};
export const SHOPPING_CART_SECTIONS = {
  SHIPPING: 'shipping',
  PAYMENT: 'payment',
  REVIEW: 'review',
  SHIPPING_PAYMENT: 'shipping-payment',
  CART: 'cart',
};
export const BANK_TRANSFER = {
  NAME: 'Canasta Rosa, SAPI de CV',
  BANK: 'Scotiabank',
  ACCOUNT: '104517649',
  CLABE: '044180001045176492',
  RFC: 'CR0170829FL1',
};
export const PICKUP_POINT = {
  ADDRESS: `Montes Urales 455, Piso 1, 
        Col. Lomas de Chapultepec III Sección, 
        C.P. 11000, Delg. Miguel Hidalgo, CDMX.`,
  SCHEDULES: '9:00 am a 6:00 pm',
  LINK:
    'https://www.google.com.mx/maps/place/Piso+1,+Calle+Montes+Urales+455,+Lomas+-+Virreyes,+Lomas+de+Chapultepec+III+Secc,+11000+Ciudad+de+M%C3%A9xico,+CDMX/@19.4282005,-99.2084926,17z/data=!3m1!4b1!4m5!3m4!1s0x85d201f674dd0a5b:0x35bacdc406953bbd!8m2!3d19.4282005!4d-99.2063039',
  NOTE: `Tus productos son almacenados un máximo 
        de 2 días si el producto es de tipo perecedero de lo contrario, 
        tienes 10 días hábiles para recoger tu orden.`,
};
export const PAYMENT_METHODS = {
  BANK_TRANSFER: {
    slug: 'bank-transfer',
    name: 'Transferencia bancaria',
  },
  CASH: {
    slug: 'cash',
    name: 'Pago en efectivo',
  },
  MERCADO_PAGO: {
    slug: 'mercado-pago',
    name: 'Mercado Pago',
  },
  STRIPE: {
    slug: 'stripe',
    name: 'Stripe',
  },
  SAVED_CARD: {
    slug: 'save-card',
    name: 'Tarjeta guardada',
  },
  PAYPAL: {
    slug: 'paypal',
    name: 'PayPal',
  },
  OXXO: {
    slug: 'oxxo',
    name: 'Oxxo',
  },
  OTROS: {
    slug: 'otros',
    name: 'Otros',
  },
};
export const SHIPPING_METHODS = {
  EXPRESS: {
    slug: 'express',
    name: 'Express',
    order: 0,
  },
  EXPRESS_CAR: {
    slug: 'express-car',
    name: 'Express Automóvil',
    order: 1,
  },
  EXPRESS_BIKE: {
    slug: 'express-moto',
    name: 'Express Moto',
    order: 2,
  },
  PICKUP_POINT: {
    slug: 'pickup-point',
    name: 'Recoger en Mercado Rosa',
    order: 4,
  },
  STANDARD: {
    slug: 'standard',
    name: 'Envíos Nacionales',
    order: 3,
  },
  NO_SHIPPING: {
    slug: 'without-shipping-method',
    name: 'Sin Envío',
    order: 3,
  },
};

export const COUPON_TYPES = {
  PERCENTUAL: 'percentual_discount_general',
  QUANTITY: 'quantity_deduction_general',
  FREE_SHIPPING: 'free_shipping_general',
  FREE_SHIPPING_ALL: 'free_shipping_all_orders',
  FREE_SHIPPING_UNIQUE: 'free_shipping_unique_use',
  FREE_SHIPPING_STORE: 'free_shipping_to_use_on_store',
  FREE_SHIPPING_GENERAL_STORE: 'free_shipping_general_with_store',
  FREE_SHIPPING_AND_PERCENTUAL: 'free_shipping_and_percentual_discount_general',
};

export const STATES_MEXICO = {
  AG: 'Aguascalientes',
  BN: 'Baja California',
  BS: 'Baja California Sur',
  CM: 'Campeche',
  CP: 'Chiapas',
  CH: 'Chihuahua',
  DF: 'Ciudad de México',
  CA: 'Coahuila',
  CL: 'Colima',
  DU: 'Durango',
  GJ: 'Guanajuato',
  GR: 'Guerrero',
  HI: 'Hidalgo',
  JA: 'Jalisco',
  MX: 'Estado de México',
  MC: 'Michoacán',
  MR: 'Morelos',
  NA: 'Nayarit',
  NL: 'Nuevo León',
  OA: 'Oaxaca',
  PU: 'Puebla',
  QE: 'Querétaro',
  QR: 'Quintana Roo',
  SL: 'San Luis Potosí',
  SI: 'Sinaloa',
  SO: 'Sonora',
  TB: 'Tabasco',
  TM: 'Tamaulipas',
  TL: 'Tlaxcala',
  VE: 'Veracruz',
  YU: 'Yucatán',
  ZA: 'Zacatecas',
};
