export type OrderClientDataType = {
  name: string,
  lastName: string,
  email: string,
  telephone: string,
};

export type OrderGroupType = {
  // Client Information
  name: string,
  email: string,
  telephone: string,

  // Gift Options
  receiver_name: string,
  receiver_note: string,
  receiver_telephone: string,

  // Coupons and promos
  coupon: null,

  // Payment Method
  payment_provider: string, // stipe | mercado-pago | paypal | bank-transfer
  payment_method_id: string, // oxxo | paypal | visa | amex | master | bank-transfer
  payment_card_token: string, // It could be an optional value. Depends if payment method is credit card.
  is_saving_card: boolean,
  is_paying_with_saved_card: boolean,
  was_paid_with_saved_card: boolean,
  required_invoice: true,

  // Origin FrontEnd
  origin_frontend: string, // mobile | web

  // Orders and Stores
  orders: Array<{
    store: string,
    products: [
      {
        id: number,
        note: string,
        product: string,
        quantity: number,
        attribute: string | null,
      },
    ],
    physical_properties: {
      shipping_date: string,
      shipping_method: string,
      shipping_schedule: number,
    },
  }>,

  // Shipping and Billing Address
  shipping_address: {
    num_ext: string,
    num_int: string,
    street: string,
    neighborhood: string,
    zip_code: string,
    city: string,
    state: string,
    latitude: string,
    longitude: string,
  },
};
