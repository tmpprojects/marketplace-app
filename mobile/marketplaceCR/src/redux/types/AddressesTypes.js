export type AddressType = {
  uuid: string,
  default_address: boolean,
  address_name: string,
  client_name: string,
  phone: string,
  num_ext: string,
  num_int: string,
  street_address: string,
  neighborhood: string,
  city: string,
  zip_code: string,
  isGuest: boolean,
};
