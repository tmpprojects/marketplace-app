export type AttributeUUIDType = {
  id: number,
  uuid: string,
  is_visible: boolean,
};

export type AttributeType = {
  value: string,
  children: Array<AttributeType>,
  attribute_type: string,
  attribute_uuid: null | AttributeUUIDType,
  attribute_stock: null | { quantity: number, price: number },
  attribute_extradata: null,
};

export type AttributeOptionType = {
  type: string,
  options: Array<string>,
  values: Array<string>,
  ref: string,
};

export type ShippingScheduleType = {
  id: number,
  name: string,
  value: number,
  className: string,
  limit_to_order: string,
  schedules: {
    pickupStartTime: string,
    pickupEndTime: string,
    pickupStart: string,
    pickupEnd: string,
    pickup: string,
    deliveryStartTime: string,
    deliveryEndTime: string,
    deliveryStart: string,
    deliveryEnd: string,
    delivery: string,
  },
};

export type ProductDetailType = {
  loaded: boolean,
  name: string,
  slug: string,
  price: number,
  stock: number,
  quantity: number,
  description: string,
  currency: { value: string },
  productMadeOnDemand: boolean,
  attributes: Array<AttributeType>,
  store: {
    name: string,
    is_on_vacation: boolean,
    vacations_ranges: Array<{ start: string, end: string }>,
    photo: { small: string, medium: string, big: string, original: string },
  },
  photos: Array<{
    id: number,
    file: string,
    photo: Array<{
      small: string,
      medium: string,
      big: string,
      original: string,
    }>,
  }>,
  physical_properties: {
    size: { display_name: string },
    fabrication_time: number,
    minimum_shipping_date: string,
    maximum_shipping_date: number,
    shipping_schedules: Array<ShippingScheduleType>,
  },
};

export type ProductProperties = {|
  uuid: null | AttributeUUIDType,
  price: number,
  stock: number,
  quantity: number,
|};

export type ShippingDateType = {|
  availableShippingSchedules: Array<ShippingScheduleType>,
  shipping_schedule: number | null,
  shipping_date: string,
|};

export type SelectedProductType = {|
  uuid: null | AttributeUUIDType,
  slug: string,
  price: number,
  stock: number,
  quantity: number,
  attributeSelectionMap: Array<number>,
  attributesList: Array<AttributeOptionType>,
  availableShippingSchedules: Array<ShippingScheduleType>,
  shipping_date: string,
  shipping_schedule: null | number,
|};
