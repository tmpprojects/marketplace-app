import DeviceInfo from 'react-native-device-info';
const CryptoJS = require('crypto-js');

export const uniqueID = DeviceInfo.getUniqueId();
const deviceID = DeviceInfo.getDeviceId();
const resultHMAC = CryptoJS.HmacSHA256(uniqueID, deviceID);

export const resultSC = CryptoJS.enc.Base64.stringify(resultHMAC);
