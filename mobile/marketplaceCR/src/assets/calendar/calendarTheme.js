/* USES DIFERENT PROPERTIES FROM CLASSIC STYLES, CUSTOM FOR THE COMPONENT*/
import colors from '@canastarosa/ds-theme/colors/js/variables';

export const calendarTheme = {
  textSectionTitleColor: colors.colorDark300,
  selectedDayBackgroundColor: colors.colorMain300,
  selectedDayTextColor: colors.colorWhite,
  todayTextColor: colors.colorMain300,
  dayTextColor: colors.colorDark200,
  textDisabledColor: colors.colorGray200,
  dotColor: colors.colorMain300,
  selectedDotColor: colors.colorWhite,
  arrowColor: colors.colorMain300,
  monthTextColor: colors.colorDark300,
  indicatorColor: colors.colorDark300,
  textDayFontFamily: 'Sarabun-Regular',
  textMonthFontFamily: 'Sarabun-SemiBold',
  textDayHeaderFontFamily: 'Sarabun-Regular',
  textDayFontSize: 13,
  textMonthFontSize: 16,
  textDayHeaderFontSize: 16,
};
