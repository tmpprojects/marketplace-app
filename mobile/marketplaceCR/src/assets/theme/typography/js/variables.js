/*

This file was created by Canasta Rosa 
under the MIT license.

*/

import { StyleSheet } from 'react-primitives';
import { colors } from '../../colors';

export default StyleSheet.create({
  //<CRText type="pageTitle" color=colors.main100/>
  title: {
    // Main title of pages the most weight over all.
    color: colors.colorDark300,
    textAlign: 'left',
    fontSize: 32,
    fontFamily: 'Sarabun-SemiBold',
  },

  subtitle: {
    // As the name implies this style is for subtitles.
    color: colors.colorDark300,
    textAlign: 'left',
    fontSize: 26,
    fontFamily: 'Sarabun-SemiBold',
  },

  subtitle2: {
    // A subtitle but....smaller..
    color: colors.colorDark300,
    textAlign: 'left',
    fontSize: 22,
    fontFamily: 'Sarabun-SemiBold',
  },

  subtitle3: {
    // The smallest of the subtitles.
    color: colors.colorDark300,
    textAlign: 'left',
    fontSize: 22,
    fontFamily: 'Sarabun-SemiBold',
  },

  button: {
    // The text style for buttons and such.

    color: colors.colorWhite,
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'Sarabun-Regular',
  },

  paragraph: {
    // As the name implies this one is for paraghraps.
    color: colors.colorDark300,
    textAlign: 'left',
    fontSize: 16,
    fontFamily: 'Sarabun-Regular',
  },

  bold: {
    // This is the bold version of the paragrhaph, to highlight something in a text.
    color: colors.colorDark300,
    textAlign: 'left',
    fontSize: 16,
    fontFamily: 'Sarabun-SemiBold',
  },

  caption: {
    // As the name implies this one is for paraghraps.
    color: colors.colorDark300,
    textAlign: 'left',
    fontSize: 13,
    fontFamily: 'Sarabun-Regular',
  },
});
