import colors from '@canastarosa/ds-theme/colors/js/variables';
import { Platform } from 'react-native';
const isAndroid = Platform.OS === 'android';

export const square = {
  width: 200,
  height: 47,
  borderColor: colors.colorWhite,
  borderWidth: 1,
  justifyContent: 'center',
  marginTop: 30,
  borderRadius: 3,
};

export const squareShippedDate = {
  width: isAndroid ? 150 : 180,
  height: 80,
  borderColor: colors.colorGray400,
  borderWidth: 1,
  justifyContent: 'center',
  marginTop: 20,
  borderRadius: 3,
};

export const squareZone = {
  width: 180,
  height: 80,
  borderColor: colors.colorMain300,
  borderWidth: 1,
  justifyContent: 'center',
  marginTop: 20,
  borderRadius: 3,
};

export const squareCart = {
  width: 200,
  height: 47,
  backgroundColor: colors.colorMain300,
  justifyContent: 'center',
  marginTop: 40,
};

export const squareCartDisabled = {
  width: 200,
  height: 47,
  backgroundColor: colors.colorDark100,
  justifyContent: 'center',
  marginTop: 40,
};

export const squareDisabled = {
  width: 200,
  height: 47,
  borderColor: colors.colorDark300,
  borderWidth: 1,
  justifyContent: 'center',
  marginTop: 40,
  borderRadius: 3,
};

export const btnInterests = {
  borderRadius: 4,
  borderWidth: 1,
  borderColor: colors.colorWhite,
};
