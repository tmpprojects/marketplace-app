export const colors = {
  corePink: '#E63976',
  coreDarkGray: '#43434C',
  white: '#fff',
  black: '#181823',
  black300: '#898989',
  iconEyeLogin: 'rgba(255,255,255,0.2)',
  facebookColor: '#3b5998',
  warningColor: '#FFD739',
  titleColor: '#323232',
  backgroundColor: '#FAF9F8',
  borderButton: '#979797',

  /*

This file was created by Canasta Rosa
under the MIT license.

*/

  /*
CANASTA'S COLORS JS
These are the colors that form our pretty site, every shade has a scale from 100 to 400;
100 been the lightest and 400 been the darkest and been 300 the default shade,
please use it wisely.
*/

  //---------------Primaries---------------//

  //Main -- This are the flagship colors.
  colorMain100: '#f6cdd3',
  colorMain200: '#e67787',
  colorMain300: '#e63976', //This is the main color fo Canasta rosa
  colorMain400: '#d83970',

  //Dark  -- This is the complementary color of our main one. Please use clear text colors if you are usign this as a background.
  colorDark100: '#6b6b7a',
  colorDark200: '#494953',
  colorDark300: '#37373e', //This is the main and default color of our text.
  colorDark400: '#2b2b30',

  //Grays -- These are the gray shades of our site.
  colorWhite: '#FFFFFF',
  colorGray100: '#ededed',
  colorGray200: '#dad8d6',
  colorGray300: '#b7b7b7',
  colorGray400: '#898989',

  //---------------Secondaries---------------//

  //Yellows -- A true sun beam.
  colorYellow100: '#faf9f8',
  colorYellow200: '#f8efc0',
  colorYellow300: '#f8db6a',
  colorYellow400: '#8f7d1d',

  //Turquiose -- These are the gray shades of our site.
  colorTurquiose100: '#c0ffee',
  colorTurquiose200: '#5bc3a7',
  colorTurquiose300: '#1db592',
  colorTurquiose400: '#217856',

  //Red -- Blood ansd stuff
  colorRed100: '#fcc6c7',
  colorRed200: '#e6afaf',
  colorRed300: '#cc3131', //The default alert color.
  colorRed400: '#960400',

  //Brown -- Wood and ground
  colorBrown100: '#fff8f2',
  colorBrown200: '#f6e1cf',
  colorBrown300: '#cfb39f', //The default alert color.
  colorBrown400: '#b5a59c',

  //---------------Terciaries---------------//

  //Green  -- It's alive.
  colorGreen100: '#e8fdc9',
  colorGreen200: '#c8dcaa',
  colorGreen300: '#78a028',
  colorGreen400: '#547400',

  //Aqua  -- Almost Blue.
  colorAqua100: '#cbf7fb',
  colorAqua200: '#a0d2d7',
  colorAqua300: '#42aaac',
  colorAqua400: '#00707c',

  //Blue  -- Like the sea!.
  colorBlue100: '#dee3fd',
  colorBlue200: '#bec3dc',
  colorBlue300: '#505087',
  colorBlue400: '#323764',

  //Violet  -- Meh.
  colorViolet100: '#f1dbf0',
  colorViolet200: '#c8b4c8',
  colorViolet300: '#825a7a',
  colorViolet400: '#501e4d',
};
