export const txtRegular = {
  fontSize: 16,
  fontFamily: 'Sarabun-Regular',
};

export const txtLight = {
  fontSize: 16,
  fontFamily: 'Sarabun-Light',
};

export const txtSemiBold = {
  fontSize: 18,
  fontFamily: 'Sarabun-SemiBold',
};

export const txtBold = {
  fontFamily: 'Sarabun-Bold',
  fontSize: 16,
};

export const txtExtraBold = {
  fontFamily: 'Sarabun-ExtraBold',
  fontSize: 26,
};
