function setEnvironment(NODE_ENV) {
  if (NODE_ENV === 'production') {
    return {
      HOST_BACKEND_LOCAL: 'https://canastarosa.com',
      HOST_BACKEND_PRODUCTION: 'https://canastarosa.com',
      EXTERNAL_AUTH:
        'https://auth.canastarosa.com/?back=https://canastarosa.com',
      FACEBOOK_APP_ID: '',
      FACEBOOK_AUTH_SECRET: '',
      DEFAULT_PAGINATION: 10,
      STRIPE_PK: 'pk_live_LTsgV1byDI1CDEBgpnqPrh6i004gdqXB5i',
      PAYPAL_USER:
        'AaoON4mRf_r_c4g-kK2vj2LjCnvBNSFS2WUstYpBqjJhULM7xFeg7LVIPkRMjcbarpKcGWBo_spVagU1',
      PAYPAL_PASSWORD:
        'EPm8tiz4y5g7E9EqexXhO-36AG8OJH4agzs_KIhM2L88gYbLjuTU4wDRjGv55HH_kr2F270tgfBjzABx',
      ZIPCODEQUERY: 'o_z',
    };
  } else if (NODE_ENV === 'development') {
    return {
      HOST_BACKEND_LOCAL:
        'http://ec2-52-14-46-40.us-east-2.compute.amazonaws.com',
      HOST_BACKEND_PRODUCTION:
        'http://ec2-52-14-46-40.us-east-2.compute.amazonaws.com',
      EXTERNAL_AUTH:
        'https://auth.canastarosa.com/?back=http://ec2-52-14-46-40.us-east-2.compute.amazonaws.com',
      FACEBOOK_APP_ID: '',
      FACEBOOK_AUTH_SECRET: '',
      DEFAULT_PAGINATION: 10,
      STRIPE_PK: 'pk_test_tLlKbUDsBTqUCUWKbpBfMqX0',
      PAYPAL_USER:
        'AaoON4mRf_r_c4g-kK2vj2LjCnvBNSFS2WUstYpBqjJhULM7xFeg7LVIPkRMjcbarpKcGWBo_spVagU1',
      PAYPAL_PASSWORD:
        'EPm8tiz4y5g7E9EqexXhO-36AG8OJH4agzs_KIhM2L88gYbLjuTU4wDRjGv55HH_kr2F270tgfBjzABx',
      ZIPCODEQUERY: 'o_z',
    };
  }
}
// VISA BACKEND: http://ec2-52-14-46-40.us-east-2.compute.amazonaws.com/
// DEVELOPMENT: https://auth.canastarosa.com/?back=https://staging.canastarosa.com
const config = setEnvironment('production');
export { config };
