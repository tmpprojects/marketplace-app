module.exports = {
  root: true,
  extends: ['@react-native-community'],
  plugins: ['unused-imports'],
  rules: {
    'react-native/no-inline-styles': 0,
    'eslint-comments/no-unused-disable': 0,
    'react/self-closing-comp': 0,
    'no-unused-vars': 0
  },
};
