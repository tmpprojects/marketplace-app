#!/bin/bash
#AppStoreDistribution
xcodebuild clean -workspace $PWD/../../ios/marketplaceCR.xcworkspace -scheme marketplaceCR
xcodebuild -workspace $PWD/../../ios/marketplaceCR.xcworkspace -scheme marketplaceCR -sdk iphoneos archive -archivePath $PWD/marketplaceCR.xcarchive
xcodebuild -exportArchive -archivePath $PWD/marketplaceCR.xcarchive -exportOptionsPlist $PWD/exportOptions.plist -exportPath $PWD/build