# Canasta Rosa Mobile App

## System Versions

- React Native 0.61.5
- React 16.9.0

## System Requirements

- npm
- yarn

## Dependencies

- react-native-vector-icons
- react-navigation
- react-native-gesture-handler
- react-navigation-stack

### For dependencies
    npm install
    npx react-native link

## Run Project

- npx react-native run-ios
- npx react-native run-android




## Security

As *Canasta Rosa* We are worry about the security of any of our products. For more reference please review [Canasta Rosa check up site](https://canastarosa.com/legales/terminos-condiciones).

## Deployment Instructions
