#!/bin/sh

VERSION=`cat .deploy-production`
CURRENTDATE=`date`

git remote set-url origin https://canastarosaservers:$GIT_TOKEN@gitlab.com/canastarosa-dev/marketplacecr.git
git config --global user.email 'soporte@canastarosa.com'
git config --global user.name 'CI Canasta Rosa'
git fetch --all
git stash

git checkout -f master
git pull origin develop

echo "Merge into master - $CURRENTDATE" > .log

git add .
git commit -m "🚀 Deploy version: $VERSION"

git fetch --prune --tags
git tag -a $VERSION -m "Deploy: $VERSION"

git push --force && git push --force --tags

ls -a

git status

#curl -X POST -H 'Content-type: application/json' --data '{"text":"Deploy project version: '$VERSION'"}' https://hooks.slack.com/services/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