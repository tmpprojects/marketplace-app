#!/usr/bin/env bash

value=$(<version.txt)
#echo $value | cut -d , -f 1

IFS=', ' read -r -a array <<< $value

export APPCENTER_CUSTOM_BUILD_CODE=${array[0]}
export APPCENTER_CUSTOM_BUILD_VERSION=${array[1]}

echo $APPCENTER_CUSTOM_BUILD_CODE
echo $APPCENTER_CUSTOM_BUILD_VERSION



